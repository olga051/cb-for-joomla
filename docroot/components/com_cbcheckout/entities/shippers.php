<?php
defined('CB_VALID_ENTRY') or die();

class EntityShippers extends KenedoEntity {

	function __construct() {
		
		$this->tableName	= '#__cbcheckout_shippers';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listing'=>10,
				'listingwidth'=>'50px',
				'order'=>100,
				);

		$this->fields->title = array(	
				'name'=>'title',
				'label'=>KText::_('Title'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>1,
				'required'=>1,
				'listing'=>20,
				'listinglink'=>1,
				'controller'=>'adminshippers',
				);
									
		$this->fields->published = array(	
				'name'=>'published',
				'label'=>KText::_('Active'),
				'type'=>'published',
				'default'=>1,
				'listing'=>30,
				'listingwidth'=>'60px',
				);
		
	}
	
}
