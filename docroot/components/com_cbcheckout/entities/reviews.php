<?php
defined('CB_VALID_ENTRY') or die();

class EntityReviews extends KenedoEntity {
	
	function __construct() {
		
		$this->tableName	= '#__cbcheckout_reviews';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listingwidth'=>'50px',
				'listing'=>10,
		);
		
		$this->fields->name = array(
				'name'=>'name',
				'label'=>KText::_('Name'),
				'type'=>'string',
				'required'=>1,
				'listing'=>20,
				'listinglink'=>1,
				'controller'=>'adminreviews',
		);
		
		$this->fields->rating = array(
				'name'=>'rating',
				'label'=>KText::_('Rating'),
				'type'=>'dropdown',
				'items'=> array(
						'0'=>KText::_('No rating'),
						'0.5'=>KText::_('0.5'),
						'1.0'=>KText::_('1.0'),
						'1.5'=>KText::_('1.5'),
						'2.0'=>KText::_('2.0'),
						'2.5'=>KText::_('2.5'),
						'3.0'=>KText::_('3.0'),
						'3.5'=>KText::_('3.5'),
						'4.0'=>KText::_('4.0'),
						'4.5'=>KText::_('4.5'),
						'5.0'=>KText::_('5.0'),
				),
				'default'=>'5.0',
		);
		
		$this->fields->comment = array(
				'name'=>'comment',
				'label'=>KText::_('Comment'),
				'type'=>'string',
				'required'=>1,
				'controller'=>'adminreviews',
				'options'=>'USE_TEXTAREA',
				'style'=>'width:196px;height:200px',
		);
		
		$this->fields->date_created = array(
				'name'=>'date_created',
				'label'=>KText::_('Creation date'),
				'type'=>'datetime',
				'default'=>'',
		);
		
		$this->fields->product_id = array(
				'name'=>'product_id',
				'label'=>KText::_('Product'),
				'defaultlabel'=>KText::_('Select Product'),
				'type'=>'join',
		
				'joinField'=>'id',
				'textField'=>'title',
				
				'modelClass'=>'ConfigboxModelAdminproducts',
				'modelMethod'=>'getItems',
		
				'parent'=>1,
				'filterparents'=>1,
		
				'required'=>0,
				'listing'=>30,
				'order'=>20,
				'filter'=>1
		);
		
		$this->fields->option_id = array(
				'name'=>'option_id',
				'label'=>KText::_('Option'),
				'defaultlabel'=>KText::_('Select Option'),
				'type'=>'join',
		
				'joinField'=>'id',
				'textField'=>'title',
		
				'modelClass'=>'ConfigboxModelAdminoptions',
				'modelMethod'=>'getItems',
		
				'parent'=>1,
				'filterparents'=>1,
		
				'required'=>0,
				'listing'=>40,
				'order'=>30,
				'filter'=>2
		);
		
		$this->fields->language_tag = array(
				'name'=>'language_tag',
				'label'=>KText::_('Language'),
				'tooltip'=>KText::_('The language of the review. The language is automatically determined by the site language at the time of writing.'),
				'type'=>'join',
		
				'required'=>1,
				'joinField'=>'tag',
				'textField'=>'label',
		
				'modelClass'=>'ConfigboxModelAdminlanguages',
				'modelMethod'=>'getLanguages',
		
				'options'=>'SKIPDEFAULTFIELD NOFILTERSAPPLY',
		);
		
		$this->fields->published = array(
				'name'=>'published',
				'label'=>KText::_('Active'),
				'default'=>1,
				'type'=>'published',
				'listing'=>140,
				'order'=>130,
				'filter'=>3,
				'listingwidth'=>'60px',
		);
		
	}
	
}
