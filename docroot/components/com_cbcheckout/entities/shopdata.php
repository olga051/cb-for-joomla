<?php
defined('CB_VALID_ENTRY') or die();

class EntityShopdata extends KenedoEntity {

	function __construct() {

		$this->tableName	= '#__cbcheckout_shopdata';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>1,
				);

		$this->fields->shopname = array(	
				'name'=>'shopname',
				'label'=>KText::_('Shop Name'),
				'type'=>'string',
				'required'=>1,
				);
		
		$this->fields->shopwebsite = array(	
				'name'=>'shopwebsite',
				'label'=>KText::_('Shop Website'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shoplogo = array (	
				'name'=>'shoplogo',
				'label'=>KText::_('Logo'),
				'type'=>'file',
				'appendSerial'=>0,
				'allowedExtensions'=>array('jpg','jpeg','gif','tif','bmp','png'),
				'filetype'=>'image',
				'allow'=>array('image/pjpeg','image/jpg','image/jpeg','image/gif','image/tif','image/bmp','image/png'),
				'size'=>'1024',
				'location'=>'components/com_cbcheckout/data/shoplogo',
				'required'=>0,
				'options'=>'FILENAME_TO_RECORD_ID PRESERVE_EXT SAVE_FILENAME',
				);
		
		$this->fields->shopaddress1 = array(
				'name'=>'shopaddress1',
				'label'=>KText::_('Address 1'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopaddress2 = array(
				'name'=>'shopaddress2',
				'label'=>KText::_('Address 2'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopzipcode = array(
				'name'=>'shopzipcode',
				'label'=>KText::_('ZIP Code'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopcity = array(
				'name'=>'shopcity',
				'label'=>KText::_('City'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopcountry = array(
				'name'=>'shopcountry',
				'label'=>KText::_('Country'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopphonesales = array(	
				'name'=>'shopphonesales',
				'label'=>KText::_('Phone Sales'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopphonesupport = array(
				'name'=>'shopphonesupport',
				'label'=>KText::_('Phone Support'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopemailsales = array(
				'name'=>'shopemailsales',
				'label'=>KText::_('Email Sales'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopemailsupport = array(
				'name'=>'shopemailsupport',
				'label'=>KText::_('Email Support'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopfax = array(
				'name'=>'shopfax',
				'label'=>KText::_('Fax'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopbankname = array(
				'name'=>'shopbankname',
				'label'=>KText::_('Bank name'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopbankaccountholder = array(
				'name'=>'shopbankaccountholder',
				'label'=>KText::_('Bank Account Holder'),
				'type'=>'string',
				'required'=>0,
				);

		$this->fields->shopbankaccount = array(
				'name'=>'shopbankaccount',
				'label'=>KText::_('Bank account number'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopbankcode = array(
				'name'=>'shopbankcode',
				'label'=>KText::_('Bank code'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopbic = array(
				'name'=>'shopbic',
				'label'=>KText::_('BIC'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopiban = array(
				'name'=>'shopiban',
				'label'=>KText::_('IBAN'),
				'type'=>'string',
				'required'=>0,
				);

		$this->fields->shopuid = array(
				'name'=>'shopuid',
				'label'=>KText::_('VAT ID'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopcomreg = array(
				'name'=>'shopcomreg',
				'label'=>KText::_('Commercial Register ID'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shopowner = array(
				'name'=>'shopowner',
				'label'=>KText::_('Company Owner'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->shoplegalvenue = array(
				'name'=>'shoplegalvenue',
				'label'=>KText::_('Legal Venue'),
				'type'=>'string',
				'required'=>0,
				);
		
		$this->fields->invoicestart = array(
				'name'=>'invoicestart',
				'type'=>'groupstart',
				'title'=>KText::_('Invoice Template'),
				'toggle'=>true,
				'default-state'=>'closed',
				'notes'=>KText::_('SHOPDATA INVOICE GROUP NOTES'),
				);
		
		$this->fields->use_custom_invoice = array(
				'name'=>'use_custom_invoice',
				'label'=>KText::_('Use a custom invoice template'),
				'tooltip'=>KText::_('Choose yes to use the custom invoice template below. Choose no to use the standard template.'),
				'type'=>'boolean',
				'default'=>0,
				);
		
		$this->fields->invoice = array(
				'name'=>'invoice',
				'label'=>KText::_('Custom invoice template'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>34,
				'options'=>'USE_HTMLEDITOR ALLOW_HTML',
				'required'=>0,
				'editorHeight'=>'900px',
				);
		
		$this->fields->invoiceend = array(
				'name'=>'invoiceend',
				'type'=>'groupend',
				);
		
		$this->fields->shopdesc = array(
				'name'=>'shopdesc',
				'label'=>KText::_('Shop Description'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>20,
				'options'=>'USE_HTMLEDITOR ALLOW_HTML',
				'required'=>0,
				);
		
		$this->fields->tac = array(
				'name'=>'tac',
				'label'=>KText::_('Terms and Conditions'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>33,
				'options'=>'USE_HTMLEDITOR ALLOW_HTML',
				'required'=>0,
				);
		
		$this->fields->refundpolicy = array(
				'name'=>'refundpolicy',
				'label'=>KText::_('Refund policy'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>35,
				'options'=>'USE_HTMLEDITOR ALLOW_HTML',
				'required'=>0,
				);

	}

}
