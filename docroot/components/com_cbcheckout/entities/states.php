<?php
defined('CB_VALID_ENTRY') or die();

class EntityStates extends KenedoEntity {
	
	function __construct() {
		
		$this->tableName	= '#__cbcheckout_states';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->general_start = array(	
				'name'=>'general_start',
				'type'=>'groupstart',
				'title'=>KText::_('General'),
				'toggle'=>true,
				'default-state'=>'opened',
		);
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listingwidth'=>'50px',
				'listing'=>10,
				);
		
		$this->fields->name = array(
				'name'=>'name',
				'label'=>KText::_('Name'),
				'type'=>'string',
				'required'=>1,
				'listing'=>20,
				'listinglink'=>1,
				'order'=>10,
				'filter'=>1,
				'search'=>1,
				'controller'=>'adminstates',
		);
		
		$this->fields->iso_code = array(	
				'name'=>'iso_code',
				'label'=>KText::_('State code'),
				'type'=>'string',
				'required'=>0,
				'listing'=>30,
				'listingwidth'=>'30px',
				'order'=>20,
				'listingwidth'=>'50px',
		);
		
		$this->fields->fips_number = array(
				'name'=>'fips_number',
				'label'=>KText::_('FIPS Number'),
				'type'=>'string',
				'required'=>1,
				'listing'=>35,
				'listingwidth'=>'30px',
				'order'=>30,
				'listingwidth'=>'50px',
		);
		
		$this->fields->country_id = array(	
				'name'=>'country_id',
				'label'=>KText::_('Country'),
				'type'=>'join',
				
				'joinField'=>'id',
				'textField'=>'country_name',
				'defaultlabel'=>KText::_('Select Country'),
				
				'modelClass'=>'CbcheckoutModelAdmincountries',
				'modelMethod'=>'getItems',
				
				'selectfields'=>array(array('country_name','country_name')),
				
				'required'=>1,
				'parent'=>1,
				'filterparents'=>1,
				'filter'=>2,
				'listing'=>40,
				'order'=>40,
		
				'listingwidth'=>'100px',
		);
		
		$this->fields->custom_1 = array(	
				'name'=>'custom_1',
				'label'=>KText::_('Custom Field 1'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->custom_2 = array(
				'name'=>'custom_2',
				'label'=>KText::_('Custom Field 2'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->custom_3 = array(
				'name'=>'custom_3',
				'label'=>KText::_('Custom Field 3'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->custom_4 = array(
				'name'=>'custom_4',
				'label'=>KText::_('Custom Field 4'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->ordering = array(	
				'name'=>'ordering',
				'label'=>KText::_('Ordering'),
				'type'=>'ordering',
				'order'=>6,
				'listing'=>15,
				'group'=>'country_id',
		);
		
		$this->fields->published = array(
				'name'=>'published',
				'label'=>KText::_('Active'),
				'type'=>'published',
				'listing'=>100,
				'listingwidth'=>'50px',
				'default'=>1,
		);
		
		$this->fields->general_end = array(	
				'name'=>'general_end',
				'type'=>'groupend',
		);
		
		$this->fields->tax_start = array(	
				'name'=>'tax_start',
				'type'=>'groupstart',
				'title'=>KText::_('Tax Override'),
				'toggle'=>true,
				'default-state'=>'opened',
		);
		
		$this->fields->tax_class_rates = array(
				'name'=>'tax_class_rates',
				'type'=>'taxclassrates',
				'taxclasstype'=>'state',
		);
		
		$this->fields->tax_end = array(	
				'name'=>'tax_end',
				'type'=>'groupend',
		);
		
	}
	
}
