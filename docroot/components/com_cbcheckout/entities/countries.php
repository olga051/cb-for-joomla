<?php
defined('CB_VALID_ENTRY') or die();

class EntityCountries extends KenedoEntity {
	
	function __construct() {
		
		$this->tableName	= '#__cbcheckout_countries';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listingwidth'=>'50px',
		);
		
		$this->fields->general_start = array(	
				'name'=>'general_start',
				'type'=>'groupstart',
				'title'=>KText::_('Country Edit General'),
				'toggle'=>true,
				'default-state'=>'opened'
		);

		$this->fields->country_name = array(	
				'name'=>'country_name',
				'label'=>KText::_('Country Name'),
				'type'=>'string',
				'required'=>1,
				'listing'=>1,
				'listinglink'=>1,
				'filter'=>2,
				'search'=>true,
				'order'=>1,
				'controller'=>'admincountries',
		);

		$this->fields->zones = array(	
				'name'=>'zones',
				'label'=>KText::_('Zones'),
				'type'=>'multiselect',
				'required'=>0,
				'storeMethod'=>'xref',
				'saveTable'=>'#__cbcheckout_xref_country_zone',
				'saveResultsField'=>'zone_id',
				'saveOtherField'=>array('country_id','id'),
				
				'modelClass'=>'CbcheckoutModelAdminzones',
				'joinField'=>'id',
				'textField'=>'label',
				
				'method'=>'getItems',

				'selectTable'=>'#__cbcheckout_zones',
				'selectOptionId'=>'id',
				'selectOptionTitle'=>'label',
				'filter'=>1
		);
		
		$this->fields->country_3_code = array(	
				'name'=>'country_3_code',
				'label'=>KText::_('Country 3 Code'),
				'type'=>'string',
				'required'=>1,
				'listing'=>2,
				'listingwidth'=>'30px',
				'order'=>2
		);
		
		$this->fields->country_2_code = array(	
				'name'=>'country_2_code',
				'label'=>KText::_('Country 2 Code'),
				'type'=>'string',
				'required'=>1,
				'listing'=>3,
				'listingwidth'=>'30px',
				'order'=>3
		);

		$this->fields->vat_free = array(	
				'name'=>'vat_free',
				'label'=>KText::_('VAT free'),
				'type'=>'boolean',
				'tooltip'=>KText::_('Send VAT free to this country.'),
				'default'=>0,
				'filter'=>2,
				'order'=>4
		);
			
		$this->fields->vat_free_with_vatin = array(	
				'name'=>'vat_free_with_vatin',
				'label'=>KText::_('VAT free with VAT IN'),
				'type'=>'boolean',
				'tooltip'=>KText::_('Send VAT free to this country if receipient supplies his VAT IN.'),
				'default'=>0,
				'filter'=>3,
		);
		
		$this->fields->published = array(
				'name'=>'published',
				'label'=>KText::_('Active'),
				'type'=>'published',
				'default'=>1,
				'listing'=>7,
				
				'order'=>7,
				'listingwidth'=>'50px',
		);
		
		$this->fields->general_end = array(
				'name'=>'general_end',
				'type'=>'groupend',
		);
		
		$this->fields->tax_start = array(
				'name'=>'tax_start',
				'type'=>'groupstart',
				'title'=>KText::_('Tax Override'),
				'toggle'=>true,
				'default-state'=>'opened'
		);
		
		$this->fields->tax_class_rates = array(
				'name'=>'tax_class_rates',
				'type'=>'taxclassrates',
				'taxclasstype'=>'country'
		);
		
		$this->fields->tax_end = array(	
				'name'=>'tax_end',
				'type'=>'groupend',
		);
		
		$this->fields->custom_start = array(
				'name'=>'custom_start',
				'type'=>'groupstart',
				'title'=>KText::_('Custom Fields'),
				'toggle'=>true,
				'default-state'=>'closed'
		);
		
		$this->fields->custom_1 = array(	
				'name'=>'custom_1',
				'label'=>KText::_('Custom Field 1'),
				'type'=>'string',
				'required'=>0
		);
		
		$this->fields->custom_2 = array(
				'name'=>'custom_2',
				'label'=>KText::_('Custom Field 2'),
				'type'=>'string',
				'required'=>0
		);
		
		$this->fields->custom_3 = array(
				'name'=>'custom_3',
				'label'=>KText::_('Custom Field 3'),
				'type'=>'string',
				'required'=>0
		);
		
		$this->fields->custom_4 = array(
				'name'=>'custom_4',
				'label'=>KText::_('Custom Field 4'),
				'type'=>'string',
				'required'=>0
		);
		
		$this->fields->custom_translatable_1 = array(
				'name'=>'custom_translatable_1',
				'label'=>KText::_('Custom Translatable 1'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>42,
				'required'=>0
		);
		
		$this->fields->custom_translatable_2 = array(
				'name'=>'custom_translatable_2',
				'label'=>KText::_('Custom Translatable 2'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>43,
				'required'=>0
		);
		
		$this->fields->custom_end = array(
				'name'=>'custom_end',
				'type'=>'groupend',
		);
		
		$this->fields->ordering = array(	
				'name'=>'ordering',
				'label'=>KText::_('Ordering'),
				'type'=>'ordering',
				'order'=>6,
				'listing'=>6,
				'listingwidth'=>'70px',
				'disableSortable'=>true,
		);
		
	}
	
}
