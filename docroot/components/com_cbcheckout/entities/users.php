<?php
defined('CB_VALID_ENTRY') or die();

class EntityUsers extends KenedoEntity {

	function __construct() {

		$this->tableName 	= '#__cbcheckout_users';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				);


		$this->fields->billing_start = array(
				'name'=>'billing_start',
				'type'=>'groupstart',
				'title'=>KText::_('Billing'),
				'toggle'=>true,
				'default-state'=>'opened',
		);

		$this->fields->billingcompanyname = array(
				'name'=>'billingcompanyname',
				'label'=>KText::_('Billing Company Name'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->billingsalutation_id = array(
				'name'=>'billingsalutation_id',
				'label'=>KText::_('Billing Salutation'),
				'type'=>'join',
				'joinField'=>'id',
				'textField'=>'title',
				'defaultlabel'=>KText::_('Choose a Salutation'),
				'modelClass'=>'CbcheckoutModelAdminsalutations',
				'modelMethod'=>'getItems',
				'required'=>1,	
		);
		
		$this->fields->billingfirstname = array(
				'name'=>'billingfirstname',
				'label'=>KText::_('Billing First Name'),
				'type'=>'string',
				'listing'=>1,
				'listingwidth'=>'80px',
				'required'=>1,
		);
			
		$this->fields->billinglastname = array(
				'name'=>'billinglastname',
				'label'=>KText::_('Billing Last Name'),
				'type'=>'string',
				'listing'=>2,
				'listinglink'=>1,
				'controller'=>'adminusers',
				'listingwidth'=>'80px',
				'required'=>1,
		);
		
		$this->fields->billingaddress1 = array(
				'name'=>'billingaddress1',
				'label'=>KText::_('Billing Address'),
				'type'=>'string',
		);
		
		$this->fields->billingaddress2 = array(	
				'name'=>'billingaddress2',
				'label'=>KText::_('Billing Address 2'),
				'type'=>'string',
				'required'=>0
		);
		
		$this->fields->billingzipcode = array(
				'name'=>'billingzipcode',
				'label'=>KText::_('Billing ZIP Code'),
				'type'=>'string',
		);
		
		$this->fields->billingcity = array(
				'name'=>'billingcity',
				'label'=>KText::_('Billing City'),
				'type'=>'string',
				'listing'=>35,
		);
		
		$this->fields->billingcountry = array(
				'name'=>'billingcountry',
				'label'=>KText::_('Billing Country'),
				'type'=>'countryselect',
				'stateFieldName'=>'billingstate',
				'defaultlabel'=>KText::_('Choose Country'),
				'required'=>1,
		);

		$this->fields->billingstate = array(
				'name'=>'billingstate',
				'label'=>KText::_('Billing State'),
				'type'=>'stateselect',
				'countryFieldName'=>'billingcountry',
		);
		
		$this->fields->billingcounty_id = array(
				'name'=>'billingcounty_id',
				'label'=>KText::_('Billing County'),
				'type'=>'countyselect',
				'stateFieldName'=>'billingstate',
		);
		
		$this->fields->billingcity_id = array(
				'name'=>'billingcity_id',
				'label'=>KText::_('Billing City ID'),
				
				'type'=>'join',
				
				'joinField'=>'id',
				'textField'=>'city_name',
				'defaultlabel'=>KText::_('Select City'),
				
				'modelClass'=>'CbcheckoutModelAdmincities',
				'modelMethod'=>'getItems',
				
		);
		
		$this->fields->billingemail = array(
				'name'=>'billingemail',
				'label'=>KText::_('Billing Email'),
				'type'=>'string',
				'required'=>1,
		);

		$this->fields->billingphone = array(
				'name'=>'billingphone',
				'label'=>KText::_('Billing Phone'),
				'type'=>'string',
				'required'=>0,
		);

		$this->fields->baseprice_end = array(
				'name'=>'baseprice_end',
				'type'=>'groupend',
		);

		$this->fields->delivery_start = array(
				'name'=>'delivery_start',
				'type'=>'groupstart',
				'title'=>KText::_('Delivery'),
				'toggle'=>true,
				'default-state'=>'opened',
		);

		$this->fields->companyname = array(
				'name'=>'companyname',
				'label'=>KText::_('Company Name'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->salutation_id = array(
				'name'=>'salutation_id',
				'label'=>KText::_('Salutation'),
				'type'=>'join',
				'joinField'=>'id',
				'textField'=>'title',
				'defaultlabel'=>KText::_('Choose a Salutation'),
				'modelClass'=>'CbcheckoutModelAdminsalutations',
				'modelMethod'=>'getItems',
		);
		
		$this->fields->firstname = array(
				'name'=>'firstname',
				'label'=>KText::_('First Name'),
				'type'=>'string',
				'required'=>0,
				'listing'=>20,
				'listinglink'=>1,
				'controller'=>'adminusers',
				'listingwidth'=>'80px',
		);
			
		$this->fields->lastname = array(
				'name'=>'lastname',
				'label'=>KText::_('Last Name'),
				'type'=>'string',
				'required'=>0,
				'listing'=>21,
				'listinglink'=>1,
				'controller'=>'adminusers',
				'listingwidth'=>'80px',
		);
		$this->fields->address1 = array(
				'name'=>'address1',
				'label'=>KText::_('Address'),
				'type'=>'string',
				'required'=>0
		);
		$this->fields->address2 = array(
				'name'=>'address2',
				'label'=>KText::_('Address 2'),
				'type'=>'string',
				'required'=>0
		);
		$this->fields->zipcode = array(	
				'name'=>'zipcode',
				'label'=>KText::_('ZIP Code'),
				'type'=>'string',
				'required'=>0,
		);
		$this->fields->city = array(
				'name'=>'city',
				'label'=>KText::_('City'),
				'type'=>'string',
				'required'=>0,
				'listing'=>25,
		);
		$this->fields->country = array(
				'name'=>'country',
				'label'=>KText::_('Country'),
				'type'=>'countryselect',
				'stateFieldName'=>'state',
				'defaultlabel'=>KText::_('Choose Country'),
		);

		$this->fields->state = array(
				'name'=>'state',
				'label'=>KText::_('CB_STATE'),
				'type'=>'stateselect',
				'countryFieldName'=>'country',
		);
		
		$this->fields->county_id = array(
				'name'=>'county_id',
				'label'=>KText::_('County'),
				'type'=>'string',
		);
		
		$this->fields->state = array(
				'name'=>'state',
				'label'=>KText::_('State'),
				'type'=>'stateselect',
				'countryFieldName'=>'country',
		);
		
		$this->fields->county_id = array(
				'name'=>'county_id',
				'label'=>KText::_('County'),
				'type'=>'countyselect',
				'stateFieldName'=>'state',
		);
		
		$this->fields->city_id = array(
				'name'=>'city_id',
				'label'=>KText::_('City ID'),
				
				'type'=>'join',
				
				'joinField'=>'id',
				'textField'=>'city_name',
				'defaultlabel'=>KText::_('Select City'),
				
				'modelClass'=>'CbcheckoutModelAdmincities',
				'modelMethod'=>'getItems',
		);

		$this->fields->email = array(
				'name'=>'email',
				'label'=>KText::_('Email'),
				'type'=>'string',
				'required'=>0,
		);

		$this->fields->phone = array(
				'name'=>'phone',
				'label'=>KText::_('Phone'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->delivery_end = array(
				'name'=>'delivery_end',
				'type'=>'groupend',
		);

		$this->fields->other_start = array(
				'name'=>'other_start',
				'type'=>'groupstart',
				'title'=>KText::_('Other'),
				'toggle'=>true,
				'default-state'=>'opened',
		);

		$this->fields->vatin = array(
				'name'=>'vatin',
				'label'=>KText::_('VAT IN'),
				'type'=>'string',
				'required'=>0,
				'tooltip'=>KText::_('The VAT identification number of the customer.'),
		);

		$this->fields->samedelivery = array(
				'name'=>'samedelivery',
				'label'=>KText::_('Billing address same as delivery'),
				'type'=>'boolean',
				'default'=>1,
		);

		$this->fields->group_id = array(
				'name'=>'group_id',
				'label'=>KText::_('Customer Group'),
				'type'=>'join',

				'joinField'=>'id',
				'textField'=>'title',

				'modelClass'=>'CbcheckoutModelAdminuserGroups',
				'modelMethod'=>'getItems',

				'required'=>1,
		);
		
		$this->fields->joomlaid = array(
				'name'=>'joomlaid',
				'label'=>KText::_('Platform User ID'),
				'type'=>'string',
				'default'=>0,
		);
		
		$this->fields->language_tag = array(
				'name'=>'language_tag',
				'label'=>KText::_('Preferred Language'),
				'type'=>'join',
		
				'joinField'=>'tag',
				'textField'=>'label',
				'defaultlabel'=>KText::_('Choose Language'),
		
				'modelClass'=>'CbcheckoutModelAdminlanguages',
				'modelMethod'=>'getLanguages',
		
				'required'=>0,
				'options'=>'NOFILTERSAPPLY',
		);

		$this->fields->newsletter = array(
				'name'=>'newsletter',
				'label'=>KText::_('Newsletter'),
				'type'=>'boolean',
				'default'=>0,
		);
		
		$this->fields->is_temporary = array(
				'name'=>'is_temporary',
				'label'=>KText::_('Temporary Customer'),
				'type'=>'boolean',
				'required'=>0,
				'default'=>0,
		);
			
		$this->fields->custom_1 = array(
				'name'=>'custom_1',
				'label'=>KText::_('Custom Field 1'),
				'type'=>'string',
				'required'=>0,
		);

		$this->fields->custom_2 = array(
				'name'=>'custom_2',
				'label'=>KText::_('Custom Field 2'),
				'type'=>'string',
				'required'=>0,
		);

		$this->fields->custom_3 = array(
				'name'=>'custom_3',
				'label'=>KText::_('Custom Field 3'),
				'type'=>'string',
				'required'=>0,
		);

		$this->fields->custom_4 = array(
				'name'=>'custom_4',
				'label'=>KText::_('Custom Field 4'),
				'type'=>'string',
				'required'=>0,
		);

		$this->fields->other_end = array(
				'name'=>'other_end',
				'type'=>'groupend',
		);
			
	}
	
	function prepare() {
		
		// Drop if parent prepare fails
		$succ = parent::prepare();
		if (!$succ) {
			return $succ;
		}
		
		// Set the gender based on the salutation
		if (!empty($this->data->salutation_id)) {
			
			$sal = CbcheckoutUserHelper::getSalutation($this->data->salutation_id);
			if ($sal) {
				$this->data->gender = $sal->gender;
			}
		}

		// Set the gender based on the salutation
		if (!empty($this->data->billingsalutation_id)) {
			$sal = CbcheckoutUserHelper::getSalutation($this->data->billingsalutation_id);
			if ($sal) {
				$this->data->billinggender = $sal->gender;
			}
		}
		
		// If a city ID is set, make sure the city name matches
		if (!empty($this->data->city_id) && empty($this->data->city)) {
			$this->data->city = CbcheckoutCountryHelper::getCityName($this->data->city_id);
		}
		
		// If a city ID is set, make sure the city name matches
		if (!empty($this->data->billingcity_id) && empty($this->data->billingcity)) {
			$this->data->billingcity = CbcheckoutCountryHelper::getCityName($this->data->billingcity_id);
		}
		
		return true;
	}

	function check( $context = 'profile' ) {
		
		$fieldTranslations = CbcheckoutUserHelper::getUserFieldTranslations();
		
		$keyRequired = 'require_'.$context;
		$userFields = CbcheckoutUserHelper::getUserFields();
		$fieldTranslations = CbcheckoutUserHelper::getUserFieldTranslations();
		$invalid = array();
		
		$deliveryFields = array('companyname','gender','firstname','lastname','address1','address2','zipcode','city','country','email','phone','state','salutation_id', 'county_id','city_id');

		foreach ($userFields as $fieldName=>$userField) {
			
			if ($fieldName == 'samedelivery') {
				continue;
			}
			
			// Skip empty delivery fields if samedelivery is set
			if ($this->data->samedelivery == 1 && in_array($fieldName,$deliveryFields)) {
				$this->data->$fieldName = $this->data->{'billing'.$fieldName};
				continue;
			}
			
			// Prepare value
			$value = trim( strval($this->data->$fieldName) );
			
			// Check for missing data
			if ($userField->$keyRequired && empty($value)) {
				
				// If no counties for that state, skip
				if ($fieldName == 'billingcounty_id' && !empty($this->data->billingstate) ) {
					if (CbcheckoutCountryHelper::hasCounties( $this->data->billingstate ) == false) {
						continue;
					}
				}
				// If no counties for that state, skip
				if ($fieldName == 'county_id' && !empty($this->data->state)) {
					if (CbcheckoutCountryHelper::hasCounties( $this->data->state ) == false) {
						continue;
					}
				}
				
				// If there are no cities in that county or no county is set, continue
				if ($fieldName == 'billingcity_id') {
					
					if (empty($this->data->billingcounty_id)) {
						continue;
					}
					elseif (CbcheckoutCountryHelper::hasCities($this->data->billingcounty_id) == false) {
						continue;
					}
					else {
						// Cheap trick to normalize city and city_id fields for js validation tooltips
						//TODO: Make this better
						$fieldName = 'billingcity';
					}
					
				}
				
				// If there are no cities in that county or no county is set, continue
				if ($fieldName == 'city_id') {
						
					if (empty($this->data->county_id)) {
						continue;
					}
					elseif (CbcheckoutCountryHelper::hasCities($this->data->county_id) == false) {
						continue;
					}
					else {
						// Cheap trick to normalize city and city_id fields for js validation tooltips
						//TODO: Make this better
						$fieldName = 'city';
					}
						
				}
				
				if ($fieldName == 'newsletter') {
					continue;
				}
				
				
				$invalid[$fieldName] = KText::sprintf('%s is required.', $fieldTranslations[$fieldName]);
			}
			elseif ($value !== '') {
				
				switch ($fieldName) {
					
					case 'samedelivery':
						continue;
						break;
						
					case 'email':
						if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
							$invalid[$fieldName] = KText::sprintf('%s is invalid.', $fieldTranslations[$fieldName]);
						}
						break;
						
					case 'billingemail':
						if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
							$invalid[$fieldName] = KText::sprintf('%s is invalid.', $fieldTranslations[$fieldName]);
						}
						break;
						
					case 'county':
						if ($value != 0 && !CbcheckoutCountryHelper::countryIdExists($value)) {
							$invalid[$fieldName] = KText::sprintf('%s is invalid.', $fieldTranslations[$fieldName]);
						}
						break;
					
					case 'billingcounty':
						if ($value != 0 && !CbcheckoutCountryHelper::countryIdExists($value)) {
							$invalid[$fieldName] = KText::sprintf('%s is invalid.', $fieldTranslations[$fieldName]);
						}
						break;
					
					case 'state':
						if ($value != 0 && !CbcheckoutCountryHelper::stateIdExists($value, $this->data->country)) {
							$invalid[$fieldName] = KText::sprintf('%s is invalid.', $fieldTranslations[$fieldName]);
						}
						break;
							
					case 'billingstate':
						if ($value != 0 && !CbcheckoutCountryHelper::stateIdExists($value, $this->data->billingcountry)) {
							$invalid[$fieldName] = KText::sprintf('%s is invalid.', $fieldTranslations[$fieldName]);
						}
						break;
						
					case 'vatin':
						if (!CbcheckoutUserHelper::checkVatIn($value, $this->data)) {
							$invalid[$fieldName] = KText::sprintf('%s is invalid.', $fieldTranslations[$fieldName]);
						}
						break;
						
					default:
						$validationExpression = trim($userField->validation_server);
						if ($validationExpression) {
							$res = preg_match($validationExpression, $value);
							if (!$res) {
								$invalid[$fieldName] = KText::sprintf('%s is invalid.', $fieldTranslations[$fieldName]);
							}
						}
						break;
				}
			}
		}
		
		if ($context != 'quotation') {
			
			if (empty($this->data->joomlaid)) {
				$db = KenedoPlatform::getDb();
				$query = "SELECT `id` FROM ".$this->tableName." WHERE `joomlaid` != 0 AND `billingemail` = '".$db->getEscaped($this->data->billingemail)."' LIMIT 1";
				$db->setQuery($query);
				$id = $db->loadResult();
				
				if ($id && $this->data->id != $id) {
					$invalid['billingemail'] = KText::_('You already have an account with that email address.');
				}
			}
		}
		
		if (count($invalid)) {
			$this->setErrors($invalid);
			return false;
		}
		else {
			return true;
		}	
		
	}

}
