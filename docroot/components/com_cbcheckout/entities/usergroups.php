<?php
defined('CB_VALID_ENTRY') or die();

class EntityUserGroups extends KenedoEntity {
	
	function __construct() {
		
		$this->tableName	= '#__cbcheckout_user_groups';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array (	
				'name'=>'id',
				'label'=>KText::_('ID'),
				'type'=>'id',
				'default'=>0,
				'listing'=>10,
				'listingwidth'=>'50px',
				);
		
		$this->fields->title = array (		
				'name'=>'title',
				'label'=>KText::_('Name'),
				'type'=>'string',
				'required'=>1,
				'listing'=> 20,
				'listinglink'=>true,
				'controller'=>'adminusergroups',
				);
				
		if (KenedoPlatform::getName() == 'joomla' && substr(KenedoPlatform::get('versionShort'),0,3) != '1.5' ) {
		
		$this->fields->joomla_user_group_id = array(
				'name'=>'joomla_user_group_id',
				'label'=>KText::_('Platform Group'),
				'tooltip'=>KText::_('The user group of your platform CMS. Choose one to assign new customers to that group.'),
				'type'=>'join',
		
				'joinField'=>'id',
				'textField'=>'title',
		
				'modelClass'=>'CbcheckoutModelAdminjoomlausergroups',
				'modelMethod'=>'getUserGroups',
		
				'required'=>1
				);
		
		}
		
		$this->fields->b2b_mode = array(
				'name'=>'b2b_mode',
				'label'=>KText::_('Tax display mode'),
				'tooltip'=>KText::_('In B2B mode, prices in the configuration are displayed net and price overviews are displayed in the form net plus tax is gross. In B2C, no net prices are shown and the included tax is displayed along with the gross price.'),
				'type'=>'radio',
				'radios'=> array(0=>KText::_('B2C'), 1=>KText::_('B2B')),
				'default'=>0,
		);
		
		$this->fields->permissions_start = array(
				'name'=>'permissions_start',
				'type'=>'groupstart',
				'title'=>KText::_('Permissions'),
				'toggle'=>true,
				'default-state'=>'opened',
		);
		
		$this->fields->enable_see_pricing = array(
				'name'=>'enable_see_pricing',
				'label'=>KText::_('Enable price display in configurator and cart'),
				'type'=>'boolean',
				'default'=>1,
		);
		
		$this->fields->enable_checkout_order = array(	
				'name'=>'enable_checkout_order',
				'label'=>KText::_('Enable checkout'),
				'type'=>'boolean',
				'default'=>1,
		);
		
		$this->fields->enable_save_order = array(
				'name'=>'enable_save_order',
				'label'=>KText::_('Enable Save Order'),
				'type'=>'boolean',
				'default'=>1,
		);
		
		$this->fields->enable_request_assistance = array(
				'name'=>'enable_request_assistance',
				'label'=>KText::_('Enable Assistance Request'),
				'type'=>'boolean',
				'default'=>0,
		);
		
		$this->fields->enable_recommendation = array(
				'name'=>'enable_request_assistance',
				'label'=>KText::_('Enable Assistance Request'),
				'type'=>'boolean',
				'default'=>0,
		);
		
		
		$this->fields->enable_request_quotation = array(
				'name'=>'enable_request_quotation',
				'label'=>KText::_('Enable Quotation Request'),
				'type'=>'boolean',
				'default'=>1,
		);
		
		$this->fields->quotation_download = array(
				'name'=>'quotation_download',
				'label'=>KText::_('Automated quotation download'),
				'type'=>'boolean',
				'default'=>1,
		);
		
		$this->fields->quotation_email = array(
				'name'=>'quotation_email',
				'label'=>KText::_('Automated quotation via email'),
				'tooltip'=>KText::_('You need to have a notification for the status Quotation sent in order. You can manage notifications at Order Management -> Notifications.'),
				'type'=>'boolean',
				'default'=>1,
		);
		
		$this->fields->permissions_end = array(
				'name'=>'permissions_end',
				'type'=>'groupend',
		);
		
		$this->fields->discounts_start = array(
				'name'=>'discounts_start',
				'type'=>'groupstart',
				'toggle'=>true,
				'title'=>KText::_('Discount Levels'),
				'default-state'=>'closed',
		);
		
		$this->fields->discounts_regular_start = array(
				'name'=>'discounts_regular_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Regular Discounts'),
		);
		
		$this->fields->level_1_start = array(
				'name'=>'level_1_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 1'),
		);
		
		$this->fields->discount_start_1 = array(
				'name'=>'discount_start_1',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_type_1 = array(
				'name'=>'discount_type_1',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_amount_1 = array(
				'name'=>'discount_amount_1',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_factor_1 = array(
				'name'=>'discount_factor_1',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_1 = array(
				'name'=>'title_discount_1',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>50
		);
		
		$this->fields->level_1_end = array(
				'name'=>'level_1_end',
				'type'=>'groupend',
		);
		
		$this->fields->level_2_start = array(
				'name'=>'level_2_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 2'),
		);
		
		$this->fields->discount_start_2 = array(
				'name'=>'discount_start_2',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_type_2 = array(
				'name'=>'discount_type_2',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_amount_2 = array(
				'name'=>'discount_amount_2',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_factor_2 = array(
				'name'=>'discount_factor_2',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_2 = array(
				'name'=>'title_discount_2',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>51
		);
		
		$this->fields->level_2_end = array(
				'name'=>'level_2_end',
				'type'=>'groupend',
		);
		
		$this->fields->level_3_start = array(
				'name'=>'level_3_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 3'),
		);
		
		$this->fields->discount_start_3 = array(
				'name'=>'discount_start_3',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_type_3 = array(
				'name'=>'discount_type_3',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_amount_3 = array(
				'name'=>'discount_amount_3',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_factor_3 = array(
				'name'=>'discount_factor_3',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_3 = array(
				'name'=>'title_discount_3',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>52
		);
		
		$this->fields->level_3_end = array(
				'name'=>'level_3_end',
				'type'=>'groupend',
		);
		
		$this->fields->level_4_start = array(
				'name'=>'level_4_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 4'),
		);
		
		$this->fields->discount_start_4 = array(
				'name'=>'discount_start_4',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_type_4 = array(
				'name'=>'discount_type_4',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_amount_4 = array(
				'name'=>'discount_amount_4',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_factor_4 = array(
				'name'=>'discount_factor_4',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_4 = array(
				'name'=>'title_discount_4',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>53
		);
		
		$this->fields->level_4_end = array(
				'name'=>'level_4_end',
				'type'=>'groupend',
		);
		
		$this->fields->level_5_start = array(
				'name'=>'level_5_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 5'),
		);
		
		$this->fields->discount_start_5 = array(
				'name'=>'discount_start_5',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_type_5 = array(
				'name'=>'discount_type_5',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_amount_5 = array(
				'name'=>'discount_amount_5',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_factor_5 = array(
				'name'=>'discount_factor_5',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_5 = array(
				'name'=>'title_discount_5',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>54
		);
		
		$this->fields->level_5_end = array(
				'name'=>'level_5_end',
				'type'=>'groupend',
		);
		
		$this->fields->discounts_regular_end = array(
				'name'=>'discounts_regular_end',
				'type'=>'groupend',
		);
		
		
		
		
		
		
		$this->fields->discounts_recurring_start = array(
				'name'=>'discounts_recurring_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Recurring Discounts'),
		);
		
		$this->fields->recurring_level_1_start = array(
				'name'=>'recurring_level_1_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 1'),
		);
		
		$this->fields->discount_recurring_start_1 = array(
				'name'=>'discount_recurring_start_1',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_type_1 = array(
				'name'=>'discount_recurring_type_1',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_recurring_amount_1 = array(
				'name'=>'discount_recurring_amount_1',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_factor_1 = array(
				'name'=>'discount_recurring_factor_1',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_recurring_1 = array(
				'name'=>'title_discount_recurring_1',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>60
		);
		
		$this->fields->recurring_level_1_end = array(
				'name'=>'recurring_level_1_end',
				'type'=>'groupend',
		);
		
		$this->fields->recurring_level_2_start = array(
				'name'=>'recurring_level_2_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 2'),
		);
		
		$this->fields->discount_recurring_start_2 = array(
				'name'=>'discount_recurring_start_2',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_type_2 = array(
				'name'=>'discount_recurring_type_2',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_recurring_amount_2 = array(
				'name'=>'discount_recurring_amount_2',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_factor_2 = array(
				'name'=>'discount_recurring_factor_2',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_recurring_2 = array(
				'name'=>'title_discount_recurring_2',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>61
		);
		
		$this->fields->recurring_level_2_end = array(
				'name'=>'recurring_level_2_end',
				'type'=>'groupend',
		);
		
		$this->fields->recurring_level_3_start = array(
				'name'=>'recurring_level_3_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 3'),
		);
		
		$this->fields->discount_recurring_start_3 = array(
				'name'=>'discount_recurring_start_3',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_type_3 = array(
				'name'=>'discount_recurring_type_3',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_recurring_amount_3 = array(
				'name'=>'discount_recurring_amount_3',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_factor_3 = array(
				'name'=>'discount_recurring_factor_3',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_recurring_3 = array(
				'name'=>'title_discount_recurring_3',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>62
		);
		
		$this->fields->recurring_level_3_end = array(
				'name'=>'recurring_level_3_end',
				'type'=>'groupend',
		);
		
		$this->fields->recurring_level_4_start = array(
				'name'=>'recurring_level_4_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 4'),
		);
		
		$this->fields->discount_recurring_start_4 = array(
				'name'=>'discount_recurring_start_4',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_type_4 = array(
				'name'=>'discount_recurring_type_4',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_recurring_amount_4 = array(
				'name'=>'discount_recurring_amount_4',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_factor_4 = array(
				'name'=>'discount_recurring_factor_4',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_recurring_4 = array(
				'name'=>'title_discount_recurring_4',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>63
		);
		
		$this->fields->recurring_level_4_end = array(
				'name'=>'recurring_level_4_end',
				'type'=>'groupend',
		);
		
		$this->fields->recurring_level_5_start = array(
				'name'=>'recurring_level_5_start',
				'type'=>'groupstart',
				'toggle'=>false,
				'title'=>KText::_('Discount Level 5'),
		);
		
		$this->fields->discount_recurring_start_5 = array(
				'name'=>'discount_recurring_start_5',
				'label'=>KText::_('For net order totals starting at:'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_type_5 = array(
				'name'=>'discount_recurring_type_5',
				'label'=>KText::_('Discount Type'),
				'type'=>'dropdown',
				'items'=> array('percentage'=>KText::_('Percentage'), 'amount'=>KText::_('Amount')),
				'default'=>'percentage',
		);
		
		$this->fields->discount_recurring_amount_5 = array(
				'name'=>'discount_recurring_amount_5',
				'label'=>KText::_('Discount Amount'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
		);
		
		$this->fields->discount_recurring_factor_5 = array(
				'name'=>'discount_recurring_factor_5',
				'label'=>KText::_('Discount Percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
		);
		
		$this->fields->title_discount_recurring_5 = array(
				'name'=>'title_discount_recurring_5',
				'type'=>'translatable',
				'label'=>KText::_('Title'),
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>64
		);
		
		$this->fields->recurring_level_5_end = array(
				'name'=>'recurring_level_5_end',
				'type'=>'groupend',
		);
		
		$this->fields->discounts_recurring_end = array(
				'name'=>'discounts_recurring_end',
				'type'=>'groupend',
		);
		
		
		
		
		$this->fields->discounts_end = array(
				'name'=>'discounts_end',
				'type'=>'groupend',
		);
		
		$this->fields->custom_start = array(
				'name'=>'custom_start',
				'type'=>'groupstart',
				'title'=>KText::_('Custom Fields'),
				'toggle'=>true,
				'default-state'=>'closed',
		);
		
		$this->fields->custom_1 = array(
				'name'=>'custom_1',
				'label'=>KText::_('Custom Field 1'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->custom_2 = array(
				'name'=>'custom_2',
				'label'=>KText::_('Custom Field 2'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->custom_3 = array(
				'name'=>'custom_3',
				'label'=>KText::_('Custom Field 3'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->custom_4 = array(
				'name'=>'custom_4',
				'label'=>KText::_('Custom Field 4'),
				'type'=>'string',
				'required'=>0,
		);
		
		$this->fields->custom_end = array(
				'name'=>'custom_end ',
				'type'=>'groupend',
		);
		
		$this->children['com_cbcheckout'] = array(
		
				'users'=> array(
						array(
								'titleField'=>'billinglastname',
								'fkField'=>'group_id',
								'controller'=>'adminusers',
								'name'=>KText::_('Customer'),
								'filterField'=>'is_temporary',
								'filterValue'=>'0',
						)
					)
				);
	}

}
