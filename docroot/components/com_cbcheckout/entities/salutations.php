<?php
defined('CB_VALID_ENTRY') or die();

class EntitySalutations extends KenedoEntity {
	
	function __construct() {
		
		$this->tableName	= '#__cbcheckout_salutations';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listingwidth'=>'50px',
				'listing'=>10,
		);
		
		$this->fields->title = array(
				'name'=>'title',
				'label'=>KText::_('Title'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>55,
				'required'=>1,
				'listing'=>20,
				'listinglink'=>1,
				'controller'=>'adminsalutations',
		);
		
		$this->fields->gender = array(
				'name'=>'gender',
				'label'=>KText::_('Gender'),
				'type'=>'radio',
				'radios'=> array(0=>KText::_('Unspecified'), 1=>KText::_('Male'), 2=>KText::_('Female')),
				'default'=>0,
				'tooltip'=>KText::_('Use this to store whether the user is male or female.'),
				'listing'=>30,
				'listingwidth'=>'50px',
		);
	}
	
}
