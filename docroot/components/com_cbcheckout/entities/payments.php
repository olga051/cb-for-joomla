<?php
defined('CB_VALID_ENTRY') or die();

class EntityPayments extends KenedoEntity {
	
	function __construct() {
		
		$this->tableName	= '#__cbcheckout_paymentoptions';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listing'=>10,
				);
		
		$this->fields->title = array(	
				'name'=>'title',
				'label'=>KText::_('Title'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>5,
				'required'=>1,
				'listing'=>30,
				'listinglink'=>1,
				'controller'=>'adminpayments',
				'order'=>10,
				);
		
		$this->fields->connector_name = array(
				'name'=>'connector_name',
				'label'=>KText::_('Payment service provider'),
		
				'type'=>'join',
		
				'joinField'=>'value',
				'textField'=>'title',
		
				'modelClass'=>'CbcheckoutModelAdminpayments',
				'modelMethod'=>'getPspConnectors',
		
				'default'=>'',
				'entity'=>'PaymentClasses',
		
				'tooltip'=>KText::_('Choose from one of the installed payment classes. After saving you will see additional specific settings at the bottom of the form.'),
				'required'=>1,
				'options'=>'',
				'listing'=>60,
				'order'=>50,
				'listingwidth'=>'150px',
		);
		
		$this->fields->published = array(
				'name'=>'published',
				'label'=>KText::_('Active'),
				'type'=>'published',
				'listing'=>70,
				'listingwidth'=>'50px',
				'order'=>60,
				'filter'=>10,
		);
		
		$this->fields->price = array(	
				'name'=>'price',
				'label'=>KText::_('Static extra charge'),
				'type'=>'string',
				'stringType'=>'price',
				'required'=>0,
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
				'tooltip'=>KText::_('Set an extra charge that customers have to pay for using this payment method.'),
				);
		
		$this->fields->percentage = array(	
				'name'=>'percentage',
				'label'=>KText::_('Extra charge percentage'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>'%',
				'required'=>0,
				'tooltip'=>KText::_('Enter an percentage for the extra charge here - 0 to 100 of the order total. The amount is added to the static extra charge.'),
			);
		
		$this->fields->price_min = array(
				'name'=>'price_min',
				'label'=>KText::_('Minimum Extra Charge'),
				'type'=>'string',
				'stringType'=>'price',
				'required'=>0,
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
				'tooltip'=>KText::_('The minimum extra charge for the payment method. Leave empty to be ignored.'),
			);
		
		$this->fields->price_max = array(
				'name'=>'price_max',
				'label'=>KText::_('Maximum Extra Charge'),
				'type'=>'string',
				'stringType'=>'price',
				'required'=>0,
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
				'tooltip'=>KText::_('The maximum extra charge for the payment method. Leave empty to be ignored.'),
			);
		
		$this->fields->taxclass_id = array(
				'name'=>'taxclass_id',
				'label'=>KText::_('Tax Class'),
				'type'=>'join',
					
				'joinField'=>'id',
				'textField'=>'title',
		
				'modelClass'=>'ConfigboxModelAdmintaxclasses',
				'modelMethod'=>'getItems',
		
				'tooltip'=>KText::_('Choose a tax class that determines which tax rate will be used. In the order management you can override the tax rate of a tax rate for each country or state. This way you can have different tax rates for one product or service, depending on the delivery country.'),
				'required'=>0,
				'options'=>'SKIPDEFAULTFIELD NOFILTERSAPPLY',
				
		);

		$this->fields->availablein = array(	
				'name'=>'availablein',
				'label'=>KText::_('Available in'),
				'type'=>'multiselect',
				'required'=>0,
				'storeMethod'=>'xref',
				'saveTable'=>'#__cbcheckout_xref_country_payment_option',
				'saveResultsField'=>'country_id',
				'saveOtherField'=>array('payment_id','id'),
				'selectTable'=>'#__cbcheckout_countries',
				
				'modelClass'=>'CbcheckoutModelAdmincountries',
				'joinField'=>'id',
				'textField'=>'label',
				
				'method'=>'getItems',
				'selectOptionId'=>'id',
				'selectOptionTitle'=>'country_name',
		);
		
		$this->fields->desc = array(	
				'name'=>'desc',
				'label'=>KText::_('Description'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>6,
				'required'=>0,
				'options'=>'USE_HTMLEDITOR ALLOW_HTML',
		);
		
		$this->fields->ordering = array(
				'name'=>'ordering',
				'label'=>KText::_('Ordering'),
				'type'=>'ordering',
				'order'=>20,
				'listing'=>20,
		);
		
	}
	
	function prepare() {
		$success = parent::prepare();
		if ($success == false) {
			return false;
		}
		
		$this->data->connector_name = str_replace('/','',$this->data->connector_name);
		$this->data->connector_name = str_replace('\\','',$this->data->connector_name);
		$this->data->connector_name = str_replace(DS,'',$this->data->connector_name);
		$this->data->connector_name = str_replace('..','',$this->data->connector_name);
		
		if ($this->data->id == 0) {
			$query = "SELECT `ordering` FROM `".$this->tableName."` ORDER BY `ordering` DESC LIMIT 1";
			$db = $this->getDb();
			$db->setQuery($query);
			$this->data->ordering = (int)$db->loadResult() + 10;
		}
		
		return true;
	}

}	
