<?php
defined('CB_VALID_ENTRY') or die();

class EntityEmails extends KenedoEntity {

	function __construct() {

		$this->tableName	= '#__cbcheckout_emails';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listingwidth'=>'50px',
				);

		$this->fields->name = array(
				'name'=>'name',
				'tooltip'=>KText::_('Internal name of the email notification.'),
				'type'=>'string',
				'label'=>KText::_('Name'),
				'required'=>1,
				'order'=>1,
				'listing'=>1,
				'listinglink'=>1,
				'controller'=>'adminemails',
				);

		$this->fields->statuscode = array(
				'name'=>'statuscode',
				'label'=>KText::_('Status Code'),
				'tooltip'=>KText::_('The numeric status code of the order, which defines if the order is paid, saved etc.'),
				'type'=>'join',

				'joinField'=>'id',
				'textField'=>'title',
				'defaultlabel'=>KText::_('Select Status'),

				'modelClass'=>'CbcheckoutModelOrder',
				'modelMethod'=>'getOrderStatuses',

				'required'=>1,
				'listing'=>3,
				'order'=>3,
				'listingwidth'=>'120px',
				);


		$this->fields->emailcustomerstart = array(	
				'name'=>'emailcustomerstart',
				'type'=>'groupstart',
				'title'=>KText::_('E-Mail Customer'),
				'toggle'=>true,
				'default-state'=>'opened',
		);

		$this->fields->send_customer = array(	
				'name'=>'send_customer',
				'label'=>KText::_('Send email'),
				'tooltip'=>KText::_('Choose yes to send the the email notification to the customer.'),
				'type'=>'boolean',
				'default'=>1,
				'required'=>1,
				);

		$this->fields->subject = array(	
				'name'=>'subject',
				'label'=>KText::_('Subject'),
				'tooltip'=>KText::_('Email subject of the notification.'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>30,
				'required'=>1,
				);
		
		$this->fields->body = array(	
				'name'=>'body',
				'label'=>KText::_('Email HTML'),
				'tooltip'=>KText::_('HTML version of the email notification for email readers that display HTML.'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>31,
				'options'=>'USE_HTMLEDITOR ALLOW_HTML',
				'required'=>0,
				);

		$this->fields->emailcustomerend = array(	
				'name'=>'emailcustomerend',
				'type'=>'groupend',
				);
		
		$this->fields->emailmanagerstart = array(	
				'name'=>'emailmanagerstart',
				'type'=>'groupstart',
				'title'=>KText::_('E-Mail Manager'),
				'toggle'=>true,
				'default-state'=>'opened'
				);

		$this->fields->send_manager = array(	
				'name'=>'send_manager',
				'label'=>KText::_('Send email'),
				'tooltip'=>KText::_('Choose yes to send the the email notification to the shop manager.'),
				'type'=>'boolean',
				'default'=>1,
				'required'=>1
				);

		$this->fields->subjectmanager= array(	
				'name'=>'subjectmanager',
				'label'=>KText::_('Subject'),
				'tooltip'=>KText::_('Email subject of the notification.'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>36,
				'required'=>0,
				);
		
		$this->fields->bodymanager = array(	
				'name'=>'bodymanager',
				'label'=>KText::_('Email HTML'),
				'tooltip'=>KText::_('HTML version of the email notification for email readers that support HTML.'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>37,
				'options'=>'USE_HTMLEDITOR ALLOW_HTML',
				'required'=>0
				);

		$this->fields->emailmanagerend = array(	
				'name'=>'emailmanagerend',
				'type'=>'groupend',
				);
		
	}

}
