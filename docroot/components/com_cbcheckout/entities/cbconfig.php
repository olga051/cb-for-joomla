<?php
defined('CB_VALID_ENTRY') or die();

class EntityCbconfig extends KenedoEntity {

	function __construct() {

		$this->tableName	= '#__cbcheckout_config';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
		);
		
		
		
		$this->fields->uploadsstart = array(
				'name'=>'uploadsstart',
				'type'=>'groupstart',
				'title'=>KText::_('File Uploads for orders'),
				'toggle'=>true,
				'default-state'=>'opened',
				'notes'=>KText::_('File Uploads enable a customer to attach files to an order after a quote was requested, an order was saved or a purchase was made. The customer finds the upload in the order view in the customer profile. The files are visible in the shop manager order display view.'),
		);
		
		$this->fields->enable_file_uploads = array(
				'name'=>'enable_file_uploads',
				'label'=>KText::_('Enable File Uploads'),
				'type'=>'boolean',
				'default'=>0,
				'tooltip'=>KText::_('Choose yes to let customers uploads files linked to their order after the order has been made.'),
		);
		
		$this->fields->allowed_file_extensions = array (
				'name'=>'allowed_file_extensions',
				'label'=>KText::_('Allowed File Extensions'),
				'tooltip'=>KText::_('Specify the file extensions to allow for uploads. Write them in the form jpg, gif, psd. Leave empty to allow any extension. php is always disallowed.'),
				'default'=>'',
				'type'=>'string',
				'stringType'=>'string',
		);
		
		$this->fields->allowed_file_mimetypes = array (
				'name'=>'allowed_file_mimetypes',
				'label'=>KText::_('Allowed MIME-Types'),
				'tooltip'=>KText::_('Specify the file mime-types to allow for uploads. Write them in the form image/jpg, image/gif. Leave empty to allow any MIME-Type.'),
				'default'=>'',
				'type'=>'string',
				'stringType'=>'string',
		);
		
		if (!KenedoFileHelper::canCheckMimeType()) {
			$this->fields->allowed_file_mimetypes['tooltip'] .= ' '.KText::_('Your server cannot determine MIME-Types. The function mime_content_type or finfo has to be on your server.');
			$this->fields->allowed_file_mimetypes['label'] .= ' - '.KText::_('Not available');
		}
		
		$max_upload = (int)(ini_get('upload_max_filesize'));
		$max_post = (int)(ini_get('post_max_size'));
		$memory_limit = (int)(ini_get('memory_limit'));
		$upload_mb = min($max_upload, $max_post, $memory_limit);
		
		$this->fields->allowed_file_size= array (
				'name'=>'allowed_file_size',
				'label'=>KText::_('Maximum File Size'),
				'tooltip'=>KText::sprintf('Specify the maximum file size to allow for uploads in MB. Server maximum is %sMB.', $upload_mb),
				'default'=>'',
				'type'=>'string',
				'stringType'=>'string',
		);
		
		$this->fields->uploadsend = array(
				'name'=>'uploadsend',
				'type'=>'groupend',
		);
		
		$this->fields->checkoutstart = array(
				'name'=>'checkoutstart',
				'type'=>'groupstart',
				'title'=>KText::_('Legacy checkout process'),
				'toggle'=>true,
				'default-state'=>'closed',
		);
		
		$this->fields->showrefundpolicy = array(
				'name'=>'showrefundpolicy',
				'label'=>KText::_('Show refund policy'),
				'type'=>'boolean',
				'default'=>0,
				'tooltip'=>KText::_('Choose yes to show your refund policy at order confirmation.'),
		);
			
		$this->fields->showrefundpolicyinline = array(
				'name'=>'showrefundpolicyinline',
				'label'=>KText::_('Show refund policy below confirmation'),
				'type'=>'boolean',
				'default'=>0,
				'tooltip'=>KText::_('Choose yes to show your refund policy inline below order confirmation.'),
		);
		
		$this->fields->checkoutend = array(
				'name'=>'checkoutend',
				'type'=>'groupend',
		);
			
	}

}
