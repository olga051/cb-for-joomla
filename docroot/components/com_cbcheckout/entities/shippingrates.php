<?php
defined('CB_VALID_ENTRY') or die();

class EntityShippingrates extends KenedoEntity {
	
	function __construct() {
		
		$this->tableName	= '#__cbcheckout_shippingrates';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array(	
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listingwidth'=>'50px',
				'listing'=>10,
				);
		
		$this->fields->ordering = array(
				'name'=>'ordering',
				'label'=>KText::_('Ordering'),
				'type'=>'ordering',
				'order'=>60,
				'listing'=>20,
		);
		
		$this->fields->title = array(	
				'name'=>'title',
				'label'=>KText::_('Title'),
				'type'=>'translatable',
				'stringTable'=>'#__cbcheckout_strings',
				'langType'=>3,
				'required'=>1,
				'listing'=>30,
				'listinglink'=>1,
				'controller'=>'adminshippingrates',
				);
		
		$this->fields->shipper = array(	
				'name'=>'shipper',
				'label'=>KText::_('Shipper'),
				'type'=>'join',
				'joinField'=>'id',
				'textField'=>'title',
				'defaultlabel'=>KText::_('Select Shipper'),
				
				'modelClass'=>'CbcheckoutModelAdminshippers',
				'modelMethod'=>'getItems',
				
				'selectfields'=>array(array('title','shipper_title')),
				'required'=>1,
				'filterparents'=>1,
				'filter'=>2,
				'parent'=>1,
				'listing'=>40,
				'listingwidth'=>'120px',
				);
									
									
		$this->fields->zone = array(	
				'name'=>'zone',
				'label'=>KText::_('Zone'),
				'type'=>'join',
				
				'joinField'=>'id',
				'textField'=>'label',
				'defaultlabel'=>KText::_('Select Zone'),
				
				'modelClass'=>'CbcheckoutModelAdminzones',
				'modelMethod'=>'getItems',
				
				'selectfields'=>array(array('label','zone_label')),
				
				'parent'=>1,
				'filterparents'=>1,
				'filter'=>1,
				'required'=>1,
				'listing'=>50,
				'listingwidth'=>'120px',
				);

		$this->fields->minweight = array(	
				'name'=>'minweight',
				'label'=>KText::_('Minimum Weight'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>CONFIGBOX_WEIGHTUNITS,
				'required'=>1,
				'listing'=>60,
				'listingwidth'=>'60px',
				'order'=>14,
				);
		
		$this->fields->maxweight = array(	
				'name'=>'maxweight',
				'label'=>KText::_('Maximum Weight'),
				'type'=>'string',
				'stringType'=>'number',
				'unit'=>CONFIGBOX_WEIGHTUNITS,
				'required'=>1,
				'listing'=>70,
				'listingwidth'=>'60px',
				'order'=>15,
				);
		
		$this->fields->deliverytime = array(	
				'name'=>'deliverytime',
				'label'=>KText::_('Delivery Time'),
				'type'=>'string',
				'stringType'=>'number',
				'required'=>0,
				'unit'=>KText::_('Days'),
				);
		
		$this->fields->price = array(	
				'name'=>'price',
				'label'=>KText::_('Price'),
				'type'=>'string',
				'stringType'=>'price',
				'unit'=>CONFIGBOX_BASE_CURRENCY_SYMBOL,
				'required'=>0,
				'listing'=>80,
				'listingwidth'=>'60px',
				'order'=>20,
				);
		
		$this->fields->taxclass_id = array(
				'name'=>'taxclass_id',
				'label'=>KText::_('Tax Class'),
				'type'=>'join',
					
				'joinField'=>'id',
				'textField'=>'title',
		
				'modelClass'=>'ConfigboxModelAdmintaxclasses',
				'modelMethod'=>'getItems',
				
				'tooltip'=>KText::_('Choose a tax class that determines which tax rate will be used. In the order management you can override the tax rate of a tax rate for each country or state. This way you can have different tax rates for one product or service, depending on the delivery country.'),
				'required'=>0,
				'options'=>'SKIPDEFAULTFIELD NOFILTERSAPPLY',
				
				);
		
		$this->fields->external_id = array(
				'name'=>'external_id',
				'label'=>KText::_('External ID'),
				'type'=>'string',
				'stringType'=>'string',
				'required'=>0
				);
		
		$this->fields->published = array(	
				'name'=>'published',
				'label'=>KText::_('Active'),
				'type'=>'published',
				'default'=>1,
				'listing'=>100,
				'listingwidth'=>'60px',
				'order'=>50,
				);
				
	}
	
	function prepare() {
	
		$response = parent::prepare();
		if ($response === false) {
			return false;
		}
		
		if ($this->data->id == 0) {
			$query = "SELECT `ordering` FROM `".$this->tableName."` ORDER BY `ordering` DESC LIMIT 1";
			$db = $this->getDb();
			$db->setQuery($query);
			$this->data->ordering = (int)$db->loadResult() + 10;
		}
	
		return true;
	}
	
}
