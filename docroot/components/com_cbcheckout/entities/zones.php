<?php
defined('CB_VALID_ENTRY') or die();

class EntityZones extends KenedoEntity {

	function __construct() {

		$this->tableName	= '#__cbcheckout_zones';
		$this->tableKey		= 'id';
		
		$this->fields = new StdClass();
		
		$this->fields->id = array (
				'name'=>'id',
				'type'=>'id',
				'default'=>0,
				'label'=>KText::_('ID'),
				'listingwidth'=>'50px',
		);

		$this->fields->label = array (		
				'name'=>'label',
				'label'=>KText::_('Name'),
				'type'=>'string',
				'listing'=>1,
				'listinglink'=>1,
				'required'=>1,
				'controller'=>'adminzones',
		);

		$this->fields->countries = array(	
				'name'=>'countries',
				'label'=>KText::_('Countries'),
				'type'=>'multiselect',
				'required'=>0,
				'storeMethod'=>'xref',
				'saveTable'=>'#__cbcheckout_xref_country_zone',
				'saveResultsField'=>'country_id',
				'saveOtherField'=>array('zone_id','id'),
				'selectTable'=>'#__cbcheckout_countries',
				
				'modelClass'=>'CbcheckoutModelAdmincountries',
				'joinField'=>'id',
				'textField'=>'country_name',
				
				'method'=>'getItems',
				'selectOptionId'=>'id',
				'selectOptionTitle'=>'country_name',
		);

	}

}
