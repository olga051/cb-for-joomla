<?php
function com_cbcheckoutTabs() {
	
	$items = array(
			'adminorders'		=>array('title'=>KText::_('Orders'), 			'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminorders'), 		'activeOn'=>array('adminorder'), ),
			'adminreviews'		=>array('title'=>KText::_('Reviews'), 			'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminreviews'), 	'activeOn'=>array('adminreviews','adminreview'), ),
			'adminusers'		=>array('title'=>KText::_('Customers'), 		'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminusers'), 		'activeOn'=>array('adminuser'), ),
			
			'adminusergroups'	=>array('title'=>KText::_('Customer Settings'),	'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminusergroups'), 	'activeOn'=>array('adminusergroups', 'adminusergroup','adminsalutations','adminsalutation','adminuserfields'), ),
			
			'adminshippers'		=>array('title'=>KText::_('Shipping'), 			'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminshippers'), 	'activeOn'=>array('adminshippers','adminshipper','admincountries','admincountry','adminstates','adminstate','adminzones','adminzone','adminshippingrates','adminshippingrate'), ),
			'adminpayments'		=>array('title'=>KText::_('Payment Methods'),	'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminpayments'), 	'activeOn'=>array('adminpayment'), ),
			'adminshopdata'		=>array('title'=>KText::_('Shop Data'), 		'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminshopdata'), ),
			'adminemails'		=>array('title'=>KText::_('Notifications'),		'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminemails'), 		'activeOn'=>array('adminemail'), ),
			'adminconfig'		=>array('title'=>KText::_('Configuration'),		'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminconfig') ),
	);
		
	$items['adminshippers']['subviews']['shippers'] 		= array('title'=>KText::_('Shippers'),			'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminshippers'), 		'activeOn'=>array('adminshippers','adminshipper'), );
	$items['adminshippers']['subviews']['countries'] 		= array('title'=>KText::_('Countries'),			'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=admincountries'), 		'activeOn'=>array('admincountry','admincountries'), );
	$items['adminshippers']['subviews']['states'] 			= array('title'=>KText::_('States'),			'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminstates'), 			'activeOn'=>array('adminstate','adminstates'), );
	$items['adminshippers']['subviews']['zones'] 			= array('title'=>KText::_('Zones'),				'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminzones'), 			'activeOn'=>array('adminzone','adminzones'), );
	$items['adminshippers']['subviews']['shippingrates'] 	= array('title'=>KText::_('Shipping Rates'),	'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminshippingrates'), 	'activeOn'=>array('adminshippingrate','adminshippingrates'), );
	
	$items['adminusergroups']['subviews']['adminusergroups'] 	= array('title'=>KText::_('Customer Groups'), 	'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminusergroups'), 	'activeOn'=>array('adminusergroups', 'adminusergroup'), );
	$items['adminusergroups']['subviews']['adminsalutations'] 	= array('title'=>KText::_('Salutations'), 		'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminsalutations'), 'activeOn'=>array('adminsalutations', 'adminsalutation'), );
	$items['adminusergroups']['subviews']['adminuserfields'] 	= array('title'=>KText::_('User Fields'),		'link'=>KLink::getRoute('index.php?option=com_cbcheckout&controller=adminuserfields'), 	'activeOn'=>array('adminuserfields'), );
	
	return $items;
}
