<?php
class CbcheckoutPositionHelper {

	static function getPositionHtml($orderRecord, $position, $showIn = 'popup', $showSkus = NULL, $inAdmin = false) {
		if ($showSkus === NULL) {
			$showSkus = CONFIGBOX_SKU_IN_ORDER_RECORD;
		}
		
		$view = KenedoView::getView('KenedoViewPosition',KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'position'.DS.'view.html.php');
		$view->assignRef('record',$orderRecord);
		$view->assignRef('position',$position);
		$view->assign('inAdmin',$inAdmin);
		$view->assign('showIn',$showIn);
		$view->assign('showSkus',$showSkus);
		return $view->getViewOutput();
	}

}