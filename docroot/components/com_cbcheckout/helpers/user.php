<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutUserHelper {
	
	static $viesWsdl = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';
	static $error;
	static $errors;
	static $userFields;
	static $taxRates;
	static $platformGroupIds;
	static $orderAddresses;
	static $userGroups;
	static $users;
	static $salutations;
	
	static function getUserId() {
		return KSession::get('user_id', 0, 'com_configbox');
	}
	
	static function getSalutationDropdown($name = 'salutation_id', $selected = NULL) {
		
		if (!isset(self::$salutations[KText::$languageTag])) {
			self::getSalutations();
		}
		
		$options = array();
		foreach (self::$salutations[KText::$languageTag] as $id=>$item) {
			$options[$id] = $item->title;
		}
		
		$selectBox = KenedoHtml::getSelectField($name, $options, $selected);
		return $selectBox;
	}
	
	static function getSalutation($salutationId) {
		if (!$salutationId) {
			return NULL;
		}
		
		if (!isset(self::$salutations[KText::$languageTag])) {
			self::getSalutations();
		}
		
		return self::$salutations[KText::$languageTag][$salutationId];
		
	}
	
	static function getSalutations() {
		
		if (!isset(self::$salutations[KText::$languageTag])) {
			$query = "SELECT * FROM `#__cbcheckout_salutations`";
			$db = KenedoPlatform::getDb();
			$db->setQuery($query);
			self::$salutations[KText::$languageTag] = $db->loadObjectList('id');
			foreach ( self::$salutations[KText::$languageTag] AS $id=>$salutation) {
				$salutation->title = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings',55,$id);
			}
		}
		
	}
	
	static function getPlatformUserGroupId($userGroupId = NULL) {
		
		if (!$userGroupId) {
			$userGroupId = CONFIGBOX_DEFAULT_USER_GROUP_ID;
		}
		
		if (empty(self::$platformGroupIds[$userGroupId])) {
			$query = "SELECT `joomla_user_group_id` FROM `#__cbcheckout_user_groups` WHERE `id` = ".(int)$userGroupId;
			$db = KenedoPlatform::getDb();
			$db->setQuery($query);
			self::$platformGroupIds[$userGroupId] = $db->loadResult();
		}
		
		return self::$platformGroupIds[$userGroupId];
		
	}
	
	static function getUserFields($groupId = NULL) {
		
		if (!isset(self::$userFields[$groupId])) {
			if ($groupId === NULL) {
				$groupId = KRequest::getInt('group_id',1);
			}	
			$db = KenedoPlatform::getDb();
			// Since this is can only be 1 currently, we hard-code the group id
			$groupId = 1;
			$query = 'SELECT * FROM `#__cbcheckout_userfields` WHERE `group_id` = '.(int)$groupId;
			
			$db->setQuery($query);			
			self::$userFields[$groupId] = $db->loadObjectList('field_name');
			
			$copy = (array)self::$userFields[$groupId]['billingcity'];
			self::$userFields[$groupId]['billingcity_id'] = $copy;
			self::$userFields[$groupId]['billingcity_id']['field_name'] = 'billingcity_id';
			self::$userFields[$groupId]['billingcity_id'] = (object)self::$userFields[$groupId]['billingcity_id'];
			
			$copy = (array)self::$userFields[$groupId]['city'];
			self::$userFields[$groupId]['city_id'] = $copy;
			self::$userFields[$groupId]['city_id']['field_name'] = 'city_id';
			self::$userFields[$groupId]['city_id'] = (object)self::$userFields[$groupId]['city'];
		}
		return self::$userFields[$groupId];
	}
	
	static function getUserIdByEmail($email) {
		if (trim($email) == '') {
			return null;
		}
		$db = KenedoPlatform::getDb();
		$db->setQuery("SELECT `id` FROM `#__cbcheckout_users` WHERE `joomlaid` != 0 AND `billingemail` = '".$db->getEscaped($email)."'");
		$id = $db->loadResult();
		return $id;
	}
	
	static function getUserByJoomlaUserId($jUserId) {
		
		$db = KenedoPlatform::getDb();
		
		$query = "SELECT * FROM `#__cbcheckout_users` WHERE `joomlaid` = '".(int)$jUserId."' LIMIT 1";
		$db->setQuery($query);
		$user = $db->loadObject();
		
		if ($user) {
			return $user;
		}
		
		$userId = self::getUserId();
		
		if ($userId == 0) {
			$user = self::initUserRecord();
		}
		else {
			$query = "SELECT * FROM `#__cbcheckout_users` WHERE `id` = ".(int)$userId;
			$db->setQuery($query);
			$user = $db->loadObject();
		}
		
		return $user;
	}
	
	static function resetUserCache($userId = NULL) {
		if ($userId) {
			if (isset(self::$users[$userId])) {
				unset(self::$users[$userId]);
			}
		}
		else {
			self::$users = array();
		}
	}
	
	static function resetOrderAddressCache($orderId = NULL) {
		if ($orderId) {
			if (isset(self::$orderAddresses[$orderId])) {
				unset(self::$orderAddresses[$orderId]);
			}
		}
		else {
			self::$orderAddresses = array();
		}
	}
	
	static function &getUser($userId = NULL, $augment = true, $init = true) {
		
		if ($userId == NULL) {
			$userId = self::getUserId();
		}
		
		if (!isset(self::$users[$userId])) {
			
			$db = KenedoPlatform::getDb();
			$query = "SELECT *, id AS user_id FROM `#__cbcheckout_users` WHERE `id` = ".(int)$userId." LIMIT 1";
			$db->setQuery($query);
			$user = $db->loadObject();
			
			if (!$user && $init == false) {
				$return = NULL;
				return $return;
			}
			
			if (!$user) {
				$user = self::initUserRecord();
			}
			
			if ($user) {
				self::$users[$userId] =& $user;
			}
			
		}
		
		if ($augment) {
			self::augmentUserRecord( self::$users[$userId] );
		}
			
		return self::$users[$userId];
		
	}
	
	static function orderAddressComplete($orderAddress, $context = 'checkout') {
		
		$entity = KenedoEntity::getEntity('Users', 'com_cbcheckout');
		if (!$entity->bind($orderAddress)) {
			return false;
		}
		
		return $entity->check($context);
	}
	
	static function saveUser($userId = NULL, $orderId = NULL, $context, $loginUser = true) {
		
		if ($userId === NULL) {
			$userId = self::getUserId();
		}
		
		$user = self::getUser($userId,false,true);
		
		// Keep old state and country info for if country is changed but state is not in the form
		$oldBillingCountry = $user->billingcountry;
		$oldDeliveryCountry = $user->country;
		
		// Get the user fields
		$userFields = self::getUserFields($user->group_id);
		
		// Collect the form data
		foreach ($userFields as $fieldName=>$userField) {
			if ( KRequest::getVar($fieldName,NULL) !== NULL ) {
				$user->$fieldName = trim(KRequest::getString($fieldName));
			}
		}
		
		// Unset the old state if none came from the form and country has changed
		$newBillingState = KRequest::getVar('billingstate',NULL);
		$newBillingCountry = KRequest::getVar('billingcountry',NULL);
		if ($newBillingState == NULL && $oldBillingCountry != $newBillingCountry) {
			$user->billingstate = '';
		}
		
		// Same for delivery
		$newDeliveryState = KRequest::getVar('state',NULL);
		
		$newDeliveryCountry = KRequest::getVar('country',NULL);
		if ($newDeliveryState == NULL && $oldDeliveryCountry != $newDeliveryCountry) {
			$user->state = '';
		}
		
		$user->is_temporary = 0;
		
		$entity = KenedoEntity::getEntity('Users', 'com_cbcheckout');
		
		if (!$entity->bind($user)) {
			self::$error = $entity->getError();
			self::$errors = $entity->getErrors();
			KLog::log('Error when binding user info to entity. Error message was: "'.$entity->getError().'".','fatal',KText::_('A system error occured.'));
			return false;
		}
		
		if (!$entity->prepare() ) {
			self::$error = $entity->getError();
			self::$errors = $entity->getErrors();
			KLog::log('Error when preparing user info to entity. Error message was: "'.$entity->getError().'".','fatal',KText::_('A system error occured.'));
			return false;
		}
		
		if (!$entity->check($context)) {
			self::$error = $entity->getError();
			self::$errors = $entity->getErrors();
			return false;
		}
		
		if (!$entity->store() ) {
			self::$error = $entity->getError();
			self::$errors = $entity->getErrors();
			KLog::log('Error when saving user info by entity. Error message was: "'.$entity->getError().'".','fatal',KText::_('A system error occured.'));
			return false;
		}
		else {
			self::$users[$userId] = NULL;
			$user = self::getUser($userId,false,false);
		}
		
		if ($context != 'quotation') {
			if (!$user->joomlaid) {
				
				$registrationData = CbcheckoutUserHelper::registerPlatformUser($user);
				
				if ( $registrationData ) {
					$db = KenedoPlatform::getDb();
					$query = "UPDATE #__cbcheckout_users SET `joomlaid` = ".(int)$registrationData->platformUserId.", `password` = '".$db->getEscaped($registrationData->passwordEncrypted)."' WHERE `id` = ".(int)$user->id." LIMIT 1";
					$db->setQuery($query);
					$succ = $db->query();
					if ($succ) {
						KLog::log('Updated CB User "'.$user->id.'" with Joomla User ID "'.(int)$registrationData->platformUserId.'"');
							
						if ($loginUser && ($context == 'checkout' or $context == 'quotation' or $context = 'assistance')) {
							$response = KenedoPlatform::p()->login($registrationData->username, $registrationData->passwordClear, array());
						}
					}
					
					// Set the platform user id to have it set in the order address record as well
					$user->joomlaid = $registrationData->platformUserId;
					
					CbcheckoutUserHelper::sendRegistrationEmail($user, $registrationData->passwordClear);
					
				}
				else {
					KLog::log('Could not register platform user. CB User ID is "'.$user->id.'". Error message is "'.self::$error.'".','error');
				}
				
			}
		}
		
		if ($context == 'quotation') {
			if (KRequest::getVar('comment',NULL) !== NULL) {
				$comment = KRequest::getString('comment');
				KSession::set('quote_comment', $comment);
			}
		}
		
		if ($orderId && $context != 'profile') {
			
			// Store the order address
			$succ = self::setOrderAddress($orderId,$user);
			if (!$succ) {
				self::$error = KText::_('Could not set order address');
				KLog::log('Could not set order address.','fatal',KText::_('System error: Could not set order address.'));
				return false;
			}
			
			// Get the order and check if delivery and payment is still valid
			$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
			$orderRecord = $orderModel->getOrderRecord($orderId);
			
			if ($orderRecord->delivery_id && $orderModel->isValidDeliveryOption($orderRecord, $orderRecord->delivery_id) == false) {
// 				KenedoPlatform::p()->sendSystemMessage(KText::_('The current delivery option is not available for your address and has been unset. Please pick another one.'));
				$orderModel->storeOrderRecordDeliveryOption($orderId, 0);
			}
			
			if ($orderRecord->payment_id && $orderModel->isValidPaymentOption($orderRecord, $orderRecord->payment_id) == false) {
// 				KenedoPlatform::p()->sendSystemMessage(KText::_('The current payment option is not available for your address and has been unset. Please pick another one.'));
				$orderModel->storeOrderRecordPaymentOption($orderId, 0);
			}
			
		}
		
		self::augmentUserRecord($user);
		
		KenedoObserver::triggerEvent('onConfigBoxUpdateUserInfo' , array($user) );
		
		return true;
	}
	
	/**
	 * Change the customer and platform user password
	 * The method seems strange. There doesn't seem to be a way to get the encrypted version of the clear password. 
	 * So we need to store the platform user first, then retrieve the password.
	 * @param int $cbUserId
	 * @param string $newPassword
	 */
	static function changeUserPassword($cbUserId, $newPassword) {
		
		$db = KenedoPlatform::getDb();
		
		$db->setQuery('SELECT `joomlaid` FROM #__cbcheckout_users WHERE `id` = '.intval($cbUserId));
		$platformId = $db->loadResult();
		
		if (!$platformId) {
			KLog::log('Could not change the password for customer with ID "'.$cbUserId.'". The customer account is not connected to a platform account.','error');
			self::$error = KText::_('We could not change your password because of a system error. Please contact us to solve this issue.');
			return false;
		}
		
		// Keep a copy of the old password for later
		$oldPassword = KenedoPlatform::p()->getUserPasswordEncoded($platformId);
		
		// Change the platform user password
		$success = KenedoPlatform::p()->changeUserPassword($platformId, $newPassword);
		
		if ($success == false) {
			self::$error = KText::_('We could not change your password because of a system error. Please contact us to solve this issue.');
			return false;
		}
		
		// Now get the password
		$password = KenedoPlatform::p()->getUserPasswordEncoded($platformId);
		
		// Update the customer password
		$db->setQuery("UPDATE `#__cbcheckout_users` SET `password` = '".$db->getEscaped($password)."' WHERE `id` = ".intval($cbUserId) );
		$success = $db->query();
		
		if ($success) {
			return true;
		}
		else {
			// Change back the password to avoid out-of-sync issues
			$success = KenedoPlatform::p()->changeUserPassword($platformId, $oldPassword);
			self::$error = KText::_('We could not change your password because of a system error. Please contact us to solve this issue.');
			return false;
		}
	}
	
	static function &getOrderAddress($orderId, $augment = true, $init = true) {
	
		if ($orderId === NULL) {
			$return = NULL;
			return $return;
		}
	
		if (!isset(self::$orderAddresses[$orderId])) {
			$db = KenedoPlatform::getDb();
			$query = "SELECT *, user_id AS id FROM `#__cbcheckout_orderaddress` WHERE `order_id` = ".(int)$orderId." LIMIT 1";
			$db->setQuery($query);
			$orderAddress = $db->loadObject();
			
			if (!$orderAddress && $init == false) {
				$return = NULL;
				return $return;
			}
			
			if (!$orderAddress) {
				$orderAddress = self::initUserRecord();
			}
			
			self::$orderAddresses[$orderId] = $orderAddress;
		}
		
		if ($augment) {
			self::augmentUserRecord( self::$orderAddresses[$orderId] );
		}
		
		return self::$orderAddresses[$orderId];
	}
	
	static function setOrderAddress($orderId, $userData = NULL) {
		
		if ($userData === NULL) {
			$userData = CbcheckoutUserHelper::getUser();
		}
		
		if ($userData) {
			$record = new stdClass();
			$record->id 				= $orderId;
			$record->order_id 			= $orderId;
			$record->user_id 			= $userData->id;
			$record->joomlaid 			= $userData->joomlaid;
			$record->billingcompanyname = $userData->billingcompanyname;
			$record->billingfirstname 	= $userData->billingfirstname;
			$record->billinglastname 	= $userData->billinglastname;
			$record->billinggender 		= $userData->billinggender;
			$record->billingsalutation_id 	= $userData->billingsalutation_id;
			$record->billingaddress1 	= $userData->billingaddress1;
			$record->billingaddress2 	= $userData->billingaddress2;
			$record->billingzipcode 	= $userData->billingzipcode;
			$record->billingcity 		= $userData->billingcity;
			$record->billingcountry 	= $userData->billingcountry;
			$record->billingstate 		= $userData->billingstate;
			$record->billingcounty_id	= $userData->billingcounty_id;
			$record->billingcity_id		= $userData->billingcity_id;
			$record->billingemail 		= $userData->billingemail;
			$record->billingphone 		= $userData->billingphone;
			$record->language_tag 		= $userData->language_tag;
				
			$record->vatin 			= $userData->vatin;
			$record->samedelivery 	= $userData->samedelivery;
			$record->group_id 		= $userData->group_id;
			$record->newsletter 	= $userData->newsletter;
				
			$record->password = '';
				
			$record->custom_1 = $userData->custom_1;
			$record->custom_2 = $userData->custom_2;
			$record->custom_3 = $userData->custom_3;
			$record->custom_4 = $userData->custom_4;
				
			// Duplicate billing to delivery info if samedelivery
			if ($userData->samedelivery == 1) {
				$record->companyname 	= $userData->billingcompanyname;
				$record->firstname 		= $userData->billingfirstname;
				$record->lastname 		= $userData->billinglastname;
				$record->gender 		= $userData->billinggender;
				$record->salutation_id 	= $userData->billingsalutation_id;
				$record->address1 		= $userData->billingaddress1;
				$record->address2 		= $userData->billingaddress2;
				$record->zipcode 		= $userData->billingzipcode;
				$record->city 			= $userData->billingcity;
				$record->country 		= $userData->billingcountry;
				$record->state 			= $userData->billingstate;
				$record->county_id		= $userData->billingcounty_id;
				$record->city_id		= $userData->billingcity_id;
				$record->email 			= $userData->billingemail;
				$record->phone 			= $userData->billingphone;
			}
			else {
				$record->companyname 	= $userData->companyname;
				$record->firstname 		= $userData->firstname;
				$record->lastname 		= $userData->lastname;
				$record->gender 		= $userData->gender;
				$record->salutation_id 	= $userData->salutation_id;
				$record->address1 		= $userData->address1;
				$record->address2 		= $userData->address2;
				$record->zipcode 		= $userData->zipcode;
				$record->city 			= $userData->city;
				$record->country 		= $userData->country;
				$record->state 			= $userData->state;
				$record->county_id		= $userData->county_id;
				$record->city_id		= $userData->city_id;
				$record->email 			= $userData->email;
				$record->phone 			= $userData->phone;
			}
		
		}

		// Check if there is an entry already
		$db = KenedoPlatform::getDb();
		$query = "SELECT `id` FROM `#__cbcheckout_orderaddress` WHERE `id` = ".(int)$orderId." LIMIT 1";
		$db->setQuery($query);
		$entryExists = (boolean)$db->loadResult();
		
		// Do the insert/update
		if ($entryExists) {
			$succ = $db->updateObject('#__cbcheckout_orderaddress', $record, 'id', false);
		} else {
			$record->created = KenedoTimeHelper::getFormattedOnly('NOW','datetime');
			$succ = $db->insertObject('#__cbcheckout_orderaddress', $record, 'id', false);
		}
		
		// Bounce if query fails
		if ($succ == false) {
			$internalMessage = 'Could not update/insert order address record. SQL error is "'.$db->getErrorMsg().'". Record is '.var_export($record,true);
			KLog::log($internalMessage, 'error', KText::_('A system error occured during storing your order\'s address information'));
			return false;
		}
		else {
			
			if (KRequest::getVar('comment',NULL) !== NULL) {
				$comment = KRequest::getString('comment');
				$query = "UPDATE `#__cbcheckout_order_records` SET `comment` = '".$db->getEscaped($comment)."' WHERE `id` = ".(int)$orderId;
				$db->setQuery($query);
				$db->query();
			}
			
			self::$orderAddresses[$orderId] = $record;
			self::augmentUserRecord( self::$orderAddresses[$orderId] );
			KenedoObserver::triggerEvent( 'onConfigBoxUpdateOrderAddress' , array(self::$orderAddresses[$orderId], $orderId) );
			return true;
		}
		
	}
	
	static function &getGroupData($groupId) {
		
		if ($groupId === NULL) {
			return NULL;
		}
		
		if (!isset(self::$userGroups[$groupId])) {
			
			$db = KenedoPlatform::getDb();
			$query = "SELECT * FROM `#__cbcheckout_user_groups` WHERE `id` = ".(int)$groupId;
			$db->setQuery($query);
			self::$userGroups[$groupId] = $db->loadObject();
			self::$userGroups[$groupId]->title_discount_1 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 50, $groupId);
			self::$userGroups[$groupId]->title_discount_2 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 51, $groupId);
			self::$userGroups[$groupId]->title_discount_3 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 52, $groupId);
			self::$userGroups[$groupId]->title_discount_4 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 53, $groupId);
			self::$userGroups[$groupId]->title_discount_5 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 54, $groupId);
			
			self::$userGroups[$groupId]->title_recurring_discount_1 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 60, $groupId);
			self::$userGroups[$groupId]->title_recurring_discount_2 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 61, $groupId);
			self::$userGroups[$groupId]->title_recurring_discount_3 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 62, $groupId);
			self::$userGroups[$groupId]->title_recurring_discount_4 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 63, $groupId);
			self::$userGroups[$groupId]->title_recurring_discount_5 = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 64, $groupId);
		}
		
		return self::$userGroups[$groupId];
	}
	
	static function augmentUserRecord(&$user) {
		
		if (!$user) {
			return false;
		}
		
		if (!isset($user->order_id)) {
			$user->order_id = NULL;
		}
		if (!isset($user->user_id)) {
			$user->user_id = NULL;
		}
		
		// Get the delivery country for billing if the country is empty (older versions got the delivery as primary set)
		if (empty($user->billingcountry) && $user->samedelivery == 1 && !empty($user->country)) {
			$user->billingcountry = $user->country;
		}
		
		// Get the billing country for shipping if the delivery country is empty (older versions did not set the delivery set if same delivery)
		if (empty($user->country) && $user->samedelivery == 1 && !empty($user->billingcountry)) {
			$user->country = $user->billingcountry;
		}
		
		// Language name
		if ($user->language_tag) {
			$language = KenedoLanguageHelper::getLanguageByTag($user->language_tag);
			if ($language) {
				$user->language_name = $language->label;
			}
			else {
				$user->language_name = '';
			}
		}
		else {
			$user->language_name = '';
		}
	
		// Country names for delivery
		if ($user->country) {
			$country = CbcheckoutCountryHelper::getCountry($user->country);
			$user->country_2_code = $country->country_2_code;
			$user->country_3_code = $country->country_3_code;
			$user->countryname   = $country->country_name;
		}
		else {
			$user->country_2_code = '';
			$user->country_3_code = '';
			$user->countryname   = '';
		}
	
		// Country names for billing
		if ($user->billingcountry) {
			$country = CbcheckoutCountryHelper::getCountry($user->billingcountry);
			$user->billingcountry_2_code = $country->country_2_code;
			$user->billingcountry_3_code = $country->country_3_code;
			$user->billingcountryname   = $country->country_name;
		}
		else {
			$user->billingcountry_2_code = '';
			$user->billingcountry_3_code = '';
			$user->billingcountryname   = '';
		}
	
		// State names for delivery
		if ($user->state) {
			$state = CbcheckoutCountryHelper::getState($user->state);
			$user->statecode = $state->iso_code;
			$user->statefips = $state->fips_number;
			$user->statename = $state->name;
		}
		else {
			$user->statecode = '';
			$user->statename = '';
		}
	
		// State names for billing
		if ($user->billingstate) {
			$state = CbcheckoutCountryHelper::getState($user->billingstate);
			$user->billingstatecode = $state->iso_code;
			$user->billingstatefips = $state->fips_number;
			$user->billingstatename = $state->name;
		}
		else {
			$user->billingstatecode = '';
			$user->billingstatefips = '';
			$user->billingstatename = '';
		}
		
		if ($user->salutation_id) {
			$user->salutation = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 55, $user->salutation_id);
		}
		else {
			$user->salutation = '';
		}
		
		if ($user->billingsalutation_id) {
			$user->billingsalutation = ConfigboxCacheHelper::getTranslation('#__cbcheckout_strings', 55, $user->billingsalutation_id);
		}
		else {
			$user->billingsalutation = '';
		}
		
		if ($user->billingcounty_id) {
			$user->billingcounty = CbcheckoutCountryHelper::getCountyName($user->billingcounty_id);
		}
		else {
			$user->billingcounty = '';
		}
		
		if ($user->county_id) {
			$user->county = CbcheckoutCountryHelper::getCountyName($user->county_id);
		}
		else {
			$user->county = '';
		}
		
	}
	
	static function authenticateUser($credentials) {
		
		$username = $credentials['username'];
		$password = $credentials['password'];
		
		if (empty($username) || empty($password)) {
			self::$error = KText::_('No username/password entered.');
			return false;
		}
		
		$db = KenedoPlatform::getDb();
		$query = "
			SELECT `id`, `password`, `joomlaid`
			FROM #__cbcheckout_users 
			WHERE `billingemail` = '".$db->getEscaped($username)."' AND `joomlaid` != 0	
			LIMIT 1";
		
		$db->setQuery($query);
		$row = $db->loadAssoc();
		
		$passwordEncoded = KenedoPlatform::p()->getUserPasswordEncoded($row['joomlaid']);
		
		$userid = $row['id'];
		
		if (!$passwordEncoded || !$userid) {
			self::$error = KText::_('Username or password incorrect.');
			return false;
		}
		
		$succ = KenedoPlatform::p()->passwordsMatch($password,$passwordEncoded);
		
		if ($succ) {
			return true;
		}
		else {
			self::$error = KText::_('Username or password incorrect.');
			return false;
		}
	}
	
	static function getPassword($length) {
		
		$salt = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$len = strlen($salt);
		$makepass = '';
		
		$stat = @stat(__FILE__);
		if(empty($stat) || !is_array($stat)) $stat = array(php_uname());
		
		mt_srand(crc32(microtime() . implode('|', $stat)));
		
		for ($i = 0; $i < $length; $i ++) {
			$makepass .= $salt[mt_rand(0, $len -1)];
		}
		
		return $makepass;
		
	}
	
	static function registerPlatformUser($cbUser) {
		
		self::$error = null;
		
		$passwordClear = self::getPassword(8);
		
		$user = new StdClass();
		$user->email 		= $cbUser->billingemail;
		$user->username 	= $cbUser->billingemail;
		$user->password 	= $passwordClear;
		$user->password2 	= $passwordClear;
		$user->name 		= $cbUser->billingfirstname . ' ' . $cbUser->billinglastname;
		
		$groups				= array(CbcheckoutUserHelper::getPlatformUserGroupId($cbUser->group_id));
		
		// The session value notifies the onAfterStoreUser method not to create a new customer record
		// That is because the method looks for a joomlaid field to decide. This cannot be set before actually having a user registered. So..
		
		KSession::set('noUserSetup',true);
		$newUser 			= KenedoPlatform::p()->registerUser($user,$groups);
		KSession::delete('noUserSetup');
		
		if ($newUser == false) {
			self::$error = KenedoPlatform::p()->getError();
			return false;
		}
		
		$registrationData = new stdClass();
		
		$registrationData->platformUserId = $newUser->id;
		$registrationData->username = $newUser->username;
		$registrationData->passwordClear = $passwordClear;
		$registrationData->passwordEncrypted = $newUser->password;
		
		return $registrationData;
		
	}
	
	static function sendRegistrationEmail( $user, $passwordClear ) {
		
		// Load the registration view content
		$registrationView = KenedoView::getView('CbcheckoutViewEmailcustomerregistration',KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'emailcustomerregistration'.DS.'view.html.php');
		$registrationView->assign('passwordClear', $passwordClear);
		ob_start();
		$registrationView->display();
		$registrationHtml = ob_get_clean();
		
		// Load the general email template and put the registration view content in it
		$emailView = KenedoView::getView('CbcheckoutViewEmailtemplate',KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'emailtemplate'.DS.'view.html.php');
		$emailView->assign('emailContent',$registrationHtml);
		ob_start();
		$emailView->display();
		$registrationHtml = ob_get_clean();
		
		// Get the shop data for email data
		$shopModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		$shopData = $shopModel->getItem();
		
		// Prepare the email data
		$email = new stdClass();
		$email->senderEmail = $shopData->shopemailsupport;
		$email->senderName	= $shopData->shopname;
		$email->receipient = $user->billingemail;
		$email->subject = KText::sprintf('EMAIL_CUSTOMER_REGISTRATION_SUBJECT',$shopData->shopname);
		$email->body = $registrationHtml;
		$email->mode = 1;
		
		// Send the email
		$sendSuccess = KenedoPlatform::p()->sendEmail($email->senderEmail, $email->senderName, $email->receipient, $email->subject, $email->body, $email->mode);
		
		if (!$sendSuccess) {
			self::$error = KText::_('Registration email could not get sent.');
			KLog::log('Registration email could not get sent to "'.$email->senderEmail.'".','warning');
			return false;
		}
	}
	
	static function loginUser($username, $password) {
		$response = KenedoPlatform::p()->login($username, $password, array());
		return $response;
	}
	
	static function onLoginUser($platformUser) {
				
		$isAdminArea = KenedoPlatform::p()->isAdminArea();
		
		if ($isAdminArea) {
			return true;
		}
		
		// Remember old user id
		$oldUserId = KSession::get('user_id', 0, 'com_configbox');
		
		$platformUserId = KenedoPlatform::p()->getUserIdByUsername($platformUser['username']);
		
		// Search for existing cbcheckout users
		$db = KenedoPlatform::getDb();
		$query = "SELECT `id` FROM #__cbcheckout_users WHERE `joomlaid` = ".(int)$platformUserId ." LIMIT 1";
		$db->setQuery($query);
		$userId = $db->loadResult();
		
		// Auto-create a non cbcheckout user in joomla
		if (!$userId) {
			
			$nameParts = explode(' ',$platformUser['fullname']);
			
			if (count($nameParts) > 1) {
				$lastName = array_pop($nameParts);
				$firstName = implode(' ',$nameParts);
			}
			else {
				$firstName = 'No first name';
				$lastName = $platformUser['fullname'];
			}
			
			$user->billingfirstname = $firstName;
			$user->firstname = $firstName;
			
			$user->billinglastname = $lastName;
			$user->lastname = $lastName;
			
			// Get the Joomla user id
			$user->joomlaid = $platformUserId;
			
			// Get the Joomla user email address
			$user->billingemail = $platformUser['email'];
			$user->email = $platformUser['email'];
			$user->password = KenedoPlatform::p()->getUserPasswordEncoded($platformUserId);
						
			// Set the user to non temporary since he is a registered platform user
			$user->is_temporary = 0;
			
			// Create user
			$userId = self::createNewUser($user);
			
		}
		
		// Set session var
		KSession::set('user_id',$userId,'com_configbox');
		
		// If user id changed, update old user's records
		if ($oldUserId != 0) {
			self::moveUserOrders($oldUserId,$userId);
		}
		
		return true;
	}
	
	static function getUserFieldTranslations() {
		return array(
			'companyname'=>KText::_('Delivery Company Name'),
			'salutation_id'=>KText::_('Delivery Salutation'),
			'firstname'=>KText::_('Delivery First Name'),
			'lastname'=>KText::_('Delivery Last Name'),
			'address1'=>KText::_('Delivery Address 1'),
			'address2'=>KText::_('Delivery Address 2'),
			'zipcode'=>KText::_('Delivery ZIP Code'),
			'city'=>KText::_('Delivery City'),
			'city_id'=>KText::_('Delivery City'),
			'country'=>KText::_('Delivery Country'),
			'state'=>KText::_('Delivery State'),
			'email'=>KText::_('Delivery Email'),
			'phone'=>KText::_('Delivery Phone'),
			'language_tag'=>KText::_('Preferred Language'),
				
			'vatin'=>KText::_('VAT IN'),
		
			'billingcompanyname'=>KText::_('Billing Company Name'),
			'billingsalutation_id'=>KText::_('Billing Salutation'),
			'billingfirstname'=>KText::_('Billing First Name'),
			'billinglastname'=>KText::_('Billing Last Name'),
			'billingaddress1'=>KText::_('Billing Address 1'),
			'billingaddress2'=>KText::_('Billing Address 2'),
			'billingzipcode'=>KText::_('Billing ZIP Code'),
			'billingcity'=>KText::_('Billing City'),
			'billingcity_id'=>KText::_('Billing City'),
			'billingcountry'=>KText::_('Billing Country'),
			'billingstate'=>KText::_('Billing State'),
			'billingemail'=>KText::_('Billing Email'),
			'billingphone'=>KText::_('Billing Phone'),

			'samedelivery'=>KText::_('Use billing address for shipping'),
			'newsletter'=>KText::_('Newsletter'),
				
			'billingcounty_id'=>KText::_('Billing County'),
			'county_id'=>KText::_('Delivery County'),
		
		);
	}
	
	static function initUserRecord() {
		
		$info = new StdClass();
		
		$db = KenedoPlatform::getDb();
		
		$userColumns = $db->getTableFields('#__cbcheckout_users',false);
		$userColumns = $userColumns['#__cbcheckout_users'];
		
		foreach ($userColumns as $colName=>$column) {
			$type = strtolower($column->Type);

			if (strstr($type,'int(') || strstr($type,'boolean(') || strstr($type,'float') || strstr($type,'decimal')) {
				$info->$colName = 0;
			}
			else {
				$info->$colName = '';
			}
		}
		
		if (!defined('CONFIGBOX_VERSION')) {
			include_once(KPATH_ROOT.DS.'components'.DS.'com_configbox'.DS.'helpers'.DS.'common.php');
			ConfigboxConfigHelper::setConfig();
		}
			
		if (defined('CONFIGBOX_DEFAULT_USER_GROUP_ID') && CONFIGBOX_DEFAULT_USER_GROUP_ID) {
			$info->group_id = CONFIGBOX_DEFAULT_USER_GROUP_ID;
		}
		
		if (defined('CONFIGBOX_ENABLE_GEOLOCATION') && CONFIGBOX_ENABLE_GEOLOCATION && class_exists('ConfigboxLocationHelper')) {
			$locationData = ConfigboxLocationHelper::getLocationByIp();

			if ($locationData) {

				if ($locationData->city) {
					$info->city = $locationData->city;
					$info->billingcity = $locationData->city;
				}

				if ($locationData->zipcode) {
					$info->zipcode = $locationData->zipcode;
					$info->billingzipcode = $locationData->zipcode;
				}

				if ($locationData->countryCode) {
					$id = CbcheckoutCountryHelper::getCountryIdByCountry2Code( $locationData->countryCode );
					$info->country = $id;
					$info->billingcountry = $id;
				}

				if ($locationData->stateFips) {
					$id = CbcheckoutCountryHelper::getStateIdByFipsNumber( $info->country, $locationData->stateFips );
					$info->state = $id;
					$info->billingstate = $id;
				}

			}
			
		}
		
		if (defined('CONFIGBOX_DEFAULT_COUNTRY_ID') && CONFIGBOX_DEFAULT_COUNTRY_ID) {
			
			if (!$info->country) {
				$info->country = CONFIGBOX_DEFAULT_COUNTRY_ID;
			}
			
			if (!$info->billingcountry) {
				$info->billingcountry = CONFIGBOX_DEFAULT_COUNTRY_ID;
			}
		}
		
		if (class_exists('KenedoPlatform')) {
			$info->language_tag = KenedoPlatform::p()->getLanguageTag();
		}
		
		if (defined('CONFIGBOX_DEFAULT_USER_GROUP_ID') && CONFIGBOX_DEFAULT_USER_GROUP_ID) {
			$info->group_id = CONFIGBOX_DEFAULT_USER_GROUP_ID;
		}
		
		
		if (defined('CONFIGBOX_NEWSLETTER_PRESET') && CONFIGBOX_NEWSLETTER_PRESET) {
			$info->newsletter = CONFIGBOX_NEWSLETTER_PRESET;
		}
		
		
		if (defined('CONFIGBOX_ALTERNATE_SHIPPING_PRESET') && CONFIGBOX_ALTERNATE_SHIPPING_PRESET) {
			$info->samedelivery = 0;
		}
		else {
			$info->samedelivery = 1;
		}
				
		return $info;
		
	}
	
	static function createNewUser($user) {
		
		$db = KenedoPlatform::getDb();
		
		$userBlueprint = self::initUserRecord();
		
		foreach ($userBlueprint as $key=>$value) {
			if (!isset($user->$key)) {
				$user->$key = $value;
			}
		}
		
		if (empty($user->created)) {
			$user->created = KenedoTimeHelper::getFormattedOnly('NOW','datetime');
		}
		
		if (empty($user->id)) {
		
			$query = "SELECT `id` FROM `#__cbcheckout_users` ORDER BY `id` DESC LIMIT 1";
			$db->setQuery($query);
			$maxCheckoutId = $db->loadResult();
				
			$query = "SELECT `id` FROM #__configbox_users ORDER BY `id` DESC LIMIT 1";
			$db->setQuery($query);
			$maxConfigboxId = $db->loadResult();
				
			$user->id = max($maxConfigboxId,$maxCheckoutId) + 1;
		
		}
		
		$succ = $db->insertObject('#__cbcheckout_users',$user,'id');
		
		if (!$succ) {
			if (class_exists('KLog')) {
				KLog::log('Error when creating new user. SQL error: "'.$db->getErrorMsg().'".','fatal',KText::_("A system error occured."));
			}
			return false;
		}
		else return $user->id;
		
	}
	
	static function moveUserOrders($oldUserId, $newUserId) {
		
		if (!$oldUserId || !$newUserId) {
			return false; 
		}
		
		if ($oldUserId == $newUserId) {
			return true;
		}
		
		$db = KenedoPlatform::getDb();
		
		$query = "UPDATE `#__cbcheckout_order_records` SET `user_id` = ".(int)$newUserId." WHERE `user_id` = ".(int)$oldUserId;
		$db->setQuery($query);
		$db->query();
		
		$query = "UPDATE `#__configbox_grandorders` SET `user_id` = ".(int)$newUserId." WHERE `user_id` = ".(int)$oldUserId;
		$db->setQuery($query);
		$db->query();
		
	}
	
	static function logoutUser($platformUser) {
		
		$isAdminArea = KenedoPlatform::p()->isAdminArea();
		
		if ($isAdminArea) {
			return true;
		}
		
		KSession::delete('user_id','com_configbox');
		KSession::delete('order_id','com_cbcheckout');
		KSession::delete('grandorder_id','com_configbox');
		KSession::terminateSession();
	}
	
	static function deleteUser($platformUser) {
		$platformUser = (array)$platformUser;
		$query = "DELETE FROM #__cbcheckout_users WHERE `joomlaid` = ".intval($platformUser['id'])." LIMIT 1";
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$db->query();
	}
	
	static function isVatFree($userRecord) {
		
		$deliveryCountry = CbcheckoutCountryHelper::getCountry($userRecord->country);
		$billingCountry = CbcheckoutCountryHelper::getCountry($userRecord->billingcountry);
		
		$isVatFree = false;
		
		if ($billingCountry && $deliveryCountry) {
				
			// If both countries are vat free - OK
			if ($billingCountry->vat_free && $deliveryCountry->vat_free) {
				$isVatFree = true;
			}
				
			// If both countries are vat free with VAT IN and VAT IN is present - OK
			if ($billingCountry->vat_free_with_vatin && $deliveryCountry->vat_free_with_vatin && $userRecord->vatin) {
				$isVatFree = true;
			}
			// If billing country is vat free with VAT IN and delivery country vat free and VAT IN - OK
			if ($billingCountry->vat_free_with_vatin && $deliveryCountry->vat_free && $userRecord->vatin) {
				$isVatFree = true;
			}
				
		}
		elseif ($billingCountry) {
				
			if ($billingCountry->vat_free) {
				$isVatFree = true;
			}
				
			if ($billingCountry->vat_free_with_vatin && $userRecord->vatin) {
				$isVatFree = true;
			}
				
		}
		
		return $isVatFree;
	}
	
	static function getTaxRate($taxClassId, &$orderAddress, $checkVatFree = true) {
		
		// If this is VAT free, tax rate is zero in any case
		if ($checkVatFree && self::isVatFree($orderAddress)) {
			return 0;
		}
		
		
		if (!isset(self::$taxRates[$taxClassId][$orderAddress->country][$orderAddress->state][$orderAddress->county_id][$orderAddress->city_id])) {
			
			$db = KenedoPlatform::getDb();
			$query = "
			SELECT *
			FROM #__configbox_tax_classes AS tcr
			LEFT JOIN #__cbcheckout_tax_class_rates AS tc ON tc.tax_class_id = ".(int)$taxClassId." AND ( (tc.state_id != 0 AND tc.state_id = ".(int)$orderAddress->state.") OR (tc.country_id != 0 AND tc.country_id = ".(int)$orderAddress->country."))
			WHERE tcr.id = ".(int)$taxClassId."
			ORDER BY tc.state_id DESC LIMIT 1
			";
			
			$db->setQuery($query);
			$taxRate = $db->loadAssoc();
			
			// If we got one, return it, else continue where we get to the default configbox taxrate
			if ($taxRate['tax_rate'] !== NULL) {
				$taxRate =  (float)$taxRate['tax_rate'];
			}
			else if ($taxRate['tax_rate'] === NULL && $taxRate['default_tax_rate'] !== NULL) {
				$taxRate = (float)$taxRate['default_tax_rate'];
			}
			else {
				KLog::log('Could not find tax rate for tax class id: '.$taxClassId, 'error', KText::_('Could not find tax rate for tax class id: "'.$taxClassId.'"'));
				return false;
			}
			
			self::$taxRates[$taxClassId][$orderAddress->country][$orderAddress->state][$orderAddress->county_id][$orderAddress->city_id] = $taxRate;
			
			// Add county and city tax
			$db = KenedoPlatform::getDb();
			$query = "
			SELECT tcr.*
			FROM #__configbox_tax_classes AS tc
			LEFT JOIN #__cbcheckout_tax_class_rates AS tcr ON tcr.tax_class_id = tc.id AND ( (tcr.county_id != 0 AND tcr.county_id = ".(int)$orderAddress->county_id.") OR (tcr.city_id != 0 AND tcr.city_id = ".(int)$orderAddress->city_id.") )
			WHERE tc.id = ".intval($taxClassId)."
			ORDER BY tcr.county_id DESC, tcr.city_id DESC
			";
			$db->setQuery($query);
			$taxRates = $db->loadAssocList();
			if ($taxRates) {
				foreach ($taxRates as $taxRate) {
					self::$taxRates[$taxClassId][$orderAddress->country][$orderAddress->state][$orderAddress->county_id][$orderAddress->city_id] += floatval($taxRate['tax_rate']);
				}
			}
			
		}
		
		return self::$taxRates[$taxClassId][$orderAddress->country][$orderAddress->state][$orderAddress->county_id][$orderAddress->city_id];

	}
	
	static function onAfterStoreUser($platformUser, $isNew, $success, $msg) {
		
		// This session variable is set in the registerPlatformUser method to prevent this method to set up a second user account.
		if (KSession::get('noUserSetup',false) == true) {
			return true;
		}
		
		$db = KenedoPlatform::getDb();
		
		// Search for existing cbcheckout users
		$query = "SELECT `id` FROM #__cbcheckout_users WHERE `joomlaid` = ".intval($platformUser['id']) ." LIMIT 1";
		$db->setQuery($query);
		$userId = $db->loadResult();
		
		// Create the customer account if there is none yet
		if (!$userId) {
			
			$user = new StdClass();
			
			$nameParts = explode(' ', $platformUser['name']);
				
			if (count($nameParts) > 1) {
				$lastName = array_pop($nameParts);
				$firstName = implode(' ',$nameParts);
			}
			else {
				$firstName = 'No first name';
				$lastName = $platformUser['name'];
			}
				
			$user->billingfirstname = $firstName;
			$user->firstname = $firstName;
				
			$user->billinglastname = $lastName;
			$user->lastname = $lastName;
				
			// Get the Joomla user id
			$user->joomlaid = $platformUser['id'];
				
			// Get the Joomla user email address
			$user->billingemail = $platformUser['email'];
			$user->email = $platformUser['email'];
				
			// Set the user to non temporary since he is a registered platform user
			$user->is_temporary = 0;

			$user->password = $platformUser['password'];
			
			// Create user
			$userId = self::createNewUser($user);
			
		}
		else {
			
			$nameParts = explode(' ', $platformUser['name']);
			
			if (count($nameParts) > 1) {
				$lastName = array_pop($nameParts);
				$firstName = implode(' ',$nameParts);
			}
			else {
				$firstName = 'No first name';
				$lastName = $platformUser['name'];
			}
			
			$query = "
			UPDATE `#__cbcheckout_users` SET
			`billingfirstname` = '".$db->getEscaped($firstName)."',
			`billinglastname` = '".$db->getEscaped($lastName)."',
			`billingemail` = '".$db->getEscaped($platformUser['email'])."',
			`password` = '".$db->getEscaped($platformUser['password'])."'
			WHERE `id` = ".intval($userId);
			$db->setQuery($query);
			$success = $db->query();
			
		}
		
		if ($userId) {
			self::resetUserCache($userId);
			$customer = self::getUser($userId);
			KenedoObserver::triggerEvent('onConfigBoxUpdateUserInfo' , array($customer) );
		}
		
		
		return true;
		
	}
	
	static function onAuthenticate( $credentials, $options, &$response ) {
		
		$success = CbcheckoutUserHelper::authenticateUser($credentials);
		
		if (!$success) {
			$response->status = (defined('JAUTHENTICATE_STATUS_FAILURE')) ? JAUTHENTICATE_STATUS_FAILURE : JAuthentication::STATUS_FAILURE;
			$response->error_message	= '';
			return false;
		}
		
		$response->status 			= (defined('JAUTHENTICATE_STATUS_SUCCESS')) ? JAUTHENTICATE_STATUS_SUCCESS : JAuthentication::STATUS_FAILURE;
		$response->error_message	= '';
		
		$db = KenedoPlatform::getDb();
		$query = "SELECT * FROM `#__cbcheckout_users` WHERE `billingemail` = '".$db->getEscaped( $credentials['username'] )."' AND `joomlaid` != 0 LIMIT 1";
		$db->setQuery($query);
		$user = $db->loadObject();
		
		$response->email = $user->billingemail;
		$response->fullname = $user->billingfirstname . ' ' . $user->billinglastname;
		$response->username = 'dummy';
		
		if (!empty($user->joomlaid)) {
			$query = "SELECT `username` FROM #__users WHERE `id` = ".intval($user->joomlaid);
			$db->setQuery($query);
			$username = $db->loadResult();
			if ($username) {
				$response->username = $username;
			}
		}
		
		return true;
		
	}
	
	static function checkVatIn($vatIn, $orderAddress) {
		
		/*
		 * Store the default socket time out to change it back later (it is lowered to prevent
		 * unnecessary long overall processing timings).
		 */
				
		$originalSocketTimeout = ini_get("default_socket_timeout");
		
		// Bounce if the SOAP extension isn't installed on the server
		if (class_exists('SoapClient') == false) {
			KLog::log('The server does not support SOAP. VAT IN check cannot be done.','warning');
			return true;
		}
		
		self::$error = '';
		
		try {
			$vies = new SoapClient(self::$viesWsdl, array("connection_timeout" => 3));
		} 
		catch (Exception $e) {
			//KenedoPlatform::p()->sendMessage(KText::_('The VAT-IN validation service is currently unavailable. Please try again later.'));
			KLog::log('The VAT-IN validation service was not available.','warning');
			return true;
		}
		
		if (!is_object($vies)) {
			KLog::log('The VAT-IN validation service was not available.','warning');
			return true;
		}
		
		// Get the billing country
		$country = CbcheckoutCountryHelper::getCountry($orderAddress->billingcountry);
		
		// Prepare the params for the SOAP request
		$param = new StdClass();
		$param->countryCode = $country->country_2_code;
		
		// Normalize the provided VAT IN
		$vatIn = str_replace(' ', '', $vatIn);
		
		if (stripos($vatIn,$country->country_2_code) === 0) {
			$vatIn = substr($vatIn,2);
		}
	
		if (strpos($vatIn,'-') === 0) {
			$vatIn = substr($vatIn,1);
		}
	
		$param->vatNumber = $vatIn;
		
		try {
			// Do the request, set the timeout to 3 seconds to prevent long processing time
			ini_set("default_socket_timeout", 3);
			$response = $vies->checkVat($param);
		}
		catch (SoapFault $e) {
			$ret = $e->faultstring;
			
			$faults = array (
					'INVALID_INPUT'       => KText::_('The provided country code is invalid or the VAT number is empty'),
					'SERVICE_UNAVAILABLE' => KText::_('The SOAP service is unavailable, try again later'),
					'MS_UNAVAILABLE'      => KText::_('The Member State service is unavailable, try again later or with another Member State'),
					'TIMEOUT'             => KText::_('The Member State service could not be reached in time, try again later or with another Member State'),
					'SERVER_BUSY'         => KText::_('The service cannot process your request. Try again later.'),
			);
			
			KLog::log('The VAT IN cannot be checked because of this error "'.$e->faultstring.'". "'.$faults[$ret].'". VAT IN was "'.$vatIn.'", Country code was "'.$country->country_2_code.'".','warning');
			ini_set("default_socket_timeout", $originalSocketTimeout);
			return true;
			
		}
		
		// Reset the socket timeout to the regular value
		ini_set("default_socket_timeout", $originalSocketTimeout);
		
		return $response->valid;
	}
	
}