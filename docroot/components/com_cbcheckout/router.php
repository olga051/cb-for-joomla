<?php
defined('_JEXEC') or die();

if (!defined('CB_VALID_ENTRY')) {
	define('CB_VALID_ENTRY',true);
}

if (!defined('DS')) {
	define('DS',DIRECTORY_SEPARATOR);
}

// Init Kenedo framework
if (is_file(JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'external'.DS.'kenedo'.DS.'init.php')) {
	require_once(JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'external'.DS.'kenedo'.DS.'init.php');
	require_once(JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'helpers'.DS.'cache.php');
}

function CbcheckoutBuildRoute( &$query ) {
	
	if (!is_file(JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'external'.DS.'kenedo'.DS.'init.php')) {
		return array();
	}
		
	$segments = array();
	
	// For IPN controller tasks
	if (!empty($query['controller']) && $query['controller'] == 'ipn') {
		
		$segments[] = 'ipn';
		
		if ($query['task'] == 'processipn') {
			$segments[] = 'processipn';
			$segments[] = $query['connector_name'];
			
			unset($query['connector_name']);
		}
		else {
			KLog::log('Unkown task for IPN controller detected. Task was "'.$query['task'].'"','error');
			return array();
		}
		
		unset($query['controller'], $query['task'], $query['system'], $query['Itemid']);
		
		return $segments;
		
	}
	
	if (!empty($query['controller']) && $query['controller'] != 'adminorders') {
		if (isset($query['Itemid'])) {
			unset($query['Itemid']);
		}
		return array();
	}
	
	if (!empty($query['view']) && $query['view'] == 'confirmation') {
		if (isset($query['Itemid'])) {
			unset($query['Itemid']);
		}
	}
	
	if (!empty($query['view']) && $query['view'] == 'userorder') {
		$link = 'index.php?option=com_cbcheckout&view=user';
		$id = KenedoRouterHelper::getItemIdByLink($link);
		if ($id) {
			$query['Itemid'] = $id;
			$segments[] = 'order';
			$segments[] = $query['order_id'];
			unset($query['order_id'], $query['view']);
			return $segments;
		}	
	}
	
	// See if we find a menu item matching the query
	
	// Build a link from the query array (make sure option is first, as it is normally in menu item links)
	$link = 'index.php?option=com_cbcheckout';
	$queryCopy = $query;
	unset($queryCopy['option'], $queryCopy['Itemid']);
	
	foreach($queryCopy as $key=>$value) {
		if (!is_array($value)) {
			$link .= '&'.$key.'='.$value;
		}
	}
	
	// Search for menu item with a corresponding link (take first best)	
	$id = KenedoRouterHelper::getItemIdByLink($link);
	
	// If we found a menu item, recreate the $query array with option and ItemId and let the app handle the rest
	if ($id) {
		$query = array('option'=>'com_cbcheckout', 'Itemid'=>$id);
		return array();
	}
	
	// For edit order routes
	if (isset($query['controller']) && $query['controller'] == 'adminorders' && isset($query['cid'])) {
		$segments[] = $query['cid'][0];
		unset($query['cid'],$query['task'],$query['controller']);
		return $segments;
	}
	
	// For all other routes
	if (isset($query['view'])) {
		$segments[] = $query['view'];
		unset($query['view']);
		return $segments;
	}
	
	return array();
	
}

function CbcheckoutParseRoute( $segments ) {
	
	if (!is_file(JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'external'.DS.'kenedo'.DS.'init.php')) {
		return array();
	}
	
	$vars = array();
	
	$vars['option'] = 'com_cbcheckout';
	
	if ($segments[0] == 'order') {
		$link = 'index.php?option=com_cbcheckout&view=user';
		$id = KenedoRouterHelper::getItemIdByLink($link);
		if ($id) {
			$vars['Itemid'] = $id;
			$vars['view'] = 'userorder';
			$vars['order_id'] = $segments[1];
			return $vars;
		}
	}
	
	// For IPN controller routes
	if ($segments[0] == 'ipn') {
		$vars['controller'] = 'ipn';
		$vars['task'] = $segments[1];
		$vars['connector_name'] = $segments[2];
		return $vars;
	}
	
	// For edit order routes
	if (is_numeric($segments[0])) {
		$vars['controller'] = 'adminorders';
		$vars['task'] = 'edit';
		$vars['cid'][0] = $segments[0];
	}
	// For all other routes
	else {
		$vars['view'] = $segments[0];
	}
	
	return $vars;
}
