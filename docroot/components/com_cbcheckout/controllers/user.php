<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerUser extends KenedoController {
	
	public $controller	 	= 'user';
	public $model		 	= 'user';
	public $listingView 	= 'user';
	public $detailsView 	= 'user';
	
	function removeOrder() {
		
		// Get and sanitize record ids
		$cids = KRequest::getArray('cid');
		foreach ($cids as &$cid) {
			$cid = (int)$cid;
		}
		
		// Bounce if no cids where found
		if (count($cids) == 0) {
			$this->setError(KText::_('No orders chosen for removal.'));
			return false;
		}
		
		// Get the store ID
		$storeId = ConfigboxStoreHelper::getStoreId();
		
		// Find the records that the user should be able to reach
		$db = KenedoPlatform::getDb();
		$query = "SELECT `id`, `user_id`, `status` FROM `#__cbcheckout_order_records` WHERE `id` IN (".implode(',',$cids).")";
		if ($storeId != 1) {
			$query .= " AND store_id = ".(int)$storeId;
		}
		$db->setQuery($query);
		$removalItems = $db->loadObjectList('id');
		
		// Bounce if nothing is there
		if (!$removalItems) {
			$this->setError(KText::_('No orders chosen for removal.'));
			return false;
		}
		
		// Get the user's id
		$userId = CbcheckoutUserHelper::getUserId();
		
		// Loop through the orders and see if the user can actually remove them
		foreach ($removalItems as $item) {
			if ($item->user_id != $userId) {
				$platformUserId = KenedoPlatform::p()->getUserId();
				if (ConfigboxPermissionHelper::canEditOrders($platformUserId) == false) {
					$this->setError(KText::_('You cannot remove orders of other customers.'));
					return false;
				}
			}
			if (ConfigboxOrderHelper::isPermittedAction('removeOrder',$item) == false) {
				$platformUserId = KenedoPlatform::p()->getUserId();
				if (ConfigboxPermissionHelper::canEditOrders($platformUserId) == false) {
					$this->setError(KText::_('You cannot remove order %s because if its status.',$item->id));
					return false;
				}
			}
				
		}
		
		// Get the ids of the order records
		$orderIds = array_keys($removalItems);
		
		// Get the model and try to remove them
		$model = KenedoModel::getModel('CbcheckoutModelAdminorders');
		$success = $model->remove($orderIds);
		
		// Send a message if removal did not work
		if ($success == false) {
			$errors = $model->getErrors();
			foreach ($errors as $error) {
				KenedoPlatform::p()->sendSystemMessage($error);
			}
		}
		else {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Order removed.'));
		}
		
		// Redirect to the customer account page
		$this->setRedirect(KLink::getRoute('index.php?option=com_cbcheckout&view=user',false));
		$this->redirect();
	}
	
	function saveUser() {
		
		$type = KRequest::getKeyword('type');
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
	
		$success = CbcheckoutUserHelper::saveUser(NULL, $orderId, $type);
		
		if ($success) {
			
			if (KRequest::getInt('success_message')) {
				KenedoPlatform::p()->sendSystemMessage( KText::_('Your data has been saved.') );
			}
			
			// Get the success redirection URL
			$url = urldecode(KRequest::getString('return_success'));
		}
		else {
			// Send a message
			KenedoPlatform::p()->sendSystemMessage( CbcheckoutUserHelper::$error );

			KSession::set('userFormState', $_POST);

			// Get the failure redirection URL
			$url = urldecode(KRequest::getString('return_failure'));
		}
		
		$this->setRedirect( $url );
		$this->redirect();
		
	}
	
	function loginUser() {
		
		// Store the old user id for later moving of orders and grandorders
		$oldUserId = CbcheckoutUserHelper::getUserId();
		
		$username = KRequest::getString('username','');
		$password = KRequest::getString('password','');
		
		// Either email or username can be used
		if (!$username) {
			$username = KRequest::getString('email','');
		}
			
		// Login the user
		$response = CbcheckoutUserHelper::loginUser($username, $password);
		
		if ($response == true) {
			
			// Set the order address as well
			$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
			$orderId = $orderModel->getId();
			if ($orderId) {
				CbcheckoutUserHelper::setOrderAddress($orderId);
			}
					
			// Send a message
			KenedoPlatform::p()->sendSystemMessage(KText::_('You have been successfully logged in.'));
			
			// Get the new user id
			$newUserId = CbcheckoutUserHelper::getUserId();
			
			// Move the oders
			CbcheckoutUserHelper::moveUserOrders($oldUserId, $newUserId);
			
			// Get the success redirection URL
			$url = urldecode(KRequest::getString('return_success'));
				
		}
		else {
			
			// Get the failure redirection URL
			$url = urldecode(KRequest::getString('return_failure'));
			
		}
		
		if (KRequest::getString('format') == 'json') {
			$jsonResponse = new stdClass();
			$jsonResponse->success = $response;
			if ($response == false) {
				$jsonResponse->errorMessage = KText::_('Login failed. Please check your email address and password.');
			}
			echo json_encode($jsonResponse);
		}
		else {
			
			// Redirect
			$this->setRedirect($url);
			$this->redirect();
			
		}
	
	
	}
	
	function registerUser() {
		
		// Store the old user id for later moving of orders and grandorders
		$oldUserId = CbcheckoutUserHelper::getUserId();
		
		// Register the user
		$userModel = KenedoModel::getModel('CbcheckoutModelUser');
		$success = $userModel->registerUser();
		
		if ($success) {
			
			// Login the user
			$success = $userModel->loginUser();
			
			if ($success) {
				
				// Set the order address as well
				$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
				$orderId = $orderModel->getId();
				if ($orderId) {
					CbcheckoutUserHelper::setOrderAddress($orderId);
				}
				
				// Get the new user id
				$newUserId = CbcheckoutUserHelper::getUserId();
				
				// Move the oders
				CbcheckoutUserHelper::moveUserOrders($oldUserId, $newUserId);
				
				// Send a message
				KenedoPlatform::p()->sendSystemMessage(KText::_('Thank you for registering. You are now registered and logged in.'));
				$this->setRedirect(KLink::getRoute('index.php?option=com_cbcheckout&view=user', false, CONFIGBOX_SECURECHECKOUT));
				$this->redirect();
				
			}
			else {
				KenedoPlatform::p()->sendSystemMessage(KText::_('Could not login after registration.'));
				$errors = $userModel->getErrors();
				foreach ($errors as $error) {
					KenedoPlatform::p()->sendSystemMessage($error);
				}
				$this->setRedirect(KLink::getRoute('index.php?option=com_cbcheckout&view=user&layout=login', false, CONFIGBOX_SECURECHECKOUT));
				$this->redirect();
			}
			
		}
		else {
			$errors = $userModel->getErrors();
			foreach ($errors as $error) {
				KenedoPlatform::p()->sendSystemMessage($error);
			}
			$this->setRedirect(KLink::getRoute('index.php?option=com_cbcheckout&view=user&layout=register',false));
			$this->redirect();
			return false;
			
		}
		
	}
	
	function sendPasswordChangeVerificationCode() {
		
		$emailAddress = KRequest::getString('email','');
		$verificationCode = CbcheckoutUserHelper::getPassword(16);
		
		$userId = CbcheckoutUserHelper::getUserIdByEmail($emailAddress);
		
		if (!$userId) {
			$jsonResponse = new stdClass();
			$jsonResponse->success = false;
			$jsonResponse->errorMessage = KText::_('There is no customer account with that email address.');
			echo json_encode($jsonResponse);
			return;
		}
		
		KSession::set('passwordChangeUserId', $userId);
		KSession::set('passwordChangeVerificationCode', $verificationCode);
		
		$shopData = KenedoModel::getModel('CbcheckoutModelAdminshopdata')->getItem();
		
		$emailSubject = KText::sprintf('PASSWORD_CHANGE_VERIFICATION_CODE_EMAIL_SUBJECT', $shopData->shopname);
		$emailBody = KText::sprintf('PASSWORD_CHANGE_VERIFICATION_CODE_EMAIL_TEXT', $verificationCode);
		
		$emailView = KenedoView::getView('CbcheckoutViewEmailtemplate',KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'emailtemplate'.DS.'view.html.php');
		$emailView->assign('emailContent', $emailBody);
		ob_start();
		$emailView->display();
		$emailBody = ob_get_clean();
		
		$email = new stdClass();
		$email->toEmail		= $emailAddress;
		$email->fromEmail	= $shopData->shopemailsales;
		$email->fromName	= $shopData->shopname;
		$email->subject		= $emailSubject;
		$email->body 		= $emailBody;
		$email->attachments	= array();
		$email->cc			= NULL;
		$email->bcc			= NULL;
		
		$dispatchResponse = KenedoPlatform::p()->sendEmail($email->fromEmail, $email->fromName, $email->toEmail, $email->subject, $email->body, true, $email->cc, $email->bcc, $email->attachments);
		
		$jsonResponse = new stdClass();
		
		if ($dispatchResponse !== true) {
			
			$jsonResponse->success = false;
			$jsonResponse->errorMessage = KText::_('We could not send you a verification code because the email dispatch failed. Please contact us to solve this issue.');
			
			// Log error message to errors
			KLog::log('Password change verification code email could not be sent. Recipient was "'.$email->toEmail.'". Sender email was "'.$email->fromEmail.'". Email body char count was "'.mb_strlen($email->body).'".', 'error');
		}
		else {
			$jsonResponse->success = true;
		}
		
		echo json_encode($jsonResponse);
		
	}
	
	function changePasswordWithCode() {
		
		// Prepare the response object
		$jsonResponse = new stdClass();
		$jsonResponse->errors = array();
		
		// Get the input
		$verificationCode = trim(KRequest::getString('code'));
		$password = trim(KRequest::getVar('password'));
		
		// Check if the verification code was supplied
		if ($verificationCode == '') {
			$jsonResponse->success = false;
			$jsonResponse->errors[] = array(
				'fieldName'=>'verification_code',
				'code'=>'no_code',
				'message'=>KText::_('Please enter the verification code you received by email.'),
			);
		}
		
		// Check if the new password was supplied
		if ($password == '') {
			$jsonResponse->success = false;
			$jsonResponse->errors[] = array(
				'fieldName'=>'new_password',
				'code'=>'no_password',
				'message'=>KText::_('Please enter a new password.'),
			);
		}
		// Check if the new password is ok
		elseif( KenedoPlatform::p()->passwordMeetsStandards($password) == false ) {
			$jsonResponse->success = false;
			$jsonResponse->errors[] = array(
					'fieldName'=>'new_password',
					'code'=>'bad_password',
					'message'=>KenedoPlatform::p()->getPasswordStandardsText(),
			);
		}
		
		// Check if the codes match
		if ($verificationCode != KSession::get('passwordChangeVerificationCode')) {
			$jsonResponse->success = false;
			
			$jsonResponse->errors[] = array(
					'fieldName'=>'verification_code',
					'code'=>'code_mismatch',
					'message'=>KText::_('The verification code is not correct. Please check and try again. You can also request a new code.'),
			);
			
		}
		
		// Bounce if problems occured so far
		if (count($jsonResponse->errors)) {
			echo json_encode($jsonResponse);
			return;
		}
		
		// Get the user ID from the verification info
		$userId = KSession::get('passwordChangeUserId');
		
		// Change the password
		$success = CbcheckoutUserHelper::changeUserPassword($userId, $password);
		
		// Do the final setup of the response
		if ($success == true) {
			
			if (KRequest::getInt('login')) {				
				$user = CbcheckoutUserHelper::getUser($userId, false, false);
				$response = CbcheckoutUserHelper::loginUser($user->billingemail, $password);
			}
			
			$jsonResponse->success = true;
			
			KSession::delete('passwordChangeUserId');
			KSession::delete('passwordChangeVerificationCode');
			
		}
		else {
			$jsonResponse->success = false;
			$jsonResponse->errors[] = array(
					'fieldName'=>'',
					'code'=>'change_password_failure',
					'message'=>CbcheckoutUserHelper::$error,
			);
		}
		
		// Do the response
		echo json_encode($jsonResponse);
		return;
		
	}
	
}
