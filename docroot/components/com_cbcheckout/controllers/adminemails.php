<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminemails extends KenedoController {
	
	public $controller	 	= 'adminemails';
	public $model		 	= 'adminemails';
	public $listingView 	= 'adminemails';
	public $detailsView 	= 'adminemail';
	
}
