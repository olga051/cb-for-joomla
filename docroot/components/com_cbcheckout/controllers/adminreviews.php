<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminreviews extends KenedoController {
	
	public $controller	 	= 'adminreviews';
	public $model		 	= 'adminreviews';
	public $listingView 	= 'adminreviews';
	public $detailsView 	= 'adminreview';
	
}
