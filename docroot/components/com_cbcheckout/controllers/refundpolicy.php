<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerRefundpolicy extends KenedoController {
	
	function display() {
		
		$shopmodel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		
		$view = $this->getView('Refundpolicy','html','CbcheckoutView');
		$view->setModel($shopmodel,true);
		$view->display();	
	}
	
}