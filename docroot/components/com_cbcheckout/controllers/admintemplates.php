<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminTemplates extends KenedoController {
	
	public $controller	 	= 'admintemplates';
	public $model		 	= 'admintemplates';
	public $listingView 	= 'admintemplates';
	public $detailsView 	= 'admintemplate';
	
}
