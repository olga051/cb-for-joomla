<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminshopdata extends KenedoController {
	
	public $controller	 	= 'adminshopdata';
	public $model		 	= 'adminshopdata';
	public $listingView 	= 'adminshopdata';
	public $detailsView 	= 'adminshopdata';
	
	function display() {
		$this->edit();
	}
}
