<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerCheckoutpspbridge extends KenedoController {
	
	function display() {
		
		KRequest::setVar('view','checkoutpspbridge');
		$view = $this->getView('checkoutpspbridge');
		
		$view->display();
	
	}
}