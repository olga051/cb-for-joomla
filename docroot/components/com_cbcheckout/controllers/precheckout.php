<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerPrecheckout extends KenedoController {
	
	function display() {
		
		KRequest::setVar('view','precheckout');
		$view = $this->getView('precheckout');
		
		$view->display();
	
	}
}