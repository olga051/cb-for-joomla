<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminsalutations extends KenedoController {
	
	public $controller	 	= 'adminsalutations';
	public $model		 	= 'adminsalutations';
	public $listingView 	= 'adminsalutations';
	public $detailsView 	= 'adminsalutation';
	
}
