<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminuserGroups extends KenedoController {
	
	public $controller	 	= 'adminusergroups';
	public $model		 	= 'adminusergroups';
	public $listingView 	= 'adminusergroups';
	public $detailsView 	= 'adminusergroup';
	
}
