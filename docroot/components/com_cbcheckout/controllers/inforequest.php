<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerInforequest extends KenedoController {
	
	public $controller	 	= 'user';
	public $model		 	= 'user';
	public $listingView 	= 'inforequest';
	public $detailsView 	= 'inforequest';

	/**
	 * This method is a bit of a multi-purpose one. It shows the RFQ/save order/assistance form normally.
	 * In case saving customer data was successful (ConfigboxControllerUser::saveUser takes care of that), then it takes care
	 * of handling the quotation/save order/assistance stuff.
	 */
	function display() {

		if (KRequest::getInt('success') != 1) {
			$this->getView('Inforequest', 'html')->display();
			return;
		}

		if (KRequest::getKeyword('type') == 'quotation') {

			$grandOrderId = KRequest::getInt('grandorder_id');
			$orderId = KRequest::getInt('configbox_order_id');

			$quoteModel = KenedoModel::getModel('CbcheckoutModelQuotation');

			// Create the order record (in the special way suitable for quotes)
			if (KRequest::getInt('from_grandorder')) {
				$orderRecordId = $quoteModel->createQuoteOrderFromCart($grandOrderId);
			}
			else {
				$orderRecordId = $quoteModel->createQuoteOrderFromCartPosition($grandOrderId, $orderId);
			}

			$user = CbcheckoutUserHelper::getUser(NULL, false, false);
			$group = CbcheckoutUserHelper::getGroupData($user->group_id);

			// Download plus email
			if ($group->quotation_download && $group->quotation_email) {

				// Prepare the quotation PDF
				$quotation = KenedoObserver::triggerEvent('onConfigBoxCreateQuotation', array($orderRecordId), true);

				// Set the status to trigger the email notification
				KenedoObserver::triggerEvent('onConfigBoxSetStatus', array($orderRecordId, 11));

				// Prepare the link and render the right view
				$link = KLink::getRoute('index.php?option=com_cbcheckout&view=quotation&order_record_id='.$quotation->order_id);
				$view = $this->getView('Inforequest', 'html');
				$view->assignRef('quotationDownloadUrl', $link);
				$view->renderView('quotation_download');

			}

			// Download, no email
			if ($group->quotation_download && !$group->quotation_email) {

				// Prepare the quotation PDF
				$quotation = KenedoObserver::triggerEvent('onConfigBoxCreateQuotation', array($orderRecordId), true);

				// Prepare the link and render the right view
				$link = KLink::getRoute('index.php?option=com_cbcheckout&view=quotation&order_record_id='.$quotation->order_id);
				$view = $this->getView('Inforequest', 'html');
				$view->assignRef('quotationDownloadUrl', $link);
				$view->renderView('quotation_download');
				return;
			}

			// No download, but email
			if (!$group->quotation_download && $group->quotation_email) {
				KenedoObserver::triggerEvent('onConfigBoxSetStatus', array($orderRecordId, 11));
				$view = $this->getView('Inforequest', 'html');
				$view->renderView('quotation_email');
				return;
			}

			// Neither download nor email
			if (!$group->quotation_download && !$group->quotation_email) {
				KenedoObserver::triggerEvent('onConfigBoxSetStatus', array($orderRecordId, 14));
				$view = $this->getView('Inforequest', 'html');
				$view->renderView('quotation_no_email_no_download');
				return;
			}

			return;

		}

		if (KRequest::getKeyword('type') == 'assistance') {

			$id = KRequest::getInt('grandorder_id');
			$link = KenedoObserver::triggerEvent('onConfigBoxGetAssistanceLink',array($id),true);
			ob_start();
			?>
			window.parent.cbj.colorbox.close();
			window.parent.location.href = '<?php echo $link->url;?>';
			<?php
			$js = ob_get_clean();
			KenedoPlatform::p()->addScriptDeclaration($js,true);
		}

		if (KRequest::getKeyword('type') == 'saveorder') {

			$id = KRequest::getInt('grandorder_id');
			$link = KenedoObserver::triggerEvent('onConfigBoxGetSaveOrderLink',array($id),true);
			ob_start();
			?>
			window.parent.cbj.colorbox.close();
			window.parent.location.href = '<?php echo $link->url;?>';
			<?php
			$js = ob_get_clean();
			KenedoPlatform::p()->addScriptDeclaration($js,true);
		}


	}

}
