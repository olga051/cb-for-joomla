<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerContract extends KenedoController {
	
	public $controller	 	= 'contract';
	public $model		 	= 'contract';
	public $listingView 	= 'contract';
	public $detailsView 	= 'contract';
	
	function __construct() {
		KRequest::setVar('format','dompdf');
		parent::__construct();
		
	}
	function display() {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$shopDataModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		
		$view = $this->getView('contract',KRequest::getKeyword('format','dompdf'));
		
		$view->setModel($shopDataModel,false);
		$view->setModel($orderModel,true);
		
		$view->display();
		
	}
	
}
