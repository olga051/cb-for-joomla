<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerDelivery extends KenedoController {
	
	public $controller	 	= 'delivery';
	public $model		 	= 'delivery';
	public $listingView 	= 'delivery';
	public $detailsView 	= 'delivery';
	
	function display() {

		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		
		if (!$orderModel->getId()) {
			echo '<p>'.KText::_('Nothing to checkout').'</p>';
			return false;
		}
		
		parent::display();
	}
	
	function saveDeliveryOption() {
		
		$model = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $model->getId();
		$deliveryId = KRequest::getInt('delivery_id');
		
		$succ = $model->storeOrderRecordDeliveryOption($orderId, $deliveryId);
		
		if (!$succ) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Could not save delivery option.'));
			$errors = $model->getErrors();
			foreach ($errors as $error) {
				KenedoPlatform::p()->sendSystemMessage($error);
			}
			$this->setRedirect(KLink::getRoute('index.php?option=com_cbcheckout&view=delivery',false));
			$this->redirect();
		}
		else {
			$this->setRedirect(KLink::getRoute('index.php?option=com_cbcheckout&view=confirmation',false));
			$this->redirect();
		}
		
		parent::display();
	}
	
}
