<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminusers extends KenedoController {
	
	public $controller	 	= 'adminusers';
	public $model		 	= 'adminusers';
	public $listingView 	= 'adminusers';
	public $detailsView 	= 'adminuser';
	
}
