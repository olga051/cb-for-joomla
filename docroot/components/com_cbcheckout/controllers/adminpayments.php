<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminpayments extends KenedoController {
	
	public $controller	 	= 'adminpayments';
	public $model		 	= 'adminpayments';
	public $listingView 	= 'adminpayments';
	public $detailsView 	= 'adminpayment';
	
}
