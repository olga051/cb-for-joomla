<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerCheckoutpayment extends KenedoController {
	
	function display() {
		
		KRequest::setVar('view','checkoutpayment');
		$view = $this->getView('checkoutpayment');
		
		$view->display();
	
	}
}