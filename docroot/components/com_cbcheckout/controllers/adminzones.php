<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminzones extends KenedoController {
	
	public $controller	 	= 'adminzones';
	public $model		 	= 'adminzones';
	public $listingView 	= 'adminzones';
	public $detailsView 	= 'adminzone';
	
}
