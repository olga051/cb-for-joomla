<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerCheckout extends KenedoController {
	
	function display() {
		
		KRequest::setVar('view','checkout');
		$view = $this->getView('checkout');
		
		$view->display();
	
	}
	
	function backToCart() {
	
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
		$orderRecord = $orderModel->getOrderRecord($orderId);
	
		$grandorderModel = KenedoModel::getModel('ConfigboxModelGrandorder');
		$grandorderModel->setId($orderRecord->grandorder_id);
		$details = $grandorderModel->getGrandOrderDetails();
		$canRemoveGrandorder 	= ConfigboxOrderHelper::isPermittedAction('removeGrandorder',$details);
		
		if ($canRemoveGrandorder) {
			$adminOrderModel = KenedoModel::getModel('CbcheckoutModelAdminorders');
			$succ = $adminOrderModel->remove(array($orderId));
		}
		
		$url = KLink::getRoute('index.php?option=com_configbox&view=cart',false);
		$this->setRedirect($url);
	
	}
	
	function storeOrderAddress() {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
		
		$success = CbcheckoutUserHelper::saveUser(NULL, $orderId, 'checkout');
		
		$errorMessages = array();
		
		if ($success == false) {
			foreach (CbcheckoutUserHelper::$errors as $fieldName=>$error) {
				
				$message['fieldName'] = $fieldName;
				$message['errorCode'] = 'unknown';
				$message['message'] = $error;
				
				// If billing email is a problem, but format is fine, assume it's about an existing account
				if ($fieldName == 'billingemail' && KRequest::getString('billingemail') && filter_var(KRequest::getString('billingemail'), FILTER_VALIDATE_EMAIL) == true ) {
					$message['errorCode'] = 'account_exists';
				}
				
				$errorMessages[] = $message;
			}
		}
		else {
			
			$orderModel->unsetOrderRecord($orderId);
			
			// Get the order and check if delivery and payment is still valid
			$orderRecord = $orderModel->getOrderRecord($orderId);
			
			$nowInvalid = ($orderRecord->delivery_id && $orderModel->isValidDeliveryOption($orderRecord, $orderRecord->delivery_id) == false);
			$noneSelected = ($orderRecord->delivery_id == 0);
				
			if ($noneSelected || $nowInvalid) {
					
				$possibleOptions = $orderModel->getOrderRecordDeliveryOptions($orderRecord);
				if ($possibleOptions) {
					$orderModel->storeOrderRecordDeliveryOption($orderId, $possibleOptions[0]->id);
				}
				else {
					$orderModel->storeOrderRecordDeliveryOption($orderId, 0);
				}
					
			}
				
			$nowInvalid = ($orderRecord->payment_id && $orderModel->isValidPaymentOption($orderRecord, $orderRecord->payment_id) == false);
			$noneSelected = ($orderRecord->payment_id == 0);
			
			if ($noneSelected || $nowInvalid) {
			
				$orderModel->storeOrderRecordPaymentOption($orderId, 0);
					
				$possibleOptions = $orderModel->getOrderRecordPaymentOptions($orderRecord);
				if ($possibleOptions) {
					$orderModel->storeOrderRecordPaymentOption($orderId, $possibleOptions[0]->id);
				}
				else {
					$orderModel->storeOrderRecordPaymentOption($orderId, 0);
				}
					
			}
		}
		
		echo json_encode($errorMessages);
		
	}
	
	function storeDeliveryOption() {
		
		$model = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $model->getId();
		$deliveryId = KRequest::getInt('id');
		
		if ($deliveryId == 0) {
			$response = new StdClass();
			$response->success = false;
			$response->errors = array(KText::_('Please choose a delivery method.'));
			echo json_encode($response);
		}
		else {
			
			$success = $model->storeOrderRecordDeliveryOption($orderId, $deliveryId);
			
			$response = new StdClass();
			
			if ($success) {
				$response->success = true;
				$response->errors = array();
			}
			else {
				$response->success = false;
				$response->errors = $model->getErrors();
			}
			
			echo json_encode($response);
			
		}
		
	}
	
	function storePaymentOption() {
		
		$model = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $model->getId();
		$paymentId = KRequest::getInt('id');
		
		if ($paymentId == 0) {
			$response = new StdClass();
			$response->success = false;
			$response->errors = array(KText::_('Please choose a payment method.'));
			echo json_encode($response);
		}
		else {
			
			$success = $model->storeOrderRecordPaymentOption($orderId, $paymentId);
			
			$response = new StdClass();
			
			if ($success) {
				$response->success = true;
				$response->errors = array();
			}
			else {
				$response->success = false;
				$response->errors = $model->getErrors();
			}
			
			echo json_encode($response);
			
		}
		
	}
	
	function placeOrder() {
		
		$model = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $model->getId();
		$orderRecord = $model->getOrderRecord($orderId);
		
		$response = new StdClass();
		$response->errors = array();
		
		$orderAddressComplete = CbcheckoutUserHelper::orderAddressComplete($orderRecord->orderAddress);
				
		if (!$orderAddressComplete) {
			$response->errors[] = KText::_('Please complete your order address.');
		}
		
		if ($orderRecord->payment_id == 0) {
			$response->errors[] = KText::_('Please choose a payment method.');
		}
		
		if (CONFIGBOX_DISABLE_DELIVERY == 0 && $orderRecord->delivery_id == 0) {
			$response->errors[] = KText::_('Please choose a delivery method.');
		}
		
		if (count($response->errors) == 0) {
			$response->success = true;
			$status = 2;			
			$model->setStatus($status, $orderId);
		}
		else {
			$response->success = false;
		}
		
		echo json_encode($response);
	}
	
}