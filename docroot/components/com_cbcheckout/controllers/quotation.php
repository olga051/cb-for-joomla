<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerQuotation extends KenedoController {

	function display() {

		$orderRecordId = KRequest::getInt('order_record_id');

		$user = CbcheckoutUserHelper::getUser(NULL, false, false);
		$group = CbcheckoutUserHelper::getGroupData($user->group_id);

		if ($group->enable_request_quotation == false) {
			$platformUserId = KenedoPlatform::p()->getUserId();
			if (ConfigboxPermissionHelper::canDownloadQuotations($platformUserId) == false) {
				KLog::log('Platform user ID "'.$platformUserId.'" tried to request a quotation, although his group permissions do not allow it.', 'permissions', KText::_('You cannot request quotations.'));
				return false;
			}
		}

		$quotationModel = KenedoModel::getModel('CbcheckoutModelQuotation');
		$quotation = $quotationModel->getQuotation($orderRecordId);

		if (!$quotation) {
			$quotation = KenedoObserver::triggerEvent('onConfigBoxCreateQuotation', array($orderRecordId), true);
		}

		if (!$quotation) {
			KLog::log('Could not create quotation for order ID "'.$orderRecordId.'".', 'error', KText::_('Could not create quotation.'));
			return false;
		}

		$filePath = CONFIGBOX_DIR_QUOTATIONS.DS.$quotation->file;
		$fileName = KText::_('quotationfile').'.pdf';

		header("Cache-Control: private", true);
		KenedoPlatform::p()->setDocumentMimeType('application/pdf');
		header("Content-Type: application/pdf");

		header("Content-Disposition: inline; filename=\"$fileName\"", true);
		readfile($filePath);
		header('Connection: close');
		header('Content-Length: '.filesize($filePath));

		die();

	}

}
