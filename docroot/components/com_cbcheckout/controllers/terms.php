<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerTerms extends KenedoController {
	
	function display() {
		
		$shopmodel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		
		$view = $this->getView('Terms','html','CbcheckoutView');
		$view->setModel($shopmodel,true);
		$view->display();	
	}
	
}
