<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerPaymentresult extends KenedoController {
	
	public $controller	 	= 'paymentresult';
	public $model		 	= 'paymentresult';
	public $listingView 	= 'paymentresult';
	public $detailsView 	= 'paymentresult';
	
	function save_payment_info() {
		$connectorName = KRequest::getString('connector_name');
		
		if (!$connectorName) {
			$connectorName = KRequest::getString('system');
		}
		
		if ($connectorName) {
			$folder = ConfigboxPspHelper::getPspConnectorFolder($connectorName);
			$file = $folder.DS.'save_payment_info.php';
			
			if (file_exists($file)) {
				include($file);
			}
		}
		
	}
	
	function display() {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		
		if (!$orderModel->getId()) {
			echo '<p>'.KText::_('Nothing to checkout').'</p>';
			return false;
		}
		
		parent::display();
	}
	
}
