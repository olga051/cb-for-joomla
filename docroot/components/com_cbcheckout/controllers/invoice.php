<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerInvoice extends KenedoController {
	
	function display() {
		
		$orderId = KRequest::getInt('order_id');
		$currentUserId = CbcheckoutUserHelper::getUserId();
		
		$query = "SELECT `user_id` FROM `#__cbcheckout_order_records` WHERE `id` = ".(int)$orderId;
		$db = KenedoPlatform::getDb();
		$db->setQuery($query);
		$orderUserId = $db->loadResult();
		
		if ($orderUserId != $currentUserId) {
			$platformUserId = KenedoPlatform::p()->getUserId();
			if (ConfigboxPermissionHelper::canDownloadInvoices($userId) !== true) {
				KLog::log('Customer with ID "'.$currentUserId.'" tried to download invoice for order ID "'.$orderId.'" of user "'.$orderUserId.'".','permissions','Invoice not found.');
				return false;
			}
		}
		
		$model = KenedoModel::getModel('CbcheckoutModelInvoice');
		$invoiceData = $model->getInvoiceData($orderId);
		
		$file = CONFIGBOX_DIR_INVOICES.DS.$invoiceData->file;
		
		$filename = KText::sprintf('Invoice_%s',$invoiceData->invoice_number). '.pdf';
		
		if (is_file($file)) {
			header("Cache-Control: private");
			header("Content-Type: application/pdf");
			header("Content-Disposition: attachment; filename=\"$filename\"");
			readfile($file);
		}
		else {
			echo 'filenotfound';
		}
		die();
	}
	
}
