<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminShippers extends KenedoController {
	
	public $controller	 	= 'adminshippers';
	public $model		 	= 'adminshippers';
	public $listingView 	= 'adminshippers';
	public $detailsView 	= 'adminshipper';

}
