<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerConfirmation extends KenedoController {
	
	public $controller	 	= 'confirmation';
	public $model		 	= 'confirmation';
	public $listingView 	= 'confirmation';
	public $detailsView 	= 'confirmation';
	
	function display() {

		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = KRequest::getInt('order_id');
		if ($orderId && $orderModel->orderBelongsToUser($orderId)) {
			$orderModel->setId($orderId);
		}
		if (!$orderModel->getId()) {
			echo '<p>'.KText::_('Nothing to checkout').'</p>';
			return false;
		}
		
		parent::display();
	}
	
	function confirm() {
		
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');

		$succ = $orderModel->confirm();
		
		if (!$succ) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Could not save confirmation.'));
			$errors = $orderModel->getErrors();
			foreach ($errors as $error) {
				KenedoPlatform::p()->sendSystemMessage($error);
			}
		}
		else {
			$msg = KText::_('Your order has been placed.');
			$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=productlisting',false),$msg);
			$this->redirect();
		}
		
		parent::display();
	}
	
	function backToCart() {
	
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
	
		$orderRecord = $orderModel->getOrderRecord($orderId);
	
		$grandorderModel = KenedoModel::getModel('ConfigboxModelGrandorder');
		$grandorderModel->setId($orderRecord->grandorder_id);
	
		$adminOrderModel = KenedoModel::getModel('CbcheckoutModelAdminorders');
		$succ = $adminOrderModel->remove(array($orderId));
	
		$url = KLink::getRoute('index.php?option=com_configbox&view=cart',false);
		$this->setRedirect($url);
	
	}
	
}
