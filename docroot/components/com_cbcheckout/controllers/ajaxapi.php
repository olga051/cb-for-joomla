<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAjaxapi extends KenedoController {
	
	public $controller	 	= 'ajaxapi';
	public $model		 	= 'ajaxapi';
	public $listingView 	= 'ajaxapi';
	public $detailsView 	= 'ajaxapi';
	
	function getStateSelectOptions() {
		$this->display();
	}
	
	function getCountySelectOptions() {
		$this->display();
	}
	
	function getCityInput() {
		$this->display();
	}
	
}
