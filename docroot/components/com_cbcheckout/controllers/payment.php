<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerPayment extends KenedoController {
	
	public $controller	 	= 'payment';
	public $model		 	= 'payment';
	public $listingView 	= 'payment';
	public $detailsView 	= 'payment';
	
	function display() {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		
		if (!$orderModel->getId()) {
			echo '<p>'.KText::_('Nothing to checkout').'</p>';
			return false;
		}
		
		parent::display();
	}
	
	function savePaymentOption() {
		
		$model = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $model->getId();
		$paymentId = KRequest::getInt('payment_id');
		
		$succ = $model->storeOrderRecordPaymentOption($orderId, $paymentId);
		
		if (!$succ) {
			KenedoPlatform::p()->sendSystemMessage(KText::_('Could not save payment option.'));
			$errors = $model->getErrors();
			foreach ($errors as $error) {
				KenedoPlatform::p()->sendSystemMessage($error);
			}
			$this->setRedirect(KLink::getRoute('index.php?option=com_cbcheckout&view=payment',false));
			$this->redirect();
		}
		else {
			$this->setRedirect(KLink::getRoute('index.php?option=com_cbcheckout&view=confirmation',false));
			$this->redirect();
		}
		
		parent::display();
	}
	
}
