<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerCheckoutaddress extends KenedoController {
	
	function display() {
		
		KRequest::setVar('view','checkoutaddress');
		$view = $this->getView('checkoutaddress');
		
		$view->display();
	
	}
}