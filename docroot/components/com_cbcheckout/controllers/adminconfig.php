<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminconfig extends KenedoController {
	
	public $controller	 	= 'adminconfig';
	public $model		 	= 'adminconfig';
	public $listingView 	= 'adminconfig';
	public $detailsView 	= 'adminconfig';
	
	function display() {
		$this->edit();
	}
}
