<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAssistance extends KenedoController {
	
	public $controller	 	= 'assistance';
	public $model		 	= 'user';
	public $listingView 	= 'inforequest';
	public $detailsView 	= 'inforequest';
	
	function display() {
		
		$userId = CbcheckoutUserHelper::getUserId();
		$user = CbcheckoutUserHelper::getUser($userId);
		$group = CbcheckoutUserHelper::getGroupData($user->group_id);
		
		if ($group->enable_request_assistance == false) {
			$platformUserId = KenedoPlatform::p()->getUserId();
			if (ConfigboxPermissionHelper::canRequestAssistance($platformUserId) == false) {
				KLog::log('Platform user ID "'.$platformUserId.'" tried to request assistance, although his group permissions do not allow it.','permissions',KText::_('You cannot request assistance.'));
				return false;
			}
		}
		
		if (KRequest::getInt('grandorder_id')) {
				
			$grandOrderId = KRequest::getInt('grandorder_id');
				
			$db = KenedoPlatform::getDb();
			$query = "SELECT `id`,`status`,`user_id` FROM `#__cbcheckout_order_records` WHERE `grandorder_id` = ".(int)$grandOrderId;
			$db->setQuery($query);
			$orderRow = $db->loadObject();
				
			if ($orderRow && $orderRow->user_id != CbcheckoutUserHelper::getUserId()) {
				$platformUserId = KenedoPlatform::p()->getUserId();
				if (ConfigboxPermissionHelper::canRequestAssistance($platformUserId) == false) {
					KLog::log('Platform user ID "'.$platformUserId.'" tried to request assistance for another customer\'s (customer with ID "'.$orderRow->userId.'") order.','permissions',KText::_('Order not found.'));
					return false;
				}
			}
				
			if ($orderRow && $orderRow->status == 12) {
				$checkoutRecordId = $orderRow->id;
			}
			else {
				// Get the grandorder object
				$grandorderModel = KenedoModel::getModel('ConfigboxModelGrandorder');
				$grandorderModel->setId($grandOrderId);
				$orderDetails = $grandorderModel->getGrandOrderDetails();
		
				$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
				$checkoutRecordId = $orderModel->createOrderRecord($orderDetails, 12);
			}
			
			KenedoObserver::triggerEvent('onConfigBoxSetStatus', array($checkoutRecordId, 12));
			
			KenedoPlatform::p()->sendSystemMessage(KText::_('Your request has been sent. We will get back to you as soon as possible.'));		
			$this->setRedirect(KLink::getRoute('index.php?option=com_configbox&view=cart&grandorder_id='.(int)$grandOrderId,false));
			$this->redirect();
			
		}
		
	}
	
}