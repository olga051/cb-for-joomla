<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminstates extends KenedoController {
	
	public $controller	 	= 'adminstates';
	public $model		 	= 'adminstates';
	public $listingView 	= 'adminstates';
	public $detailsView 	= 'adminstate';

}
