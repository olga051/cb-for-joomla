<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminshippingrates extends KenedoController {
	
	public $controller	 	= 'adminshippingrates';
	public $model		 	= 'adminshippingrates';
	public $listingView 	= 'adminshippingrates';
	public $detailsView 	= 'adminshippingrate';

}
