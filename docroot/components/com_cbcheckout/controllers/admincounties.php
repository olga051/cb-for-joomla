<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdmincounties extends KenedoController {
	
	public $controller	 	= 'admincounties';
	public $model		 	= 'admincounties';
	public $listingView 	= 'admincounties';
	public $detailsView 	= 'admincounty';

}
