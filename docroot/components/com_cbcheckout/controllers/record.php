<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerRecord extends KenedoController {
	
	function display() {
		
		KRequest::setVar('view','record');
		$view = $this->getView('record');
		
		$view->display();
	
	}
}