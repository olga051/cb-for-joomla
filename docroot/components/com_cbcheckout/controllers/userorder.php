<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerUserorder extends KenedoController {
	
	public $controller	 	= 'userorder';
	public $model		 	= 'userorder';
	public $listingView 	= 'userorder';
	public $detailsView 	= 'userorder';
	
	function addOrderFile() {

		$response = new StdClass();
		
		$orderId = KRequest::getInt('orderId');
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		
		$doesBelong = $orderModel->orderBelongsToUser($orderId);
		// Check if the order belongs to the current users
		if ($doesBelong == false) {
			
			// If not, check if the user has extended permissions
			if (!ConfigboxPermissionHelper::canEditOrders()) {
				$response->message = KText::_('This order does not belong to your user account and you have no permissions to edit orders as shop manager.');
				$response->success = false;
				$this->sendResponse($response);
				return;
			}
			
		}
		
		$comment = KRequest::getString('comment');
		$file = KRequest::getFile('orderFile',NULL);
		
		if ($file) {
			$succ = move_uploaded_file($file['tmp_name'], KenedoPlatform::get('tmpPath').DS.$file['name']);
			$file = KenedoPlatform::get('tmpPath').DS.$file['name'];
		}
		else {
			$response->success = false;
			$response->message = KText::_('Please choose a file before submitting.');
			$this->sendResponse($response);
			return;
		}
		
		if ($succ == false) {
			KLog::log('A file could not be stored because the Temp folder "'.realpath(KenedoPlatform::get('tmpPath')).'" is write protected. Check folder permissions for this folder','error');
			$response->success = false;
			$response->message = KText::_('Could not upload file. Target folder is not writable.');
			$this->sendResponse($response);
			return;
		}
		
		$model = KenedoModel::getModel('CbcheckoutModelOrderfiles');
		$storedFile = $model->addOrderFile($orderId, $file, $comment);
		if ($storedFile === false) {
			if ($file) unlink( $file );
			$response->success = false;
			$response->message = $model->getError();
			$this->sendResponse($response);
			return;
		}
		else {

			$model->notifyOnOrderFile($orderId, $storedFile);

			$response->success = true;
			$response->fileId = $storedFile->id;
			$response->fileName = $storedFile->file;
			$response->comment = $storedFile->comment;
			$response->message = KText::_('File uploaded');
			$response->infoIcon = ($storedFile->comment) ? KenedoHtml::getTooltip('<a class="fa fa-info-circle"></a>', hsc($storedFile->comment)) : '';
			$this->sendResponse($response);
			return;
		}
		
	}
	
	function deleteOrderFile() {
		$fileId = KRequest::getInt('fileId');
		$model = KenedoModel::getModel('CbcheckoutModelOrderfiles');
		
		$response = new StdClass();
		
		if (!$model->fileBelongsToUser($fileId) && !ConfigboxPermissionHelper::canDeleteOrderFiles()) {
			$response->success = false;
			$response->message = KText::_('This file can only be deleted by the owner of this order or a shop manager with system privileges to remove order files.');
		}
		else {
			
			$success = $model->deleteOrderFile($fileId);
			if ($success == false) {
				$response->success = false;
				$response->message = $model->getError();
			}
			else {
				$response->success = true;
				$response->message = '';
			}
			
		}
		
		echo json_encode($response);
		
	}
	
	function getOrderFile() {
		
		$fileId = KRequest::getInt('fileId');
		$model = KenedoModel::getModel('CbcheckoutModelOrderfiles');
		$belongs = $model->fileBelongsToUser($fileId);
		
		if (!$belongs) {
			$canEdit = ConfigboxPermissionHelper::canEditOrders();
			if (!$canEdit) {
				$this->setError(KText::_('This file can only be accessed by the owner of this order or a shop manager with system privileges to remove order files.'));
				return false;	
			}
		}
		
		$file = $model->getOrderFile($fileId);
		
		$mimeType = KenedoFileHelper::getMimeType($file->fullPath);
		if ($mimeType) {
			// header("Content-Type: ".$mimeType);
		}
		header("Content-Disposition: attachment; filename=\"$file->file\"");
		readfile($file->fullPath);
		die();
		
	}
	
	protected function sendResponse($response) {
		?>
		<script type="text/javascript">
		window.parent.responseData = <?php echo json_encode($response);?>;
		</script>
		<?php
	}
		
}
