<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdmincities extends KenedoController {
	
	public $controller	 	= 'admincities';
	public $model		 	= 'admincities';
	public $listingView 	= 'admincities';
	public $detailsView 	= 'admincity';

}
