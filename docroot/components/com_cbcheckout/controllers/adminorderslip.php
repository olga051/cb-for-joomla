<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminorderslip extends KenedoController {
	
	public $controller	 	= 'adminorderslip';
	public $model		 	= 'adminorderslip';
	public $listingView 	= 'adminorderslip';
	public $detailsView 	= 'adminorderslip';
	
	function display() {
		
		$orderId = KRequest::getInt('order_id');
		
		$slipView = KenedoView::getView('CbcheckoutViewAdminorderslip');
		$slipView->assign('orderId',$orderId);
		
		// Generate the PDF
		ob_start();
		$slipView->display();
		$html = ob_get_clean();
		
		// Replace the EURO character with an HTML entity to circumvent issues
		$html = str_replace('€','&#8364;',$html);
		
		$filename = 'manufacturing-slip-'.$orderId.'.pdf';
		
		// Init the dompdf app
		ConfigboxDomPdfHelper::init();
		
		// Load the template HTML and render the PDF
		$dompdf = new DOMPDF();
		$dompdf->load_html($html);
		$dompdf->render();

		echo $dompdf->stream($filename,array('Attachment'=>1));
		die();
		
	}
	
}