<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAddress extends KenedoController {
	
	public $controller	 	= 'address';
	public $model		 	= 'address';
	public $listingView 	= 'address';
	public $detailsView 	= 'address';
	
	function display() {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		
		if (!$orderModel->getId()) {
			echo '<p>'.KText::_('Nothing to checkout').'</p>';
			return false;
		}
		
		parent::display();	
	}
	
}
