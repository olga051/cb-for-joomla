<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdmincountries extends KenedoController {
	
	public $controller	 	= 'admincountries';
	public $model		 	= 'admincountries';
	public $listingView 	= 'admincountries';
	public $detailsView 	= 'admincountry';
	
}
