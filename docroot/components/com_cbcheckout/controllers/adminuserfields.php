<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerAdminuserfields extends KenedoController {
	
	public $controller	 	= 'adminuserfields';
	public $model		 	= 'adminuserfields';
	public $listingView 	= 'adminuserfields';
	public $detailsView 	= 'adminuserfields';
	
}
