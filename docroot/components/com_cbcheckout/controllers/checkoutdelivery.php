<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutControllerCheckoutdelivery extends KenedoController {
	
	function display() {
		
		KRequest::setVar('view','checkoutdelivery');
		$view = $this->getView('checkoutdelivery');
		
		$view->display();
	
	}
}