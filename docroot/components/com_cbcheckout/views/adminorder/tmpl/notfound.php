<?php
defined('CB_VALID_ENTRY') or die();
?>
<div id="view-<?php echo hsc($this->view);?>" class="<?php hsc($this->renderViewCssClasses());?>">

	<?php
	if (isset($this->pageTabs) && count($this->pageTabs)) {
		echo KenedoViewHelper::renderTabItems($this->pageTabs);
	}
	?>

	<div class="page-content kenedo-details-form">

		<div class="floatright"><a class="backend-button-small" onclick="history.back()"><?php echo KText::_('Back');?></a></div>


		<p>Order not found</p>

	</div> <!-- .page-content -->
</div>



