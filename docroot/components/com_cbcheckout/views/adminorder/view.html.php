<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewAdminorder extends KenedoView {
	
	function display() {
		
		$this->assignRef('pageTabs',KenedoViewHelper::getTabItems());
		
		$cid = KRequest::getArray('cid');
		if ($cid) {
			$orderId = $cid[0];
		}
		else {
			$orderId = KRequest::getInt('id');
		}

		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderRecord = $orderModel->getOrderRecord( $orderId );

		if (!$orderRecord) {
			$this->addViewCssClasses();
			$this->renderView('notfound');
			return;
		}
		
		if (CONFIGBOX_ENABLE_FILE_UPLOADS) {
			$filesModel = KenedoModel::getModel('CbcheckoutModelOrderfiles');
			$files = $filesModel->getOrderFiles($orderId);
			$this->assignRef('orderFiles',$files);
		}

		$validExtensions = trim(CONFIGBOX_ALLOWED_FILE_EXTENSIONS);
		$hasLimitations = false;
		if ($validExtensions) {
			$extensions = strtolower($validExtensions);
			$extensions = str_replace(',', ' ', $extensions);
			$extensions = str_replace('.', '', $extensions);
			$extensions = str_replace('  ', ' ', $extensions);
			$extensions = explode(' ',$extensions);
			$validExtensions = array_map('trim',$extensions);
			if (count($validExtensions)) {
				$hasLimitations = true;
			}
		}

		if (CONFIGBOX_ALLOWED_FILE_SIZE) {
			$this->assign('fileUploadMaximumSize', CONFIGBOX_ALLOWED_FILE_SIZE);
		}

		$this->assign('fileUploadHasLimitations', $hasLimitations);
		$this->assign('allowedFileExtensions', $validExtensions);

		
		$invoiceModel = KenedoModel::getModel('CbcheckoutModelInvoice');

		$invoiceData = $invoiceModel->getInvoiceData( $orderId );
		$this->assignRef('invoiceData',$invoiceData);
		
		$nextInvoiceSerial = $invoiceModel->getNextInvoiceSerial();
		$this->assignRef('nextInvoiceSerial',$nextInvoiceSerial);
		
		$invoicePrefix = $invoiceModel->getInvoicePrefix();

		$this->assignRef('invoicePrefix', $invoicePrefix);
		

		
		if ($orderRecord) {
			// Get the order record view HTML
			$orderRecordView = KenedoView::getView('CbcheckoutViewRecord',KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'record'.DS.'view.html.php');
			$orderRecordView->assignRef('orderRecord',$orderRecord);
			$orderRecordView->assign('showIn','shopmanager');
			$orderRecordView->assign('showChangeLinks',false);
			$orderRecordView->assign('showProductDetails',true);
			$orderRecordHtml = $orderRecordView->getViewOutput();
			$this->assignRef('orderRecordHtml',$orderRecordHtml);
		}
		else {
			$this->assign('orderRecordHtml', '');
		}
		
		$userEditFormReachable = KenedoPlatform::p()->platformUserEditFormIsReachable();
		$userCanEditUsers = KenedoPlatform::p()->userCanEditPlatformUsers();
		$this->assign('platformUserEditFormIsReachable', $userEditFormReachable);
		$this->assign('userCanEditPlatformUsers', $userCanEditUsers);
		
		if ($userEditFormReachable && $userCanEditUsers && !empty($orderRecord->orderAddress->joomlaid)) {
			$this->assign('showPlatformUserEditLink', true);			
			$this->assignRef('urlPlatformUserEditForm', KenedoPlatform::p()->getPlatformUserEditUrl($orderRecord->orderAddress->joomlaid));
		}
		else {
			$this->assign('showPlatformUserEditLink', false);
		}
		
		
		
		$this->assignRef('orderRecord', $orderRecord);
		
		$statusSelect = $orderModel->getStatusDropDown( $orderRecord->status );
		$this->assign('statusSelect',$statusSelect);
		
		$this->addViewCssClasses();
		
		$this->renderView();
		
	}
	
}