<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewInvoice extends KenedoView {
	
	function display() {
		
		// Order ID has to be passed via ->assign()
		$orderId = $this->orderId;
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderModel->resetOrderData();
		$orderRecord = $orderModel->getOrderRecord($orderId);
		$this->assignRef('orderRecord', $orderRecord);
		
		$this->assign('paymentMethodHtml', '');
		$this->assign('orderRecordHtml', '');
		
		if ($orderRecord) {
			
			$shopDataModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
			$shopDataModel->_setId($orderRecord->store_id);
			$shopData = $shopDataModel->getItem();
			
			$this->assign('useCustomTemplate', $shopData->use_custom_invoice == 1);
			$this->assignRef('shopData', $shopData);
			
			$view = KenedoView::getView('CbcheckoutViewRecord',KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'record'.DS.'view.html.php');
			$view->assignRef('orderRecord', $orderRecord);
			$view->assign('showIn','quotation');
			$view->assign('showChangeLinks',false);
			$view->assign('showProductDetails',false);
			$html = $view->getViewOutput();
			$this->assignRef('orderRecordHtml',$html);
			
			$connectorName = $this->orderRecord->payment->connector_name;
			$folder = ConfigboxPspHelper::getPspConnectorFolder($connectorName);
			
			if (is_file($folder.DS.'invoice.php')) {
				ob_start();
				include($folder.DS.'invoice.php');
				$html = ob_get_clean();
				
				KLog::log($html, 'payment');
				$this->assign('paymentMethodHtml', $html);
			}
			
		}
		
		$filePath = KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'data'.DS.'shoplogo'.DS. $this->shopData->shoplogo;
		if (is_file($filePath)) {
				
			$this->assign('useShopLogo',true);
			$this->assign('shopLogoUrl', KPATH_ROOT_URL.'components/com_cbcheckout/data/shoplogo'.DS. $this->shopData->shoplogo);
				
			$image = new ConfigboxImageResizer($filePath);
			
			$maxWidth = 400;
			$maxHeight = 60;
			
			if ($image->width > $maxWidth || $image->height > $maxHeight) {
				$dimensions = $image->getDimensions($maxWidth, $maxHeight, 'containment');
				$this->assign('shopLogoWidth',	intval($dimensions['optimalWidth']));
				$this->assign('shopLogoHeight',	intval($dimensions['optimalHeight']));
			}
			else {
				$this->assign('shopLogoWidth',	$image->width);
				$this->assign('shopLogoHeight',	$image->height);
			}
				
		}
		else {
			$this->assign('useShopLogo',false);
			$this->assign('shopLogoWidth',	0);
			$this->assign('shopLogoHeight',	0);
		}
	
		$this->renderView();
	}
	
}
