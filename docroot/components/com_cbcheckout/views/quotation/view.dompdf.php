<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewQuotation extends KenedoView {
	
	function display() {
		
		$shopDataModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		$shopData = $shopDataModel->getItem();
		$this->assignRef('shopData', $shopData);
		
		// Order ID has to be passed via ->assign()
		$orderId = $this->orderId;
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderRecord = $orderModel->getOrderRecord($orderId);
		$this->assignRef('orderRecord', $orderRecord);
		
		$maxWidth = 400;
		$maxHeight = 60;
		
		$filePath = KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'data'.DS.'shoplogo'.DS. $this->shopData->shoplogo;
		if ($filePath) {
			
			$this->assign('useShopLogo',true);
			$this->assign('shopLogoUrl', KPATH_ROOT_URL.'components/com_cbcheckout/data/shoplogo'.DS. $this->shopData->shoplogo);
			
			$image = new ConfigboxImageResizer($filePath);
			
			if ($image->width > $maxWidth || $image->height > $maxHeight) {
				$dimensions = $image->getDimensions($maxWidth, $maxHeight, 'containment');
				$this->assign('shopLogoWidth',	intval($dimensions['optimalWidth']));
				$this->assign('shopLogoHeight',	intval($dimensions['optimalHeight']));
			}
			else {
				$this->assign('shopLogoWidth',	$image->width);
				$this->assign('shopLogoHeight',	$image->height);
			}
			
		}
		else {
			$this->assign('useShopLogo',false);
		}
		
		$this->renderView();
	}
}