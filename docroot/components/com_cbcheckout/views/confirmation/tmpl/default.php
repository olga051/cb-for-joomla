<?php 
defined('CB_VALID_ENTRY') or die(); 
?>
<div id="com_cbcheckout">
	<div id="view-confirmation">
	
	<?php if ($this->orderRecord) { ?>
		
		<?php if (KRequest::getInt('print','0') != 1) { ?>
			<div class="print-link"><a class="new-tab" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=confirmation&tmpl=component&print=1');?>"><?php echo KText::_('Print Version');?></a></div>
		<?php } ?>
		<h1 class="componentheading"><?php echo KText::_('Please verify your information')?></h1>
		
		<div class="clear"></div>
		
		<div class="order-address">
			<h2><?php echo KText::_('Address');?></h2>
			<div class="addresses">
				<div class="order-address-billing">
					<h3 class="order-address-heading"><?php echo KText::_('Billing Address');?></h3>
					<table class="order-address-table">
						<?php if ($this->orderRecord->orderAddress->billingcompanyname) { ?>
						<tr>
							<td class="key"><?php echo KText::_('Company');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->billingcompanyname);?></td>
						</tr>
						<?php } ?>
						<tr>
							<td class="key"><?php echo KText::_('Name');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->billingfirstname);?> <?php echo hsc($this->orderRecord->orderAddress->billinglastname);?></td>
						</tr>
						<tr>
							<td class="key"><?php echo KText::_('Address');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->billingaddress1);?></td>
						</tr>
						<?php if ($this->orderRecord->orderAddress->billingaddress2) { ?>
						<tr>
							<td class="key"><?php echo KText::_('Address 2');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->billingaddress2);?></td>
						</tr>
						<?php } ?>
						<tr>
							<td class="key"><?php echo KText::_('ZIP Code');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->billingzipcode);?></td>
						</tr>
						<tr>
							<td class="key"><?php echo KText::_('City');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->billingcity);?></td>
						</tr>
						<tr>
							<td class="key"><?php echo KText::_('Country');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->billingcountryname);?></td>
						</tr>
					</table>
				</div>
				
				<?php if ($this->orderRecord->orderAddress->samedelivery == 0) { ?>
					<div class="order-address-delivery">
						<h3 class="order-address-heading"><?php echo KText::_('Delivery Address');?></h3>
						<table class="order-address-table">
							<?php if ($this->orderRecord->orderAddress->companyname) { ?>
							<tr>
								<td class="key"><?php echo KText::_('Company');?>:</td>
								<td><?php echo hsc($this->orderRecord->orderAddress->companyname);?></td>
							</tr>
							<?php } ?>
							<tr>
								<td class="key"><?php echo KText::_('Name');?>:</td>
								<td><?php echo hsc($this->orderRecord->orderAddress->firstname);?> <?php echo hsc($this->orderRecord->orderAddress->lastname);?></td>
							</tr>
							<tr>
								<td class="key"><?php echo KText::_('Address');?>:</td>
								<td><?php echo hsc($this->orderRecord->orderAddress->address1);?></td>
							</tr>
							<?php if ($this->orderRecord->orderAddress->address2) { ?>
							<tr>
								<td class="key"><?php echo KText::_('Address 2');?>:</td>
								<td><?php echo hsc($this->orderRecord->orderAddress->address2);?></td>
							</tr>
							<?php } ?>
							<tr>
								<td class="key"><?php echo KText::_('ZIP Code');?>:</td>
								<td><?php echo hsc($this->orderRecord->orderAddress->zipcode);?></td>
							</tr>
							<tr>
								<td class="key"><?php echo KText::_('City');?>:</td>
								<td><?php echo hsc($this->orderRecord->orderAddress->city);?></td>
							</tr>
							<tr>
								<td class="key"><?php echo KText::_('Country');?>:</td>
								<td><?php echo hsc($this->orderRecord->orderAddress->countryname);?></td>
							</tr>
						</table>
						
					</div>
				<?php } ?>
				
				<a class="trigger-change-order-address" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=address');?>"><?php echo KText::_('change')?></a>
				<div class="clear"></div>
			</div>
		</div>
		
		<div class="terms-and-conditions">
			<h2><?php echo KText::_('Terms and Conditions');?></h2>
			<div class="checks">
				
				<div class="terms-links">
					<div class="toc-link">
						<a class="rovedo-modal" data-modal-width="900" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=terms&tmpl=component');?>"><?php echo KText::_('Terms and Conditions')?></a>
					</div>
					<?php if (CONFIGBOX_SHOWREFUNDPOLICY) { ?>
						<div class="refund-policy-link">
							<a class="rovedo-modal" data-modal-width="900" data-modal-height="700" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=refundpolicy&tmpl=component');?>"><?php echo KText::_('Refund Policy')?></a>
						</div>
					<?php } ?>
				</div>
				
				<div class="confirmation-checkboxes">
				
					<div class="checkbox-toc">
						<input type="checkbox" name="tcconfirmed" value="1" id="tcconfirmed" />
						<label for="tcconfirmed"><?php echo KText::_('I agree to the terms and conditions.');?></label> 
					</div>
					
					<?php if (CONFIGBOX_SHOWREFUNDPOLICY) { ?>
						<div class="checkbox-refund-policy">
							<input type="checkbox" name="rpconfirmed" value="1" id="rpconfirmed" />
							<label for="rpconfirmed"><?php echo KText::_('I agree to the refund policy.');?></label>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		
		<div class="clear"></div>
		
		<div class="order-overview-wrapper">
			<h2><?php echo KText::_('Order Overview');?></h2>
			<?php echo $this->orderRecordHtml;?>
		
			<?php if ($this->orderComplete == false) { ?>
			
				<?php if ($this->orderAddressComplete == false) { ?>
					<div class="order-validation"><?php echo KText::_('To continue, please complete your order address information.');?> <a href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=address');?>"><?php echo KText::_('Complete');?></a></div>
				<?php } ?>
				
				<?php if ($this->deliveryChosen == false) { ?>
					<div class="order-validation"><?php echo KText::_('To continue, please choose a delivery method.');?> <a class="trigger-change-delivery" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=delivery');?>"><?php echo KText::_('Choose delivery method');?></a></div>
				<?php } ?>
				
				<?php if ($this->paymentChosen == false) { ?>
					<div class="order-validation"><?php echo KText::_('To continue, please choose a payment method.');?> <a class="trigger-change-payment" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=payment');?>"><?php echo KText::_('Choose payment method');?></a></div>
				<?php } ?>
				
			<?php } else { ?>
			
				<div class="order-payment-system">
					<?php 
					if (KRequest::getInt('print','0') != 1 && !empty($this->orderRecord->payment)) {						
						require($this->pspBridgeFilePath);
					}
					?>
				</div>
			
			<?php } ?>
		</div>
		
		<?php if (CONFIGBOX_SHOWREFUNDPOLICYINLINE) { ?>
		<div id="refundPolicy">
			<h3><?php echo KText::_('Refund Policy');?></h3>
			<?php echo $this->shopdata->refundpolicy; ?>
		</div>
		<?php } ?>
	<?php } else { ?>
		<p><?php echo KText::_('Order not found. Your session may have expired.');?></p>
		<div>
			<a rel="nofollow" class="floatleft leftmost navbutton-medium next continue-shopping" href="<?php echo KLink::getRoute('index.php?option=com_configbox&view=productlisting&pcat_id='.CONFIGBOX_CONTINUE_LISTING_ID,true,CONFIGBOX_SECURECHECKOUT);?>">
				<span class="nav-center"><?php echo KText::_('Continue Shopping')?></span>
			</a>
		</div>
	<?php } ?>
</div>
</div>

<script type="text/javascript">

function validatetc() {

	var tcConfirmed = cbj('#tcconfirmed').prop('checked');
	var rpConfirmed = cbj('#rpconfirmed').prop('checked');
	
	if (1 == <?php echo CONFIGBOX_SHOWREFUNDPOLICY;?>) {
		if (!rpConfirmed) {
			alert("<?php echo KText::_('Please agree to the refund policy first.');?>");
			return false;
		}
	}
	if (!tcConfirmed) {
		alert("<?php echo KText::_('Please agree to the Terms and Conditions first.');?>");
		return false;
	}

	return true;
}	
</script>
