<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewConfirmation extends KenedoView {
	
	function display(){
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
				
		// Get the order record
		$orderRecord = $orderModel->getOrderRecord($orderId);
		$this->assignRef('orderRecord',$orderRecord);
		
		if ($orderRecord) {
			$view = KenedoView::getView('CbcheckoutViewRecord',KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'record'.DS.'view.html.php');
			$view->assignRef('orderRecord',$orderRecord);
			$view->assign('showIn','confirmation');
			$view->assign('showChangeLinks',true);
			$view->assign('showProductDetails',true);
			$html = $view->getViewOutput();
			$this->assignRef('orderRecordHtml', $html);
		}
		else {
			$this->assign('orderRecordHtml','');
		}
		
		// Get shop data
		$shopModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		$shopdata = $shopModel->getItem();
		$this->assignRef('shopdata',$shopdata);
		
		$orderAddressComplete = CbcheckoutUserHelper::orderAddressComplete($orderRecord->orderAddress);
		$this->assignRef('orderAddressComplete',$orderAddressComplete);
		
		$deliveryChosen = (!empty($orderRecord->delivery) || CONFIGBOX_DISABLE_DELIVERY);
		$this->assignRef('deliveryChosen',$deliveryChosen);
		
		$paymentChosen = !empty($orderRecord->payment);
		$this->assignRef('paymentChosen',$paymentChosen);
		
		$this->assign('orderComplete', ($orderAddressComplete && $deliveryChosen && $paymentChosen) );
		
		$this->assign('statid',0);
		
		if (!empty($orderRecord->payment)) {
			
			$langUrlCode = KenedoPlatform::p()->getLanguageUrlCode( CONFIGBOX_LANGUAGE_TAG );
			
			// New path generation for payment options
			$notificationPath = KLink::getRoute('index.php?lang='.$langUrlCode.'&option=com_cbcheckout&task=processipn&controller=ipn&connector_name='.$orderRecord->payment->connector_name,false);
			
			$successPath = KLink::getRoute('index.php?option=com_cbcheckout&view=userorder&order_id='.$this->orderRecord->id,false);
			$failurePath = KLink::getRoute('index.php?option=com_cbcheckout&view=confirmation',false);
			$cancelPath = KLink::getRoute('index.php?option=com_cbcheckout&view=confirmation',false);
					
			$prefixNormal = 'http://'.KPATH_HOST;
			$prefixSecure = 'https://'.KPATH_HOST;
			
			if (CONFIGBOX_SECURECHECKOUT) {
				$successUrl 		= $prefixSecure . $successPath;
				$failureUrl 		= $prefixSecure . $failurePath;
				$cancelUrl 			= $prefixSecure . $cancelPath;
				$notificationUrl 	= $prefixSecure . $notificationPath;
			}
			else {
				$successUrl 		= $prefixNormal . $successPath;
				$failureUrl 		= $prefixNormal . $failurePath;
				$cancelUrl 			= $prefixNormal . $cancelPath;
				$notificationUrl 	= $prefixNormal . $notificationPath;
			}
			
			$notificationUrlNormal = $prefixNormal . $notificationPath;
			$notificationUrlSecure = $prefixSecure . $notificationPath;
			
			$pspBridgeFilePath = ConfigboxPspHelper::getPspConnectorFolder($orderRecord->payment->connector_name) . DS . 'bridge.php';
			$this->assignRef('pspBridgeFilePath', $pspBridgeFilePath);
			
			$this->assignRef('successUrl', $successUrl);
			$this->assignRef('failureUrl', $failureUrl);
			$this->assignRef('cancelUrl', $cancelUrl);
			$this->assignRef('notificationUrl', $notificationUrl);
			$this->assignRef('notificationUrlNormal', $notificationUrlNormal);
			$this->assignRef('notificationUrlSecure', $notificationUrlSecure);
			
		}
		$this->renderView();
	}
	
	function getFieldClasses($fields,$field) {
		$fieldClasses = array();
		$fieldClasses[] = 'order-address-field field-'.$field;
		if (!empty($fields[$field]->require_checkout)) {
			$fieldClasses[] = 'required';
		}
		return implode(' ',$fieldClasses);
	}
	
}
