<?php 
defined('CB_VALID_ENTRY') or die(); 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link type="text/css" rel="stylesheet" href="<?php echo KenedoPlatform::p()->getDocumentBase().'components/com_cbcheckout/assets/css/com_cbcheckout.css';?>" />
	<title><?php echo KText::sprintf( 'Contract for order %s',$this->orderId );?></title>

</head>
<body>
<div id="com_cbcheckout"><div id="view-contract"><div id="template-default_dompdf">


<div class="shopdata">
	
	<?php echo $this->shopData->shopname;?><br />
	<?php if ( !empty($this->shopData->shopaddress1) ) { 
		echo $this->shopData->shopaddress1.'<br />';
	}
	if ( !empty($this->shopData->shopaddress2) ) { 
		echo $this->shopData->shopaddress2.'<br />';
	}
	if ( !empty($this->shopData->shopzipcode) ) { 
		echo $this->shopData->shopzipcode.' '.$this->shopData->shopcity.'<br />';
	}
	?>
	<?php echo ($this->shopData->shopemailsales);?><br />
	<?php echo ($this->shopData->shopphonesales);?><br />

	
</div>

<h1><?php echo KText::sprintf( 'Contract for order %s',$this->orderId );?></h1>
<div><?php echo $this->currentdate;?></div>
<br />
<br />
<?php
if ($this->orderDetails->netTotalRecurring) {
	?>
	<h3><?php echo KText::_('Initial Costs');?></h3>
	<br />
	<br />
	<?php 
}
?>
<table class="invoicetable" cellpadding="3" cellspacing="0" border="1">
	<tr>
		<th class="colproduct"><?php echo KText::_('Product');?></th>
		<th class="colquantity"><?php echo KText::_('Quantity');?></th>
		<th class="colconfiguration"><?php echo KText::_('Configuration');?></th>
		<th class="colnet"><?php echo KText::_('Net');?></th>
		<th class="colvat"><?php echo KText::_('VAT');?></th>
		<th class="colgross"><?php echo KText::_('Gross');?></th>
	</tr>
	<?php
	foreach ($this->orderDetails->orders as $orderitem) {
		
		?>
		<tr>
			<td class="colproduct"><?php echo $orderitem['productTitle'];?></td>
			<td class="colquantity"><?php echo $orderitem['quantity'];?></td>
			<td class="colconfiguration">
				
				<?php
				foreach ($orderitem['elements'] as &$element) {
					?>
						<span class="elementcell"><?php echo $element['elementTitle'];?>: <?php echo $element['optionTitle'];?></span>
						<br />
				<?php } ?>
				
			</td>
			<td class="colnet"><?php echo cbprice($orderitem['total_net']);?></td>
			<td class="colvat"><?php echo cbprice($orderitem['total_tax']);?><br />(<?php echo $orderitem['prodInfo']['taxrate']; ?>% <?php echo KText::_('VAT');?>)</td>
			<td class="colgross"><?php echo cbprice($orderitem['total_gross']);?></td>
		</tr>
		<?php
	}
	?>
</table>
<br />
<table class="invoicetable" cellpadding="3" cellspacing="0" border="1" >
	
	<tr>
		<td class="other"><?php echo KText::_('Total');?></td>
		<td class="colnet"><?php echo cbprice($this->order->price - $this->order->inctaxorder ); ?></td>
		<td class="colvat"><?php echo cbprice($this->order->inctaxorder);?></td>
		<td class="colgross"><?php echo cbprice($this->order->price);?></td>
	</tr>
</table>


<?php 

if ($this->orderDetails->netTotalRecurring) {
	?>
	<br />
	<h3><?php echo KText::_('Recurring Costs');?></h3>
	<table class="invoicetable" cellpadding="3" cellspacing="0" border="1">
		<tr>
			<th class="colproduct"><?php echo KText::_('Product');?></th>
			<th class="colquantity"><?php echo KText::_('Quantity');?></th>
			<th class="colinterval"><?php echo KText::_('Interval');?></th>
			<th class="colnet"><?php echo KText::_('Net');?></th>
			<th class="colvat"><?php echo KText::_('VAT');?></th>
			<th class="colgross"><?php echo KText::_('Gross');?></th>
		</tr>
		<?php
		foreach ($this->orderDetails->orders as $order) {
			if ($order['total_recurring_net'] == 0) continue;
			?>
			<tr>
				<td class="colproduct"><?php echo $order['productTitle'];?></td>
				<td class="colquantity"><?php echo $order['quantity'];?></td>
				<td class="colinterval"><?php echo $order['prodInfo']['recurring_interval'];?></td>
				<td class="colnet"><?php echo cbprice($order['total_recurring_net']);?></td>
				<td class="colvat"><?php echo cbprice($order['total_recurring_tax']);?><br />(<?php echo $order['prodInfo']['taxrate']; ?>% <?php echo KText::_('VAT');?>)</td>
				<td class="colgross"><?php echo cbprice($order['total_recurring_gross']);?></td>
			</tr>
			<?php
		}
		
		?>
	
	</table>
	<?php 	
	if (count($this->intervals) == 1) {
		?>
		<br />
		
		<table class="invoicetable" cellpadding="3" cellspacing="0" border="1" >
			
			<tr>
				<td class="other"><?php echo KText::_('Total');?></td>
				<td class="colnet"><?php echo cbprice($this->orderDetails->netTotalRecurring); ?></td>
				<td class="colvat"><?php echo cbprice($this->orderDetails->taxTotalRecurring);?></td>
				<td class="colgross"><?php echo cbprice($this->orderDetails->totalRecurring);?></td>
			</tr>
		</table>
		<?php
	}
	
	?>
	
	
	
	<?php
	
}
?>
<h3><?php echo KText::_('Contract Details');?></h3>
<br />
<?php
foreach ($this->orderDetails->orders as $orderitem) {
	
	if ($orderitem['prodInfo']['contract_details']) {
		
		?>
		<h4><?php echo KText::sprintf('Contract details for %s',$orderitem['prodInfo']['title']);?></h4>
		<div>
			<?php echo $orderitem['prodInfo']['contract_details'];?>
		</div>
		<?php
		if (isset($orderitem['elements'])) {
			foreach ($orderitem['elements'] as $element) {
				
				if ($element['contract_details']) {
					?>
					<h4><?php echo KText::sprintf('Contract details for %s',$element['elementTitle'] . ' '.$element['optionTitle']);?></h4>
					<div>
						<?php echo $element['contract_details'];?>
					</div>
					<?php
				}
				
			}
		}
	}
	
	
}
?>


</div></div></div>
</body>
</html>