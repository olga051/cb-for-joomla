<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewContract extends KenedoView {
	
	function display($tpl = NULL) {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$shopDataModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
				
		// Get current date	
		$this->assign('currentdate', KenedoTimeHelper::getFormatted('NOW') );
		
		$this->assignRef('shopData',$shopDataModel->getItem());
		$this->assignRef('orderId',$orderModel->getId());
		$this->assignRef('order',$orderModel->getOrderRecord( $orderModel->getId() ));
		
		$this->assign('intervals',array());
		
		ob_start();
		$this->renderView();
		$result = ob_get_clean();
		
		ConfigboxDomPdfHelper::init();
		
		$result = str_replace('€','EUR ',$result);
		$result = utf8_decode($result);

		$dompdf = new DOMPDF();
		$dompdf->load_html($result);
		$dompdf->render();
		
		$filename = KText::sprintf('Contract for order %s',$orderModel->getId()). '.pdf';
		ob_end_clean();
		$dompdf->stream($filename);
	}
	
}