<?php 
defined('CB_VALID_ENTRY') or die(); 
?>
<div id="com_cbcheckout">
	<div id="view-payment">
	
	<h1 class="componentheading"><?php echo KText::_('Choose your preferred payment option')?></h1>
	
	<form action="<?php echo KLink::getRoute('index.php',true,CONFIGBOX_SECURECHECKOUT);?>" method="post">
		
		<fieldset>
			<legend><?php echo KText::_('Payment information');?></legend>
			<table class="payment-options">
				<tr>
					<th class="payment-option-input">&nbsp;</th>
					<th class="payment-option-title"><?php echo KText::_('Payment Type');?></th>
					<th class="payment-option-description"><?php echo KText::_('Description');?></th>
					<th class="payment-option-price"><?php if ($this->optionsHavePricing) echo KText::_('Price');?></th>
				</tr>
				<?php foreach ($this->options as &$option) { ?>
					<tr>
						<td class="payment-option-input"><input id="radio<?php echo (int)$option->id;?>" type="radio" name="payment_id" value="<?php echo (int)$option->id;?>" <?php echo ($option->id == $this->selected) ? 'checked="checked"':'';?> /></td>
						<td class="payment-option-title"><label for="radio<?php echo $option->id;?>"><?php echo hsc($option->title);?></label></td> 
						<td class="payment-option-description"><?php echo $option->description;?></td> 
						<td class="payment-option-price"><?php echo cbprice( ($this->mode == 'b2b') ? $option->priceNet : $option->priceGross, true, true); ?></td>
					</tr>
				<?php } ?>
			</table>
		</fieldset>
		<div id="buttons">
			<input type="hidden" name="controller" 	value="payment" />
			<input type="hidden" name="option" 		value="com_cbcheckout" />
			<input type="hidden" name="view" 		value="payment" />
			<input type="hidden" name="task" 		value="savePaymentOption" />
			
			<a rel="next nofollow" class="navbutton-medium next" onclick="cbj(this).parents('form').submit()">
				<span class="nav-center"><?php echo KText::_('Save');?></span>
			</a>
			
		</div>
	</form>

	</div>
</div>
