<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewAdminorders extends KenedoView {
	
	function display() {

		$model = KenedoModel::getModel('CbcheckoutModelAdminorders');

		$this->assignRef( 'listingInfo',	$model->getListingInfo() );
		
		$this->assignRef( 'pageTabs',		KenedoViewHelper::getTabItems());
		$this->assignRef( 'pageTitle',		$model->getListingTitle());
		$this->assignRef( 'pageTasks',		$model->getListingTasks());
		
		$items = $model->getOrders();
		$this->assignRef('items',$items);
		
		$statusDropdown = $model->getStatusDropdown();
		$this->assignRef('statusDropdown',$statusDropdown);
		
		$pagination = $model->getPagination();
		$this->assignRef('pagination',$pagination);
		
		$lists['order'] = 				KenedoViewHelper::getUpdatedState('com_cbcheckout.filter_order'				, 'filter_order'		,'a.created'	,'string');
		$lists['order_Dir'] = 			KenedoViewHelper::getUpdatedState('com_cbcheckout.filter_order_Dir'			, 'filter_order_Dir'	,''				,'string');
		$lists['filter_nameorder'] = 	KenedoViewHelper::getUpdatedState('com_cbcheckout.filters.filter_nameorder'	, 'filter_nameorder'	,''				,'string');
		$lists['filter_startdate'] = 	KenedoViewHelper::getUpdatedState('com_cbcheckout.filters.filter_startdate'	, 'filter_startdate'	,''				,'string');
		$lists['filter_enddate'] = 		KenedoViewHelper::getUpdatedState('com_cbcheckout.filters.filter_enddate'	, 'filter_enddate'		,''				,'string');

		$this->assignRef('lists',$lists);
		
		$this->addViewCssClasses();
		
		$this->renderView();
		
	}
	
}
