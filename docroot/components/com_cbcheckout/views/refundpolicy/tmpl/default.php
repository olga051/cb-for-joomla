<?php 
defined('CB_VALID_ENTRY') or die(); 
?>

<div id="com_cbcheckout"><div id="view-refundpolicy">

<?php if (KRequest::getKeyword('tmpl') == 'component') { ?>
	<div style="margin:10px">
<?php } ?>

<h2 class="componentheading"><?php echo hsc(KText::_('Refund Policy'));?></h2>

<?php echo $this->refundPolicy;?>

<?php if (KRequest::getKeyword('tmpl') == 'component') { ?>
	</div>
<?php } ?>

</div></div>