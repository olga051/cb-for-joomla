<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewRefundpolicy extends KenedoView {
	
	function display(){
		
		$shopdataModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		$shopData = $shopdataModel->getItem();
		$refundPolicy = $shopData->refundpolicy;
		$this->assignRef('refundPolicy',$refundPolicy);
		
		$this->renderView();
	}
}