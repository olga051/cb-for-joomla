<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewCheckoutpayment extends KenedoView {
	
	function display(){
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
		$orderRecord = $orderModel->getOrderRecord($orderId);
		
		$this->assign('mode', $orderRecord->groupData->b2b_mode ? 'b2b' : 'b2c' );
		
		$paymentOptions = $orderModel->getOrderRecordPaymentOptions($orderRecord);
		$selected = $orderRecord->payment_id;
		
		if ($selected == 0) {
			$selected = $paymentOptions[0]->id;
		}
		
		$optionsHavePricing = false;
		foreach ($paymentOptions as &$option) {
			if ($option->basePriceNet) {
				$optionsHavePricing = true;
			}
		}
		
		$this->assignRef('optionsHavePricing',$optionsHavePricing);
		$this->assignRef('selected',$selected);
		$this->assignRef('options',$paymentOptions);
		
		$this->renderView();
	}
}