<?php 
defined('CB_VALID_ENTRY') or die();

// Helper function to display class="required" for fitting fields
function requiredClass($fields,$field,$action) {
	$key = 'require_'.$action;
	if ($fields[$field]->$key) return 'class="order-address-field required field-'.$field.'"';
	else return 'class="order-address-field field-'.$field.'"';
}
$showKey = 'show_'.$this->type;
?>
<div id="com_cbcheckout">
<div id="view-user">
<div id="layout-editprofile">

	<h1 class="componentheading"><?php echo KText::_('Your delivery and billing information')?></h1>
	
	<form  id="order-address-form" action="<?php echo KLink::getRoute('index.php',true,CONFIGBOX_SECURECHECKOUT);?>" method="post">
		
		<fieldset id="billingaddress">
			<legend><?php echo KText::_('Billing Address');?></legend>
			<table class="order-address-fields">
				<tbody>
				
				<?php if ($this->userFields['billingcompanyname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingcompanyname',$this->type);?>>
						<td class="key"><?php echo KText::_('Company');?></td>
						<td><input class="textfield" type="text" name="billingcompanyname" value="<?php echo hsc($this->customer->billingcompanyname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingsalutation_id']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingsalutation_id',$this->type);?>>
						<td class="key"><?php echo KText::_('Salutation');?></td>
						<td><?php echo CbcheckoutUserHelper::getSalutationDropdown('billingsalutation_id',$this->customer->billingsalutation_id);?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingfirstname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingfirstname',$this->type);?>>
						<td class="key"><?php echo KText::_('First Name');?></td>
						<td><input class="textfield" type="text" name="billingfirstname" value="<?php echo hsc($this->customer->billingfirstname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billinglastname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billinglastname',$this->type);?>>
						<td class="key"><?php echo KText::_('Last Name');?></td>
						<td><input class="textfield" type="text" name="billinglastname" value="<?php echo hsc($this->customer->billinglastname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingaddress1']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingaddress1',$this->type);?>>
						<td class="key"><?php echo KText::_('Address 1');?></td>
						<td><input class="textfield" type="text" name="billingaddress1" value="<?php echo hsc($this->customer->billingaddress1);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingaddress2']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingaddress2',$this->type);?>>
						<td class="key"><?php echo KText::_('Address 2');?></td>
						<td><input class="textfield" type="text" name="billingaddress2" value="<?php echo hsc($this->customer->billingaddress2);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingzipcode']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingzipcode',$this->type);?>>
						<td class="key"><?php echo KText::_('ZIP Code');?></td>
						<td><input class="textfield" type="text" name="billingzipcode" value="<?php echo hsc($this->customer->billingzipcode);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingcountry']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingcountry',$this->type);?>>
						<td class="key"><?php echo KText::_('Country');?></td>
						<td><?php echo CbcheckoutCountryHelper::createCountrySelect('billingcountry',$this->customer->billingcountry,KText::_('Select Country'),'billingstate'); ?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingstate']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'state',$this->type);?>>
						<td class="key"><?php echo KText::_('State');?></td>
						<td><?php echo CbcheckoutCountryHelper::createStateSelect('billingstate',$this->customer->billingstate,$this->customer->billingcountry, NULL, 'billingcounty_id');?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingcounty_id']->$showKey) { ?>
					<tr <?php echo requiredClass($this->userFields,'billingcounty_id',$this->type);?> style="display:<?php echo(CbcheckoutCountryHelper::hasCounties($this->customer->billingstate)) ? 'block':'none'; ?>">
						<td class="key"><?php echo KText::_('County');?></td>
						<td><?php echo CbcheckoutCountryHelper::createCountySelect('billingcounty_id', $this->customer->billingcounty_id, $this->customer->billingstate, KText::_('Select County'), 'billingcity');?></td>
					</tr>
					<?php } ?>
					
					<?php if ($this->userFields['billingcity']->$showKey) { ?>
					<tr <?php echo requiredClass($this->userFields,'billingcity',$this->type);?>>
						<td class="key"><?php echo KText::_('City');?></td>
						<td class="form-field">
							<?php echo CbcheckoutCountryHelper::getCityInputField('billingcity', $this->customer->billingcity_id, KText::_('Select City'), $this->customer->billingcounty_id, $this->customer->billingcity );?>
						</td>
					</tr>
					<?php } ?>
					
					<?php if ($this->userFields['billingemail']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingemail',$this->type);?>>
						<td class="key"><?php echo KText::_('Email');?></td>
						<td><input class="textfield" type="text" name="billingemail" value="<?php echo hsc($this->customer->billingemail);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingphone']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingphone',$this->type);?>>
						<td class="key"><?php echo KText::_('Phone');?></td>
						<td><input class="textfield" type="text" name="billingphone" value="<?php echo hsc($this->customer->billingphone);?>" /></td>
					</tr>
					<?php endif;?>
					
				</tbody>					
			</table>
		</fieldset>
		
		<fieldset id="deliveryaddress"<?php echo ($this->customer->samedelivery) ? ' style="display:none"':'';?>>
			<legend><?php echo KText::_('Shipping Address');?></legend>
			<table>
				<tbody>
					<?php if ($this->userFields['companyname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'companyname',$this->type);?>>
						<td class="key"><?php echo KText::_('Company');?></td>
						<td><input class="textfield" type="text" name="companyname" value="<?php echo hsc($this->customer->companyname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['salutation_id']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'salutation_id',$this->type);?>>
						<td class="key"><?php echo KText::_('Salutation');?></td>
						<td><?php echo CbcheckoutUserHelper::getSalutationDropdown('salutation_id',$this->customer->salutation_id);?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['firstname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'firstname',$this->type);?>>
						<td class="key"><?php echo KText::_('First Name');?></td>
						<td><input class="textfield" type="text" name="firstname" value="<?php echo hsc($this->customer->firstname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['lastname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'lastname',$this->type);?>>
						<td class="key"><?php echo KText::_('Last Name');?></td>
						<td><input class="textfield" type="text" name="lastname" value="<?php echo hsc($this->customer->lastname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['address1']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'address1',$this->type);?>>
						<td class="key"><?php echo KText::_('Address 1');?></td>
						<td><input class="textfield" type="text" name="address1" value="<?php echo hsc($this->customer->address1);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['address2']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'address2',$this->type);?>>
						<td class="key"><?php echo KText::_('Address 2');?></td>
						<td><input class="textfield" type="text" name="address2" value="<?php echo hsc($this->customer->address2);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['zipcode']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'zipcode',$this->type);?>>
						<td class="key"><?php echo KText::_('ZIP Code');?></td>
						<td><input class="textfield" type="text" name="zipcode" value="<?php echo hsc($this->customer->zipcode);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['country']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'country',$this->type);?>>
						<td class="key"><?php echo KText::_('Country');?></td>
						<td><?php echo CbcheckoutCountryHelper::createCountrySelect('country',$this->customer->country,KText::_('Select Country'),'state'); ?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['state']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'state',$this->type);?>>
						<td class="key"><?php echo KText::_('State');?></td>
						<td><?php echo CbcheckoutCountryHelper::createStateSelect('state',$this->customer->state,$this->customer->country, NULL, 'county_id');?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['county_id']->$showKey) { ?>
					<tr <?php echo requiredClass($this->userFields,'county_id',$this->type);?> style="display:<?php echo(CbcheckoutCountryHelper::hasCounties($this->customer->state)) ? 'block':'none'; ?>">
						<td class="key"><?php echo KText::_('County');?></td>
						<td><?php echo CbcheckoutCountryHelper::createCountySelect('county_id', $this->customer->county_id, $this->customer->state, KText::_('Select County'), 'city');?></td>
					</tr>
					<?php } ?>
					
					<?php if ($this->userFields['city']->$showKey) { ?>
					<tr <?php echo requiredClass($this->userFields,'city',$this->type);?>>
						<td class="key"><?php echo KText::_('City');?></td>
						<td class="form-field">
							<?php echo CbcheckoutCountryHelper::getCityInputField('city', $this->customer->city_id, KText::_('Select City'), $this->customer->county_id, $this->customer->city );?>
						</td>
					</div>
					<?php } ?>
					
					<?php if ($this->userFields['email']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'email',$this->type);?>>
						<td class="key"><?php echo KText::_('Email');?></td>
						<td><input class="textfield" type="text" name="email" value="<?php echo hsc($this->customer->email);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['phone']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'phone',$this->type);?>>
						<td class="key"><?php echo KText::_('Phone');?></td>
						<td><input class="textfield" type="text" name="phone" value="<?php echo hsc($this->customer->phone);?>" /></td>
					</tr>
					<?php endif;?>
				</tbody>
			</table>
		</fieldset>
		
		<?php if ($this->userFields['samedelivery']->$showKey or $this->userFields['vatin']->$showKey or $this->userFields['language_tag']->$showKey or $this->userFields['newsletter']->$showKey) { ?>
			
			<div class="clear"></div>
			
			<fieldset id="otherinfo">
				<legend><?php echo KText::_('Other');?></legend>
				<table class="order-address-fields">
				
					<?php if ($this->userFields['samedelivery']->$showKey) :?>
						<tr <?php echo requiredClass($this->userFields,'samedelivery',$this->type);?>>
							<td class="key"><?php echo KText::_('Ship to same address');?></td>
							<td>
								<input type="radio" <?php echo ($this->customer->samedelivery != 0)? 'checked="checked"':'';?> id="delivery-yes" name="samedelivery" value="1" />
								<label for="delivery-yes"><?php echo KText::_('CBYES');?></label>
								<input type="radio" <?php echo ($this->customer->samedelivery != 1)? 'checked="checked"':'';?>id="delivery-no" name="samedelivery" value="0" />
								<label for="delivery-no"><?php echo KText::_('CBNO');?></label>
							</td>
						</tr>
					<?php endif;?>
							
					<?php if ($this->userFields['vatin']->$showKey) :?>
						<tr <?php echo requiredClass($this->userFields,'vatin',$this->type);?>>
							<td class="key"><?php echo KText::_('VAT IN');?></td>
							<td><input type="text" name="vatin" value="<?php echo hsc($this->customer->vatin);?>" /></td>
						</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['language_tag']->$showKey) :?>
						<tr <?php echo requiredClass($this->userFields,'language_tag',$this->type);?>>
							<td class="key"><?php echo KText::_('Language');?></td>
							<td><?php echo ConfigboxLanguageHelper::getLangSelect($this->customer->language_tag,'language_tag'); ?></td>
						</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['newsletter']->$showKey) :?>
						<tr <?php echo requiredClass($this->userFields,'newsletter',$this->type);?>>
							<td class="key"><?php echo KText::_('Newsletter');?></td>
							<td>
								<input type="radio" <?php echo ($this->customer->newsletter == 1)? 'checked="checked"':'';?> id="newsletter-yes" name="newsletter" value="1" />
								<label for="newsletter-yes"><?php echo KText::_('CBYES');?></label>
								<input type="radio" <?php echo ($this->customer->newsletter != 1)? 'checked="checked"':'';?> id="newsletter-no" name="newsletter" value="0" />
								<label for="newsletter-no"><?php echo KText::_('CBNO');?></label>
							</td>
						</tr>
					<?php endif;?>
					
				</table>
			</fieldset>
		<?php } ?>
		<div class="clear"></div>
		
		<div class="hidden-fields">
			<input type="hidden" name="option" 			value="com_cbcheckout" />
			<input type="hidden" name="controller" 		value="user" />
			<input type="hidden" name="task" 			value="saveUser" />
			<input type="hidden" name="type" 			value="profile" />
			<input type="hidden" name="success_message" value="1" />
			<input type="hidden" name="return_failure" 	value="<?php echo urlencode( KLink::getRoute('index.php?option=com_cbcheckout&view=user&layout=editprofile', false, CONFIGBOX_SECURECHECKOUT) );?>" />
			<input type="hidden" name="return_success" 	value="<?php echo urlencode( KLink::getRoute('index.php?option=com_cbcheckout&view=user', false, CONFIGBOX_SECURECHECKOUT) );?>" />
		</div>
		
		<div id="buttons">	
			<a rel="nofollow" class="navbutton-medium next trigger-send-address-form">
				<span class="nav-center"><?php echo KText::_('Save');?></span>
			</a>
			<a rel="nofollow" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=user');?>" class="back navbutton-medium">
				<span class="nav-center"><?php echo KText::_('Close');?></span>
			</a>
		</div>
	</form>
	<div class="clear"></div>
</div>
</div>
</div>

