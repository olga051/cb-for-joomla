<?php 
defined('CB_VALID_ENTRY') or die(); 
?>
<div id="com_cbcheckout">
<div id="view-user">
<div id="layout-login">

<h1 class="componentheading"><?php echo KText::_('Login');?></h1>	

<form method="post" action="<?php echo KLink::getRoute('index.php',true,CONFIGBOX_SECURECHECKOUT);?>">
	
	<table>
		<tr>
			<td><label for="cbcheckout_email"><?php echo KText::_('Email Address');?></label></td>
			<td><input id="cbcheckout_email" type="text" name="email" value="" /></td>
		</tr>
		<tr>
			<td><label for="cbcheckout_email"><?php echo KText::_('Password');?></label></td>
			<td><input id="cbcheckout_email" type="password" name="password" value="" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" name="submitform" value="<?php echo KText::_('Login');?>" /></td>
		</tr>
	</table>
	
	<p><?php echo KText::_('No account yet?');?>&nbsp;<a href="<?php echo KLink::getRoute('index.php?view=user&layout=register');?>"><?php echo KText::_('Register');?></a></p>

	<div class="hidden-fields">
		<input type="hidden" name="option" 			value="com_cbcheckout" />
		<input type="hidden" name="controller" 		value="user" />
		<input type="hidden" name="task" 			value="loginUser" />
		<input type="hidden" name="return_failure" 	value="<?php echo urlencode(KLink::getRoute('index.php?option=com_cbcheckout&view=user&layout=login',false, CONFIGBOX_SECURECHECKOUT));?>" />
		<input type="hidden" name="return_success" 	value="<?php echo urlencode(KLink::getRoute('index.php?option=com_cbcheckout&view=user', false, CONFIGBOX_SECURECHECKOUT));?>" />
	</div>
</form>

<script type="text/javascript">
	cbj('#cbcheckout_email').focus();
</script>

</div>
</div>
</div>