<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewUser extends KenedoView {
	
	function display(){
		
		KenedoPlatform::p()->setMetaTag('robots','noindex');
		
		$orderModel  	= KenedoModel::getModel('CbcheckoutModelOrder');
		$ordersModel  	= KenedoModel::getModel('CbcheckoutModelAdminorders');
		
		$customer = CbcheckoutUserHelper::getUser();
		$this->assignRef('customer',$customer);
		
		$orders = $ordersModel->getUserOrders();
		$orderStatuses = $orderModel->getOrderStatuses();
		$this->assignRef('orderStatuses',$orderStatuses);
		
		$orderPageStatuses = array(2,3,4,5,6,7,11,13,14);
		
		foreach ($orders as $order) {
			$order->statusString = $orderStatuses[$order->status]->title;
			$order->toUserOrders = (in_array($order->status,$orderPageStatuses));
		}
		
		$this->assignRef('orders',$orders);
		
		$downloads = $ordersModel->getDownloads();
		$this->assignRef('downloads',$downloads);
		
		$user = CbcheckoutUserHelper::getUser();
		$userFields = CbcheckoutUserHelper::getUserFields($user->group_id);
		$this->assignRef('userFields',$userFields);
			
		KenedoPlatform::p()->addScriptDeclaration("var userFields = ".json_encode($userFields).";");
		
		$type = 'profile';
		$this->assignRef('type',$type);
		
		$template = KRequest::getKeyword('layout','default');
		
		$this->renderView( $template );
		
	}
}
