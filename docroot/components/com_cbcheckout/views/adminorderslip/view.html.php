<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewAdminorderslip extends KenedoView {
	
	function display() {
		
		$orderId = KRequest::getInt('order_id');
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderRecord = $orderModel->getOrderRecord($orderId);
		$this->assignRef('orderRecord', $orderRecord);
		
		$shopDataModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		$shopDataModel->_setId($orderRecord->store_id);
		$shopData = $shopDataModel->getItem();
		$this->assignRef('shopData', $shopData);
		
		$maxWidth = 400;
		$maxHeight = 60;
		
		$filePath = KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'data'.DS.'shoplogo'.DS. $this->shopData->shoplogo;
		if ($filePath) {
				
			$this->assign('useShopLogo',true);
			$this->assign('shopLogoUrl', KPATH_ROOT_URL.'components/com_cbcheckout/data/shoplogo'.DS. $this->shopData->shoplogo);
				
			$image = new ConfigboxImageResizer($filePath);
				
			if ($image->width > $maxWidth || $image->height > $maxHeight) {
				$dimensions = $image->getDimensions($maxWidth, $maxHeight, 'containment');
				$this->assign('shopLogoWidth',	intval($dimensions['optimalWidth']));
				$this->assign('shopLogoHeight',	intval($dimensions['optimalHeight']));
			}
			else {
				$this->assign('shopLogoWidth',	$image->width);
				$this->assign('shopLogoHeight',	$image->height);
			}
				
		}
		else {
			$this->assign('useShopLogo',false);
			$this->assign('shopLogoHeight',0);
			$this->assign('shopLogoWidth',0);
		}
		
		$this->renderView();
	}
	
}
