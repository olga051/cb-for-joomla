<?php 
defined('CB_VALID_ENTRY') or die(); 
$showPricing = ConfigboxPermissionHelper::canSeePricing();
$b2b = $this->orderRecord->groupData->b2b_mode;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link type="text/css" rel="stylesheet" href="<?php echo KPATH_ROOT_URL;?>components/com_configbox/assets/css/pdf-quotation.css" />
<link type="text/css" rel="stylesheet" href="<?php echo KPATH_ROOT_URL;?>components/com_configbox/data/customization/style_overrides/css/style_overrides.css" />
<title><?php echo KText::sprintf( 'Quotation for order %s', intval($this->orderRecord->id) );?></title>
</head>
<body>

<script type="text/php">

if (isset($pdf)) {

$footer = $pdf->open_object();

$h = $pdf->get_height();
$w = $pdf->get_width();

$fontFace = Font_Metrics::get_font("Helvetica", "normal");
$fontColor = array(0,0,0);
$fontSize = 8;
$footerBackgroundColor = array(238/255,238/255,238/255);

// Header line
$pdf->line(43, 100, $w - 43, 100, array(238/255,238/255,238/255), 1);

// Fold lines
$pdf->line(0, $h / 2, 10, $h /2 , array(238/255,238/255,238/255), 1);
$pdf->line(0, $h / 3 * 1, 10, $h / 3 * 1 , array(238/255,238/255,238/255), 1);
$pdf->line(0, $h / 3 * 2, 10, $h / 3 * 2 , array(238/255,238/255,238/255), 1);


$offsetHeaderTextH = $w - 100;
$offsetHeaderTextV = (100 - <?php echo intval($this->shopLogoHeight);?>) / 2;

// Shop logo
<?php if ($this->useShopLogo) { ?>
	$pdf->image("<?php echo $this->shopLogoUrl;?>", $w - <?php echo intval($this->shopLogoWidth);?> - 43, $offsetHeaderTextV, <?php echo intval($this->shopLogoWidth);?>, <?php echo intval($this->shopLogoHeight);?>);
<?php } ?>

$offsetFooter = $h - 80;

// Pagination
$textWidth = Font_Metrics::get_text_width('<?php echo KText::_('PAGINATION_PAGE');?> 4 <?php echo KText::_('PAGINATION_OF');?> 4', $fontFace, $fontSize);
$pdf->page_text(($w/2) - ($textWidth / 2) , $offsetFooter - 16, '<?php echo KText::_('PAGINATION_PAGE');?> {PAGE_NUM} <?php echo KText::_('PAGINATION_OF');?> {PAGE_COUNT}', $fontFace, $fontSize, $fontColor);


// Footer background
$pdf->filled_rectangle(0, $h-80, $w, 80, $footerBackgroundColor);

$offsetLineVertical = $offsetFooter + 15;
$offsetLineHorizontal = 43;	

$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo hsc($this->shopData->shopname);?>", $fontFace, $fontSize, $fontColor);
$offsetLineVertical += 10;

$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo hsc($this->shopData->shopaddress1);?>", $fontFace, $fontSize, $fontColor);
$offsetLineVertical += 10;

$var = "<?php echo hsc($this->shopData->shopaddress2);?>";
if ($var) {
	$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo hsc($this->shopData->shopaddress2);?>", $fontFace, $fontSize, $fontColor);
	$offsetLineVertical += 10;
}

$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo hsc($this->shopData->shopzipcode . ' '. $this->shopData->shopcity);?>", $fontFace, $fontSize, $fontColor);
$offsetLineVertical += 10;

$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo hsc($this->shopData->shopcountry);?>", $fontFace, $fontSize, $fontColor);
$offsetLineVertical += 10;


$offsetLineVertical = $offsetFooter + 15;
$offsetLineHorizontal = 200;
$valueOffset = 50;

$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo KText::_('Email');?>:", $fontFace, $fontSize, $fontColor);
$pdf->text($offsetLineHorizontal + $valueOffset, $offsetLineVertical, "<?php echo hsc($this->shopData->shopemailsales);?>", $fontFace, $fontSize, $fontColor);
$offsetLineVertical += 10;

$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo KText::_('Website');?>:", $fontFace, $fontSize, $fontColor);
$pdf->text($offsetLineHorizontal + $valueOffset, $offsetLineVertical, "<?php echo hsc($this->shopData->shopwebsite);?>", $fontFace, $fontSize, $fontColor);
$offsetLineVertical += 10;

$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo KText::_('Phone');?>:", $fontFace, $fontSize, $fontColor);
$pdf->text($offsetLineHorizontal + $valueOffset, $offsetLineVertical, "<?php echo hsc($this->shopData->shopphonesales);?>", $fontFace, $fontSize, $fontColor);
$offsetLineVertical += 10;

$var = "<?php echo hsc($this->shopData->shopfax);?>";
if ($var) {
	$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo KText::_('Fax');?>:", $fontFace, $fontSize, $fontColor);
	$pdf->text($offsetLineHorizontal + $valueOffset, $offsetLineVertical, "<?php echo hsc($this->shopData->shopfax);?>", $fontFace, $fontSize, $fontColor);
	$offsetLineVertical += 10;
}


$offsetLineVertical = $offsetFooter + 15;
$offsetLineHorizontal = 400;
$valueOffset = 95;

$var = "<?php echo hsc($this->shopData->shopowner);?>";
if ($var) {
	$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo KText::_('Company owner');?>:", $fontFace, $fontSize, $fontColor);
	$pdf->text($offsetLineHorizontal + $valueOffset, $offsetLineVertical, "<?php echo hsc($this->shopData->shopowner);?>", $fontFace, $fontSize, $fontColor);
	$offsetLineVertical += 10;
}

$var = "<?php echo hsc($this->shopData->shopuid);?>";
if ($var) {
	$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo KText::_('VAT IN');?>:", $fontFace, $fontSize, $fontColor);
	$pdf->text($offsetLineHorizontal + $valueOffset, $offsetLineVertical, "<?php echo hsc($this->shopData->shopuid);?>", $fontFace, $fontSize, $fontColor);
	$offsetLineVertical += 10;
}

$var = "<?php echo hsc($this->shopData->shoplegalvenue);?>";
if ($var) {
	$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo KText::_('Legal Venue');?>:", $fontFace, $fontSize, $fontColor);
	$pdf->text($offsetLineHorizontal + $valueOffset, $offsetLineVertical, "<?php echo hsc($this->shopData->shoplegalvenue);?>", $fontFace, $fontSize, $fontColor);
	$offsetLineVertical += 10;
}

$var = "<?php echo hsc($this->shopData->shopcomreg);?>";
if ($var) {
	$pdf->text($offsetLineHorizontal, $offsetLineVertical, "<?php echo KText::_('Company reg. no.');?>:", $fontFace, $fontSize, $fontColor);
	$pdf->text($offsetLineHorizontal + $valueOffset, $offsetLineVertical, "<?php echo hsc($this->shopData->shopcomreg);?>", $fontFace, $fontSize, $fontColor);
	$offsetLineVertical += 10;
}

$pdf->close_object();
$pdf->add_object($footer, "all");
}

</script>

<div id="com_cbcheckout"><div id="view-quotation"><div id="view-slip"> 

<table class="layout-table customer-information">
	<tr>
		<td class="left-column">
			<div class="wrapper">
				<div class="company-name"><?php echo hsc($this->orderRecord->orderAddress->billingcompanyname);?></div>
				<div class="name"><span class="salutation"><?php echo hsc($this->orderRecord->orderAddress->billingsalutation);?></span> <span class="firstname"><?php echo hsc($this->orderRecord->orderAddress->billingfirstname) . ' '. hsc($this->orderRecord->orderAddress->billinglastname);?></span></div>
				<div class="address1"><?php echo hsc($this->orderRecord->orderAddress->billingaddress1);?></div>
				<?php if ($this->orderRecord->orderAddress->billingaddress2) { ?>
					<div class="address2"><?php echo hsc($this->orderRecord->orderAddress->billingaddress2);?></div>
				<?php } ?>
				<div class="city"><span class="zipcode"><?php echo hsc($this->orderRecord->orderAddress->billingzipcode);?></span> <span class="city"><?php echo hsc($this->orderRecord->orderAddress->billingcity);?></span></div>
				<div class="country"><?php echo hsc($this->orderRecord->orderAddress->billingcountryname);?></div>
			</div>
		</td>
		<td class="right-column">
			<div class="wrapper">
				
				<table align="right">
					<tr class="order-id">
						<td class="key"><?php echo KText::_('Order no.')?>:</td>
						<td class="value"><?php echo intval($this->orderRecord->id);?></td>
					</tr>
					<tr class="order-date">
						<td class="key"><?php echo KText::_('Date')?>:</td>
						<td class="value"><?php echo hsc( KenedoTimeHelper::getFormatted('NOW', KText::_('CONFIGBOX_DATEFORMAT_PHP_DATE')) );?></td>
					</tr>
					<tr class="customer-email">
						<td class="key"><?php echo KText::_('Email')?>:</td>
						<td class="value"><?php echo hsc($this->orderRecord->orderAddress->billingemail);?></td>
					</tr>
					<?php if ($this->orderRecord->orderAddress->billingphone) { ?>
						<tr class="customer-phone">
							<td class="key"><?php echo KText::_('Phone')?>:</td>
							<td class="value"><?php echo hsc($this->orderRecord->orderAddress->billingphone);?></td>
						</tr>
					<?php } ?>
					<?php if ($this->orderRecord->store_id) { ?>
						<tr class="store-id">
							<td class="key"><?php echo KText::_('Store ID')?>:</td>
							<td class="value"><?php echo hsc($this->orderRecord->store_id);?></td>
						</tr>
					<?php } ?>
				</table>
				
			</div>
		</td>
	</tr>
</table>


<h1 class="document-title"><?php echo KText::_( 'Manufacturing Slip' );?></h1>

<div class="products-overview">
	<h2><?php echo KText::_('Product Configuration');?></h2>
	<div class="positions">
		<?php
		foreach ($this->orderRecord->positions as $position) {
			echo CbcheckoutPositionHelper::getPositionHtml($this->orderRecord, $position, 'quotation');
		}
		?>
		<div class="clear"></div>
	</div>
</div>



</div></div></div></div>
</body>
</html>
