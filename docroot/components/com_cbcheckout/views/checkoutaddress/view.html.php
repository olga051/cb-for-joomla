<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewCheckoutaddress extends KenedoView {
	
	function display() {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
		$orderRecord = $orderModel->getOrderRecord($orderId);
		$this->assignRef('orderRecord', $orderRecord);
		$this->renderView();
	}
}