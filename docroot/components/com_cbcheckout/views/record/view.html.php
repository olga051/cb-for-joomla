<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewRecord extends KenedoView {
	
	function display() {
		
		if (empty($this->orderRecord)) {
			$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
			$orderId = $orderModel->getId();
			$orderRecord = $orderModel->getOrderRecord($orderId);
			$this->assign('showProductDetails',true);
			$this->assign('hideSkus',true);
			$this->assignRef('orderRecord',$orderRecord);
		}
		
		$this->renderView();
		
	}
	
}