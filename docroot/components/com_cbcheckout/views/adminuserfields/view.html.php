<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewAdminuserfields extends KenedoView {
	
	function display($tpl = NULL) {
		
		$this->assignRef('pageTabs',KenedoViewHelper::getTabItems());
		
		$model = KenedoModel::getModel('CbcheckoutModelAdminuserfields');
		
		$this->assignRef( 'pageTitle',		$model->getDetailsTitle());
		$this->assignRef( 'pageTasks',		$model->getDetailsTasks());
		
		$userFields = $model->getItems();
		$userFieldTranslations = CbcheckoutUserHelper::getUserFieldTranslations();
		
		foreach ($userFields as $key=>$userField) {
			$userField->title = $userFieldTranslations[$key];
		}
		
		usort($userFields,array('CbcheckoutViewAdminuserfields','sortUserFields'));
		
		$this->assignRef('userFields', $userFields );
		$this->assignRef('userFieldTranslations', $userFieldTranslations);
		
		$this->addViewCssClasses();
		
		$this->renderView();
		
	}
	
	static function sortUserFields($a, $b) {
		return strcmp($a->title, $b->title);
	}
	
}
