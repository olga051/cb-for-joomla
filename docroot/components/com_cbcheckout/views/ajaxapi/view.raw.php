<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewAjaxapi extends KenedoView {
	
	function display($tpl = NULL) {
		
		$task = strtolower(KRequest::getKeyword('task',''));
		
		switch ($task) {
			
			case 'getstateselectoptions':
				$data = CbcheckoutCountryHelper::getStateSelectOptions(KRequest::getInt('country_id'));
				$this->assign('data',$data);
				$this->assign('selectedId', KRequest::getString('selected_id'));
				$template = 'stateselectoptions';
				$this->renderView($template);
				break;
				
			case 'getcountyselectoptions':
				$data = CbcheckoutCountryHelper::getCountySelectOptions(KRequest::getInt('state_id'));
				$this->assign('data',$data);
				$this->assign('selectedId', KRequest::getString('selected_id'));
				$template = 'stateselectoptions';
				$this->renderView($template);
				break;
				
			case 'getcityinput':
				echo CbcheckoutCountryHelper::getCityInputField( KRequest::getString('name'), KRequest::getString('selected_id'), KText::_('Select City'), KRequest::getInt('county_id'), KRequest::getString('city_name') );
				break;
			
		}
		
		
	}
	
}
