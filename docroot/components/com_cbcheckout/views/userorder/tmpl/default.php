<?php 
defined('CB_VALID_ENTRY') or die(); 
?>
<div id="com_cbcheckout"><div id="view-userorder">
	
	<h1 class="componentheading"><?php echo KText::_('Your order')?></h1>
	
	<?php if (CONFIGBOX_ENABLE_FILE_UPLOADS) { ?>
		<div class="order-files">
			<h2><?php echo KText::_('Files');?></h2>
		
			<ul class="order-file-list">
				<?php foreach ($this->orderFiles as $upload) { ?>
					<li id="file-<?php echo (int)$upload->id;?>" class="order-file">
						<a title="<?php echo hsc($upload->comment);?>" class="file-name"><?php echo hsc($upload->file);?></a>
						<?php echo ($upload->comment) ? KenedoHtml::getTooltip('<a class="fa fa-info-circle"></a>', hsc($upload->comment)) : '';?>
						<a class="file-delete"><?php echo KText::_('Delete');?></a>
					</li>
				<?php } ?>
				<li id="order-file-blueprint" class="order-file"><a class="file-name"></a> <a class="comment-popup-trigger"></a> <a class="file-delete"><?php echo KText::_('Delete');?></a></li>
			</ul>
			
			
			<div class="file-upload">
				
				<a class="upload-file-toggle button-frontend-small"><?php echo KText::_('Upload a new file');?></a>
				
				<div class="upload-file-content">
					
					<b><?php echo KText::_('Upload a new file');?></b>
					<form enctype="multipart/form-data" method="post" id="file-upload-form" target="file-upload-iframe">
						<div>

							<?php if ($this->fileUploadHasLimitations) { ?>
								<div class="file-limitations-extensions">
									<?php echo KText::sprintf('Allowed file extensions are %s.', hsc(implode(',',$this->allowedFileExtensions)) );?>
								</div>
							<?php } ?>

							<?php if ($this->fileUploadMaximumSize) { ?>
								<div class="file-limitations-size">
									<?php echo KText::sprintf('Maximum file size is %s MB.', hsc($this->fileUploadMaximumSize) );?>
								</div>
							<?php } ?>

							<div class="form-item form-item-file">
								<label for="orderFile"><?php echo KText::_('File');?></label>
								<input name="orderFile" id="orderFile" type="file" />
							</div>
							
							<div class="form-item form-item-comment">
								<label for="comment"><?php echo KText::_('Comment');?></label>
								<textarea name="comment" id="comment" rows="4" cols="20" class="comment-field"></textarea>
							</div>
							
							<div class="form-controls">
								<a class="form-cancel button-frontend-small"><?php echo KText::_('Close');?></a>
								<a class="form-send button-frontend-small"><?php echo KText::_('Send');?></a>
							</div>
							
							<div class="hidden-fields">
								<input type="submit" name="submit-buttom" value="submit" />
								<input type="hidden" name="option" value="com_cbcheckout" />
								<input type="hidden" name="controller" value="userorder" />
								<input type="hidden" name="task" value="addOrderFile" />
								<input type="hidden" name="format" value="raw" />
								<input type="hidden" name="orderId" value="<?php echo (int)$this->orderRecord->id;?>" />
								</div>
						</div>
					</form>
					
				</div>
			</div>
			
			<iframe style="display:none" src="about:blank" class="file-upload-iframe" id="file-upload-iframe" name="file-upload-iframe"></iframe>
			
			
		</div>
	<?php } ?>

	<div class="order-status">
		<h2><?php echo KText::_('Order Status');?></h2>
		<p><?php echo KText::sprintf('Status of your order is %s.','<b>'.$this->orderRecord->statusString.'</b>');?></p>
	</div>
	
	
	<div class="order-address">
		<h2><?php echo KText::_('Address');?></h2>
		<div class="addresses">
			<div class="order-address-billing">
				<h3><?php echo KText::_('Billing Address');?></h3>
				<table>
					<?php if ($this->orderRecord->orderAddress->billingcompanyname) { ?>
					<tr>
						<td class="key"><?php echo KText::_('Company');?>:</td>
						<td><?php echo hsc($this->orderRecord->orderAddress->billingcompanyname);?></td>
					</tr>
					<?php } ?>
					<tr>
						<td class="key"><?php echo KText::_('Name');?>:</td>
						<td><?php echo hsc($this->orderRecord->orderAddress->billingfirstname);?> <?php echo hsc($this->orderRecord->orderAddress->billinglastname);?></td>
					</tr>
					<tr>
						<td class="key"><?php echo KText::_('Address');?>:</td>
						<td><?php echo hsc($this->orderRecord->orderAddress->billingaddress1);?></td>
					</tr>
					<?php if ($this->orderRecord->orderAddress->billingaddress2) { ?>
					<tr>
						<td class="key"><?php echo KText::_('Address 2');?>:</td>
						<td><?php echo hsc($this->orderRecord->orderAddress->billingaddress2);?></td>
					</tr>
					<?php } ?>
					<tr>
						<td class="key"><?php echo KText::_('ZIP Code');?>:</td>
						<td><?php echo hsc($this->orderRecord->orderAddress->billingzipcode);?></td>
					</tr>
					<tr>
						<td class="key"><?php echo KText::_('City');?>:</td>
						<td><?php echo hsc($this->orderRecord->orderAddress->billingcity);?></td>
					</tr>
					<tr>
						<td class="key"><?php echo KText::_('Country');?>:</td>
						<td><?php echo hsc($this->orderRecord->orderAddress->billingcountryname);?></td>
					</tr>
				</table>
			</div>
		
			
			<?php if ($this->orderRecord->orderAddress->samedelivery == 0) { ?>
				<div class="order-address-delivery">
					<h3><?php echo KText::_('Delivery Address');?></h3>
					<table>
						<?php if ($this->orderRecord->orderAddress->companyname) { ?>
						<tr>
							<td class="key"><?php echo KText::_('Company');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->companyname);?></td>
						</tr>
						<?php } ?>
						<tr>
							<td class="key"><?php echo KText::_('Name');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->firstname);?> <?php echo hsc($this->orderRecord->orderAddress->lastname);?></td>
						</tr>
						<tr>
							<td class="key"><?php echo KText::_('Address');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->address1);?></td>
						</tr>
						<?php if ($this->orderRecord->orderAddress->address2) { ?>
						<tr>
							<td class="key"><?php echo KText::_('Address 2');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->address2);?></td>
						</tr>
						<?php } ?>
						<tr>
							<td class="key"><?php echo KText::_('ZIP Code');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->zipcode);?></td>
						</tr>
						<tr>
							<td class="key"><?php echo KText::_('City');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->city);?></td>
						</tr>
						<tr>
							<td class="key"><?php echo KText::_('Country');?>:</td>
							<td><?php echo hsc($this->orderRecord->orderAddress->countryname);?></td>
						</tr>
					</table>
				</div>		
			<?php } ?>
			<div class="clear"></div>
		</div>
	</div>
	
	<div class="clear"></div>
	
	<div class="order-overview">
		<h2><?php echo KText::_('Order Overview');?></h2>
		<?php echo $this->orderRecordHtml; ?>
	</div>
	
	<div class="clear"></div>
		
	
	<div class="buttons">
		<a rel="prev" class="navbutton-medium back floatleft leftmost" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=user',true,CONFIGBOX_SECURECHECKOUT);?>">
			<span class="nav-title"><?php echo KText::_('Back');?></span>
			<span class="nav-text"><?php echo KText::_('To Customer Profile');?></span>
		</a>
		<?php if ($this->orderRecord->status == 11 || $this->orderRecord->status == 14) { ?>
		<a rel="next" class="navbutton-medium next floatright rightmost" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=checkout&cborder_id='.$this->orderRecord->id,true,CONFIGBOX_SECURECHECKOUT);?>">
			<span class="nav-title"><?php echo KText::_('Purchase');?></span>
			<span class="nav-text"><?php echo KText::_('To Checkout');?></span>
		</a>
		<?php } ?>
		<div class="clear"></div>
	</div>
	
</div></div>

