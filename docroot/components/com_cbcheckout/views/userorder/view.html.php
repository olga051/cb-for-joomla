<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewUserorder extends KenedoView {
	
	function display() {
		
		$orderId = KRequest::getInt('order_id');
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$doesBelong = $orderModel->orderBelongsToUser($orderId);
		
		if ($doesBelong == false) {
			echo KText::_('This order does not belong to your user account.');
			return;
		}
		
		if (CONFIGBOX_ENABLE_FILE_UPLOADS) {
			$filesModel = KenedoModel::getModel('CbcheckoutModelOrderfiles');
			$files = $filesModel->getOrderFiles($orderId);
			$this->assignRef('orderFiles',$files);
		}

		$validExtensions = trim(CONFIGBOX_ALLOWED_FILE_EXTENSIONS);
		$hasLimitations = false;
		if ($validExtensions) {
			$extensions = strtolower($validExtensions);
			$extensions = str_replace(',', ' ', $extensions);
			$extensions = str_replace('.', '', $extensions);
			$extensions = str_replace('  ', ' ', $extensions);
			$extensions = explode(' ',$extensions);
			$validExtensions = array_map('trim',$extensions);
			if (count($validExtensions)) {
				$hasLimitations = true;
			}
		}

		if (CONFIGBOX_ALLOWED_FILE_SIZE) {
			$this->assign('fileUploadMaximumSize', CONFIGBOX_ALLOWED_FILE_SIZE);
		}

		$this->assign('fileUploadHasLimitations', $hasLimitations);
		$this->assign('allowedFileExtensions', $validExtensions);
		
		$orderRecord = $orderModel->getOrderRecord($orderId);
		
		if ($orderRecord) {
			$view = KenedoView::getView('CbcheckoutViewRecord',KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'record'.DS.'view.html.php');
			$view->assignRef('orderRecord',$orderRecord);
			$view->assign('showIn','confirmation');
			$view->assign('showChangeLinks',false);
			$view->assign('showProductDetails',true);
			$html = $view->getViewOutput();
			$this->assignRef('orderRecordHtml',$html);
		}
		else {
			$this->assign('orderRecordHtml','');
		}
		
		$orderStatuses = $orderModel->getOrderStatuses();
		$orderRecord->statusString = $orderStatuses[$orderRecord->status]->title;
		
		$this->assignRef('orderRecord', $orderRecord);
		
		$invoiceModel = KenedoModel::getModel('CbcheckoutModelInvoice');
		$invoiceData = $invoiceModel->getInvoiceData( $orderId );
		$this->assignRef('invoiceData',$invoiceData);
				
		

		$this->renderView();
		
	}
	
}