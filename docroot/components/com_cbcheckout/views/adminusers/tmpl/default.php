<?php
defined('CB_VALID_ENTRY') or die();
?>
<div id="view-<?php echo hsc($this->view);?>" class="<?php hsc($this->renderViewCssClasses());?>">

<div class="kenedo-listing-form">

	<?php 
	if (isset($this->pageTabs) && count($this->pageTabs)) {
		echo KenedoViewHelper::renderTabItems($this->pageTabs);
	}
	?>
	
	<div class="kenedo-tasks">
			<ul class="kenedo-task-list">
				<?php $returnLink = KLink::base64UrlEncode( KLink::getRoute('index.php?option=com_cbcheckout&controller=adminusers',false) );?>
				<li class="task-add"><a href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&controller=adminusers&task=edit');?>&return=<?php echo $returnLink;?>" class="backend-button-small"><?php echo KText::_('Add');?></a></li>
				<li class="task task-remove"><a class="backend-button-small"><?php echo KText::_('Delete');?></a></li>
			</ul>
			<div class="clear"></div>
		</div>

	<?php if (isset($this->filterBoxes) && count($this->filterBoxes)) { ?>
		<div class="kenedo-filters">
			<div class="kenedo-filter-list">
				<?php foreach ($this->filterBoxes as $filterBox) { ?>
					<div class="kenedo-filter"><?php echo $filterBox;?></div>
				<?php } ?>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
	<?php } ?>
	
	<?php if (!empty($this->pageTitle)) { ?>
		<h1 class="kenedo-page-title"><?php echo hsc($this->pageTitle);?></h1>
	<?php } ?>
	<div class="clear"></div>
	<table class="kenedo-listing" cellspacing="0">
		<thead>
			<tr>
				<th class="field-id" style="width:5px"><input type="checkbox" name="checkall" class="kenedo-check-all-items" /><?php echo KText::_( 'ID' ); ?></th>
				<th class="field-billingname" style="width:220px"><?php echo KText::_('Name');?></th>				
				<th class="field-billingcompanyname" style="width:150px"><?php echo KText::_('Company');?></th>				
				<th class="field-billingcity" style="width:120px"><?php echo KText::_('City');?></th>				
				<th class="field-billingcountry" style="width:120px"><?php echo KText::_('Country');?></th>
				<th class="field-order_count" style="white-space:nowrap"><?php echo KText::_('Order Count');?></th>
				<th>&nbsp;</th>
				
				</tr>			
		</thead>
		
		<tbody>
			<?php 
			$returnUrl = KLink::base64UrlEncode( KLink::getRoute('index.php?option='.KRequest::getKeyword('option').'&controller='.$this->controller, false) );
			foreach ($this->items as $item) { 
				?>
				<tr>
				
					<td class="field-id">
						<input type="checkbox" name="cid[]" class="kenedo-item-checkbox" value="<?php echo (int)$item->id;?>">
						<span><?php echo (int)$item->id;?></span>
					</td>
					<?php 
					$link = KLink::getRoute('index.php?option=com_cbcheckout&controller=adminusers&task=edit&cid[]='.$item->id.'&return='.$returnUrl);
					?>
					<td class="field-billingname"><a class="listing-link" href="<?php echo $link?>"><?php echo hsc($item->billingfirstname . ' '. $item->billinglastname);?></a></td>
					<td class="field-billingcompanyname"><?php echo hsc($item->billingcompanyname);?></td>
					<td class="field-billingcity"><?php echo hsc($item->billingcity);?></td>
					<td class="field-billingcountry"><?php echo hsc(CbcheckoutCountryHelper::getCountryName($item->billingcountry));?></td>
					<td class="field-order_count" style="white-space:nowrap">
						<?php echo (int)$item->order_count;?>
						<?php if ($item->order_count) { ?>
							<a class="kenedo-new-tab" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&controller=adminorders&filter_user_id='.(int)$item->id); ?>"><?php echo KText::_('Display');?></a>
						<?php } ?>
					</td>
					<td>&nbsp;</td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

	<div class="kenedo-pagination">
		<?php echo $this->pagination;?>
	</div>

	<div class="kenedo-hidden-fields">
		<input type="hidden" id="order_field"	name="listing_order_field" 	value="<?php echo $this->listingInfo->orderField;?>"/> 
		<input type="hidden" id="order_dir" 	name="listing_order_dir" 	value="<?php echo $this->listingInfo->orderDir;?>" />
		<input type="hidden" id="start" 		name="limitstart"			value="<?php echo $this->listingInfo->start;?>" />
		<input type="hidden" id="option" 		name="option" 				value="<?php echo KRequest::getKeyword('option');?>" />
		<input type="hidden" id="controller"	name="controller" 			value="<?php echo KRequest::getKeyword('controller');?>" />
		<input type="hidden" id="task" 			name="task" 				value="display" />
		<input type="hidden" id="tmpl"			name="tmpl" 				value="<?php echo KRequest::getString('tmpl','index');?>" />
		<input type="hidden" id="lang"			name="lang" 				value="<?php echo substr(KenedoPlatform::get('languageTag'),0,2);?>" />
		<input type="hidden" id="parampicker"	name="parampicker" 			value="<?php echo KRequest::getInt('parampicker',0);?>" />
		<input type="hidden" id="pickerobject"	name="pickerobject" 		value="<?php echo KRequest::getKeyword('pickerobject','');?>" />
		
		<?php 
		$listingData = array(
			'base-url'				=> KLink::getRoute('index.php?option='.KRequest::getKeyword('option').'&controller='.$this->controller.'&lang='.KText::$languageCode),
			'option'				=> hsc(KRequest::getKeyword('option','')),
			'task'					=> 'display',
			'ajax_sub_view'			=> ($this->isAjaxSubview()) ? '1':'0',
			'tmpl'					=> hsc(KRequest::getKeyword('tmpl','component')),
			'in_modal'				=> hsc(KRequest::getInt('in_modal','0')),
			'format'				=> 'raw',
			
			'limitstart'			=> hsc($this->listingInfo->start),
			'limit'					=> hsc($this->listingInfo->limit),
			'listing_order_field'	=> hsc($this->listingInfo->orderField),
			'listing_order_dir'		=> hsc($this->listingInfo->orderDir),
			'parampicker'			=> hsc(KRequest::getInt('parampicker',0)),
			'pickerobject'			=> hsc(KRequest::getKeyword('pickerobject','')),
			'pickermethod'			=> hsc(KRequest::getKeyword('pickermethod','')),
			'return'				=> KLink::base64UrlEncode( KLink::getRoute('index.php?option='.KRequest::getKeyword('option').'&controller='.$this->controller.'&lang='.KText::$languageCode, false) ),
			'add-link'				=> KLink::base64UrlEncode( KLink::getRoute('index.php?option='.KRequest::getKeyword('option').'&controller='.$this->controller.'&task=edit', false) ),
			'ids'					=> '',
			'ordering-items'		=> '',
			'key'					=> KRequest::getString('key',''),
			);
		
		$listingData['add-link'] = KLink::base64UrlEncode( KLink::getRoute('index.php?option='.KRequest::getKeyword('option').'&controller='.$this->controller.'&task=edit&return='.$listingData['return'], false) );
		
		foreach ($listingData as $key=>$data) {
			?>
			<div class="listing-data listing-data-<?php echo $key;?>" data-key="<?php echo $key;?>" data-value="<?php echo $data;?>"></div>
			<?php
		}
		?>
	
	</div>
	<div class="clear"></div>
</div>
</div>
