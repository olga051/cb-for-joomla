<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewAddress extends KenedoView {
	
	function display($tpl = null){
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderRecord = $orderModel->getOrderRecord( $orderModel->getId() );
		$this->assignRef('orderRecord',$orderRecord);
		
		$userFields = CbcheckoutUserHelper::getUserFields($orderRecord->orderAddress->group_id);
		$this->assignRef('userFields',$userFields);
		
		KenedoPlatform::p()->addScriptDeclaration("var userFields = ".json_encode($userFields).";",true);
		
		$this->renderView();
		
	}
}
