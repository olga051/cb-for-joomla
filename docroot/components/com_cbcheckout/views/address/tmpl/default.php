<?php 
defined('CB_VALID_ENTRY') or die(); 

// Helper function to display class="required" for fitting fields
function checkoutRequired($fields,$field) {
	if (isset($fields[$field]->require_checkout) && $fields[$field]->require_checkout) return 'class="required field-'.$field.'"';
	else return 'class="field-'.$field.'"';
}
?>
<div id="com_cbcheckout">
<div id="view-address">
<div id="template-default">

	<h1 class="componentheading"><?php echo KText::_('Your delivery and billing information')?></h1>
	
	<?php if (empty($this->orderRecord->orderAddress->joomlaid) && CONFIGBOX_SHOW_RECURRING_LOGIN_CART) { ?>
		<fieldset id="recurring-customer-login">
			<legend><?php echo KText::_('Returning Customers');?></legend>
			
				<p class="registered-users-intro"><?php echo KText::_('Sign in to automatically fill in your preferred address below.');?></p>
				
				<form action="<?php echo KLink::getRoute('index.php', true, CONFIGBOX_SECURECHECKOUT);?>" method="post">
					<table>
						<tr class="item-username">
							<td class="key"><label for="recurring-email"><?php echo KText::_('Email');?></label></td>
							<td><input type="text" id="recurring-email" name="email" value="" /></td>
						</tr>
						<tr class="item-password">
							<td class="key"><label for="recurring-password"><?php echo KText::_('Password');?></label></td>
							<td><input type="password" id="recurring-password" name="password" value="" /></td>
						</tr>
						<tr class="item-buttons">
							<td colspan="2">
								<a class="new-tab" href="<?php echo KenedoPlatform::get('passwordResetLink');?>"><?php echo KText::_('Retrieve lost password');?></a>
								<input type="submit" name="submitlogin" value="<?php echo KText::_('Sign in');?>" />
							</td>
						</tr>
					</table>
					<div class="hidden-fields">
						<input type="hidden" name="option" 			value="com_cbcheckout" />
						<input type="hidden" name="controller" 		value="user" />
						<input type="hidden" name="task" 			value="loginUser" />
						<input type="hidden" name="return_failure" 	value="<?php echo urlencode(KLink::getRoute('index.php?option=com_cbcheckout&view=address', false, CONFIGBOX_SECURECHECKOUT));?>" />
						<input type="hidden" name="return_success" 	value="<?php echo urlencode(KLink::getRoute('index.php?option=com_cbcheckout&view=address', false, CONFIGBOX_SECURECHECKOUT));?>" />
					</div>
				</form>
				
		</fieldset>
		<div class="clear"></div>
	<?php } ?>
	
	<form id="order-address-form" action="<?php echo KLink::getRoute('index.php', true, CONFIGBOX_SECURECHECKOUT);?>" method="post">
		
		<fieldset id="billingaddress">
			<legend><?php echo KText::_('Billing Address');?></legend>
			<table>
				<tbody>
					
					<?php if ($this->userFields['billingcompanyname']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingcompanyname');?>>
						<td class="key"><?php echo KText::_('Company Name');?></td>
						<td><input class="textfield" type="text" name="billingcompanyname" value="<?php echo hsc($this->orderRecord->orderAddress->billingcompanyname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingsalutation_id']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingsalutation_id');?>>
						<td class="key"><?php echo KText::_('Salutation');?></td>
						<td><?php echo CbcheckoutUserHelper::getSalutationDropdown('billingsalutation_id',$this->orderRecord->orderAddress->billingsalutation_id);?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingfirstname']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingfirstname');?>>
						<td class="key"><?php echo KText::_('First Name');?></td>
						<td><input class="textfield" type="text" name="billingfirstname" value="<?php echo hsc($this->orderRecord->orderAddress->billingfirstname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billinglastname']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billinglastname');?>>
						<td class="key"><?php echo KText::_('Last Name');?></td>
						<td><input class="textfield" type="text" name="billinglastname" value="<?php echo hsc($this->orderRecord->orderAddress->billinglastname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingaddress1']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingaddress1');?>>
						<td class="key"><?php echo KText::_('Address 1');?></td>
						<td><input class="textfield" type="text" name="billingaddress1" value="<?php echo hsc($this->orderRecord->orderAddress->billingaddress1);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingaddress2']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingaddress2');?>>
						<td class="key"><?php echo KText::_('Address 2');?></td>
						<td><input class="textfield" type="text" name="billingaddress2" value="<?php echo hsc($this->orderRecord->orderAddress->billingaddress2);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingzipcode']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingzipcode');?>>
						<td class="key"><?php echo KText::_('ZIP Code');?></td>
						<td><input class="textfield" type="text" name="billingzipcode" value="<?php echo hsc($this->orderRecord->orderAddress->billingzipcode);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingcity']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingcity');?>>
						<td class="key"><?php echo KText::_('City');?></td>
						<td><input class="textfield" type="text" name="billingcity" value="<?php echo hsc($this->orderRecord->orderAddress->billingcity);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingcountry']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingcountry');?>>
						<td class="key"><?php echo KText::_('Country');?></td>
						<td><?php echo CbcheckoutCountryHelper::createCountrySelect('billingcountry', $this->orderRecord->orderAddress->billingcountry, KText::_('Select a country'), 'billingstate');?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingstate']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingstate');?>>
						<td class="key"><?php echo KText::_('State');?></td>
						<td><?php echo CbcheckoutCountryHelper::createStateSelect('billingstate', $this->orderRecord->orderAddress->billingstate, $this->orderRecord->orderAddress->billingcountry);?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingemail']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingemail');?>>
						<td class="key"><?php echo KText::_('Email');?></td>
						<td><input class="textfield" type="text" name="billingemail" value="<?php echo hsc($this->orderRecord->orderAddress->billingemail);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingphone']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'billingphone');?>>
						<td class="key"><?php echo KText::_('Phone');?></td>
						<td><input class="textfield" type="text" name="billingphone" value="<?php echo hsc($this->orderRecord->orderAddress->billingphone);?>" /></td>
					</tr>
					<?php endif;?>
				</tbody>
			</table>
		</fieldset>
	
		<fieldset id="deliveryaddress"<?php echo ($this->orderRecord->orderAddress->samedelivery) ? ' style="display:none"':'';?>>
			<legend><?php echo KText::_('Shipping Address');?></legend>
			<table>
				<tbody>
					
					<?php if ($this->userFields['companyname']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'companyname');?>>
						<td class="key"><?php echo KText::_('Company Name');?></td>
						<td><input class="textfield" type="text" name="companyname" value="<?php echo hsc($this->orderRecord->orderAddress->companyname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['salutation_id']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'salutation_id');?>>
						<td class="key"><?php echo KText::_('Salutation');?></td>
						<td><?php echo CbcheckoutUserHelper::getSalutationDropdown('salutation_id',$this->orderRecord->orderAddress->salutation_id);?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['firstname']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'firstname');?>>
						<td class="key"><?php echo KText::_('First Name');?></td>
						<td><input class="textfield" type="text" name="firstname" value="<?php echo hsc($this->orderRecord->orderAddress->firstname);?>" /></td>
					</tr>
					
					<?php endif;?>
					<?php if ($this->userFields['lastname']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'lastname');?>>
						<td class="key"><?php echo KText::_('Last Name');?></td>
						<td><input class="textfield" type="text" name="lastname" value="<?php echo hsc($this->orderRecord->orderAddress->lastname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['address1']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'address1');?>>
						<td class="key"><?php echo KText::_('Address 1');?></td>
						<td><input class="textfield" type="text" name="address1" value="<?php echo hsc($this->orderRecord->orderAddress->address1);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['address2']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'address2');?>>
						<td class="key"><?php echo KText::_('Address 2');?></td>
						<td><input class="textfield" type="text" name="address2" value="<?php echo hsc($this->orderRecord->orderAddress->address2);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['zipcode']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'zipcode');?>>
						<td class="key"><?php echo KText::_('ZIP Code');?></td>
						<td><input class="textfield" type="text" name="zipcode" value="<?php echo hsc($this->orderRecord->orderAddress->zipcode);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['city']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'city');?>>
						<td class="key"><?php echo KText::_('City');?></td>
						<td><input class="textfield" type="text" name="city" value="<?php echo hsc($this->orderRecord->orderAddress->city);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['country']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'country');?>>
						<td class="key"><?php echo KText::_('Country');?></td>
						<td><?php echo CbcheckoutCountryHelper::createCountrySelect('country', $this->orderRecord->orderAddress->country, KText::_('Select a country'), 'state');?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['state']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'state');?>>
						<td class="key"><?php echo KText::_('State');?></td>
						<td><?php echo CbcheckoutCountryHelper::createStateSelect('state', $this->orderRecord->orderAddress->state, $this->orderRecord->orderAddress->country);?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['email']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'email');?>>
						<td class="key"><?php echo KText::_('Email');?></td>
						<td><input class="textfield" type="text" name="email" value="<?php echo hsc($this->orderRecord->orderAddress->email);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['phone']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'phone');?>>
						<td class="key"><?php echo KText::_('Phone');?></td>
						<td><input class="textfield" type="text" name="phone" value="<?php echo hsc($this->orderRecord->orderAddress->phone);?>" /></td>
					</tr>
					<?php endif;?>
					
				</tbody>
			</table>
		</fieldset>
		
		<div class="clear"></div>
		
		<?php if ( $this->userFields['samedelivery']->show_checkout || $this->userFields['newsletter']->show_checkout || $this->userFields['vatin']->show_checkout || $this->userFields['language_tag']->show_checkout) { ?>
		
			<fieldset id="otherinfo">
				<legend><?php echo KText::_('Other');?></legend>
				<table>
					
					<?php if ($this->userFields['vatin']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'vatin');?>>
						<td class="key"><?php echo KText::_('VAT IN');?></td>
						<td><input class="textfield" type="text" name="vatin" value="<?php echo hsc($this->orderRecord->orderAddress->vatin);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['samedelivery']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'samedelivery');?>>
						<td class="key"><?php echo KText::_('Ship to same address');?></td>
						<td>
							<input type="radio" <?php echo ($this->orderRecord->orderAddress->samedelivery == 1)? 'checked="checked"':'';?> id="delivery-yes" name="samedelivery" value="1" />
							<label for="delivery-yes"><?php echo KText::_('CBYES');?></label>
							<input type="radio" <?php echo ($this->orderRecord->orderAddress->samedelivery != 1)? 'checked="checked"':'';?> id="delivery-no" name="samedelivery" value="0" />
							<label for="delivery-no"><?php echo KText::_('CBNO');?></label>
						</td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['language_tag']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'language_tag');?>>
						<td class="key"><?php echo KText::_('Language');?></td>
						<td><?php echo ConfigboxLanguageHelper::getLangSelect( $this->orderRecord->orderAddress->language_tag, 'language_tag' ); ?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['newsletter']->show_checkout) :?>
					<tr <?php echo checkoutRequired($this->userFields,'newsletter');?>>
						<td class="key"><?php echo KText::_('Newsletter');?></td>
						<td>
							<input type="radio" <?php echo ($this->orderRecord->orderAddress->newsletter == 1)? 'checked="checked"':'';?> id="newsletter-yes" name="newsletter" value="1" />
							<label for="newsletter-yes"><?php echo KText::_('CBYES');?></label>
							<input type="radio" <?php echo ($this->orderRecord->orderAddress->newsletter != 1)? 'checked="checked"':'';?> id="newsletter-no" name="newsletter" value="0" />
							<label for="newsletter-no"><?php echo KText::_('CBNO');?></label>
						</td>
					</tr>
					<?php endif;?>
					
					<tr class="order-comment">
						<td class="key"><?php echo KText::_('Comment');?></td>
						<td><textarea name="comment" cols="20" rows="5"></textarea></td>
					</tr>
					
				</table>
			</fieldset>
		<?php } ?>
		
		<div class="clear"></div>
		
		<div class="hidden-fields">
			<input type="hidden" name="option" 			value="com_cbcheckout" />
			<input type="hidden" name="controller" 		value="user" />
			<input type="hidden" name="task" 			value="saveUser" />
			<input type="hidden" name="type" 			value="checkout" />
			<input type="hidden" name="success_message" value="0" />
			<input type="hidden" name="return_failure" 	value="<?php echo urlencode(KLink::getRoute('index.php?view=address', 		false, CONFIGBOX_SECURECHECKOUT));?>" />
			<input type="hidden" name="return_success" 	value="<?php echo urlencode(KLink::getRoute('index.php?view=confirmation', 	false, CONFIGBOX_SECURECHECKOUT));?>" />
		</div>
		
		<div id="buttons">
			<a rel="next nofollow" class="navbutton-medium next trigger-send-address-form">
				<span class="nav-center"><?php echo KText::_('Save');?></span>
			</a>
		</div>
	</form>
</div>	
</div>
</div>

