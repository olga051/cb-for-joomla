<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewEmailcustomerregistration extends KenedoView {
	
	function display() {
			
		$shopModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		
		$shopData = $shopModel->getItem();
		$this->assignRef('shopData',$shopData);
		
		$customer = CbcheckoutUserHelper::getUser();
		$this->assignRef('customer',$customer);
		
		$this->renderView();
	}
	
}