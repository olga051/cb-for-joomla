<?php 
defined('CB_VALID_ENTRY') or die(); 
?>
<div id="com_cbcheckout">
	<div id="view-delivery">
	
	<h1 class="componentheading"><?php echo KText::_('Choose your preferred delivery option')?></h1>
	
	<form action="<?php echo KLink::getRoute('index.php',true,CONFIGBOX_SECURECHECKOUT);?>" method="post">
		
		<fieldset>
			<legend><?php echo KText::_('Delivery information');?></legend>
			<table>
				<tr>
					<th class="delivery-option-input">&nbsp;</th>
					<th class="delivery-option-title"><?php echo KText::_('Delivery Option');?></th>
					<th class="delivery-option-price"><?php if ($this->optionsHavePricing) echo KText::_('Price');?></th>
					<th class="delivery-option-delivery-time"><?php echo KText::_('Delivery time');?></th>
				</tr>
				<?php foreach ($this->options as &$option) { ?>
					<tr>
						<td class="delivery-option-input"><input id="radio<?php echo (int)$option->id;?>" type="radio" name="delivery_id" value="<?php echo (int)$option->id;?>" <?php echo ($option->id == $this->selected) ? 'checked="checked"':'';?> /></td>
						<td class="delivery-option-title"><label for="radio<?php echo (int)$option->id;?>"><?php echo hsc($option->rateTitle);?></label></td> 
						<td class="delivery-option-price"><?php echo cbprice( ($this->mode == 'b2b') ? $option->priceNet : $option->priceGross, true, true); ?></td>
						<td class="delivery-option-delivery-time"><?php echo hsc($option->deliverytime);?> <?php echo KText::_('Working days');?></td> 
					</tr>
				<?php } ?>
			</table>
		</fieldset>
		<div id="buttons">
			<input type="hidden" name="controller" 	value="delivery" />
			<input type="hidden" name="option" 		value="com_cbcheckout" />
			<input type="hidden" name="view" 		value="delivery" />
			<input type="hidden" name="task" 		value="saveDeliveryOption" />
			
			<a rel="next nofollow" class="navbutton-medium next" onclick="cbj(this).parents('form').submit()">
				<span class="nav-center"><?php echo KText::_('Save');?></span>
			</a>
			
		</div>
	</form>
	
	</div>
</div>
