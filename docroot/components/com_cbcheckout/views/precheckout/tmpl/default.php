<?php defined('CB_VALID_ENTRY') or die();?>
<div id="com_cbcheckout">
<div id="view-precheckout">
<div id="template-default">
<div class="<?php echo ConfigboxDeviceHelper::getDeviceClasses();?>">

<div class="wrapper-returning-guest">
	
	<div class="wrapper-returning">
		<div class="wrapper-returning-inner">
			
			<form method="post" action="<?php echo KLink::getRoute('index.php',true,CONFIGBOX_SECURECHECKOUT);?>">
				
				<div class="hidden-fields">
					<input type="hidden" name="option" 			value="com_cbcheckout" />
					<input type="hidden" name="controller" 		value="user" />
					<input type="hidden" name="task" 			value="loginUser" />
					<input type="hidden" name="return_failure" 	value="<?php echo urlencode(KLink::getRoute('index.php?option=com_cbcheckout&view=precheckout',false, CONFIGBOX_SECURECHECKOUT));?>" />
					<input type="hidden" name="return_success" 	value="<?php echo urlencode(KLink::getRoute('index.php?option=com_cbcheckout&view=checkout', false, CONFIGBOX_SECURECHECKOUT));?>" />
				</div>
				
				<h1 class="precheckout-heading"><?php echo KText::_('Returning Customer');?></h1>
				
				<div class="precheckout-fields">
					
					<div class="precheckout-field precheckout-field-email">
						<label class="precheckout-label" for="cbcheckout_email"><?php echo KText::_('Email');?></label>
						<input class="textfield" id="cbcheckout_email" type="text" name="email" value="" />
					</div>
					
					<div class="precheckout-field precheckout-field-password">
						<label class="precheckout-label" for="cbcheckout_password"><?php echo KText::_('Password');?></label>
						<input class="textfield" id="cbcheckout_password" type="password" name="password" value="" />
					</div>
					
					<div class="precheckout-buttons">
						<a class="navbutton-medium precheckout-login-button leftmost form-submit-button"><span class="nav-center"><?php echo KText::_('Login');?></span></a>
						<a class="new-tab lost-password-link" href="<?php echo KenedoPlatform::get('passwordResetLink');?>"><?php echo KText::_('Retrieve lost password');?></a>							
					</div>
					
				</div>
				
			</form>
		</div>
	</div> <!-- .wrapper-returning -->
	
	<div class="wrapper-guest">
		<div class="wrapper-guest-inner">
			<h1 class="precheckout-heading"><?php echo KText::_('New Customer');?></h1>
					
			<div class="guest-comment"><?php echo KText::_('CHECKOUT_NEW_CUSTOMER_NOTE');?></div>
			
			<div class="guest-button">
				<a href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&view=checkout');?>" class="precheckout-button-continue navbutton-medium leftmost"><span class="nav-center"><?php echo KText::_('Continue as guest');?></span></a>
			</div>
		</div>
	</div>
	
	<div class="clear"></div>

</div>

<div class="wrapper-back-button">
	<a class="back-to-cart-button navbutton-medium leftmost rightmost" href="<?php echo KLink::getRoute('index.php?option=com_cbcheckout&controller=checkout&task=backToCart');?>"><span class="nav-center"><?php echo KText::_('Back to Cart');?></span></a>
</div>


<script type="text/javascript">
	cbj('#cbcheckout_email').focus();
</script>

</div>
</div>
</div>
</div>