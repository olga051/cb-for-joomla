<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewCheckoutdelivery extends KenedoView {
	
	function display() {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
		$orderRecord = $orderModel->getOrderRecord($orderId);
		
		$this->assign('mode', $orderRecord->groupData->b2b_mode ? 'b2b' : 'b2c' );
		
		$shippingOptions = $orderModel->getOrderRecordDeliveryOptions($orderRecord);
		$selected = $orderRecord->delivery_id;
		
		if ($selected == 0) {
			$selected = (isset($shippingOptions[0]->id)) ? $shippingOptions[0]->id : 0;
		}
		
		$optionsHavePricing = false;
		foreach ($shippingOptions as $option) {
			$option->title = $option->rateTitle;
			if ($option->priceNet) {
				$optionsHavePricing = true;
			}
		}
		
		$this->assignRef('optionsHavePricing',$optionsHavePricing);
		$this->assignRef('selected',$selected);
		$this->assignRef('options',$shippingOptions);
		
		$this->renderView();
	}
}