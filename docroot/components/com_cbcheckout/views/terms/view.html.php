<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewTerms extends KenedoView {
	
	function display(){
		
		$shopdataModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		$shopData = $shopdataModel->getItem();
		$terms = $shopData->tac;
		$this->assignRef('terms',$terms);
		
		$this->renderView();
	}
}
