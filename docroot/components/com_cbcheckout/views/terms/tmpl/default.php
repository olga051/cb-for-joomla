<?php 
defined('CB_VALID_ENTRY') or die(); 
?>

<div id="com_cbcheckout"><div id="view-terms">

<?php if (KRequest::getKeyword('tmpl') == 'component') { ?>
	<div style="margin:10px">
<?php } ?>

<h2 class="componentheading"><?php echo hsc(KText::_('Terms and Conditions'));?></h2>

<?php echo $this->terms;?>

<?php if (KRequest::getKeyword('tmpl') == 'component') { ?>
	</div>
<?php } ?>

</div></div>