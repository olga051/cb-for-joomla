<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewCheckout extends KenedoView {
	
	function display() {
		
		// Get order model and order id
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = KRequest::getInt('order_id');
		if ($orderId) {
			$belongs = $orderModel->orderBelongsToUser($orderId);
			if (!$belongs) {
				echo 'This order does not belong to you.';
				return;
			}
		}
		else {
			$orderId = $orderModel->getId();
		}
		
		$orderRecord = $orderModel->getOrderRecord($orderId);
		
		
		
		// Bounce if we got no order record
		if (!$orderRecord) {
			echo KText::_('Order not found.');
			return;
		}
		
		// Check if the order can be checked out
//		$grandorderModel = KenedoModel::getModel('ConfigboxModelGrandorder');
//		$grandorderModel->setId($orderRecord->grandorder_id);
//		$details = $grandorderModel->getGrandorderDetails();
//		$canCheckout = ConfigboxOrderHelper::isPermittedAction('checkoutOrder',$details);
		
		// Bounce if we got no order record
//		if (!$canCheckout) {
//			echo KText::_('This order is finished.');
//			return;
//		}
		
		// Assign the order record data
		$this->assignRef('orderRecord',$orderRecord);
		
		// Assign the customer user fields and make them available as a JS object
		$userFields = CbcheckoutUserHelper::getUserFields($orderRecord->orderAddress->group_id);
		$this->assignRef('userFields',$userFields);
		KenedoPlatform::p()->addScriptDeclaration("var userFields = ".json_encode($userFields).";",true);
		
		// Legacy thing: Set the validatetc method called in PSP connectors
		KenedoPlatform::p()->addScriptDeclaration("var validatetc = function(){return true}",true);
		
		// Put URLs and other data in the JS object
		$js = "com_configbox.addressViewUrl = '".KLink::getRoute('index.php?option=com_cbcheckout&view=checkoutaddress&format=raw')."';\n";
		$js .= "com_configbox.deliveryViewUrl = '".KLink::getRoute('index.php?option=com_cbcheckout&view=checkoutdelivery&format=raw')."';\n";
		$js .= "com_configbox.paymentViewUrl = '".KLink::getRoute('index.php?option=com_cbcheckout&view=checkoutpayment&format=raw')."';\n";
		$js .= "com_configbox.recordViewUrl = '".KLink::getRoute('index.php?option=com_cbcheckout&view=record&format=raw')."';\n";
		$js .= "com_configbox.pspBridgeViewUrl = '".KLink::getRoute('index.php?option=com_cbcheckout&view=checkoutpspbridge&format=raw')."';\n";
		$js .= "com_configbox.agreeToTerms = ".((CONFIGBOX_EXPLICIT_AGREEMENT_TERMS) ? 'true':'false').";\n";
		$js .= "com_configbox.agreeToRefundPolicy = ".((CONFIGBOX_EXPLICIT_AGREEMENT_RP) ? 'true':'false').";\n";
		$js .= "com_configbox.lang.checkoutPleaseAgreeBoth = '".KText::_('Please agree to the terms and conditions and to the refund policy.')."';\n";
		$js .= "com_configbox.lang.checkoutPleaseAgreeTerms = '".KText::_('Please agree to the terms and conditions.')."';\n";
		$js .= "com_configbox.lang.checkoutPleaseAgreeRefundPolicy = '".KText::_('Please agree to the refund policy.')."';\n";
		KenedoPlatform::p()->addScriptDeclaration($js,true);
		
		// Links to some pages
		$termsLink = '<a class="rovedo-modal modal-link-terms" data-modal-width="900" data-modal-height="700" href="'.KLink::getRoute('index.php?option=com_cbcheckout&view=terms&tmpl=component').'">'.KText::_('Terms and Conditions').'</a>';
		$refundLink = '<a class="rovedo-modal modal-link-refund-policy" data-modal-width="900" data-modal-height="700" href="'.KLink::getRoute('index.php?option=com_cbcheckout&view=refundpolicy&tmpl=component').'">'.KText::_('Refund Policy').'</a>';
		$this->assign('linkTerms',$termsLink);
		$this->assign('linkRefundPolicy',$refundLink);
		
		// Back to cart URL
		$backToCartUrl = KLink::getRoute('index.php?option=com_cbcheckout&controller=checkout&task=backToCart');
		$this->assign('backToCartUrl',$backToCartUrl);
		
		// Pass over whether to use delivery or not
		$this->assign('useDelivery', (CONFIGBOX_DISABLE_DELIVERY == 0));
		
		$orderAddressComplete = CbcheckoutUserHelper::orderAddressComplete($orderRecord->orderAddress);
		$this->assignRef('orderAddressComplete',$orderAddressComplete);
		
		// Assign the order address subview HTML
		if ($orderAddressComplete) {
			ob_start();
			$view = KenedoView::getView('CbcheckoutViewCheckoutaddress', KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'checkoutaddress'.DS.'view.html.php');
			$view->display();
			$html = ob_get_clean();
		}
		else {
			$html = '';
		}
		$this->assignRef('orderAddressHtml', $html);
		
		// Assign the delivery subview HTML
		ob_start();
		$view = KenedoView::getView('CbcheckoutViewCheckoutdelivery', KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'checkoutdelivery'.DS.'view.html.php');
		$view->display();
		$html = ob_get_clean();
		$this->assignRef('deliveryHtml', $html);
		
		// Assign the payment subview HTML
		ob_start();
		$view = KenedoView::getView('CbcheckoutViewCheckoutpayment', KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'checkoutpayment'.DS.'view.html.php');
		$view->display();
		$html = ob_get_clean();
		$this->assignRef('paymentHtml', $html);
		
		// Assign the checkout record subview HTML
		ob_start();
		$view = KenedoView::getView('CbcheckoutViewRecord', KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'views'.DS.'record'.DS.'view.html.php');
		$view->assignRef('orderRecord',$orderRecord);
		$view->assign('showProductDetails',true);
		$view->assign('hideSkus',true);
		$view->display();
		$html = ob_get_clean();
		$this->assignRef('orderRecordHtml', $html);
		
		// Render the view
		$this->renderView();
		
	}
	
	function getFieldClasses($fields,$field) {
	
		$fieldClasses = array();
		$fieldClasses[] = 'order-address-field field-'.$field;
	
		if (!empty($fields[$field]->require_checkout)) {
			$fieldClasses[] = 'required';
		}
	
		return implode(' ',$fieldClasses);
	
	}
	
}