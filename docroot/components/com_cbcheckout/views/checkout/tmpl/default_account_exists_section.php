<?php
defined('CB_VALID_ENTRY') or die();
?>
<div class="account-exists-section">

	<div class="checkout-login-form">
		
		<p><?php echo KText::_('You already have an account in our store with this email address. Please provide your password to continue.');?> <?php echo KText::_('You can also');?> <a class="trigger-send-password-verification-code"><?php echo KText::_('set a new password');?></a>.</p>
		
		<div class="<?php echo $this->getFieldClasses($this->userFields, 'password');?> required">
			<label for="password" class="field-label"><?php echo KText::_('Password');?></label>
			<div class="form-field">
				<input class="textfield" type="password" id="password" name="password" value="" />
				
			</div>
			<div class="validation-tooltip"></div>
		</div>
		
	</div>
	
	<div class="checkout-password-change-form">
		
		<p><?php echo KText::_('We have sent a verification code to your email address. Please enter the code and pick a new password.');?> <a class="trigger-show-checkout-login-form"><?php echo KText::_('Cancel');?></a></p>
		
		<div class="<?php echo $this->getFieldClasses($this->userFields, 'verification_code');?> required">
			<label for="verification_code" class="field-label"><?php echo KText::_('Verification Code');?></label>
			<div class="form-field">
				<input class="textfield" type="text" id="verification_code" name="verification_code" value="" />
			</div>
			<div class="validation-tooltip"></div>
		</div>
		
		<div class="<?php echo $this->getFieldClasses($this->userFields, 'new_password');?> required">
			<label for="new_password" class="field-label"><?php echo KText::_('New Password');?></label>
			<div class="form-field">
				<input class="textfield" type="text" id="new_password" name="new_password" value="" />
			</div>
			<div class="validation-tooltip"></div>
		</div>
		
	</div>
	
</div>