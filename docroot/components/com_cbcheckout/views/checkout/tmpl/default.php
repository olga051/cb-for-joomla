<?php
defined('CB_VALID_ENTRY') or die();
?>
<div id="com_cbcheckout">
<div id="view-checkout">
	
	<h1 class="page-title page-title-checkout-page"><?php echo KText::_('Order Placement');?></h1>
	
	<div class="wrapper-order-address">
		<div class="order-address-form"<?php echo ($this->orderAddressComplete) ? 'style="display:none"':'';?>>
			<?php 
			// Render the template default_orderaddressform
			$this->renderView('default_orderaddressform');
			?>
		</div>
		<div class="order-address-display"<?php echo ($this->orderAddressComplete) ? '':'style="display:none"';?>>
			<?php 
			// Coming from view cbcheckout/checkoutaddress
			echo $this->orderAddressHtml;
			?>
		</div>
	</div>
	
	<?php if ($this->useDelivery) { ?>
		<div class="wrapper-delivery-options">
			<?php 
			// Coming from view cbcheckout/checkoutdelivery
			echo $this->deliveryHtml;
			?>
		</div>
	<?php } ?>
	
	<div class="wrapper-payment-options">
		<?php 
		// Coming from view cbcheckout/checkoutpayment
		echo $this->paymentHtml;
		?>
	</div>
	
	<h2 class="step-title"><?php echo KText::_('Review your order');?></h2>
	<div class="wrapper-order-record">
			<?php 
			// Coming from view cbcheckout/record
			echo $this->orderRecordHtml;
			?>
	</div>
	
	<div class="wrapper-agreements">
		<?php 
		// Render the template default_orderaddress
		$this->renderView('default_agreements');
		?>
	</div>
	
	<div class="wrapper-psp-bridge" style="display:none"></div>
	
	<div class="wrapper-order-buttons">
		<?php 
		// Render the template default_orderaddress
		$this->renderView('default_orderbuttons');
		?>
	</div>
	
	
	
</div> <!-- #view-checkout -->
</div> <!-- #com_cbcheckout -->