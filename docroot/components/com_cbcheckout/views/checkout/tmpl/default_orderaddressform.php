<?php
defined('CB_VALID_ENTRY') or die();
?>
<h2 class="step-title"><?php echo KText::_('Your address');?></h2>

<div class="order-address-sections<?php echo ($this->orderRecord->orderAddress->samedelivery) ? '':' show-delivery-fields';?>">
	
	<div class="order-address-section order-address-section-billing">
		
		<div class="order-address-section-heading">
			<div class="text-address-heading"><?php echo KText::_('Billing');?></div>
		</div>
		
		<div class="order-address-fields">
			
			<?php if ($this->userFields['billingcompanyname']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingcompanyname');?>">
				<label for="billingcompanyname" class="field-label"><?php echo KText::_('Company');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingcompanyname" name="billingcompanyname" value="<?php echo hsc($this->orderRecord->orderAddress->billingcompanyname);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['billingsalutation_id']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingsalutation_id');?>">
				<label for="billingsalutation_id" class="field-label"><?php echo KText::_('Salutation');?></label>
				<div class="form-field">
					<?php echo CbcheckoutUserHelper::getSalutationDropdown('billingsalutation_id',$this->orderRecord->orderAddress->billingsalutation_id);?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['billingfirstname']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingfirstname');?>">
				<label for="billingfirstname" class="field-label"><?php echo KText::_('First Name');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingfirstname" name="billingfirstname" value="<?php echo hsc($this->orderRecord->orderAddress->billingfirstname);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['billinglastname']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billinglastname');?>">
				<label for="billinglastname" class="field-label"><?php echo KText::_('Last Name');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billinglastname" name="billinglastname" value="<?php echo hsc($this->orderRecord->orderAddress->billinglastname);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['billingaddress1']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingaddress1');?>">
				<label for="billingaddress1" class="field-label"><?php echo KText::_('Address');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingaddress1" name="billingaddress1" value="<?php echo hsc($this->orderRecord->orderAddress->billingaddress1);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['billingaddress2']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingaddress2');?>">
				<label for="billingaddress2" class="field-label"><?php echo KText::_('Address 2');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingaddress2" name="billingaddress2" value="<?php echo hsc($this->orderRecord->orderAddress->billingaddress2);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if (CbcheckoutCountryHelper::systemUsesCities() == false) { ?>
				
				<?php if ($this->userFields['billingzipcode']->show_checkout) { ?>
				<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingzipcode');?>">
					<label for="billingzipcode" class="field-label"><?php echo KText::_('ZIP code');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="billingzipcode" name="billingzipcode" value="<?php echo hsc($this->orderRecord->orderAddress->billingzipcode);?>" />
					</div>
					<div class="validation-tooltip"></div>
				</div>
				<?php } ?>
				
				<?php if ($this->userFields['billingcity']->show_checkout) { ?>
				<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingcity');?>">
					<label for="billingcity" class="field-label"><?php echo KText::_('City');?></label>
					<div class="form-field">
						<?php echo CbcheckoutCountryHelper::getCityInputField('billingcity', $this->orderRecord->orderAddress->billingcity_id, KText::_('Select City'), $this->orderRecord->orderAddress->billingcounty_id, $this->orderRecord->orderAddress->billingcity );?>
					</div>
					<div class="validation-tooltip"></div>
				</div>
				<?php } ?>
			<?php } ?>
			
			<?php if ($this->userFields['billingcountry']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingcountry');?>">
				<label for="billingcountry" class="field-label"><?php echo KText::_('Country');?></label>
				<div class="form-field">
					<?php echo CbcheckoutCountryHelper::createCountrySelect('billingcountry', $this->orderRecord->orderAddress->billingcountry, KText::_('Select a country'), 'billingstate');?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['billingstate']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingstate');?>">
				<label for="billingstate" class="field-label"><?php echo KText::_('State');?></label>
				<div class="form-field">
					<?php echo CbcheckoutCountryHelper::createStateSelect('billingstate', $this->orderRecord->orderAddress->billingstate, $this->orderRecord->orderAddress->billingcountry, NULL, 'billingcounty_id');?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['billingcounty_id']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingcounty_id');?>" style="display:<?php echo(CbcheckoutCountryHelper::hasCounties($this->orderRecord->orderAddress->billingstate)) ? 'block':'none'; ?>">
				<label for="billingcounty" class="field-label"><?php echo KText::_('County');?></label>
				<div class="form-field">
					<?php echo CbcheckoutCountryHelper::createCountySelect('billingcounty_id', $this->orderRecord->orderAddress->billingcounty_id, $this->orderRecord->orderAddress->billingstate, KText::_('Select County'), 'billingcity');?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if (CbcheckoutCountryHelper::systemUsesCities() == true) { ?>
				
				<?php if ($this->userFields['billingzipcode']->show_checkout) { ?>
				<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingzipcode');?>">
					<label for="billingzipcode" class="field-label"><?php echo KText::_('ZIP code');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="billingzipcode" name="billingzipcode" value="<?php echo hsc($this->orderRecord->orderAddress->billingzipcode);?>" />
					</div>
					<div class="validation-tooltip"></div>
				</div>
				<?php } ?>
				
				<?php if ($this->userFields['billingcity']->show_checkout) { ?>
				<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingcity');?>">
					<label for="billingcity" class="field-label"><?php echo KText::_('City');?></label>
					<div class="form-field">
						<?php echo CbcheckoutCountryHelper::getCityInputField('billingcity', $this->orderRecord->orderAddress->billingcity_id, KText::_('Select City'), $this->orderRecord->orderAddress->billingcounty_id, $this->orderRecord->orderAddress->billingcity );?>
					</div>
					<div class="validation-tooltip"></div>
				</div>
				<?php } ?>
				
			<?php } ?>
			
			<?php if ($this->userFields['billingemail']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingemail');?>">
				<label for="billingemail" class="field-label"><?php echo KText::_('Email');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingemail" name="billingemail" value="<?php echo hsc($this->orderRecord->orderAddress->billingemail);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['billingphone']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'billingphone');?>">
				<label for="billingphone" class="field-label"><?php echo KText::_('Phone');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="billingphone" name="billingphone" value="<?php echo hsc($this->orderRecord->orderAddress->billingphone);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['vatin']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'vatin');?>">
				<label for="vatin" class="field-label"><?php echo KText::_('VAT IN');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="vatin" name="vatin" value="<?php echo hsc($this->orderRecord->orderAddress->vatin);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['language_tag']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'language_tag');?>">
				<label for="language_tag" class="field-label"><?php echo KText::_('Language');?></label>
				<div class="form-field">
					<?php echo ConfigboxLanguageHelper::getLangSelect( $this->orderRecord->orderAddress->language_tag, 'language_tag' ); ?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['newsletter']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'newsletter');?>">
				<label class="field-label"><?php echo KText::_('Newsletter');?></label>
				<div class="form-field">
					<input type="radio" <?php echo ($this->orderRecord->orderAddress->newsletter == 1)? 'checked="checked"':'';?> id="newsletter-yes" name="newsletter" value="1" />
					<label class="radio-button-label" for="newsletter-yes"><?php echo KText::_('CBYES');?></label>
					<input type="radio" <?php echo ($this->orderRecord->orderAddress->newsletter != 1)? 'checked="checked"':'';?> id="newsletter-no" name="newsletter" value="0" />
					<label class="radio-button-label" for="newsletter-no"><?php echo KText::_('CBNO');?></label>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
		</div> <!-- .order-address-fields -->
		
	</div> <!-- .order-address-section-billing -->
	
	
	<div class="order-address-section order-address-section-delivery">
		
		<div class="order-address-section-heading">
			
			<?php if ($this->userFields['samedelivery']->show_checkout) { ?>
				<div class="different-shipping-address-toggle">
					<input class="trigger-toggle-same-delivery" type="checkbox" value="1" name="samedelivery" id="samedelivery"<?php echo ($this->orderRecord->orderAddress->samedelivery) ? ' checked="checked"':'';?> />
					<label for="samedelivery"><?php echo KText::_('Same as billing');?></label>
				</div>
				
				<div class="text-address-heading"><?php echo KText::_('Delivery');?></div>
				
			<?php } ?>
			
		</div>
		
		<div class="order-address-fields">
			
			<?php if ($this->userFields['companyname']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'companyname');?>">
				<label for="companyname" class="field-label"><?php echo KText::_('Company Name');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="companyname" name="companyname" value="<?php echo hsc($this->orderRecord->orderAddress->companyname);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['salutation_id']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'salutation_id');?>">
				<label for="salutation_id" class="field-label"><?php echo KText::_('Salutation');?></label>
				<div class="form-field">
					<?php echo CbcheckoutUserHelper::getSalutationDropdown('salutation_id',$this->orderRecord->orderAddress->salutation_id);?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['firstname']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'firstname');?>">
				<label for="firstname" class="field-label"><?php echo KText::_('First Name');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="firstname" name="firstname" value="<?php echo hsc($this->orderRecord->orderAddress->firstname);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['lastname']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'lastname');?>">
				<label for="lastname" class="field-label"><?php echo KText::_('Last Name');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="lastname" name="lastname" value="<?php echo hsc($this->orderRecord->orderAddress->lastname);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['address1']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'address1');?>">
				<label for="address1" class="field-label"><?php echo KText::_('Address');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="address1" name="address1" value="<?php echo hsc($this->orderRecord->orderAddress->address1);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['address2']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'address2');?>">
				<label for="address2" class="field-label"><?php echo KText::_('Address 2');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="address2" name="address2" value="<?php echo hsc($this->orderRecord->orderAddress->address2);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if (CbcheckoutCountryHelper::systemUsesCities() == false) { ?>
				
				<?php if ($this->userFields['zipcode']->show_checkout) { ?>
				<div class="<?php echo $this->getFieldClasses($this->userFields, 'zipcode');?>">
					<label for="zipcode" class="field-label"><?php echo KText::_('ZIP code');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="zipcode" name="zipcode" value="<?php echo hsc($this->orderRecord->orderAddress->zipcode);?>" />
					</div>
					<div class="validation-tooltip"></div>
				</div>
				<?php } ?>
			
				<?php if ($this->userFields['city']->show_checkout) { ?>
				<div class="<?php echo $this->getFieldClasses($this->userFields, 'city');?>">
					<label for="city" class="field-label"><?php echo KText::_('City');?></label>
					<div class="form-field">
						<?php echo CbcheckoutCountryHelper::getCityInputField('city', $this->orderRecord->orderAddress->city_id, KText::_('Select City'), $this->orderRecord->orderAddress->county_id, $this->orderRecord->orderAddress->city );?>
					</div>
					<div class="validation-tooltip"></div>
				</div>
				<?php } ?>
				
			<?php } ?>
			
			<?php if ($this->userFields['country']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'country');?>">
				<label for="country" class="field-label"><?php echo KText::_('Country');?></label>
				<div class="form-field">
					<?php echo CbcheckoutCountryHelper::createCountrySelect('country', $this->orderRecord->orderAddress->country, KText::_('Select a country'), 'state');?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['state']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'state');?>">
				<label for="state" class="field-label"><?php echo KText::_('State');?></label>
				<div class="form-field">
					<?php echo CbcheckoutCountryHelper::createStateSelect('state', $this->orderRecord->orderAddress->state, $this->orderRecord->orderAddress->country, NULL, 'county_id');?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['county_id']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'county_id');?>" style="display:<?php echo(CbcheckoutCountryHelper::hasCounties($this->orderRecord->orderAddress->state)) ? 'block':'none'; ?>">
				<label for="county" class="field-label"><?php echo KText::_('County');?></label>
				<div class="form-field">
					<?php echo CbcheckoutCountryHelper::createCountySelect('county_id', $this->orderRecord->orderAddress->county_id, $this->orderRecord->orderAddress->state, KText::_('Select County'), 'city');?>
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if (CbcheckoutCountryHelper::systemUsesCities() == true) { ?>
				
				<?php if ($this->userFields['zipcode']->show_checkout) { ?>
				<div class="<?php echo $this->getFieldClasses($this->userFields, 'zipcode');?>">
					<label for="zipcode" class="field-label"><?php echo KText::_('ZIP code');?></label>
					<div class="form-field">
						<input class="textfield" type="text" id="zipcode" name="zipcode" value="<?php echo hsc($this->orderRecord->orderAddress->zipcode);?>" />
					</div>
					<div class="validation-tooltip"></div>
				</div>
				<?php } ?>
				
				<?php if ($this->userFields['city']->show_checkout) { ?>
				<div class="<?php echo $this->getFieldClasses($this->userFields, 'city');?>">
					<label for="city" class="field-label"><?php echo KText::_('City');?></label>
					<div class="form-field">
						<?php echo CbcheckoutCountryHelper::getCityInputField('city', $this->orderRecord->orderAddress->city_id, KText::_('Select City'), $this->orderRecord->orderAddress->county_id, $this->orderRecord->orderAddress->city );?>
					</div>
					<div class="validation-tooltip"></div>
				</div>
				<?php } ?>
				
			<?php } ?>
			
			<?php if ($this->userFields['email']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'email');?>">
				<label for="email" class="field-label"><?php echo KText::_('Email');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="email" name="email" value="<?php echo hsc($this->orderRecord->orderAddress->email);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
			<?php if ($this->userFields['phone']->show_checkout) { ?>
			<div class="<?php echo $this->getFieldClasses($this->userFields, 'phone');?>">
				<label for="phone" class="field-label"><?php echo KText::_('Phone');?></label>
				<div class="form-field">
					<input class="textfield" type="text" id="phone" name="phone" value="<?php echo hsc($this->orderRecord->orderAddress->phone);?>" />
				</div>
				<div class="validation-tooltip"></div>
			</div>
			<?php } ?>
			
		</div> <!-- .order-address-fields -->
		
	</div> <!-- .order-address-section-delivery -->

	<div class="clear"></div>
	
</div> <!-- .order-address-sections -->	

<div class="order-address-field field-comment">
	<label class="field-label" for="comment"><?php echo KText::_('Comment');?></label>
	<div class="form-field">
		<textarea id="comment" name="comment" cols="20" rows="5"><?php echo hsc($this->orderRecord->comment);?></textarea>
	</div>
</div>

<?php 
$this->renderView('default_account_exists_section');
?>

<div class="order-address-buttons">
	<a class="trigger-save-order-address navbutton-medium leftmost"><span class="nav-center"><i class="fa fa-spin fa-spinner loading-indicator"></i><?php echo KText::_('Update Address');?></span></a>
</div>


