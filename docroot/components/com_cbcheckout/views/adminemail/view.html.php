<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewAdminemail extends KenedoView {
	
	function display($tpl = null) {
		
		$this->assignRef('pageTabs',KenedoViewHelper::getTabItems());
		
		$model = $this->getModel();
		
		$userKeys = $model->getUserInfoKeys();
		$orderKeys = $model->getOrderInfoKeys();
		$storeKeys = $model->getStoreInfoKeys();

		$this->assignRef('userKeys',$userKeys);
		$this->assignRef('orderKeys',$orderKeys);
		$this->assignRef('storeKeys',$storeKeys);

		
		parent::display();
	}
	
}
