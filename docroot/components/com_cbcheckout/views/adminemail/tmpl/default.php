<?php
defined('CB_VALID_ENTRY') or die();
?>
<div id="view-<?php echo hsc($this->view);?>" class="<?php hsc($this->renderViewCssClasses());?>">

<form class="kenedo-details-form" method="post" target="kenedo-form-target" enctype="multipart/form-data" action="<?php echo KLink::getRoute('index.php');?>">

	<?php echo (count($this->pageTabs)) ? KenedoViewHelper::renderTabItems($this->pageTabs) : '';?>
	<?php echo (count($this->pageTasks)) ? KenedoViewHelper::renderTaskItems($this->pageTasks) : ''; ?>
	<?php if (!empty($this->pageTitle)) { ?><h1 class="kenedo-page-title"><?php echo hsc($this->pageTitle);?></h1><?php } ?>
	
	<div class="kenedo-messages">
		<div class="kenedo-messages-error"></div>
		<div class="kenedo-messages-notice"></div>
	</div>
	
	<div class="kenedo-fields">
		<?php 
		foreach($this->fields as $fieldName=>$field) {
			$field->setData($this->item);
			if ($field->usesWrapper()) {
				?>
			 	<div id="<?php echo $field->getCssId();?>" class="<?php echo $field->renderCssClasses();?>">
			 		<?php if ($field->doesShowAdminLabel()) { ?>
			 			<div class="field-label"><?php echo $field->getLabelAdmin();?></div>
			 		<?php } ?>
			 		<div class="field-body"><?php echo $field->getBodyAdmin();?></div>
			 	</div>
				<?php
			} 
			else {
				echo $field->getBodyAdmin();
			}
		}
		?>
		<div class="clear"></div>
	</div>
	
	<?php include(dirname(__FILE__).DS.'placeholders.php');?>
	
	<?php if (!empty($this->itemUsage)) { ?>
		<div class="kenedo-item-usage">
			<div class="kenedo-usage-message"><?php echo KText::_('The item is in use in the following entries:');?></div>
			<?php foreach ($this->itemUsage as $fieldName=>$items) { ?>
				<?php foreach ($items as $item) { ?>
				<div>
					<span class="kenedo-candelete-usage-entry-name"><?php echo hsc($fieldName);?></span> - <a class="kenedo-new-tab kenedo-candelete-usage-entry-link" href="<?php echo $item->link;?>" class="new-tab"><?php echo hsc($item->title);?></a>
				</div>
				<?php } ?>
			<?php } ?>
		</div>
	<?php } ?>
	
	<div class="kenedo-hidden-fields">
		<input type="hidden" id="option" 		name="option" 				value="<?php echo hsc(KRequest::getKeyword('option'));?>" />
		<input type="hidden" id="controller"	name="controller" 			value="<?php echo hsc($this->controller);?>" />
		<input type="hidden" id="task" 			name="task" 				value="save" />
		<input type="hidden" id="ajax_sub_view" name="ajax_sub_view" 		value="<?php echo ($this->isAjaxSubview()) ? '1':'0';?>" />		
		<input type="hidden" id="in_modal"		name="in_modal" 			value="<?php echo hsc(KRequest::getInt('in_modal','0'));?>" />
		<input type="hidden" id="tmpl"			name="tmpl" 				value="component" />
		<input type="hidden" id="id"			name="id" 					value="<?php echo (int)$this->item->id; ?>" />
		<input type="hidden" id="lang"			name="lang" 				value="<?php echo hsc(substr(KenedoPlatform::get('languageTag'),0,2));?>" />
		<!-- unencoded return url "<?php echo $this->returnUrl;?>" -->
		<input type="hidden" id="return" 		name="return" 				value="<?php echo KLink::base64UrlEncode($this->returnUrl);?>" />
		<input type="hidden" id="key" 			name="key" 					value="<?php echo KRequest::getString('key','');?>" />
		<input type="hidden" id="form_custom_1" name="form_custom_1" 		value="<?php echo hsc(KRequest::getString('form_custom_1'));?>" />
		<input type="hidden" id="form_custom_2" name="form_custom_2" 		value="<?php echo hsc(KRequest::getString('form_custom_2'));?>" />
		<input type="hidden" id="form_custom_3" name="form_custom_3" 		value="<?php echo hsc(KRequest::getString('form_custom_3'));?>" />
		<input type="hidden" id="form_custom_4" name="form_custom_4" 		value="<?php echo hsc(KRequest::getString('form_custom_4'));?>" />
		<?php if (KenedoPlatform::getName() == 'magento') { ?>		
		<input type="hidden" id="form_key" 		name="form_key" 			value="<?php echo Mage::getSingleton('core/session')->getFormKey();?>" />
		<?php } ?>
	</div>
	
	<div class="kenedo-footer-info">
		<div class="kenedo-version"><?php echo KText::sprintf('ConfigBox Product Configurator by Elovaris Applications - Version %s',CONFIGBOX_VERSION);?></div>
		<div class="kenedo-help"></div>		
	</div>
	
	<div class="clear"></div>
	
</form>
<iframe id="kenedo-form-target" name="kenedo-form-target" src="about:blank"></iframe>
</div>
