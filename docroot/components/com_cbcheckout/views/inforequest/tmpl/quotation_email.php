<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this CbcheckoutViewInforequest */
?>
<div id="com_cbcheckout">
	<div id="view-inforequest">
		<div id="template-default">
			<div class="quotation-download">
				<p><?php echo KText::_('Thank you for your quotation request. We sent you an email with your quotation attached.');?></p>
				<p><a class="trigger-close-modal button-frontend-small"><?php echo KText::_('Close');?></a></p>
			</div>
		</div>
	</div>
</div>
