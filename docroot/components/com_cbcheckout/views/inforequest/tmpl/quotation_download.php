<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this CbcheckoutViewInforequest */
?>
<div id="com_cbcheckout">
	<div id="view-inforequest">
		<div id="template-default">
			<div class="quotation-download">
				<p><?php echo KText::_('Thank you, your quotation is ready to download.');?></p>
				<p><a class="trigger-close-modal button-frontend-small"><?php echo KText::_('Close');?></a> <a class="new-tab button-frontend-small" href="<?php echo $this->quotationDownloadUrl;?>"><?php echo KText::_('Download');?></a></p>
			</div>
		</div>
	</div>
</div>
