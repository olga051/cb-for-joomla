<?php 
defined('CB_VALID_ENTRY') or die(); 

// Helper function to display class="required" for fitting fields
function requiredClass($fields,$field,$action) {
	$key = 'require_'.$action;
	if ($fields[$field]->$key) return 'class="required order-address-field field-'.$field.'"';
	else return 'class="order-address-field field-'.$field.'"';
}
$showKey = 'show_'.$this->type;
?>
<div id="com_cbcheckout">
<div id="view-inforequest">
<div id="template-default">
<div id="inforequest-type-<?php echo hsc($this->type);?>">

	<div class="info-request-text">
		<?php if ($this->type == 'assistance') { ?>
			<p><?php echo KText::_('To give you assistance we would need contact information from you.');?></p>
		<?php } elseif ($this->type == 'saveorder') { ?>
			<p><?php echo KText::_('To store your order we would need contact information from you.');?></p>
		<?php } else { ?>
			<p><?php echo KText::_('To send you a quotation we would need contact information from you.');?></p>
		<?php } ?>
	</div>
	
	<?php if (empty($this->customer->joomlaid) && CONFIGBOX_SHOW_RECURRING_LOGIN_CART) { ?>
		<fieldset id="recurring-customer-login">
			<legend><?php echo KText::_('Returning Customers');?></legend>
			
				<p class="registered-users-intro"><?php echo KText::_('Sign in to automatically fill in your preferred address below.');?></p>
				
				<form action="<?php echo KLink::getRoute('index.php', true);?>" method="post">
					<table>
						<tr class="item-username">
							<td class="key"><label for="recurring-email"><?php echo KText::_('Email');?></label></td>
							<td><input type="text" id="recurring-email" name="email" value="" /></td>
						</tr>
						<tr class="item-password">
							<td class="key"><label for="recurring-password"><?php echo KText::_('Password');?></label></td>
							<td><input type="password" id="recurring-password" name="password" value="" /></td>
						</tr>
						<tr class="item-buttons">
							<td colspan="2">
								<a class="new-tab" href="<?php echo KenedoPlatform::get('passwordResetLink');?>"><?php echo KText::_('Retrieve lost password');?></a>
								<input type="submit" name="submitlogin" value="<?php echo KText::_('Sign in');?>" />
							</td>
						</tr>
					</table>
					<div class="hidden-fields">
						<input type="hidden" name="option" 					value="com_cbcheckout" />
						<input type="hidden" name="controller" 				value="user" />
						<input type="hidden" name="task" 					value="loginUser" />
						<input type="hidden" name="return_failure" 			value="<?php echo $this->failureUrl;?>" />
						<input type="hidden" name="return_success" 			value="<?php echo $this->successUrl;?>" />
						<input type="hidden" name="from_grandorder" 		value="<?php echo (int)$this->fromGrandOrder;?>" />
						<input type="hidden" name="from_configbox_order" 	value="<?php echo (int)$this->fromConfigboxOrder;?>" />
						<input type="hidden" name="grandorder_id" 			value="<?php echo (int)$this->grandOrderId; ?>" />
						<input type="hidden" name="configbox_order_id" 		value="<?php echo (int)$this->orderId; ?>" />
						</div>
				</form>
				
		</fieldset>
		<div class="clear"></div>
	<?php } ?>
	
	<fieldset id="info-request" class="info-request info-request-<?php echo hsc($this->type);?>">
		<legend><?php echo KText::_('Contact information');?></legend>
		<form id="order-address-form" action="<?php echo KLink::getRoute('index.php?option=com_cbcheckout');?>" method="post">
			<table class="order-address-fields">
				<tbody>
					
					<?php if ($this->userFields['billingcompanyname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingcompanyname',$this->type);?>>
						<td class="key"><?php echo KText::_('Company');?></td>
						<td><input class="textfield" type="text" name="billingcompanyname" value="<?php echo hsc($this->customer->billingcompanyname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingsalutation_id']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingsalutation_id',$this->type);?>>
						<td class="key"><?php echo KText::_('Salutation');?></td>
						<td><?php echo CbcheckoutUserHelper::getSalutationDropdown('billingsalutation_id',$this->customer->billingsalutation_id);?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingfirstname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingfirstname',$this->type);?>>
						<td class="key"><?php echo KText::_('First Name');?></td>
						<td><input class="textfield" type="text" name="billingfirstname" value="<?php echo hsc($this->customer->billingfirstname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billinglastname']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billinglastname',$this->type);?>>
						<td class="key"><?php echo KText::_('Last Name');?></td>
						<td><input class="textfield" type="text" name="billinglastname" value="<?php echo hsc($this->customer->billinglastname);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingaddress1']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingaddress1',$this->type);?>>
						<td class="key"><?php echo KText::_('Address 1');?></td>
						<td><input class="textfield" type="text" name="billingaddress1" value="<?php echo hsc($this->customer->billingaddress1);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingaddress2']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingaddress2',$this->type);?>>
						<td class="key"><?php echo KText::_('Address 2');?></td>
						<td><input class="textfield" type="text" name="billingaddress2" value="<?php echo hsc($this->customer->billingaddress2);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingzipcode']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingzipcode',$this->type);?>>
						<td class="key"><?php echo KText::_('ZIP Code');?></td>
						<td><input class="textfield" type="text" name="billingzipcode" value="<?php echo hsc($this->customer->billingzipcode);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if (CbcheckoutCountryHelper::systemUsesCities() == false && $this->userFields['billingcity']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingcity',$this->type);?>>
						<td class="key"><?php echo KText::_('City');?></td>
						<td><input class="textfield" type="text" name="billingcity" value="<?php echo hsc($this->customer->billingcity);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingcountry']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingcountry',$this->type);?>>
						<td class="key"><?php echo KText::_('Country');?></td>
						<td><?php echo CbcheckoutCountryHelper::createCountrySelect('billingcountry',$this->customer->billingcountry,KText::_('Select Country'),'billingstate'); ?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingstate']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingstate',$this->type);?>>
						<td class="key"><?php echo KText::_('State');?></td>
						<td><?php echo CbcheckoutCountryHelper::createStateSelect('billingstate',$this->customer->billingstate,$this->customer->billingcountry, NULL, 'billingcounty_id');?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingcounty_id']->$showKey) { ?>
					<tr <?php echo requiredClass($this->userFields,'billingcounty_id',$this->type);?> style="display:<?php echo(CbcheckoutCountryHelper::hasCounties($this->customer->billingstate)) ? 'block':'none'; ?>">
						<td class="key"><?php echo KText::_('County');?></td>
						<td><?php echo CbcheckoutCountryHelper::createCountySelect('billingcounty_id', $this->customer->billingcounty_id, $this->customer->billingstate, KText::_('Select County'), 'billingcity');?></td>
					</tr>
					<?php } ?>
					
					<?php if (CbcheckoutCountryHelper::systemUsesCities() == true && $this->userFields['billingcity']->$showKey) { ?>
					<tr <?php echo requiredClass($this->userFields,'billingcity',$this->type);?>>
						<td class="key"><?php echo KText::_('City');?></td>
						<td class="form-field">
							<?php echo CbcheckoutCountryHelper::getCityInputField('billingcity', $this->customer->billingcity_id, KText::_('Select City'), $this->customer->billingcounty_id, $this->customer->billingcity );?>
						</td>
					</div>
					<?php } ?>
					
					<?php if ($this->userFields['billingemail']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingemail',$this->type);?>>
						<td class="key"><?php echo KText::_('Email');?></td>
						<td><input class="textfield" type="text" name="billingemail" value="<?php echo hsc($this->customer->billingemail);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['billingphone']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'billingphone',$this->type);?>>
						<td class="key"><?php echo KText::_('Phone');?></td>
						<td><input class="textfield" type="text" name="billingphone" value="<?php echo hsc($this->customer->billingphone);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['language_tag']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'language_tag',$this->type);?>>
						<td class="key"><?php echo KText::_('Language');?></td>
						<td><?php echo ConfigboxLanguageHelper::getLangSelect($this->customer->language_tag, 'language_tag'); ?></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['vatin']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'vatin',$this->type);?>>
						<td class="key"><?php echo KText::_('VAT IN');?></td>
						<td><input class="textfield" type="text" name="vatin" value="<?php echo hsc($this->customer->vatin);?>" /></td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->userFields['newsletter']->$showKey) :?>
					<tr <?php echo requiredClass($this->userFields,'newsletter',$this->type);?>>
						<td class="key"><?php echo KText::_('RFQ_FORM_NEWSLETTER');?></td>
						<td>
							<input type="radio" <?php echo ($this->customer->newsletter == 1)? 'checked="checked"':'';?> id="newsletter-yes" name="newsletter" value="1" />
							<label for="newsletter-yes"><?php echo KText::_('CBYES');?></label>
							<input type="radio" <?php echo ($this->customer->newsletter != 1)? 'checked="checked"':'';?> id="newsletter-no" name="newsletter" value="0" />
							<label for="newsletter-no"><?php echo KText::_('CBNO');?></label>
						</td>
					</tr>
					<?php endif;?>
					
					<?php if ($this->type == 'quotation') { ?>
					<tr class="order-comment">
						<td class="key"><?php echo KText::_('RFQ_FORM_COMMENT');?></td>
						<td><textarea class="comment-textarea" name="comment" cols="20" rows="5"></textarea></td>
					</tr>
					<?php } ?>
					
				</tbody>
			</table>
			<div class="hidden-fields">
				<input type="hidden" name="samedelivery"	value="1" />
				<input type="hidden" name="option" 			value="com_cbcheckout" />
				<input type="hidden" name="controller" 		value="user" />
				<input type="hidden" name="tmpl" 			value="component" />
				<input type="hidden" name="lang" 			value="<?php echo hsc(KText::$languageCode);?>" />
				<input type="hidden" name="task" 			value="saveUser" />
				<input type="hidden" name="success_message" value="0" />
				<input type="hidden" name="type" 			value="<?php echo hsc($this->type);?>" />
				<input type="hidden" name="return_failure" 	value="<?php echo $this->failureUrl;?>" />
				<input type="hidden" name="return_success" 	value="<?php echo $this->successUrl;?>" />
			</div>
		</form>
	</fieldset>
	
	<div id="buttons">
		
		<a rel="next nofollow" class="navbutton-medium next floatright button-send-info-request trigger-send-address-form">
			
			<?php if ($this->type == 'quotation') { ?>
				<span class="nav-center"><?php echo KText::_('RFQ_FORM_CONTINUE_REQUEST_QUOTE');?></span>
			<?php } elseif($this->type == 'assistance') { ?>
				<span class="nav-center"><?php echo KText::_('RFQ_FORM_CONTINUE_REQUEST_ASSISTANCE');?></span>
			<?php } elseif($this->type == 'saveorder') { ?>
				<span class="nav-center"><?php echo KText::_('RFQ_FORM_CONTINUE_SAVE_ORDER');?></span>
			<?php } ?>
		</a>
		
		<a class="navbutton-medium floatleft leftmost button-cancel-info-request" onclick="window.parent.cbj.colorbox.close();">
			<span class="nav-center"><?php echo KText::_('Close');?></span>
		</a>
		
	</div>
	<div class="clear"></div>
	
	<script type="text/javascript">
	cbj(document).ready(function(){

		window.parent.cbj('body').css('overflow','auto');
		
		window.parent.cbj.colorbox.resize({
			innerHeight: cbj('body').outerHeight() + 30 + 'px'
	        
	    });
	});
	</script>
</div>
</div>
</div>
</div>