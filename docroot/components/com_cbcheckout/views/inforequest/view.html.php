<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewInforequest extends KenedoView {
	
	function display() {

		$userId = CbcheckoutUserHelper::getUserId();

		$userData = CbcheckoutUserHelper::getUser($userId, false, true);

		if (KRequest::getVar('success', NULL) == 0) {
			$formState = KSession::get('userFormState', array());
			foreach ($userData as $key=>$value) {
				if (!empty($formState[$key])) {
					$userData->$key = $formState[$key];
				}
			}
			KSession::delete('userFormState');
		}

		$this->assignRef('customer',$userData);

		$user = CbcheckoutUserHelper::getUser();
		$userFields = CbcheckoutUserHelper::getUserFields($user->group_id);

		$this->assignRef('userFields',$userFields);

		KenedoPlatform::p()->addScriptDeclaration("var userFields = ".json_encode($userFields).";", true);

		$type = KRequest::getKeyword('type','quotation');
		$this->assignRef('type',$type);

		$fromGrandOrder = KRequest::getInt('from_grandorder',0);
		$this->assignRef('fromGrandOrder',$fromGrandOrder);

		$fromConfigboxOrder = KRequest::getInt('from_configbox_order',0);
		$this->assignRef('fromConfigboxOrder',$fromConfigboxOrder);

		$grandOrderId = KRequest::getInt('grandorder_id',0);
		$this->assignRef('grandOrderId',$grandOrderId);

		$orderId = KRequest::getInt('configbox_order_id',0);
		$this->assignRef('orderId',$orderId);


		if ($fromGrandOrder) {
			$this->assign('successUrl', urlencode( KLink::getRoute('index.php?option=com_cbcheckout&view=inforequest&tmpl=component&type='.hsc($type).'&from_grandorder=1&grandorder_id='.(int)$grandOrderId.'&success=1', false) ));
			$this->assign('failureUrl', urlencode( KLink::getRoute('index.php?option=com_cbcheckout&view=inforequest&tmpl=component&type='.hsc($type).'&from_grandorder=1&grandorder_id='.(int)$grandOrderId.'&success=0', false) ));
		}
		elseif($fromConfigboxOrder) {
			$this->assign('successUrl', urlencode( KLink::getRoute('index.php?option=com_cbcheckout&view=inforequest&tmpl=component&type='.hsc($type).'&from_configbox_order=1&configbox_order_id='.(int)$orderId.'&grandorder_id='.(int)$grandOrderId.'&success=1', false) ));
			$this->assign('failureUrl', urlencode( KLink::getRoute('index.php?option=com_cbcheckout&view=inforequest&tmpl=component&type='.hsc($type).'&from_configbox_order=1&configbox_order_id='.(int)$orderId.'&grandorder_id='.(int)$grandOrderId.'&success=0', false) ));
		}
		else {
			return;
		}

		$this->renderView();
		
	}
}