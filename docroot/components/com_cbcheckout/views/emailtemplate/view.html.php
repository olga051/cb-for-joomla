<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewEmailtemplate extends KenedoView {
	
	function display() {
			
		$shopModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		$shopData = $shopModel->getItem();
		$this->assignRef('shopData',$shopData);
		
		$maxWidth = 250;
		$maxHeight = 60;
		
		$filePath = KPATH_ROOT.DS.'components'.DS.'com_cbcheckout'.DS.'data'.DS.'shoplogo'.DS. $this->shopData->shoplogo;
		if ($filePath) {
				
			$this->assign('useShopLogo',true);
			$this->assign('shopLogoUrl', KPATH_ROOT_URL.'components/com_cbcheckout/data/shoplogo'.DS. $this->shopData->shoplogo);
				
			$image = new ConfigboxImageResizer($filePath);
				
			if ($image->width > $maxWidth || $image->height > $maxHeight) {
				$dimensions = $image->getDimensions($maxWidth, $maxHeight, 'containment');
				$this->assign('shopLogoWidth',	intval($dimensions['optimalWidth']));
				$this->assign('shopLogoHeight',	intval($dimensions['optimalHeight']));
			}
			else {
				$this->assign('shopLogoWidth',	$image->width);
				$this->assign('shopLogoHeight',	$image->height);
			}
				
		}
		else {
			$this->assign('useShopLogo',false);
		}
		
		if (empty($this->emailContent)) {
			echo 'No email content set';
		}
		else {
			$this->renderView();
		}
		
	}
	
}