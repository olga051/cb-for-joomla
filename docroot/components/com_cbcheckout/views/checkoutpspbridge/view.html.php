<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewCheckoutpspbridge extends KenedoView {
	
	function display() {
		
		$orderModel = KenedoModel::getModel('CbcheckoutModelOrder');
		$orderId = $orderModel->getId();
		
		$orderRecord = $orderModel->getOrderRecord($orderId);
		$this->assignRef('orderRecord',$orderRecord);
		
		// Get shop data
		$shopModel = KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		$shopdata = $shopModel->getItem();
		$this->assignRef('shopdata',$shopdata);
		
		$this->assign('statid',0);
		
		if (!empty($orderRecord->payment)) {
				
			$langUrlCode = KenedoPlatform::p()->getLanguageUrlCode( CONFIGBOX_LANGUAGE_TAG );
				
			// New path generation for payment options
			$notificationPath = KLink::getRoute('index.php?Itemid=0&option=com_cbcheckout&task=processipn&controller=ipn&connector_name='.$orderRecord->payment->connector_name,false);
				
			$successPath = KLink::getRoute('index.php?option=com_cbcheckout&view=userorder&order_id='.$this->orderRecord->id,false);
			$failurePath = KLink::getRoute('index.php?option=com_cbcheckout&view=checkout',false);
			$cancelPath = KLink::getRoute('index.php?option=com_cbcheckout&view=checkout',false);
				
			$prefixNormal = 'http://'.KPATH_HOST;
			$prefixSecure = 'https://'.KPATH_HOST;
				
			if (CONFIGBOX_SECURECHECKOUT) {
				$successUrl 		= $prefixSecure . $successPath;
				$failureUrl 		= $prefixSecure . $failurePath;
				$cancelUrl 			= $prefixSecure . $cancelPath;
				$notificationUrl 	= $prefixSecure . $notificationPath;
			}
			else {
				$successUrl 		= $prefixNormal . $successPath;
				$failureUrl 		= $prefixNormal . $failurePath;
				$cancelUrl 			= $prefixNormal . $cancelPath;
				$notificationUrl 	= $prefixNormal . $notificationPath;
			}
				
			$notificationUrlNormal = $prefixNormal . $notificationPath;
			$notificationUrlSecure = $prefixSecure . $notificationPath;
				
			$pspBridgeFilePath = ConfigboxPspHelper::getPspConnectorFolder($orderRecord->payment->connector_name) . DS . 'bridge.php';
			$this->assignRef('pspBridgeFilePath', $pspBridgeFilePath);
				
			$this->assignRef('successUrl', $successUrl);
			$this->assignRef('failureUrl', $failureUrl);
			$this->assignRef('cancelUrl', $cancelUrl);
			$this->assignRef('notificationUrl', $notificationUrl);
			$this->assignRef('notificationUrlNormal', $notificationUrlNormal);
			$this->assignRef('notificationUrlSecure', $notificationUrlSecure);
			
			$this->renderView();
			
		}
		
	}
}