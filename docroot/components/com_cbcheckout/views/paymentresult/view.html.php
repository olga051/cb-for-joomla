<?php
defined('CB_VALID_ENTRY') or die();

class CbcheckoutViewPaymentresult extends KenedoView {
	
	function display(){
		
		$orderModel 	= KenedoModel::getModel('CbcheckoutModelOrder');
		$shopDataModel 	= KenedoModel::getModel('CbcheckoutModelAdminshopdata');
		
		$orderId 		= $orderModel->getId();
		$orderRecord 	= $orderModel->getOrderRecord($orderId);
		$shopData 		= $shopDataModel->getItem();
		
		$this->assign('linkToOrder', KLink::getRoute('index.php?option=com_cbcheckout&view=userorder&order_id='.$orderRecord->id));
		$this->assign('linkToCustomerProfile', KLink::getRoute('index.php?option=com_cbcheckout&view=user'));
		$this->assign('linkToDefaultProductListing', KLink::getRoute('index.php?option=com_configbox&view=productlisting&pcat_id='.CONFIGBOX_CONTINUE_LISTING_ID));
		
		$this->assignRef('user',		$orderRecord->orderAddress);
		$this->assignRef('orderRecord',	$orderRecord);
		$this->assignRef('shopdata',	$shopData);
		$this->assignRef('total',		$orderRecord->payableAmount);
		
		$placeOrderPermitted = ConfigboxOrderHelper::isPermittedAction('placeOrder',$orderRecord);
		$this->assignRef('placeOrderPermitted',$placeOrderPermitted);
				
		if ($orderRecord->payment->connector_name) {
			
			$connectorFolder = ConfigboxPspHelper::getPspConnectorFolder($orderRecord->payment->connector_name);
			
			if (file_exists($connectorFolder.DS.'result.php' )) {
				include($connectorFolder.DS.'result.php');
				return true;
			}
		}
		
		$this->renderView();
		
	}
	
}