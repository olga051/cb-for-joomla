<?php 
defined('CB_VALID_ENTRY') or die(); 
?>
<div id="com_cbcheckout">
	<div id="view-paymentresult">
	
		<p><?php echo KText::_('Thank you for your order.');?>
		<?php  
		if (!empty($this->shopdata->shopemailsales)) {
			echo KText::_('If you have any further questions please send an email to'). ' ';
			?>
			<a href="mailto:<?php echo $this->shopdata->shopemailsales;?>"><?php echo $this->shopdata->shopemailsales;?></a>
			<?php
			
			if (!empty($this->shopdata->shopphonesales)) {
				echo KText::sprintf('or call us at %s',$this->shopdata->shopphonesales);
			}
			?>.
			
			<?php
		}
		?>
		</p>
		
		<ul>		
			<li><a href="<?php echo $this->linkToOrder;?>"><?php echo KText::_('See your order status');?></a></li>
			<li><a href="<?php echo $this->linkToCustomerProfile;?>"><?php echo KText::_('Go to your customer profile');?></a></li>
			<li><a href="<?php echo $this->linkToDefaultProductListing;?>"><?php echo KText::_('Continue shopping');?></a></li>
		</ul>
		
	</div>
</div>