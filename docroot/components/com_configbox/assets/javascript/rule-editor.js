/**
 * @module configbox/ruleEditor
 */
define(['cbj', 'kenedo', 'configbox/server'], function(cbj, kenedo, server) {
	"use strict";

	/**
	 * @exports configbox/ruleEditor
	 */
	var module = {

		initRuleEditor: function() {

			// Create a jQuery function for making an HTML element a dropzone
			cbj.fn.makeDropZone = function() {

				if (typeof(arguments[0]) != 'undefined') {
					var draggedItem = arguments[0];
				}

				this.droppable({

					hoverClass: 'drag-over',
					tolerance: 'pointer',
					drop: function( event, ui ) {

						// Get a clone of the draggable and remove draggable attributes
						var newItem = cbj(ui.helper).clone(true,true).removeClass('ui-draggable ui-draggable-dragging').removeAttr('style');

						// Remove the original item if moved inside the item
						if (typeof(draggedItem) != 'undefined' && draggedItem.closest('#rule').length) {
							cbj(ui.draggable).draggable('option','revertDuration',0);
							draggedItem.remove();
						}

						// Make it a new draggable
						newItem.makeDraggableItem();

						// Replace the drop-area with the new item
						cbj(this).replaceWith(newItem);

					}

				});
			};

			cbj.fn.makeDraggableItem = function() {

				this.draggable({

					addClasses: false,
					revert: true,
					helper: 'clone',
					opacity: 0.8,

					start: function() {

						// Hide original if in rule
						if (cbj(this).closest('#rule').length) {
							cbj(this).hide();
						}

						// Remove pre-existing drop-areas
						cbj('#rule .drop-area').not('.initial').remove();

						// Set revert duration to 100ms
						cbj(this).draggable('option','revertDuration',100);

						var dropZoneSelectorBefore = '';
						var dropZoneSelectorAfter = '';

						// Figure out where to put drop zones
						if (cbj(this).is('.condition') || cbj(this).is('.bracket')) {
							dropZoneSelectorBefore = '.combinator:first-child';
							dropZoneSelectorAfter = '.combinator';
						}

						if (cbj(this).is('.combinator')) {
							dropZoneSelectorBefore = '.condition + .condition, .bracket + .bracket, .condition + .bracket, .condition:first-child, .bracket:first-child';
							dropZoneSelectorAfter = '.condition:last-child, .bracket:last-child';
						}

						if (!dropZoneSelectorBefore || !dropZoneSelectorAfter) {
							throw('Could not figure out dropzone selectors.');
						}

						if (cbj('#rule .drop-area').length == 0) {
							var dropZoneMarkup = '<span class="drop-area">&nbsp;</span>';

							// Add drop zone markup
							cbj('#rule').find(dropZoneSelectorBefore).before(dropZoneMarkup);
							cbj('#rule').find(dropZoneSelectorAfter).after(dropZoneMarkup);
							cbj('#rule').find('.bracket:empty').html(dropZoneMarkup);

							if (cbj('#rule .condition').length == 0) {
								cbj('#rule').html(dropZoneMarkup);
							}

							if (cbj(this).closest('#rule').length) {
								cbj(this).after(dropZoneMarkup);
							}

							// Add drop zone functionality
							cbj('#rule .drop-area').makeDropZone( cbj(this) );
							cbj('#rule .parameter-drop-area').makeDropZone( cbj(this) );

						}

					},

					stop: function() {

						// Remove drop-zones
						if ( cbj('#rule .item').length !== 0) {
							cbj('#rule .drop-area').remove();
						}

						// Show the previously hidden original item
						cbj(this).show();
					}

				});

				return this;

			};

			// Make items in terms draggable
			cbj('.view-adminruleeditor #rule .item').makeDraggableItem();

			// Make combinators draggable
			cbj('#combinator-blueprints .item').makeDraggableItem();

			// Make items in the selected panel draggable
			cbj('.view-adminruleeditor .selected-panel .item').makeDraggableItem();

			// On selection of a new panel, make items in there draggable
			cbj('.view-adminruleeditor').on('panelSelected', function() {
				cbj('.view-adminruleeditor .selected-panel .item').makeDraggableItem();
			});

			// Add drop zone functionality to initial area
			cbj('.view-adminruleeditor .drop-area').makeDropZone();

			// Clicks on items make the item selected (and other items unselected, unless shift key is held)
			cbj('.view-adminruleeditor #rule').on('click', '.item', function( event ) {

				// Unselect anything, unless shift key is held
				if (!event.shiftKey) {
					cbj('.view-adminruleeditor #rule .selected').removeClass('selected');
				}

				// Clicks on .input make no selection
				if (cbj(event.target).is('.input')) {
					return;
				}

				// Add the 'selected' class to the item clicked on
				if (cbj(event.target).is('.item')) {
					cbj(event.target).addClass('selected');
					return;
				}

				if (cbj(event.target).closest('.item').length != 0) {
					cbj(event.target).closest('.item').addClass('selected');
				}

			});

			// Clicks on the rule area (outside items) should unselect any item
			cbj('.view-adminruleeditor').on('click', function(event) {
				// Unless the click was made on an item, unselect anthing
				if (cbj(event.target).is('.item') == false && cbj(event.target).closest('.item').length == 0) {
					cbj('.view-adminruleeditor #rule .selected').removeClass('selected');
				}
			});

			// Trigger for item squeezer button
			cbj('.view-adminruleeditor').on('click', '.button-limit-condition-width', function() {
				cbj('#rule').toggleClass('squeeze');
			});

			// Trigger for removing items
			cbj('.view-adminruleeditor').on('click', '.button-remove-selected-items', function() {
				module.removeSelectedItems();
			});

			// Trigger for putting items in brackets
			cbj('.view-adminruleeditor').on('click', '.button-put-in-brackets', function() {
				module.putInBrackets();
			});

			// Key-press functionality
			cbj(document).keyup(function(event) {

				switch (event.which) {

					// Back-space for removing items
					case 46:
						module.removeSelectedItems();
						break;

				}

				event.preventDefault();
			});

			// Store button
			cbj('.view-adminruleeditor').on('click', '.button-store', module.storeRule);

			// Cancel button
			cbj('.view-adminruleeditor').on('click', '.button-cancel', function() {
				cbj(this).closest('.modal').modal('hide');
			});

			// Tab functionality for switching between condition types
			cbj('.view-adminruleeditor').on('click', '.picker-tab', function() {
				var panelId = cbj(this).attr('id');
				cbj(this).addClass('selected-tab').siblings().removeClass('selected-tab');
				cbj('.picker-panels .' + panelId).addClass('selected-panel').siblings().removeClass('selected-panel');
				cbj('.view-adminruleeditor').trigger('panelSelected', [panelId]);
			});

			// Adjust the text field widths in condition items on startup
			cbj('.view-adminruleeditor .condition input[type=text]').each(module.adjustTextFieldWidth);

			// Adjust the text field widths in condition items on change
			cbj('.view-adminruleeditor').on('keyup change click', '.condition input[type=text]', module.adjustTextFieldWidth);

			// Clicks on .condition-operator make the operator picker appear
			cbj('.view-adminruleeditor').on('click', '.condition-operator', function() {

				// Remove any existing pickers
				cbj('.condition .operator-picker').remove();

				var operatorNode;

				// Questions with predefined answers get the 'is' and 'is not' operator, rest the full lower than etc.
				if (cbj(this).closest('.condition').data('field') == 'selectedOption.id') {
					operatorNode = cbj('#operator-picker-blueprint .operator-picker-short');
				}
				else {
					operatorNode = cbj('#operator-picker-blueprint .operator-picker-full');
				}

				// Clone and copy the picker in place
				operatorNode.clone().appendTo(cbj(this));

			});

			// Remove the operator picker on clicks anywhere but the .condition-operator element (which is there to make it appear)
			cbj('.view-adminruleeditor').on('click', function(event) {

				if (cbj(event.target).is('.condition-operator')) {
					return;
				}

				cbj('.condition .operator-picker').remove();

			});

			// Functionality for choosing an operator for condition
			cbj('.view-adminruleeditor #rule, .view-adminruleeditor #condition-picker').on('click', '.operator', function() {
				var operator = cbj(this).data('operator');
				cbj(this).closest('.condition').data('operator', operator).attr('data-operator', operator);
				cbj(this).closest('.condition-operator').text(cbj(this).text() + ' ');
			});

			/* Questions panel - filtering functionality - START */

			// Page filter functionality
			cbj('body').on('change', '.view-adminruleeditor .page-filter select', function() {

				// Get the selected page ID
				var pageId = cbj(this).val();

				// Hide all questions
				cbj('.question-picker li').removeClass('shown').removeClass('selected');

				// Prepare the CSS selector for shown questions
				var selector;

				if (pageId == 0) {
					selector = '.question-picker .question-list li';
				}
				else {
					selector = '.question-picker .question-list li.page-' + pageId;
				}

				// Show them
				cbj( selector ).addClass('shown');

				// Reset the text filter
				cbj('#question-filter').val('');

				// Show anything that was hidden by the text filter
				cbj('.question-picker li.hidden-by-question-filter').removeClass('hidden-by-question-filter');

			});

			// Key up for the text filter
			cbj('#question-filter').keyup(function(){

				var filterText = cbj(this).val();
				filterText = cbj.trim( filterText ).toLowerCase();

				if (filterText == '') {
					cbj('.question-picker li').removeClass('hidden-by-question-filter');
				}

				cbj('.question-picker li.shown').each(function(){
					var text = cbj.trim(cbj(this).text()).toLowerCase();
					if ( text.indexOf(filterText) == -1) {
						cbj(this).addClass('hidden-by-question-filter');
					}
					else {
						cbj(this).removeClass('hidden-by-question-filter');
					}
				});

			});

			// Clicks on the left questions make the conditions for that question appear
			cbj('.view-adminruleeditor').on('click', '.question-list li', function() {

				var questionId = cbj(this).data('question-id');
				cbj('#answer-group-' + questionId).show().siblings().hide();

				// Make the shown items draggable
				cbj('#question-attributes #answer-group-' + questionId + ' .item').makeDraggableItem();

				cbj(this).addClass('picked').siblings().removeClass('picked');

				cbj('#question-attributes').show();

			});

			// Make the conditions of the first shown question show up
			// cbj('.view-adminruleeditor .question-picker .shown').first().trigger('click');


		},


		storeRule : function(event) {

			// Stop propagation, just in case
			event.stopPropagation();

			// Remove all drop areas (just in case there are any left)
			cbj('.view-adminruleeditor #rule .drop-area').remove();

			// Get the ID of the return field
			var returnId = cbj('.view-adminruleeditor').data('return-field-id');

			// Prepare the rule JSON
			var jsonRule = '';

			// Get the rule's conditions
			var ruleConditions = module.getRuleItems(cbj(this).closest('.view-adminruleeditor').find('#rule'));

			// Modify the parent's controls
			if (ruleConditions.length) {
				// Hide the parent form's edit button (will show the rule instead)
				// cbj('#edit-button-' + returnId).hide();
				jsonRule = JSON.stringify(ruleConditions).replace(/^"/g,'').replace(/"$/g,'');
			}

			// Write the rule json string to the input field of the form
			cbj('#'+returnId).val(jsonRule).trigger('change');

			// Make the edit controls of the rule into display-only fields
			// Got to be done on the original in the rule editor, when copying the HTML the values of the inputs aren't updated.
			cbj('.view-adminruleeditor #rule').find('input').each(function(){
				cbj(this).replaceWith('<span class="condition-value">' + cbj(this).val() + '</span>');
			});

			// Copy the rule HTML over to the display wrapper of the parent's form (To show the rule)
			cbj('#rule-text-' + returnId).html( cbj('.view-adminruleeditor #rule').html() );

			if (jsonRule) {
				cbj('#rule-text-' + returnId).closest('.rule-wrapper').addClass('has-rule').removeClass('has-no-rule');
			}
			else {
				cbj('#rule-text-' + returnId).closest('.rule-wrapper').removeClass('has-rule').addClass('has-no-rule');
			}

			// Close the modal window
			cbj(this).closest('.modal').modal('hide');

		},

		/**
		 *
		 * @param {jQuery} parentHtml jQuery that has the wrapper of the rule items selected
		 * @returns {Array} With objects for each item containing its metadata (or arrays for brackets containing the same)
		 */
		getRuleItems : function (parentHtml) {

			var items = [];
			var itemsHtml = parentHtml.children('.item');

			for (var i in itemsHtml) {
				if (itemsHtml.hasOwnProperty(i)) {
					if (parseInt(i) == i) { // hasOwnProperty check of sorts
						var itemData = module.getItemMetadata(cbj(itemsHtml[i]));

						switch(itemData.type) {

							// On brackets we recurse into the child html
							case 'bracket':
								items[i] = module.getRuleItems(cbj(itemsHtml[i]));
								break;

							// For functions we check for parameters and them in there are any
							case 'function':

								var parameters = cbj(itemsHtml[i]).find('.parameter');

								// Go into the function's parameter items (they got the same structure as regular conditions)
								itemData.parameters = [];
								for (var p in parameters) {
									if (parameters.hasOwnProperty(p)) {
										if (parseInt(p) == p) { // hasOwnProperty check of sorts
											// Get the items of the parameters..
											var parameterItems = module.getRuleItems(cbj(parameters[p]));
											// ..and push them into the meta data
											itemData.parameters.push(parameterItems);
										}
									}
								}

								items[i] = itemData;
								break;

							// Here's the usual case, simply add the metadata
							default :
								items[i] = itemData;
								break;

						}
					}
				}
			}
			return items;

		},

		/**
		 * Extracts all data attributes from the item
		 * @param {jQuery} item jQuery object with the item selected (the <span> of each item)
		 * @returns {{}}
		 */
		getItemMetadata: function(item) {

			var metaData = cbj(item).data();

			// Get rid of uiDraggable. Can't simply delete it, that would remove it from the element since it is referenced
			var returnData = {};
			for (var i in metaData) {
				if (metaData.hasOwnProperty(i)) {
					if (i != 'uiDraggable') {
						returnData[i] = metaData[i];
					}
				}
			}

			// Get all input values and add them to the meta data
			cbj(item).find('.input').each(function() {

				// Replace localized decimal symbol and check if the number is valid
				var input = cbj(this).val();
				var testValue = input.replace(server.config.decimalSymbol, '.');
				var possibleNumber = Number(testValue);
				// Check if that made sense, if so, use the number version
				if (isNaN(possibleNumber) == false) {
					input = possibleNumber;
				}
				// Get the meta data key
				var key = cbj(this).data('data-key');
				returnData[key] = input;

			});

			return returnData;

		},

		/**
		 * Takes the selected items (identified by the .selected class) and wrap them with bracket HTML
		 */
		putInBrackets : function() {

			// Make some unique ID..
			var newId = Math.ceil(Math.random() * 20000);
			// ..and add it as ID attribute to the created bracket HTML
			cbj('.view-adminruleeditor .selected').wrapAll('<span class="item bracket" data-type="bracket" id="bracket-' + newId + '" />');
			// ..in order to reference to it for making it draggable
			cbj('#bracket-' + newId).makeDraggableItem().removeAttr('id');

			// Unselect any selected items
			cbj('#rule .selected').removeClass('selected');

		},

		/**
		 * Removes the selected items from the rule
		 */
		removeSelectedItems: function() {

			// Loop through all .selected items in the terms
			cbj('.view-adminruleeditor .selected').each(function(){

				// If we deal with a bracket, its content stays. We detach, clone the bracket's children, then insert them after the bracket
				if (cbj(this).is('.bracket')) {
					cbj(this).children().detach().clone(true,true).insertAfter(cbj(this));
				}

				// The combinator after the item to remove gets removed too
				cbj(this).next('.combinator').remove();

				// In case it's the last item we remove, remove the combinator before it
				if (cbj(this).is(':last-child')) {
					cbj(this).prev('.combinator').remove();
				}

				// Finally remove the item
				cbj(this).remove();

			});

			// For any function parameters that got removed, replace the void with the parameter drop area
			cbj('.view-adminruleeditor #rule .parameter').each(function(i,parameter){
				parameter = cbj(parameter);
				if (parameter.find('.item').length === 0) {
					var html = '<span class="parameter-drop-area">'+ parameter.data('parameter-name') +'</span>';
					parameter.html(html);
				}
			});

		},

		/**
		 * Adjusts the width of the text field (whatever we got with cbj(this)) to fit its content
		 */
		adjustTextFieldWidth: function() {

			// Get the entered text (or the placeholder text if empty)
			var text = (cbj(this).val() != '') ? cbj(this).val() : cbj(this).attr('placeholder');

			// Insert the text into the test span
			cbj('#width-tester').text(text);

			// Apply relevant styles
			cbj('#width-tester').css('font-size',cbj(this).css('font-size'));
			cbj('#width-tester').css('font-family',cbj(this).css('font-family'));

			// Get the width of the tester span
			var width = parseInt(cbj('#width-tester').css('width'));

			if (width < 10) {
				width = 10;
			}
			width += 10;

			// Apply the width (plus 10px) to the text-field
			cbj(this).css('width', width + 'px');
		}

	};

	return module;

});