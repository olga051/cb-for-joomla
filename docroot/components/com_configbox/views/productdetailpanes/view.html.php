<?php
defined('CB_VALID_ENTRY') or die();

class ConfigboxViewProductdetailpanes extends KenedoView {

	public $component = 'com_configbox';
	public $controllerName = '';

	/**
	 * @var object $product CB product data
	 * @see ConfigboxModelProduct::getProduct
	 */
	public $product;

	/**
	 * @var string 'configuratorPage'|'productPage' Indicates where the panes are rendered
	 */
	public $parentView;

	/**
	 * @return NULL
	 */
	function getDefaultModel() {
		return NULL;
	}

}
