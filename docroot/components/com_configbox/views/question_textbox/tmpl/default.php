<?php
defined('CB_VALID_ENTRY') or die();
/** @var $this ConfigboxViewQuestion_Textbox */
?>
<div id="<?php echo hsc($this->questionCssId);?>" class="<?php echo hsc($this->questionCssClasses);?>" <?php echo $this->questionDataAttributes;?>>

	<?php echo $this->getViewOutput('question_edit_buttons');?>

	<?php echo $this->getViewOutput('question_heading');?>

	<div class="answers">

		<?php echo $this->getViewOutput('question_decoration');?>

		<div class="form-group">

			<?php if ($this->showLabel) { ?>
				<label for="input-<?php echo hsc($this->questionCssId);?>"><?php echo hsc($this->question->title);?></label>
			<?php } ?>

			<?php if ($this->question->unit) { ?><div class="input-group"><?php } ?>

				<input value="<?php echo hsc($this->selection);?>" type="text" id="input-<?php echo hsc($this->questionCssId);?>" class="form-control" aria-label="<?php echo hsc($this->question->title);?>" <?php echo ($this->question->disableControl) ? 'disabled="disabled"' : '';?> />

				<?php if ($this->question->unit) { ?>
					<span class="input-group-addon"><?php echo hsc($this->question->unit);?></span>
				<?php } ?>

			<?php if ($this->question->unit) { ?></div><?php } ?>

			<span class="help-block validation-message-target">
				<?php echo ($this->hasValidationMessage) ? hsc($this->validationMessage) : '';?>
			</span>

		</div>

	</div>

</div>