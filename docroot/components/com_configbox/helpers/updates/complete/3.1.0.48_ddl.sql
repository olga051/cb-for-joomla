create table sltxh_cbcheckout_order_cities
(
	id mediumint unsigned not null,
	order_id int unsigned default '0' not null,
	city_name varchar(200) default '' not null,
	county_id mediumint unsigned not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	ordering mediumint not null,
	published enum('0', '1') default '1' not null,
	primary key (id, order_id)
)
engine=InnoDB
;

create index order_id
	on sltxh_cbcheckout_order_cities (order_id)
;

create index state_id
	on sltxh_cbcheckout_order_cities (county_id)
;

create table sltxh_cbcheckout_order_configurations
(
	id int unsigned auto_increment
		primary key,
	position_id int unsigned null,
	price_net decimal(20,4) default '0.0000' not null,
	price_overrides varchar(1024) default '[]' not null,
	price_recurring_net decimal(20,4) default '0.0000' not null,
	price_recurring_overrides varchar(1024) default '[]' not null,
	element_id int unsigned null,
	element_type varchar(30) not null,
	weight decimal(20,3) default '0.000' not null,
	xref_id int unsigned null,
	option_id int unsigned null,
	value text not null,
	output_value text not null,
	element_code varchar(255) default '' not null,
	option_sku varchar(255) default '' not null,
	option_image varchar(50) default '' not null,
	show_in_overviews tinyint unsigned default '1' not null,
	element_custom_1 text not null,
	element_custom_2 text not null,
	element_custom_3 text not null,
	element_custom_4 text not null,
	assignment_custom_1 text not null,
	assignment_custom_2 text not null,
	assignment_custom_3 text not null,
	assignment_custom_4 text not null,
	option_custom_1 text not null,
	option_custom_2 text not null,
	option_custom_3 text not null,
	option_custom_4 text not null
)
engine=InnoDB collate=utf8_unicode_ci
;

create index position_id
	on sltxh_cbcheckout_order_configurations (position_id)
;

create index element_id
	on sltxh_cbcheckout_order_configurations (element_id)
;

create index xref_id
	on sltxh_cbcheckout_order_configurations (xref_id)
;

create table sltxh_cbcheckout_order_counties
(
	id mediumint unsigned not null,
	order_id int unsigned default '0' not null,
	county_name varchar(200) default '' not null,
	state_id mediumint unsigned not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	ordering mediumint not null,
	published enum('0', '1') default '1' not null,
	primary key (id, order_id)
)
engine=InnoDB
;

create index order_id
	on sltxh_cbcheckout_order_counties (order_id)
;

create index state_id
	on sltxh_cbcheckout_order_counties (state_id)
;

create table sltxh_cbcheckout_order_countries
(
	id mediumint unsigned not null,
	order_id int unsigned default '0' not null,
	country_name varchar(64) null,
	country_3_code char(3) null,
	country_2_code char(2) null,
	vat_free tinyint(1) default '1' not null,
	vat_free_with_vatin tinyint(1) default '1' not null,
	published tinyint(1) default '1' not null,
	ordering mediumint not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	primary key (id, order_id)
)
engine=InnoDB
;

create index order_id
	on sltxh_cbcheckout_order_countries (order_id)
;

create index idx_country_name
	on sltxh_cbcheckout_order_countries (country_name)
;

create table sltxh_cbcheckout_order_currencies
(
	id mediumint unsigned not null,
	order_id int unsigned default '0' not null,
	base tinyint(1) default '0' not null,
	multiplicator decimal(10,5) unsigned not null,
	symbol varchar(10) default '' not null,
	code varchar(10) default '' not null,
	`default` tinyint(1) default '0' not null,
	ordering mediumint default '0' not null,
	published tinyint(1) default '1' not null,
	primary key (id, order_id)
)
engine=InnoDB
;

create index order_id
	on sltxh_cbcheckout_order_currencies (order_id)
;

create index code
	on sltxh_cbcheckout_order_currencies (code)
;

create table sltxh_cbcheckout_order_invoices
(
	id int auto_increment
		primary key,
	invoice_number_prefix varchar(10) default '' not null,
	invoice_number_serial int unsigned not null,
	order_id int unsigned null,
	file varchar(100) not null,
	released_by int default '0' not null,
	released_on datetime not null comment 'UTC Timing',
	changed enum('0', '1') default '0' not null,
	original_file varchar(100) default '' not null,
	changed_by int unsigned default '0' not null,
	changed_on datetime null,
	constraint unique_prefix_serial
		unique (invoice_number_prefix, invoice_number_serial),
	constraint order_id
		unique (order_id)
)
engine=InnoDB collate=utf8_unicode_ci
;

create table sltxh_cbcheckout_order_payment_methods
(
	order_id int unsigned default '0' not null,
	id mediumint unsigned default '0' not null,
	price decimal(20,4) default '0.0000' not null,
	taxclass_id mediumint unsigned default '0' not null,
	params varchar(255) default '' not null,
	ordering mediumint default '0' not null,
	percentage decimal(20,3) default '0.000' not null,
	price_min decimal(20,4) default '0.0000' not null,
	price_max decimal(20,4) default '0.0000' not null,
	connector_name varchar(50) default '' not null,
	primary key (order_id, id)
)
engine=InnoDB
;

create index index_price
	on sltxh_cbcheckout_order_payment_methods (price)
;

create index index_price_min
	on sltxh_cbcheckout_order_payment_methods (price_min)
;

create index index_price_max
	on sltxh_cbcheckout_order_payment_methods (price_max)
;

create table sltxh_cbcheckout_order_payment_trackings
(
	id int unsigned auto_increment
		primary key,
	user_id int unsigned not null,
	order_id int unsigned null,
	got_tracked enum('0', '1') default '0' not null
)
engine=InnoDB
;

create index user_id
	on sltxh_cbcheckout_order_payment_trackings (user_id, order_id)
;

create index order_id
	on sltxh_cbcheckout_order_payment_trackings (order_id)
;

create table sltxh_cbcheckout_order_positions
(
	id int unsigned auto_increment
		primary key,
	order_id int unsigned null,
	product_id mediumint unsigned default '0' not null,
	product_sku varchar(255) default '' not null,
	product_image varchar(50) default '' not null,
	quantity mediumint unsigned default '1' not null,
	weight decimal(20,3) unsigned default '0.000' not null,
	taxclass_id mediumint unsigned default '0' not null,
	taxclass_recurring_id mediumint unsigned default '0' not null,
	product_base_price_net decimal(20,4) unsigned default '0.0000' not null,
	product_base_price_overrides varchar(1024) default '[]' not null,
	product_base_price_recurring_net decimal(20,4) unsigned default '0.0000' not null,
	product_base_price_recurring_overrides varchar(1024) default '[]' not null,
	price_net decimal(20,4) unsigned default '0.0000' not null,
	price_recurring_net decimal(20,4) unsigned default '0.0000' not null,
	open_amount_net decimal(20,4) unsigned default '0.0000' not null,
	using_deposit tinyint unsigned default '0' not null,
	dispatch_time tinyint unsigned default '0' not null,
	product_custom_1 text not null,
	product_custom_2 text not null,
	product_custom_3 text not null,
	product_custom_4 text not null
)
engine=InnoDB collate=utf8_unicode_ci
;

create index order_id
	on sltxh_cbcheckout_order_positions (order_id)
;

create index product_id
	on sltxh_cbcheckout_order_positions (product_id)
;

alter table sltxh_cbcheckout_order_configurations
	add constraint sltxh_cbcheckout_order_configurations_ibfk_1
		foreign key (position_id) references sltxh_cbcheckout_order_positions (id)
			on update cascade
;

create table sltxh_cbcheckout_order_quotations
(
	order_id int unsigned auto_increment
		primary key,
	created_on datetime null comment 'UTC',
	created_by int unsigned default '0' not null,
	file varchar(50) default '' not null
)
engine=InnoDB collate=utf8_unicode_ci
;

create table sltxh_cbcheckout_order_records
(
	id int unsigned auto_increment
		primary key,
	user_id int unsigned null,
	delivery_id mediumint unsigned null,
	payment_id mediumint unsigned null,
	cart_id int unsigned null,
	store_id int unsigned default '0' not null,
	created_on datetime null comment 'UTC',
	paid tinyint default '0' not null,
	paid_on datetime null,
	status smallint(5) unsigned default '0' not null,
	invoice_released enum('0', '1') default '0' not null,
	comment text not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	coupon_discount_net decimal(20,4) default '0.0000' not null,
	transaction_id text not null,
	transaction_data text not null,
	custom_5 text not null,
	custom_6 text not null,
	custom_7 text not null,
	custom_8 text not null,
	custom_9 text not null,
	custom_10 text not null
)
engine=InnoDB collate=utf8_unicode_ci
;

create index user_id
	on sltxh_cbcheckout_order_records (user_id)
;

create index cart_id
	on sltxh_cbcheckout_order_records (cart_id)
;

alter table sltxh_cbcheckout_order_cities
	add constraint sltxh_cbcheckout_order_cities_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

alter table sltxh_cbcheckout_order_counties
	add constraint sltxh_cbcheckout_order_counties_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

alter table sltxh_cbcheckout_order_countries
	add constraint sltxh_cbcheckout_order_countries_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

alter table sltxh_cbcheckout_order_currencies
	add constraint sltxh_cbcheckout_order_currencies_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

alter table sltxh_cbcheckout_order_invoices
	add constraint sltxh_cbcheckout_order_invoices_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

alter table sltxh_cbcheckout_order_payment_methods
	add constraint sltxh_cbcheckout_order_payment_methods_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

alter table sltxh_cbcheckout_order_payment_trackings
	add constraint sltxh_cbcheckout_order_payment_trackings_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

alter table sltxh_cbcheckout_order_positions
	add constraint sltxh_cbcheckout_order_positions_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

alter table sltxh_cbcheckout_order_quotations
	add constraint sltxh_cbcheckout_order_quotations_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
;

create table sltxh_cbcheckout_order_salutations
(
	id smallint(5) unsigned not null,
	order_id int unsigned default '0' not null,
	gender enum('1', '2') default '1' not null,
	primary key (id, order_id),
	constraint sltxh_cbcheckout_order_salutations_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
)
engine=InnoDB
;

create index order_id
	on sltxh_cbcheckout_order_salutations (order_id)
;

create table sltxh_cbcheckout_order_shipping_methods
(
	order_id int unsigned default '0' not null,
	id mediumint unsigned default '0' not null,
	shipper_id mediumint unsigned not null,
	zone_id mediumint unsigned not null,
	minweight decimal(20,4) unsigned default '0.0000' not null,
	maxweight decimal(20,4) unsigned default '0.0000' not null,
	deliverytime mediumint not null,
	price decimal(20,4) default '0.0000' not null,
	taxclass_id mediumint unsigned not null,
	external_id varchar(100) default '' not null,
	ordering mediumint default '0' not null,
	primary key (order_id, id),
	constraint sltxh_cbcheckout_order_shipping_methods_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
)
engine=InnoDB
;

create index minweight
	on sltxh_cbcheckout_order_shipping_methods (minweight)
;

create index maxweight
	on sltxh_cbcheckout_order_shipping_methods (maxweight)
;

create index price
	on sltxh_cbcheckout_order_shipping_methods (price)
;

create index ordering
	on sltxh_cbcheckout_order_shipping_methods (ordering)
;

create table sltxh_cbcheckout_order_states
(
	id mediumint unsigned not null,
	order_id int unsigned default '0' not null,
	country_id mediumint unsigned not null,
	name varchar(50) default '' not null,
	iso_code varchar(50) default '' not null,
	fips_number varchar(5) default '' not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	ordering mediumint default '0' not null,
	published tinyint(1) unsigned default '1' not null,
	primary key (id, order_id),
	constraint sltxh_cbcheckout_order_states_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
)
engine=InnoDB
;

create index order_id
	on sltxh_cbcheckout_order_states (order_id)
;

create index country_id
	on sltxh_cbcheckout_order_states (country_id)
;

create index iso_fips
	on sltxh_cbcheckout_order_states (iso_code, fips_number)
;

create index ordering
	on sltxh_cbcheckout_order_states (ordering, published)
;

create table sltxh_cbcheckout_order_strings
(
	order_id int unsigned default '0' not null,
	`table` varchar(50) default '' not null collate utf8_unicode_ci,
	type int(5) unsigned not null comment '1:shippers',
	`key` bigint unsigned not null,
	language_tag char(5) default '' not null,
	text text not null,
	primary key (order_id, `table`, type, `key`, language_tag),
	constraint sltxh_cbcheckout_order_strings_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
)
engine=InnoDB
;

create table sltxh_cbcheckout_order_tax_class_rates
(
	order_id int unsigned null,
	tax_class_id mediumint unsigned not null,
	city_id int unsigned default '0' not null,
	county_id int unsigned default '0' not null,
	zone_id mediumint unsigned not null,
	state_id mediumint unsigned not null,
	country_id int unsigned not null,
	tax_rate decimal(4,2) unsigned not null,
	default_tax_rate decimal(10,3) unsigned not null,
	tax_code varchar(100) default '' not null,
	constraint unique_all
		unique (order_id, tax_class_id, city_id, county_id, state_id, country_id),
	constraint sltxh_cbcheckout_order_tax_class_rates_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
)
engine=InnoDB
;

create table sltxh_cbcheckout_order_user_groups
(
	order_id int unsigned default '0' not null,
	group_id int unsigned default '0' not null,
	discount_start_1 decimal(20,4) unsigned default '0.0000' not null,
	discount_start_2 decimal(20,4) unsigned default '0.0000' not null,
	discount_start_3 decimal(20,4) unsigned default '0.0000' not null,
	discount_start_4 decimal(20,4) unsigned default '0.0000' not null,
	discount_start_5 decimal(20,4) unsigned default '0.0000' not null,
	discount_factor_1 decimal(20,4) default '0.0000' not null,
	discount_factor_2 decimal(20,4) default '0.0000' not null,
	discount_factor_3 decimal(20,4) default '0.0000' not null,
	discount_factor_4 decimal(20,4) default '0.0000' not null,
	discount_factor_5 decimal(20,4) default '0.0000' not null,
	discount_amount_1 decimal(20,4) default '0.0000' not null,
	discount_amount_2 decimal(20,4) default '0.0000' not null,
	discount_amount_3 decimal(20,4) default '0.0000' not null,
	discount_amount_4 decimal(20,4) default '0.0000' not null,
	discount_amount_5 decimal(20,4) default '0.0000' not null,
	discount_type_1 varchar(32) default 'percentage' not null,
	discount_type_2 varchar(32) default 'percentage' not null,
	discount_type_3 varchar(32) default 'percentage' not null,
	discount_type_4 varchar(32) default 'percentage' not null,
	discount_type_5 varchar(32) default 'percentage' not null,
	discount_recurring_amount_1 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_2 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_3 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_4 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_5 decimal(20,4) default '0.0000' not null,
	discount_recurring_type_1 varchar(32) default 'percentage' not null,
	discount_recurring_type_2 varchar(32) default 'percentage' not null,
	discount_recurring_type_3 varchar(32) default 'percentage' not null,
	discount_recurring_type_4 varchar(32) default 'percentage' not null,
	discount_recurring_type_5 varchar(32) default 'percentage' not null,
	discount_recurring_start_1 decimal(20,4) default '0.0000' not null,
	discount_recurring_start_2 decimal(20,4) default '0.0000' not null,
	discount_recurring_start_3 decimal(20,4) default '0.0000' not null,
	discount_recurring_start_4 decimal(20,4) default '0.0000' not null,
	discount_recurring_start_5 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_1 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_2 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_3 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_4 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_5 decimal(20,4) default '0.0000' not null,
	title varchar(255) default '' not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	enable_checkout_order tinyint(1) unsigned default '1' not null,
	enable_see_pricing tinyint unsigned default '1' not null,
	enable_save_order tinyint unsigned default '1' not null,
	enable_request_quotation tinyint unsigned default '1' not null,
	b2b_mode tinyint unsigned default '0' not null,
	joomla_user_group_id mediumint unsigned default '0' not null,
	quotation_download enum('0', '1') default '1' not null,
	quotation_email enum('0', '1') default '1' not null,
	primary key (order_id, group_id),
	constraint sltxh_cbcheckout_order_user_groups_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
)
engine=InnoDB collate=utf8_unicode_ci
;

create table sltxh_cbcheckout_order_users
(
	id int unsigned not null,
	order_id int unsigned default '0' not null,
	companyname varchar(255) not null,
	gender enum('1', '2') default '1' not null,
	firstname varchar(255) not null,
	lastname varchar(255) not null,
	address1 varchar(255) not null,
	address2 varchar(255) not null,
	zipcode varchar(15) not null,
	city varchar(255) not null,
	country mediumint unsigned null,
	email varchar(255) not null,
	phone varchar(255) not null,
	billingcompanyname varchar(255) not null,
	billingfirstname varchar(255) not null,
	billinglastname varchar(255) not null,
	billinggender enum('1', '2') not null,
	billingaddress1 varchar(255) not null,
	billingaddress2 varchar(255) not null,
	billingzipcode varchar(15) not null,
	billingcity varchar(255) not null,
	billingcountry mediumint unsigned null,
	billingemail varchar(255) not null,
	billingphone varchar(255) not null,
	samedelivery tinyint(1) default '1' not null,
	created datetime not null,
	vatin varchar(200) default '' not null,
	group_id mediumint unsigned default '0' not null,
	newsletter tinyint(1) unsigned default '0' not null,
	platform_user_id mediumint unsigned default '0' not null,
	salutation_id mediumint unsigned null,
	billingsalutation_id mediumint unsigned null,
	state mediumint unsigned null,
	billingstate mediumint unsigned null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	language_tag char(5) not null,
	county_id mediumint unsigned null,
	billingcounty_id mediumint unsigned null,
	city_id mediumint unsigned null,
	billingcity_id mediumint unsigned null,
	primary key (id, order_id),
	constraint sltxh_cbcheckout_order_users_ibfk_1
		foreign key (order_id) references sltxh_cbcheckout_order_records (id)
			on update cascade
)
engine=InnoDB
;

create index order_id
	on sltxh_cbcheckout_order_users (order_id)
;

create index country
	on sltxh_cbcheckout_order_users (country)
;

create index billingcountry
	on sltxh_cbcheckout_order_users (billingcountry)
;

create index group_id
	on sltxh_cbcheckout_order_users (group_id)
;

create index salutation_id
	on sltxh_cbcheckout_order_users (salutation_id)
;

create index billingsalutation_id
	on sltxh_cbcheckout_order_users (billingsalutation_id)
;

create index language_tag
	on sltxh_cbcheckout_order_users (language_tag)
;

alter table sltxh_cbcheckout_order_records
	add constraint sltxh_cbcheckout_order_records_ibfk_1
		foreign key (user_id) references sltxh_cbcheckout_order_users (id)
			on update cascade on delete set null
;

create table sltxh_configbox_active_languages
(
	tag char(5) not null
		primary key
)
engine=InnoDB
;

create table sltxh_configbox_calculation_codes
(
	id mediumint unsigned auto_increment
		primary key,
	element_id_a int unsigned null,
	element_id_b int unsigned null,
	element_id_c int unsigned null,
	element_id_d int unsigned null,
	code text not null
)
engine=InnoDB
;

create index element_id_a
	on sltxh_configbox_calculation_codes (element_id_a)
;

create index element_id_b
	on sltxh_configbox_calculation_codes (element_id_b)
;

create index element_id_c
	on sltxh_configbox_calculation_codes (element_id_c)
;

create index element_id_d
	on sltxh_configbox_calculation_codes (element_id_d)
;

create table sltxh_configbox_calculation_formulas
(
	id mediumint unsigned not null
		primary key,
	calc text not null
)
engine=InnoDB
;

create table sltxh_configbox_calculation_matrices
(
	id mediumint unsigned auto_increment
		primary key,
	column_element_id int unsigned null,
	row_element_id int unsigned null,
	round smallint(5) unsigned not null,
	lookup_value tinyint(2) default '0' not null,
	multiplicator decimal(20,5) default '0.00000' not null,
	multielementid int unsigned null,
	column_calc_id mediumint unsigned null,
	row_calc_id mediumint unsigned null,
	calcmodel_id_multi mediumint unsigned null,
	row_type enum('none', 'question', 'calculation') default 'none' not null,
	column_type enum('none', 'question', 'calculation') default 'none' not null
)
engine=InnoDB
;

create index column_element_id
	on sltxh_configbox_calculation_matrices (column_element_id)
;

create index row_element_id
	on sltxh_configbox_calculation_matrices (row_element_id)
;

create index multielementid
	on sltxh_configbox_calculation_matrices (multielementid)
;

create index column_calc_id
	on sltxh_configbox_calculation_matrices (column_calc_id)
;

create index row_calc_id
	on sltxh_configbox_calculation_matrices (row_calc_id)
;

create index calcmodel_id_multi
	on sltxh_configbox_calculation_matrices (calcmodel_id_multi)
;

create table sltxh_configbox_calculation_matrices_data
(
	id mediumint unsigned not null,
	x bigint not null,
	y bigint not null,
	value decimal(20,4) default '0.0000' not null,
	ordering smallint(5) unsigned not null,
	primary key (id, x, y)
)
engine=InnoDB
;

create index ordering
	on sltxh_configbox_calculation_matrices_data (ordering)
;

create table sltxh_configbox_calculations
(
	id mediumint unsigned auto_increment
		primary key,
	name varchar(200) not null,
	type varchar(10) not null,
	product_id int unsigned null
)
engine=InnoDB
;

create index product_id
	on sltxh_configbox_calculations (product_id)
;

alter table sltxh_configbox_calculation_codes
	add constraint sltxh_configbox_calculation_codes_ibfk_1
		foreign key (id) references sltxh_configbox_calculations (id)
			on update cascade on delete cascade
;

alter table sltxh_configbox_calculation_formulas
	add constraint sltxh_configbox_calculation_formulas_ibfk_1
		foreign key (id) references sltxh_configbox_calculations (id)
			on update cascade on delete cascade
;

alter table sltxh_configbox_calculation_matrices
	add constraint sltxh_configbox_calculation_matrices_ibfk_1
		foreign key (id) references sltxh_configbox_calculations (id)
			on update cascade on delete cascade
;

alter table sltxh_configbox_calculation_matrices
	add constraint sltxh_configbox_calculation_matrices_ibfk_5
		foreign key (column_calc_id) references sltxh_configbox_calculations (id)
;

alter table sltxh_configbox_calculation_matrices
	add constraint sltxh_configbox_calculation_matrices_ibfk_6
		foreign key (row_calc_id) references sltxh_configbox_calculations (id)
;

alter table sltxh_configbox_calculation_matrices
	add constraint sltxh_configbox_calculation_matrices_ibfk_7
		foreign key (calcmodel_id_multi) references sltxh_configbox_calculations (id)
;

alter table sltxh_configbox_calculation_matrices_data
	add constraint sltxh_configbox_calculation_matrices_data_ibfk_1
		foreign key (id) references sltxh_configbox_calculations (id)
			on update cascade on delete cascade
;

create table sltxh_configbox_cart_position_configurations
(
	id int unsigned auto_increment
		primary key,
	cart_position_id int unsigned not null,
	prod_id int unsigned null,
	element_id int unsigned null,
	selection varchar(2000) not null
)
engine=InnoDB
;

create index cart_position_id
	on sltxh_configbox_cart_position_configurations (cart_position_id)
;

create index prod_id
	on sltxh_configbox_cart_position_configurations (prod_id)
;

create index element_id
	on sltxh_configbox_cart_position_configurations (element_id)
;

create table sltxh_configbox_cart_positions
(
	id int unsigned auto_increment
		primary key,
	cart_id int unsigned not null,
	prod_id int unsigned null,
	quantity smallint(5) unsigned default '1' not null,
	created datetime not null,
	finished tinyint(1) not null
)
engine=InnoDB
;

create index cart_id
	on sltxh_configbox_cart_positions (cart_id)
;

create index prod_id
	on sltxh_configbox_cart_positions (prod_id)
;

create index created
	on sltxh_configbox_cart_positions (created)
;

create index finished
	on sltxh_configbox_cart_positions (finished)
;

alter table sltxh_configbox_cart_position_configurations
	add constraint sltxh_configbox_cart_position_configurations_ibfk_1
		foreign key (cart_position_id) references sltxh_configbox_cart_positions (id)
			on update cascade on delete cascade
;

create table sltxh_configbox_carts
(
	id int unsigned auto_increment
		primary key,
	user_id int unsigned not null,
	created_time datetime not null
)
engine=InnoDB
;

create index user_id
	on sltxh_configbox_carts (user_id)
;

create index created_time
	on sltxh_configbox_carts (created_time)
;

alter table sltxh_configbox_cart_positions
	add constraint sltxh_configbox_cart_positions_ibfk_1
		foreign key (cart_id) references sltxh_configbox_carts (id)
			on update cascade on delete cascade
;

create table sltxh_configbox_cities
(
	id mediumint unsigned auto_increment
		primary key,
	city_name varchar(200) default '' not null,
	county_id mediumint unsigned not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	ordering mediumint not null,
	published enum('0', '1') default '1' not null
)
engine=InnoDB
;

create index state_id
	on sltxh_configbox_cities (county_id)
;

CREATE TABLE `sltxh_configbox_config` (
  `id` smallint(6) NOT NULL,
  `lastcleanup` bigint(20) unsigned DEFAULT '0',
  `usertime` mediumint(8) unsigned DEFAULT '24',
  `unorderedtime` mediumint(8) unsigned DEFAULT '24',
  `intervals` mediumint(8) unsigned DEFAULT '12',
  `labelexpiry` mediumint(8) unsigned DEFAULT '28',
  `securecheckout` enum('0','1') DEFAULT '0',
  `weightunits` varchar(16) DEFAULT '',
  `defaultprodimage` varchar(32) DEFAULT '',
  `product_key` varchar(64) DEFAULT '',
  `license_manager_satellites` text,
  `page_nav_cart_button_last_page_only` enum('0','1') NOT NULL DEFAULT '0',
  `page_nav_block_on_missing_selections` enum('0','1') NOT NULL DEFAULT '0',
  `label_element_custom_1` varchar(255) NOT NULL DEFAULT '',
  `label_element_custom_2` varchar(255) NOT NULL DEFAULT '',
  `label_element_custom_3` varchar(255) NOT NULL DEFAULT '',
  `label_element_custom_4` varchar(255) NOT NULL DEFAULT '',
  `label_element_custom_translatable_1` varchar(255) NOT NULL DEFAULT '',
  `label_element_custom_translatable_2` varchar(255) NOT NULL DEFAULT '',
  `label_assignment_custom_1` varchar(255) NOT NULL DEFAULT '',
  `label_assignment_custom_2` varchar(255) NOT NULL DEFAULT '',
  `label_assignment_custom_3` varchar(255) NOT NULL DEFAULT '',
  `label_assignment_custom_4` varchar(255) NOT NULL DEFAULT '',
  `label_option_custom_1` varchar(255) NOT NULL DEFAULT '',
  `label_option_custom_2` varchar(255) NOT NULL DEFAULT '',
  `label_option_custom_3` varchar(255) NOT NULL DEFAULT '',
  `label_option_custom_4` varchar(255) NOT NULL DEFAULT '',
  `use_internal_element_names` enum('0','1') NOT NULL DEFAULT '0',
  `enable_geolocation` enum('0','1') NOT NULL DEFAULT '0',
  `geolocation_type` varchar(32) NOT NULL DEFAULT 'maxmind_geoip2_db',
  `maxmind_license_key` varchar(200) NOT NULL DEFAULT '',
  `maxmind_user_id` varchar(32) DEFAULT '',
  `pm_show_delivery_options` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pm_show_payment_options` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `label_product_custom_1` varchar(200) NOT NULL DEFAULT '',
  `label_product_custom_2` varchar(200) NOT NULL DEFAULT '',
  `label_product_custom_3` varchar(200) NOT NULL DEFAULT '',
  `label_product_custom_4` varchar(200) NOT NULL DEFAULT '',
  `label_product_custom_5` varchar(200) NOT NULL DEFAULT '',
  `label_product_custom_6` varchar(200) NOT NULL DEFAULT '',
  `enable_reviews_products` enum('0','1') NOT NULL DEFAULT '0',
  `continue_listing_id` int(10) unsigned DEFAULT NULL,
  `enable_performance_tracking` enum('0','1') NOT NULL DEFAULT '0',
  `pm_show_regular_first` enum('0','1') NOT NULL DEFAULT '1',
  `pm_regular_show_overview` enum('0','1') NOT NULL DEFAULT '1',
  `pm_regular_show_prices` enum('0','1') NOT NULL DEFAULT '1',
  `pm_regular_show_categories` enum('0','1') NOT NULL DEFAULT '1',
  `pm_regular_show_elements` enum('0','1') NOT NULL DEFAULT '1',
  `pm_regular_show_elementprices` enum('0','1') NOT NULL DEFAULT '1',
  `pm_regular_expand_categories` enum('0','1','2') NOT NULL DEFAULT '2',
  `pm_recurring_show_overview` enum('0','1') NOT NULL DEFAULT '1',
  `pm_recurring_show_prices` enum('0','1') NOT NULL DEFAULT '1',
  `pm_recurring_show_categories` enum('0','1') NOT NULL DEFAULT '1',
  `pm_recurring_show_elements` enum('0','1') NOT NULL DEFAULT '1',
  `pm_recurring_show_elementprices` enum('0','1') NOT NULL DEFAULT '1',
  `pm_recurring_expand_categories` enum('0','1','2') NOT NULL DEFAULT '2',
  `show_conversion_table` enum('0','1') NOT NULL DEFAULT '0',
  `page_nav_show_tabs` enum('0','1') NOT NULL DEFAULT '0',
  `label_option_custom_5` varchar(100) NOT NULL DEFAULT '',
  `label_option_custom_6` varchar(100) NOT NULL DEFAULT '',
  `language_tag` char(5) NOT NULL DEFAULT '',
  `pm_regular_show_taxes` enum('0','1') NOT NULL DEFAULT '0',
  `pm_regular_show_cart_button` enum('0','1') NOT NULL DEFAULT '0',
  `pm_recurring_show_taxes` enum('0','1') NOT NULL DEFAULT '0',
  `pm_recurring_show_cart_button` enum('0','1') NOT NULL DEFAULT '0',
  `pm_show_net_in_b2c` enum('0','1') NOT NULL DEFAULT '0',
  `review_notification_email` varchar(255) NOT NULL DEFAULT '',
  `default_customer_group_id` mediumint(8) unsigned DEFAULT NULL,
  `default_country_id` mediumint(8) unsigned DEFAULT NULL,
  `disable_delivery` enum('0','1') NOT NULL DEFAULT '0',
  `sku_in_order_record` enum('0','1') NOT NULL DEFAULT '0',
  `newsletter_preset` enum('0','1') NOT NULL DEFAULT '0',
  `alternate_shipping_preset` enum('0','1') NOT NULL DEFAULT '0',
  `show_recurring_login_cart` enum('0','1') NOT NULL DEFAULT '0',
  `explicit_agreement_terms` enum('0','1') NOT NULL DEFAULT '0',
  `explicit_agreement_rp` enum('0','1') NOT NULL DEFAULT '0',
  `enable_invoicing` enum('0','1') NOT NULL DEFAULT '0',
  `send_invoice` enum('0','1') NOT NULL DEFAULT '0',
  `invoice_generation` enum('0','1','2') NOT NULL DEFAULT '0',
  `invoice_number_prefix` varchar(32) NOT NULL DEFAULT '',
  `invoice_number_start` int(10) unsigned NOT NULL DEFAULT '1',
  `page_nav_show_buttons` enum('0','1') NOT NULL DEFAULT '1',
  `structureddata` enum('0','1') NOT NULL DEFAULT '1',
  `structureddata_in` enum('configurator','product') NOT NULL DEFAULT 'configurator',
  PRIMARY KEY (`id`)
)
;


CREATE INDEX continue_listing_id ON sltxh_configbox_config (continue_listing_id)
;

CREATE INDEX default_customer_group_id ON sltxh_configbox_config (default_customer_group_id)
;

CREATE INDEX default_country_id ON sltxh_configbox_config (default_country_id)
;


create table sltxh_configbox_connectors
(
	id mediumint unsigned auto_increment
		primary key,
	name varchar(100) not null,
	ordering mediumint not null,
	published tinyint unsigned default '1' not null,
	after_system tinyint unsigned default '1' not null,
	file varchar(500) not null
)
engine=InnoDB
;

create index ordering
	on sltxh_configbox_connectors (ordering)
;

create index published
	on sltxh_configbox_connectors (published)
;

create table sltxh_configbox_counties
(
	id mediumint unsigned auto_increment
		primary key,
	county_name varchar(200) default '' not null,
	state_id mediumint unsigned not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	ordering mediumint not null,
	published enum('0', '1') default '1' not null
)
engine=InnoDB
;

create index state_id
	on sltxh_configbox_counties (state_id)
;

alter table sltxh_configbox_cities
	add constraint sltxh_configbox_cities_ibfk_1
		foreign key (county_id) references sltxh_configbox_counties (id)
;

create table sltxh_configbox_countries
(
	id mediumint unsigned not null
		primary key,
	country_name varchar(64) null,
	country_3_code char(3) null,
	country_2_code char(2) null,
	vat_free enum('0', '1') default '1' not null,
	vat_free_with_vatin enum('0', '1') default '1' not null,
	published enum('0', '1') default '1' not null,
	ordering mediumint default '0' not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null
)
engine=InnoDB
;

create index idx_country_name
	on sltxh_configbox_countries (country_name)
;

create index country_2_code
	on sltxh_configbox_countries (country_2_code)
;

create index vat_free
	on sltxh_configbox_countries (vat_free)
;

create index vat_free_with_vatin
	on sltxh_configbox_countries (vat_free_with_vatin)
;

create index published
	on sltxh_configbox_countries (published)
;

create index ordering
	on sltxh_configbox_countries (ordering)
;

alter table sltxh_configbox_config
	add constraint sltxh_configbox_config_ibfk_2
		foreign key (default_country_id) references sltxh_configbox_countries (id)
;

create table sltxh_configbox_currencies
(
	id mediumint unsigned auto_increment
		primary key,
	base tinyint(1) default '0' not null,
	multiplicator decimal(10,5) unsigned default '1.00000' not null,
	symbol varchar(10) not null,
	code varchar(10) not null,
	`default` tinyint(1) default '0' not null,
	ordering mediumint not null,
	published tinyint(1) not null,
	constraint code
		unique (code)
)
engine=InnoDB
;

create index ordering
	on sltxh_configbox_currencies (ordering)
;

create index published
	on sltxh_configbox_currencies (published)
;

create table sltxh_configbox_customers
(
	id int unsigned not null
		primary key,
	billing_company_name varchar(128) default '' null,
	billing_first_name varchar(128) default '' null,
	billing_last_name varchar(128) default '' null,
	billing_address_1 varchar(128) default '' null,
	billing_address_2 varchar(128) default '' null,
	billing_postal_code varchar(128) default '' null,
	billing_email varchar(128) default '' null,
	billing_phone varchar(128) default '' null,
	billing_salutation_id mediumint unsigned null,
	billing_salutation_text varchar(128) default '' null,
	billing_gender enum('na', 'male', 'female') default 'na' not null,
	billing_city_id mediumint unsigned null,
	billing_county_id mediumint unsigned null,
	billing_state_id mediumint unsigned null,
	billing_country_id mediumint unsigned null,
	shipping_company_name varchar(128) default '' null,
	shipping_first_name varchar(128) default '' null,
	shipping_last_name varchar(128) default '' null,
	shipping_address_1 varchar(128) default '' null,
	shipping_address_2 varchar(128) default '' null,
	shipping_postal_code varchar(128) default '' null,
	shipping_email varchar(128) default '' null,
	shipping_phone varchar(128) default '' null,
	shipping_salutation_id mediumint unsigned null,
	shipping_city_id mediumint unsigned null,
	shipping_county_id mediumint unsigned null,
	shipping_state_id mediumint unsigned null,
	shipping_country_id mediumint unsigned null,
	same_delivery enum('0', '1') default '1' not null,
	vatin varchar(128) default '' null,
	group_id mediumint unsigned null,
	platform_user_id mediumint unsigned null,
	language_tag char(5) null,
	newsletter enum('0', '1') default '0' not null,
	custom_1 text null,
	custom_2 text null,
	custom_3 text null,
	custom_4 text null,
	custom_5 text null,
	custom_6 text null,
	custom_7 text null,
	custom_8 text null,
	is_temporary enum('0', '1') default '1' not null,
	created_on datetime null,
	password varchar(300) default '' null
)
engine=InnoDB
;

create index group_id
	on sltxh_configbox_customers (group_id)
;

create table sltxh_configbox_elements
(
	id int unsigned auto_increment
		primary key,
	page_id int unsigned null,
	el_image varchar(100) not null,
	layoutname varchar(50) not null,
	required tinyint(1) not null,
	validate tinyint(1) not null,
	minval varchar(255) null,
	maxval varchar(255) null,
	calcmodel mediumint unsigned null,
	calcmodel_recurring mediumint unsigned null,
	multiplicator float not null,
	published tinyint(1) not null,
	ordering mediumint not null,
	asproducttitle tinyint(1) unsigned default '0' null,
	default_value text not null,
	show_in_overview tinyint(1) default '1' not null,
	text_calcmodel tinyint(1) default '0' not null,
	element_custom_1 varchar(255) default '' not null,
	element_custom_2 varchar(255) default '' not null,
	element_custom_3 varchar(255) default '' not null,
	element_custom_4 varchar(255) default '' not null,
	rules text not null,
	internal_name varchar(255) default '' not null,
	element_css_classes varchar(100) default '' not null,
	calcmodel_id_min_val mediumint unsigned null,
	calcmodel_id_max_val mediumint unsigned null,
	upload_extensions varchar(255) default 'png, jpg, jpeg, gif, tif' not null,
	upload_mime_types varchar(255) default '' not null,
	upload_size_mb float unsigned default '1' not null,
	slider_steps float unsigned default '1' not null,
	calcmodel_weight mediumint unsigned null,
	choices text not null,
	desc_display_method enum('0', '1', '2') default '1' not null,
	behavior_on_activation enum('none', 'select_default', 'select_any', '') default 'none' not null,
	behavior_on_changes enum('silent', 'confirm') default 'silent' not null,
	question_type varchar(64) default '' not null,
	prefill_on_init enum('0', '1') default '0' not null,
	input_restriction enum('plaintext', 'integer', 'decimal', '') default 'plaintext' not null,
	set_min_value enum('none', 'static', 'calculated') default 'none' not null,
	set_max_value enum('none', 'static', 'calculated') default 'none' not null,
	show_unit enum('1', '0') default '0' not null,
	title_display enum('heading', 'label', 'none') default 'heading' not null,
	is_shapediver_control enum('0', '1') default '0' not null,
	shapediver_parameter_id varchar(255) default '' not null,
	behavior_on_inconsistency enum('deselect', 'replace_with_default', 'replace_with_any') default 'deselect' null,
	display_while_disabled enum('hide', 'grey_out') default 'hide' null,
	shapediver_geometry_name varchar(255) default '' not null,
	constraint sltxh_configbox_elements_ibfk_14
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_19
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_24
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_29
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_34
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_4
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_9
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_10
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_15
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_20
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_25
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_30
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_35
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_5
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_12
		foreign key (calcmodel_id_min_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_17
		foreign key (calcmodel_id_min_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_2
		foreign key (calcmodel_id_min_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_22
		foreign key (calcmodel_id_min_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_27
		foreign key (calcmodel_id_min_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_32
		foreign key (calcmodel_id_min_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_7
		foreign key (calcmodel_id_min_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_13
		foreign key (calcmodel_id_max_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_18
		foreign key (calcmodel_id_max_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_23
		foreign key (calcmodel_id_max_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_28
		foreign key (calcmodel_id_max_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_3
		foreign key (calcmodel_id_max_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_33
		foreign key (calcmodel_id_max_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_8
		foreign key (calcmodel_id_max_val) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_11
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_16
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_21
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_26
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_31
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_36
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_elements_ibfk_6
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id)
)
engine=InnoDB
;

create index `page_id-ordering`
	on sltxh_configbox_elements (page_id, ordering)
;

create index calcmodel
	on sltxh_configbox_elements (calcmodel)
;

create index calcmodel_recurring
	on sltxh_configbox_elements (calcmodel_recurring)
;

create index published
	on sltxh_configbox_elements (published)
;

create index ordering
	on sltxh_configbox_elements (ordering)
;

create index calcmodel_id_min_val
	on sltxh_configbox_elements (calcmodel_id_min_val)
;

create index calcmodel_id_max_val
	on sltxh_configbox_elements (calcmodel_id_max_val)
;

create index calcmodel_weight
	on sltxh_configbox_elements (calcmodel_weight)
;

create index is_shapediver_control
	on sltxh_configbox_elements (is_shapediver_control)
;

alter table sltxh_configbox_calculation_codes
	add constraint sltxh_configbox_calculation_codes_ibfk_2
		foreign key (element_id_a) references sltxh_configbox_elements (id)
;

alter table sltxh_configbox_calculation_codes
	add constraint sltxh_configbox_calculation_codes_ibfk_3
		foreign key (element_id_b) references sltxh_configbox_elements (id)
;

alter table sltxh_configbox_calculation_codes
	add constraint sltxh_configbox_calculation_codes_ibfk_4
		foreign key (element_id_c) references sltxh_configbox_elements (id)
;

alter table sltxh_configbox_calculation_codes
	add constraint sltxh_configbox_calculation_codes_ibfk_5
		foreign key (element_id_d) references sltxh_configbox_elements (id)
;

alter table sltxh_configbox_calculation_matrices
	add constraint sltxh_configbox_calculation_matrices_ibfk_2
		foreign key (column_element_id) references sltxh_configbox_elements (id)
;

alter table sltxh_configbox_calculation_matrices
	add constraint sltxh_configbox_calculation_matrices_ibfk_3
		foreign key (row_element_id) references sltxh_configbox_elements (id)
;

alter table sltxh_configbox_calculation_matrices
	add constraint sltxh_configbox_calculation_matrices_ibfk_4
		foreign key (multielementid) references sltxh_configbox_elements (id)
;

alter table sltxh_configbox_cart_position_configurations
	add constraint sltxh_configbox_cart_position_configurations_ibfk_3
		foreign key (element_id) references sltxh_configbox_elements (id)
			on update cascade on delete cascade
;

create table sltxh_configbox_examples
(
	id int unsigned auto_increment
		primary key,
	product_id int unsigned not null,
	published tinyint unsigned not null,
	ordering int not null
)
engine=InnoDB charset=latin1
;

create table sltxh_configbox_groups
(
	id mediumint unsigned auto_increment
		primary key,
	title varchar(255) not null,
	discount_start_1 decimal(20,4) unsigned default '0.0000' not null,
	discount_factor_1 decimal(20,4) default '0.0000' not null,
	discount_start_2 decimal(20,4) unsigned default '0.0000' not null,
	discount_factor_2 decimal(20,4) default '0.0000' not null,
	discount_start_3 decimal(20,4) unsigned default '0.0000' not null,
	discount_factor_3 decimal(20,4) default '0.0000' not null,
	discount_start_4 decimal(20,4) unsigned default '0.0000' not null,
	discount_factor_4 decimal(20,4) default '0.0000' not null,
	discount_start_5 decimal(20,4) unsigned default '0.0000' not null,
	discount_factor_5 decimal(20,4) default '0.0000' not null,
	custom_1 text not null,
	custom_2 text not null,
	custom_3 text not null,
	custom_4 text not null,
	enable_checkout_order enum('0', '1') default '1' not null,
	enable_see_pricing enum('0', '1') default '1' not null,
	enable_save_order enum('0', '1') default '1' not null,
	enable_request_quotation enum('0', '1') default '1' not null,
	b2b_mode enum('0', '1') default '1' not null,
	joomla_user_group_id mediumint unsigned default '0' not null,
	quotation_download enum('0', '1') default '1' not null,
	quotation_email enum('0', '1') default '1' not null,
	discount_amount_1 decimal(20,4) default '0.0000' not null,
	discount_amount_2 decimal(20,4) default '0.0000' not null,
	discount_amount_3 decimal(20,4) default '0.0000' not null,
	discount_amount_4 decimal(20,4) default '0.0000' not null,
	discount_amount_5 decimal(20,4) default '0.0000' not null,
	discount_type_1 varchar(32) default 'percentage' not null,
	discount_type_2 varchar(32) default 'percentage' not null,
	discount_type_3 varchar(32) default 'percentage' not null,
	discount_type_4 varchar(32) default 'percentage' not null,
	discount_type_5 varchar(32) default 'percentage' not null,
	discount_recurring_start_1 decimal(20,4) default '0.0000' not null,
	discount_recurring_start_2 decimal(20,4) default '0.0000' not null,
	discount_recurring_start_3 decimal(20,4) default '0.0000' not null,
	discount_recurring_start_4 decimal(20,4) default '0.0000' not null,
	discount_recurring_start_5 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_1 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_2 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_3 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_4 decimal(20,4) default '0.0000' not null,
	discount_recurring_factor_5 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_1 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_2 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_3 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_4 decimal(20,4) default '0.0000' not null,
	discount_recurring_amount_5 decimal(20,4) default '0.0000' not null,
	discount_recurring_type_1 varchar(32) default 'percentage' not null,
	discount_recurring_type_2 varchar(32) default 'percentage' not null,
	discount_recurring_type_3 varchar(32) default 'percentage' not null,
	discount_recurring_type_4 varchar(32) default 'percentage' not null,
	discount_recurring_type_5 varchar(32) default 'percentage' not null
)
engine=InnoDB
;

create index joomla_user_group_id
	on sltxh_configbox_groups (joomla_user_group_id)
;

alter table sltxh_configbox_config
	add constraint sltxh_configbox_config_ibfk_1
		foreign key (default_customer_group_id) references sltxh_configbox_groups (id)
;

create table sltxh_configbox_listings
(
	id int unsigned auto_increment
		primary key,
	layoutname varchar(100) not null,
	published enum('0', '1') default '0' not null,
	product_sorting enum('0', '1') default '0' not null
)
engine=InnoDB
;

create index published
	on sltxh_configbox_listings (published)
;

alter table sltxh_configbox_config
	add constraint sltxh_configbox_config_ibfk_3
		foreign key (continue_listing_id) references sltxh_configbox_listings (id)
			on update cascade on delete set null
;

create table sltxh_configbox_notifications
(
	id mediumint unsigned auto_increment
		primary key,
	name varchar(40) not null,
	type varchar(50) not null,
	statuscode mediumint not null,
	send_customer tinyint(1) default '1' not null,
	send_manager tinyint(1) default '1' not null
)
engine=InnoDB
;

create index statuscode
	on sltxh_configbox_notifications (statuscode)
;

create table sltxh_configbox_oldlabels
(
	id mediumint unsigned auto_increment
		primary key,
	type int(11) unsigned not null,
	`key` mediumint unsigned not null,
	label varchar(255) not null,
	prod_id mediumint unsigned not null,
	created bigint unsigned not null,
	language_tag char(5) not null,
	constraint uniqe_strings
		unique (type, `key`, language_tag)
)
engine=InnoDB
;

create index prod_id
	on sltxh_configbox_oldlabels (prod_id)
;

create index created
	on sltxh_configbox_oldlabels (created)
;

create table sltxh_configbox_options
(
	id int unsigned auto_increment
		primary key,
	sku varchar(60) default '' not null,
	price decimal(20,4) default '0.0000' not null,
	price_overrides varchar(1024) default '[]' not null,
	price_recurring decimal(20,4) default '0.0000' not null,
	price_recurring_overrides varchar(1024) default '[]' not null,
	weight decimal(20,3) default '0.000' not null,
	option_custom_1 varchar(255) default '' not null,
	option_custom_2 varchar(255) default '' not null,
	option_custom_3 varchar(255) default '' not null,
	option_custom_4 varchar(255) default '' not null,
	option_image varchar(200) default '' not null,
	available enum('0', '1') default '0' not null,
	availibility_date date null,
	was_price decimal(20,4) default '0.0000' not null,
	was_price_recurring decimal(20,4) default '0.0000' not null,
	disable_non_available enum('0', '1') default '0' not null
)
engine=InnoDB
;

create index sku
	on sltxh_configbox_options (sku)
;

create table sltxh_configbox_pages
(
	id int unsigned auto_increment
		primary key,
	visualization_view varchar(100) default '' not null,
	layoutname varchar(100) not null,
	published enum('0', '1') default '0' not null,
	ordering smallint(6) default '0' not null,
	product_id int unsigned null,
	css_classes varchar(255) default '' not null
)
engine=InnoDB
;

create index published
	on sltxh_configbox_pages (published)
;

create index ordering
	on sltxh_configbox_pages (ordering)
;

create index product_id
	on sltxh_configbox_pages (product_id)
;

alter table sltxh_configbox_elements
	add constraint sltxh_configbox_elements_ibfk_1
		foreign key (page_id) references sltxh_configbox_pages (id)
;

create table sltxh_configbox_payment_methods
(
	id mediumint unsigned auto_increment
		primary key,
	price decimal(20,4) default '0.0000' not null,
	taxclass_id mediumint unsigned not null,
	params varchar(255) not null,
	ordering smallint(6) default '0' not null,
	published enum('0', '1') default '0' not null,
	percentage decimal(20,4) default '0.0000' not null,
	price_min decimal(20,4) default '0.0000' not null,
	price_max decimal(20,4) default '0.0000' not null,
	connector_name varchar(50) default '' not null
)
engine=InnoDB
;

create index taxclass_id
	on sltxh_configbox_payment_methods (taxclass_id)
;

create index ordering
	on sltxh_configbox_payment_methods (ordering)
;

create index published
	on sltxh_configbox_payment_methods (published)
;

create table sltxh_configbox_product_detail_panes
(
	id int unsigned auto_increment
		primary key,
	product_id int unsigned not null,
	heading_icon_filename varchar(30) not null,
	css_classes varchar(255) default '' not null,
	ordering mediumint not null
)
engine=InnoDB
;

create index product_id
	on sltxh_configbox_product_detail_panes (product_id)
;

create index ordering
	on sltxh_configbox_product_detail_panes (ordering)
;

create table sltxh_configbox_products
(
	id int unsigned auto_increment
		primary key,
	sku varchar(60) not null,
	prod_image varchar(50) not null,
	baseimage varchar(100) not null,
	opt_image_x mediumint unsigned null,
	opt_image_y mediumint unsigned not null,
	baseprice decimal(20,4) unsigned default '0.0000' not null,
	baseprice_recurring decimal(20,4) unsigned default '0.0000' not null,
	baseprice_overrides varchar(1024) default '[]' not null,
	baseprice_recurring_overrides varchar(1024) default '[]' not null,
	baseweight decimal(20,3) unsigned default '0.000' not null,
	taxclass_id mediumint unsigned not null,
	taxclass_recurring_id mediumint unsigned not null,
	layoutname varchar(100) not null,
	published enum('0', '1') default '0' not null,
	pm_show_delivery_options enum('0', '1', '2') default '2' not null,
	pm_show_payment_options enum('0', '1', '2') default '2' not null,
	product_custom_1 text not null,
	product_custom_2 text not null,
	product_custom_3 text not null,
	product_custom_4 text not null,
	enable_reviews enum('0', '1', '2') default '2' not null,
	external_reviews_id varchar(200) default '' not null,
	dispatch_time tinyint unsigned not null,
	pm_show_regular_first tinyint(2) default '2' not null,
	pm_regular_show_overview tinyint(2) default '2' not null,
	pm_regular_show_prices tinyint(2) default '2' not null,
	pm_regular_show_categories tinyint(2) default '2' not null,
	pm_regular_show_elements tinyint(2) default '2' not null,
	pm_regular_show_elementprices tinyint(2) default '2' not null,
	pm_regular_expand_categories tinyint(2) default '2' not null,
	pm_recurring_show_overview tinyint(2) default '2' not null,
	pm_recurring_show_prices tinyint(2) default '2' not null,
	pm_recurring_show_categories tinyint(2) default '2' not null,
	pm_recurring_show_elements tinyint(2) default '2' not null,
	pm_recurring_show_elementprices tinyint(2) default '2' not null,
	pm_recurring_expand_categories tinyint(3) default '3' not null,
	page_nav_show_tabs enum('0', '1', '2') default '2' not null,
	show_buy_button enum('0', '1') default '1' not null,
	was_price decimal(20,4) default '0.0000' not null,
	was_price_recurring decimal(20,4) default '0.0000' not null,
	pm_show_net_in_b2c enum('0', '1', '2') default '0' not null,
	pm_regular_show_taxes enum('0', '1', '2') default '2' not null,
	pm_regular_show_cart_button enum('0', '1', '2') default '2' not null,
	pm_recurring_show_taxes enum('0', '1', '2') default '2' not null,
	pm_recurring_show_cart_button enum('0', '1', '2') default '2' not null,
	product_detail_panes_method enum('accordion', 'tabs') default 'tabs' not null,
	product_detail_panes_in_listings enum('0', '1') default '0' not null,
	product_detail_panes_in_product_pages enum('0', '1') default '1' not null,
	product_detail_panes_in_configurator_steps enum('0', '1') default '0' not null,
	page_nav_show_buttons enum('0', '1', '2') default '2' not null,
	page_nav_block_on_missing_selections enum('0', '1', '2') default '2' not null,
	page_nav_cart_button_last_page_only enum('0', '1', '2') default '2' not null,
	visualization_type enum('none', 'composite', 'shapediver') default 'none' not null,
	shapediver_model_data text not null,
	use_recurring_pricing enum('0', '1') default '0' not null,
	show_product_details_button enum('0', '1') default '0' null,
	product_details_page_type enum('none', 'cms_page', 'configbox_page') default 'none' null
)
engine=InnoDB
;

create index taxclass_id
	on sltxh_configbox_products (taxclass_id)
;

create index taxclass_recurring_id
	on sltxh_configbox_products (taxclass_recurring_id)
;

create index published
	on sltxh_configbox_products (published)
;

alter table sltxh_configbox_calculations
	add constraint sltxh_configbox_calculations_ibfk_1
		foreign key (product_id) references sltxh_configbox_products (id)
			on update set null on delete set null
;

alter table sltxh_configbox_cart_position_configurations
	add constraint sltxh_configbox_cart_position_configurations_ibfk_2
		foreign key (prod_id) references sltxh_configbox_products (id)
			on update cascade on delete cascade
;

alter table sltxh_configbox_cart_positions
	add constraint sltxh_configbox_cart_positions_ibfk_2
		foreign key (prod_id) references sltxh_configbox_products (id)
			on update cascade on delete cascade
;

alter table sltxh_configbox_pages
	add constraint sltxh_configbox_pages_ibfk_1
		foreign key (product_id) references sltxh_configbox_products (id)
;

alter table sltxh_configbox_product_detail_panes
	add constraint sltxh_configbox_product_detail_panes_ibfk_1
		foreign key (product_id) references sltxh_configbox_products (id)
;

create table sltxh_configbox_reviews
(
	id int unsigned auto_increment
		primary key,
	name varchar(100) not null,
	rating decimal(2,1) not null,
	comment text not null,
	published enum('0', '1') default '0' not null,
	language_tag varchar(5) not null,
	date_created datetime not null comment 'UTC',
	product_id int unsigned null,
	constraint sltxh_configbox_reviews_ibfk_1
		foreign key (product_id) references sltxh_configbox_products (id)
			on update cascade on delete cascade
)
engine=InnoDB
;

create index published
	on sltxh_configbox_reviews (published, language_tag)
;

create index date_created
	on sltxh_configbox_reviews (date_created)
;

create index product_id
	on sltxh_configbox_reviews (product_id)
;

create table sltxh_configbox_salutations
(
	id smallint(5) unsigned auto_increment
		primary key,
	gender enum('1', '2') default '1' not null
)
engine=InnoDB
;

create table if not exists sltxh_configbox_session
(
	id varchar(128) not null
		primary key,
	user_agent varchar(200) default '' not null,
	ip_address varchar(100) default '' not null,
	data text not null,
	updated bigint unsigned not null
)
engine=InnoDB
;

create index updated
	on sltxh_configbox_session (updated)
;

create table sltxh_configbox_shippers
(
	id mediumint unsigned auto_increment
		primary key,
	published enum('0', '1') default '0' not null
)
engine=InnoDB
;

create index published
	on sltxh_configbox_shippers (published)
;


CREATE TABLE sltxh_configbox_zones
(
  id    MEDIUMINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  label VARCHAR(100) NOT NULL
)
  ENGINE = InnoDB
;

create table sltxh_configbox_shipping_methods
(
	id mediumint unsigned auto_increment
		primary key,
	shipper_id mediumint unsigned not null,
	zone_id mediumint unsigned not null,
	minweight decimal(20,4) unsigned default '0.0000' not null,
	maxweight decimal(20,4) unsigned default '0.0000' not null,
	deliverytime tinyint unsigned default '0' not null,
	price float not null,
	taxclass_id mediumint unsigned not null,
	published enum('0', '1') default '0' not null,
	external_id varchar(100) default '' not null,
	ordering smallint(6) default '0' not null,
	constraint sltxh_configbox_shipping_methods_ibfk_1
		foreign key (shipper_id) references sltxh_configbox_shippers (id),
	constraint sltxh_configbox_shipping_methods_ibfk_3
		foreign key (zone_id) references sltxh_configbox_zones (id)
)
engine=InnoDB
;

create index shipper_id
	on sltxh_configbox_shipping_methods (shipper_id)
;

create index zone_id
	on sltxh_configbox_shipping_methods (zone_id)
;

create index taxclass_id
	on sltxh_configbox_shipping_methods (taxclass_id)
;

create index published
	on sltxh_configbox_shipping_methods (published)
;

create index external_id
	on sltxh_configbox_shipping_methods (external_id)
;

create index ordering
	on sltxh_configbox_shipping_methods (ordering)
;

create table sltxh_configbox_shopdata
(
	id mediumint unsigned not null
		primary key,
	shopname varchar(200) not null,
	shoplogo varchar(100) not null,
	shopaddress1 varchar(200) not null,
	shopaddress2 varchar(200) not null,
	shopzipcode varchar(40) not null,
	shopcity varchar(100) not null,
	shopcountry varchar(100) not null,
	shopphonesales varchar(100) not null,
	shopphonesupport varchar(100) not null,
	shopemailsales varchar(100) not null,
	shopemailsupport varchar(100) not null,
	shoplinktotc varchar(150) not null,
	shopfax varchar(100) not null,
	shopbankname varchar(100) not null,
	shopbankaccountholder varchar(100) not null,
	shopbankaccount varchar(100) not null,
	shopbankcode varchar(100) not null,
	shopbic varchar(100) not null,
	shopiban varchar(100) not null,
	shopuid varchar(100) not null,
	shopcomreg varchar(100) not null,
	shopwebsite varchar(255) null,
	shopowner varchar(255) null,
	shoplegalvenue varchar(255) null
)
engine=InnoDB
;

create table sltxh_configbox_states
(
	id mediumint unsigned auto_increment
		primary key,
	country_id mediumint unsigned null,
	name varchar(50) default '' not null,
	iso_code varchar(50) default '' not null,
	fips_number varchar(5) default '' not null,
	custom_1 varchar(255) default '' null,
	custom_2 varchar(255) default '' null,
	custom_3 varchar(255) default '' null,
	custom_4 varchar(255) default '' null,
	ordering smallint(6) default '0' not null,
	published enum('0', '1') default '1' not null,
	constraint sltxh_configbox_states_ibfk_1
		foreign key (country_id) references sltxh_configbox_countries (id)
)
engine=InnoDB
;

create index country_id
	on sltxh_configbox_states (country_id)
;

create index iso_fips
	on sltxh_configbox_states (iso_code, fips_number)
;

create index ordering
	on sltxh_configbox_states (ordering, published)
;

create index published
	on sltxh_configbox_states (published)
;

alter table sltxh_configbox_counties
	add constraint sltxh_configbox_counties_ibfk_1
		foreign key (state_id) references sltxh_configbox_states (id)
;

create table sltxh_configbox_strings
(
	type smallint(5) unsigned not null comment 'See langType in the property definition .',
	`key` int unsigned not null comment 'Primary key value for the regarding record.',
	language_tag char(5) not null,
	text text not null,
	constraint uniqe_strings
		unique (type, `key`, language_tag)
)
engine=InnoDB
;

create table if not exists sltxh_configbox_system_vars
(
	`key` varchar(128) not null
		primary key,
	value text not null
)
engine=InnoDB
;

create table sltxh_configbox_tax_class_rates
(
	tax_class_id mediumint unsigned not null,
	city_id mediumint unsigned null,
	county_id mediumint unsigned null,
	state_id mediumint unsigned null,
	country_id mediumint unsigned null,
	tax_rate decimal(4,2) unsigned default '0.00' not null,
	tax_code varchar(100) default '' not null,
	constraint unique_all
		unique (tax_class_id, city_id, county_id, state_id, country_id),
	constraint sltxh_configbox_tax_class_rates_ibfk_2
		foreign key (city_id) references sltxh_configbox_cities (id)
			on update cascade on delete cascade,
	constraint sltxh_configbox_tax_class_rates_ibfk_3
		foreign key (county_id) references sltxh_configbox_counties (id)
			on update cascade on delete cascade,
	constraint sltxh_configbox_tax_class_rates_ibfk_4
		foreign key (state_id) references sltxh_configbox_states (id)
			on update cascade on delete cascade,
	constraint sltxh_configbox_tax_class_rates_ibfk_5
		foreign key (country_id) references sltxh_configbox_countries (id)
			on update cascade on delete cascade
)
engine=InnoDB
;

create index city_id
	on sltxh_configbox_tax_class_rates (city_id)
;

create index county_id
	on sltxh_configbox_tax_class_rates (county_id)
;

create index state_id
	on sltxh_configbox_tax_class_rates (state_id)
;

create index country_id
	on sltxh_configbox_tax_class_rates (country_id)
;

create table sltxh_configbox_tax_classes
(
	id mediumint unsigned auto_increment
		primary key,
	title varchar(255) not null,
	default_tax_rate decimal(4,2) unsigned not null,
	id_external varchar(100) default '' not null
)
engine=InnoDB
;

alter table sltxh_configbox_payment_methods
	add constraint sltxh_configbox_payment_methods_ibfk_1
		foreign key (taxclass_id) references sltxh_configbox_tax_classes (id)
;

alter table sltxh_configbox_products
	add constraint sltxh_configbox_products_ibfk_1
		foreign key (taxclass_id) references sltxh_configbox_tax_classes (id)
;

alter table sltxh_configbox_products
	add constraint sltxh_configbox_products_ibfk_2
		foreign key (taxclass_recurring_id) references sltxh_configbox_tax_classes (id)
;

alter table sltxh_configbox_shipping_methods
	add constraint sltxh_configbox_shipping_methods_ibfk_2
		foreign key (taxclass_id) references sltxh_configbox_tax_classes (id)
;

alter table sltxh_configbox_tax_class_rates
	add constraint sltxh_configbox_tax_class_rates_ibfk_1
		foreign key (tax_class_id) references sltxh_configbox_tax_classes (id)
			on update cascade on delete cascade
;

create table sltxh_configbox_user_field_definitions
(
	id mediumint unsigned auto_increment
		primary key,
	field_name varchar(30) not null,
	show_checkout enum('0', '1') default '0' not null,
	require_checkout enum('0', '1') default '0' not null,
	show_quotation enum('0', '1') default '0' not null,
	require_quotation enum('0', '1') default '0' not null,
	show_saveorder enum('0', '1') default '0' not null,
	require_saveorder enum('0', '1') default '0' not null,
	show_profile enum('0', '1') default '0' not null,
	require_profile enum('0', '1') default '0' not null,
	validation_browser varchar(255) default '' not null,
	validation_server varchar(255) default '' not null
)
engine=InnoDB
;

create table sltxh_configbox_users
(
	id int unsigned auto_increment
		primary key,
	platform_user_id mediumint unsigned not null,
	companyname varchar(255) not null,
	gender enum('1', '2') default '1' not null,
	firstname varchar(255) not null,
	lastname varchar(255) not null,
	address1 varchar(255) not null,
	address2 varchar(255) not null,
	zipcode varchar(15) not null,
	city varchar(255) not null,
	country mediumint unsigned null,
	email varchar(255) not null,
	phone varchar(255) not null,
	billingcompanyname varchar(255) not null,
	billingfirstname varchar(255) not null,
	billinglastname varchar(255) not null,
	billinggender enum('1', '2') not null,
	billingaddress1 varchar(255) not null,
	billingaddress2 varchar(255) not null,
	billingzipcode varchar(15) not null,
	billingcity varchar(255) not null,
	billingcountry mediumint unsigned null,
	billingemail varchar(255) not null,
	billingphone varchar(255) not null,
	samedelivery tinyint(1) default '1' not null,
	created datetime not null,
	password varchar(300) not null,
	vatin varchar(200) not null,
	group_id mediumint unsigned null,
	newsletter tinyint(1) unsigned default '0' not null,
	is_temporary tinyint(1) unsigned default '0' not null,
	salutation_id mediumint unsigned null,
	billingsalutation_id mediumint unsigned null,
	state mediumint unsigned null,
	billingstate mediumint unsigned null,
	custom_1 varchar(255) default '' null,
	custom_2 varchar(255) default '' null,
	custom_3 varchar(255) default '' null,
	custom_4 varchar(255) default '' null,
	language_tag char(5) not null,
	county_id mediumint unsigned null,
	billingcounty_id mediumint unsigned null,
	city_id mediumint unsigned null,
	billingcity_id mediumint unsigned null,
	constraint sltxh_configbox_users_ibfk_1
		foreign key (group_id) references sltxh_configbox_groups (id)
			on update cascade on delete set null
)
engine=InnoDB
;

create index platform_user_id
	on sltxh_configbox_users (platform_user_id)
;

create index country
	on sltxh_configbox_users (country)
;

create index billingcountry
	on sltxh_configbox_users (billingcountry)
;

create index created
	on sltxh_configbox_users (created)
;

create index group_id
	on sltxh_configbox_users (group_id)
;

create index is_temporary
	on sltxh_configbox_users (is_temporary)
;

create index salutation_id
	on sltxh_configbox_users (salutation_id)
;

create index billingsalutation_id
	on sltxh_configbox_users (billingsalutation_id)
;

create index state
	on sltxh_configbox_users (state)
;

create index billingstate
	on sltxh_configbox_users (billingstate)
;

create index language_tag
	on sltxh_configbox_users (language_tag)
;

create index county_id
	on sltxh_configbox_users (county_id)
;

create index billingcounty_id
	on sltxh_configbox_users (billingcounty_id)
;

create index city_id
	on sltxh_configbox_users (city_id)
;

create index billingcity_id
	on sltxh_configbox_users (billingcity_id)
;

alter table sltxh_configbox_carts
	add constraint sltxh_configbox_carts_ibfk_1
		foreign key (user_id) references sltxh_configbox_users (id)
			on update cascade on delete cascade
;

create table sltxh_configbox_xref_country_payment_method
(
	payment_id mediumint unsigned not null,
	country_id mediumint unsigned not null,
	primary key (country_id, payment_id),
	constraint sltxh_configbox_xref_country_payment_method_ibfk_2
		foreign key (payment_id) references sltxh_configbox_payment_methods (id)
			on update cascade on delete cascade,
	constraint sltxh_configbox_xref_country_payment_method_ibfk_1
		foreign key (country_id) references sltxh_configbox_countries (id)
			on update cascade on delete cascade
)
engine=InnoDB
;

create index sltxh_configbox_xref_country_payment_method_ibfk_2
	on sltxh_configbox_xref_country_payment_method (payment_id)
;

create table sltxh_configbox_xref_country_zone
(
	zone_id mediumint unsigned not null,
	country_id mediumint unsigned not null,
	primary key (country_id, zone_id),
	constraint zone_id
		foreign key (zone_id) references sltxh_configbox_zones (id)
			on update cascade on delete cascade,
	constraint country_id
		foreign key (country_id) references sltxh_configbox_countries (id)
			on update cascade on delete cascade
)
engine=InnoDB
;

create index zone_id
	on sltxh_configbox_xref_country_zone (zone_id)
;

create table sltxh_configbox_xref_element_option
(
	id int unsigned auto_increment
		primary key,
	element_id int unsigned null,
	option_id int unsigned null,
	`default` int(1) unsigned not null,
	visualization_image varchar(100) default '' not null,
	visualization_stacking mediumint default '0' not null,
	visualization_view varchar(100) default '' not null,
	confirm_deselect tinyint(1) default '1' not null,
	calcmodel mediumint unsigned null,
	price_calculation_overrides varchar(1024) default '[]' not null,
	calcmodel_recurring mediumint unsigned null,
	price_recurring_calculation_overrides varchar(1024) default '[]' not null,
	ordering mediumint not null,
	published int(1) unsigned not null,
	assignment_custom_1 varchar(255) default '' not null,
	assignment_custom_2 varchar(255) default '' not null,
	assignment_custom_3 varchar(255) default '' not null,
	assignment_custom_4 varchar(255) default '' not null,
	rules text not null,
	option_picker_image varchar(100) default '' not null,
	calcmodel_weight mediumint unsigned null,
	shapediver_choice_value varchar(512) default '' not null,
	display_while_disabled enum('like_question', 'hide', 'grey_out') default 'like_question' null,
	constraint sltxh_configbox_xref_element_option_ibfk_1
		foreign key (element_id) references sltxh_configbox_elements (id),
	constraint sltxh_configbox_xref_element_option_ibfk_2
		foreign key (option_id) references sltxh_configbox_options (id),
	constraint sltxh_configbox_xref_element_option_ibfk_12
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_15
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_18
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_21
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_3
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_6
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_9
		foreign key (calcmodel) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_10
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_13
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_16
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_19
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_22
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_4
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_7
		foreign key (calcmodel_recurring) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_11
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_14
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_17
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_20
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_23
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_5
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id),
	constraint sltxh_configbox_xref_element_option_ibfk_8
		foreign key (calcmodel_weight) references sltxh_configbox_calculations (id)
)
engine=InnoDB
;

create index element_id
	on sltxh_configbox_xref_element_option (element_id)
;

create index option_id
	on sltxh_configbox_xref_element_option (option_id)
;

create index calcmodel
	on sltxh_configbox_xref_element_option (calcmodel)
;

create index calcmodel_recurring
	on sltxh_configbox_xref_element_option (calcmodel_recurring)
;

create index published
	on sltxh_configbox_xref_element_option (published)
;

create index calcmodel_weight
	on sltxh_configbox_xref_element_option (calcmodel_weight)
;

create index shapediver_choice_value
	on sltxh_configbox_xref_element_option (shapediver_choice_value)
;

create table sltxh_configbox_xref_listing_product
(
	id int unsigned auto_increment
		primary key,
	listing_id int unsigned null,
	product_id int unsigned null,
	ordering smallint(6) default '0' not null,
	constraint `listing_id-product_id`
		unique (listing_id, product_id),
	constraint sltxh_configbox_xref_listing_product_ibfk_2
		foreign key (listing_id) references sltxh_configbox_listings (id)
			on update cascade on delete cascade,
	constraint sltxh_configbox_xref_listing_product_ibfk_1
		foreign key (product_id) references sltxh_configbox_products (id)
			on update cascade on delete cascade
)
engine=InnoDB
;

create index product_id
	on sltxh_configbox_xref_listing_product (product_id)
;

create index ordering
	on sltxh_configbox_xref_listing_product (ordering)
;

