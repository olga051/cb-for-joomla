<?php
defined('CB_VALID_ENTRY') or die();

class ObserverWordpress {

	function onAfterStoreRecord($modelName, $data) {

		if (KenedoPlatform::getName() != 'wordpress') {
			return;
		}

		$postSettings = [

			'adminlistings' =>
				[
					'class_name' => 'ConfigboxModelAdminlistings',
					'meta_key' => 'cb_listing_id',
					'meta_value_field' => 'id',
					'post_type' => 'cb_product_listing',
					'title_field' => 'title',
					'content_template' => '[configbox view="productlisting" id="%s"]',
					'status_field' => 'published',
					'post_name_field' => 'title',
					'post_name_filter_callback' => function($data, $setting) { return strtolower(str_replace(' ', '-', $data->title)); },
				],

			'adminproducts' =>
				[
					'class_name' => 'ConfigboxModelAdminproducts',
					'meta_key' => 'cb_product_id',
					'meta_value_field' => 'id',
					'post_type' => 'cb_product',
					'title_field' => 'title',
					'content_template' => '[configbox view="product" id="%s"]',
					'status_field' => 'published',
					'post_name_field' => 'label',
					'post_name_filter_callback' => null,
				],

			'adminpages' =>
				[
					'class_name' => 'ConfigboxModelAdminpages',
					'meta_key' => 'cb_page_id',
					'meta_value_field' => 'id',
					'post_type' => 'cb_page',
					'title_field' => 'title',
					'content_template' => '[configbox view="configuratorpage" id="%s"]',
					'status_field' => 'published',
					'post_name_field' => 'title',
					'post_name_filter_callback' => function($data, $setting) {
						$product = KenedoModel::getModel('ConfigboxModelAdminproducts')->getRecord($data->product_id);
						return $product->label .'/'.$data->label;
					},
				]
		];

		if (!isset($postSettings[$modelName])) {
			return;
		}

		$setting = $postSettings[$modelName];

		$record = KenedoModel::getModel($setting['class_name'])->getRecord($data->id);

		// We go into postmeta and find posts with the right ID (we look for multiple ones for in case there is a mess)
		$db = KenedoPlatform::getDb();
		$query = "SELECT `post_id` FROM `#__postmeta` WHERE `meta_key` = '".$db->getEscaped($setting['meta_key'])."' and `meta_value` = ".intval($record->{$setting['meta_value_field']});
		$db->setQuery($query);
		$postIds = $db->loadResultList();

		$postId = null;

		// In case we somehow got more than one, we delete them and start over
		if (count($postIds) > 1) {
			foreach ($postIds as $id) {
				wp_delete_post($id, true);
			}
		}
		elseif(count($postIds) == 1) {
			$postId = $postIds[0];
		}

		$array = [
			'post_type' => $setting['post_type'],
			'post_title' => $record->{$setting['title_field']},
			'post_status' => (!$setting['status_field'] || $record->{$setting['status_field']} == '1') ? 'publish' : 'draft',
			'post_name' => is_callable($setting['post_name_filter_callback']) ? call_user_func($setting['post_name_filter_callback'], $record, $setting) : $record->{$setting['post_name_field']},
			'post_content' => sprintf($setting['content_template'], $record->id),
		];

		if ($postId) {
			$array['ID'] = $postId;
			$response = wp_update_post($array, true);
			if (!is_int($response)) {
				KLog::log(var_export($response, true), 'error');
				return;
			}
			update_post_meta($response, $setting['meta_key'], $record->id);
		}
		else {
			$response = wp_insert_post($array, true);
			if (!is_int($response)) {
				KLog::log(var_export($response, true), 'error');
				return;
			}
			update_post_meta($response, $setting['meta_key'], $record->id);
		}


		return;

	}

	function onAfterCopyRecord($modelName, $newData) {

		if (KenedoPlatform::getName() != 'wordpress') {
			return;
		}

		// We can do the same as for storing
		$this->onAfterStoreRecord($modelName, $newData);

	}
}