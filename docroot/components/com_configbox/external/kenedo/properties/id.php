<?php 
defined('CB_VALID_ENTRY') or die();

class KenedoPropertyId extends KenedoProperty {
	
	function getCellContentInListingTable($record) {
		ob_start();
		?>
		<input type="checkbox" name="cid[]" class="kenedo-item-checkbox" value="<?php echo intval($record->{$this->propertyName});?>" />
		<span><?php echo intval($record->{$this->propertyName}); ?></span>
		<?php
		return ob_get_clean();
	}
	
	function isInListing() {
		return true;	
	}
	
	function usesWrapper() {
		return false;
	}
	
	function getBodyAdmin() {
		return '';
	}

	function getDataKeysForBaseTable($data) {
		$key = $this->model->getTableKey();
		if (!empty($data->{$key})) {
			return array($this->propertyName);
		}
		else {
			return array();
		}
	}

}