<?php
defined('CB_VALID_ENTRY') or die();

class KenedoPlatformWordpress implements InterfaceKenedoPlatform {

	/**
	 * @var string[]
	 * @see renderHeadScriptDeclarations, addScriptDeclaration
	 */
	public $headScriptDeclarations = array();

	/**
	 * @var string[]
	 * @see renderBodyScriptDeclarations, addScriptDeclaration
	 */
	public $bodyScriptDeclarations = array();

	/**
	 * @var KenedoDatabaseWordpress
	 */
	protected $db;

	/**
	 * @var string[] $errors
	 */
	protected $errors;

	/**
	 * @var string[]
	 */
	protected $stylesheetUrls = array();

	/**
	 * @var string[]
	 */
	protected $inlineStyles = array();

	/**
	 * @var string[][] array of arrays with keys 'url', 'type', 'defer' and 'async'
	 */
	protected $scriptAssets = array();

	public function initialize() {

		KLog::log('Got request coming on with this: "'.var_export($_REQUEST, true), 'debug');

		/*
		 * Mind that the getRoute method also manipulates URLs a little
		 */

		// CB deals with controller or view, WP with page. So here we sneak in page (copied from controller or view)
		if (in_array(KRequest::getKeyword('page'), [NULL, 'configbox'])) {

			$controllerName = KRequest::getKeyword('controller','');
			$viewName = KRequest::getKeyword('view','');

			if ($controllerName) {
				KLog::log('Manipulating page param. Taking controller name "'.$controllerName.'"', 'custom_wp_requests');
				KRequest::setVar('page', $controllerName);
			}
			if ($viewName) {
				KLog::log('Manipulating page param. Taking view name "'.$viewName.'"', 'debug');
				KRequest::setVar('page', $viewName);
			}

		}

		// If there is no 'action' in the request, then create one (in the <page>.<task> form)
		if (in_array(KRequest::getKeyword('action'), [NULL])) {

			// We best don't manipulate the action if the request does not deal with a CB page. For now let it be existence of controller or view param
			$controllerName = KRequest::getKeyword('controller','');
			$viewName = KRequest::getKeyword('view','');
			if ($controllerName or $viewName) {
				$action = KRequest::getKeyword('page').'.'.KRequest::getKeyword('task', 'display');
				KLog::log('Manipulating action param. Making it "'.$action.'"', 'debug');
				KRequest::setVar('action', $action);
			}


		}

		// Add the actions that make inline script tags rendered in template
		add_action('wp_head', array($this, 'renderHeadScriptDeclarations'));
		add_action('wp_footer', array($this, 'renderBodyScriptDeclarations'));

		add_action('admin_head', array($this, 'renderHeadScriptDeclarations'));
		add_action('admin_footer', array($this, 'renderBodyScriptDeclarations'));

	}
	
	public function getDbConnectionData() {

		$connection = new stdClass();
		$connection->hostname 	= DB_HOST;
		$connection->username 	= DB_USER;
		$connection->password 	= DB_PASSWORD;
		$connection->database 	= DB_NAME;
		$connection->prefix 	= $GLOBALS['table_prefix'];
				
		return $connection;
	
	}
	
	public function &getDb() {
		if (!$this->db) {
			require_once(dirname(__FILE__).DS.'database.php');
			$this->db = new KenedoDatabaseWordpress();
		}
		
		return $this->db;
	}

	//TODO: Test
	public function redirect($url, $httpCode = 303) {


		$headers = headers_list();

		header("Location: $url", true, $httpCode);

		wp_redirect($url, $httpCode);
	}

	//TODO: Implement
	public function logout() {
		wp_logout();
	}

	//TODO: Implement
	public function authenticate($username, $passwordClear) {
		$response = wp_authenticate($username, $passwordClear);
		return (is_wp_error($response) == false);
	}

	//TODO: Test
	public function login($username) {

		$id = $this->getUserIdByUsername($username);

		$credentials = array(
			'remember'=>1,
			'username'=>$username,
			'password'=>'dummy',
		);

		$secure_cookie = apply_filters( 'secure_signon_cookie', is_ssl(), $credentials );

		wp_set_auth_cookie($id, $credentials['remember'], $secure_cookie);

		wp_signon();
	}

	//TODO: Implement
	public function getTemplateName() {
		return 'template';
	}

	//TODO: Implement
	public function sendSystemMessage($text, $type = NULL) {

	}

	//TODO: Implement
	public function getVersionShort() {
		return 1.0;
	}

	//TODO: Implement
	public function getDebug() {
		return WP_DEBUG;
	}
	
	public function getDefaultListlimit() {
		return 50;
	}

	//TODO: Test
	public function getConfigOffset() {
		$string = get_option('timezone_string');

		$validOnes = DateTimeZone::listIdentifiers();
		if (in_array($string, $validOnes)) {
			return $string;
		}
		else {
			return 'UTC';
		}
	}
	//TODO: Test
	public function getMailerFromName() {
		return get_bloginfo('name');
	}

	//TODO: Test
	public function getMailerFromEmail() {
		return get_bloginfo('admin_email');
	}

	//TODO: Implement
	public function getTmpPath() {
		return $this->getRootDirectory().'/tmp';
	}

	//TODO: Implement
	public function getLogPath() {
		return $this->getRootDirectory().'/logs';
	}

	//TODO: Test
	public function getLanguageTag() {

		$tag = get_locale();
		return str_replace('_', '-', $tag);

	}
	
	//TODO: Check if good enough
	public function getLanguageUrlCode($languageTag = NULL) {
		return $this->getLanguageTag();
	}
	
	//TODO: Check if good enough
	public function getDocumentType() {
		return KRequest::getKeyword('format','html');
	}

	public function addStylesheet($path, $type='text/css', $media = 'all') {

		if (in_array($path, $this->stylesheetUrls) == false) {

			$this->stylesheetUrls[] = $path;
			wp_enqueue_style( uniqid(), $path );

		}

	}

	public function addStyleDeclaration($css) {

		$this->inlineStyles[] = $css;
		wp_add_inline_style(uniqid(), $css);

	}

	public function addScript($path, $type = "text/javascript", $defer = false, $async = false) {

		$this->scriptAssets[$path] = array(
			'url' => $path,
			'type' => $type,
			'defer' => $defer,
			'async' => $async,
		);

		wp_enqueue_script(uniqid(), $path);

	}

	public function addScriptDeclaration($js, $newTag = false, $toBody = false) {

		if ($toBody) {
			$array =& $this->bodyScriptDeclarations;
		}
		else {
			$array =& $this->headScriptDeclarations;
		}

		if ($newTag) {
			$array[] = $js;
		}
		else {
			if (count($array) == 0) {
				$array[] = $js;
			}
			else {
				end($array);
				$key = key($array);
				$array[$key] .= "\n".$js;
			}
		}

	}

	public function renderStyleSheetLinks() {

		foreach ($this->stylesheetUrls as $url) {
			?>
			<link href="<?php echo hsc($url);?>" rel="stylesheet" />
			<?php
		}
	}

	public function renderStyleDeclarations() {

		foreach ($this->inlineStyles as $css) {
			?>
			<style type="text/css">
				<?php echo $css;?>
			</style>
			<?php
		}

	}

	public function renderScriptAssets() {
		foreach ($this->scriptAssets as $asset) { ?>
			<script type="<?php echo hsc($asset['type']);?>" async="<?php echo ($asset['async']) ? 'true':'false';?>" defer="<?php echo ($asset['defer']) ? 'true':'false';?>"></script>
		<?php }
	}


	public function renderHeadScriptDeclarations() {

		foreach ($this->headScriptDeclarations as $js) {
			?>
			<script type="text/javascript">
				<?php echo $js;?>
			</script>
			<?php

		}

	}

	public function renderBodyScriptDeclarations() {
		$output = '';
		foreach ($this->bodyScriptDeclarations as $js) {
			$output .= '<script type="text/javascript">'."\n";
			$output .= $js."\n";
			$output .= '</script>';
		}
		echo $output;
	}

	public function isAdminArea() {
		return is_admin();
	}
	
	public function isSiteArea() {
		return (is_admin() == false);
	}
	
	public function autoload($className, $classPath) {
		include_once($classPath);
	}
	
	public function processContentModifiers($text) {	
		return $text;
	}
	
	public function triggerEvent($eventName, $data) {
		return array(true);
	}
	
	public function raiseError($errorCode, $errorMessage) {
		throw new Exception($errorMessage, $errorCode);
	}
	
	public function renderHtmlEditor($dataFieldKey, $content, $width, $height, $cols, $rows) {	
		return '<textarea name="'.hsc($dataFieldKey).'" id="'.hsc($dataFieldKey).'" class="kenedo-html-editor not-initialized" style="width:'.(int)$width.'px; height:'.$height.'px" rows="'.(int)$rows.'" cols="'.(int)$cols.'">'.hsc($content).'</textarea>';
	}

	//TODO: Implement
	public function sendEmail($from, $fromName, $recipient, $subject, $body, $isHtml = false, $cc = NULL, $bcc = NULL, $attachmentPath = NULL) {
		return true;
	}

	//TODO: Use
	public function getGeneratorTag() {
		return '';
	}

	public function setGeneratorTag($string) {
		$this->setMetaTag('generator', $string);
	}
	
	public function getUrlBase() {
		return get_site_url();
	}

	public function getUrlBaseAssets() {
		return get_site_url();
	}
	
	public function getDocumentBase() {
		return $this->getUrlBase();
	}
	//TODO: Implement
	public function setDocumentBase($string) {
		return true;
	}
	//TODO: Implement
	public function setDocumentMimeType($mime) {
		header('Content-Type: '.$mime);
	}
	//TODO: Use
	public function getDocumentTitle() {
		return wp_title(NULL, false);
	}

	//TODO: Implement
	public function setDocumentTitle($string) {

	}

	//TODO: Implement
	public function setMetaTag($tag,$content) {

	}
	
	public function isLoggedIn() {
		return is_user_logged_in();
	}
	//TODO: Test
	public function getUserId() {
		$user = wp_get_current_user();
		return $user->ID;
	}

	//TODO: Test
	public function getUserName($userId = NULL) {
		if ($userId === NULL) {
			$user = get_user_by('id', $userId);
		}
		else {
			$user = wp_get_current_user();
		}

		return $user->user_login;

	}

	//TODO: TEST
	public function getUserFullName($userId = NULL) {

		if ($userId === NULL) {
			$user = get_user_by('id', $userId);
		}
		else {
			$user = wp_get_current_user();
		}

		return $user->display_name;

	}
	//TODO: Implement
	public function getUserPasswordEncoded($userId = NULL) {
		return 'abc';
	}
	//TODO: Implement
	public function getUserIdByUsername($username) {
		return 1;
	}
	//TODO: Implement
	public function getUserGroupId($userId = NULL) {
		return 1;
	}
	//TODO: Implement
	public function getUserTimezoneName($userId = NULL) {

		$string = get_option('timezone_string');

		$validOnes = DateTimeZone::listIdentifiers();
		if (in_array($string, $validOnes)) {
			return $string;
		}
		else {
			return 'UTC';
		}

	}
	//TODO: Implement
	public function registerUser($data, $groupIds = array()) {
		
		$userObject = new stdClass();
		
		$userObject->id 		= 99;
		$userObject->name 		= 'name';
		$userObject->username 	= 'username';
		$userObject->password 	= 'password';
		
		return $userObject;
		
	}
	
	protected function unsetErrors() {
		$this->errors = array();
	}
	
	protected function setError($error) {
		$this->errors[] = $error;
	}
	
	protected function setErrors($errors) {
		if (is_array($errors) && count($errors)) {
			$this->errors = array_merge((array)$this->errors,$errors);
		}
	}
	
	public function getErrors() {
		return $this->errors;
	}
	
	public function getError() {
		if (is_array($this->errors) && count($this->errors)) {
			return end($this->errors);
		}
		else {
			return '';
		}
	}
	//TODO: Test
	public function isAuthorized($task,$userId = NULL, $minGroupId = NULL) {
		return current_user_can('edit_pages');
	}

	//TODO: Implement
	public function passwordsMatch($passwordClear, $passwordEncrypted) {
		return true;
	}
	
	public function passwordMeetsStandards($password) {
		if (mb_strlen($password) < 8) {
			return false;
		}
		if ( preg_match("/[0-9]/", $password) == 0 || preg_match("/[a-zA-Z]/", $password) == 0) {
			return false;
		}
	
		return true;
	}
	
	public function getPasswordStandardsText() {
		return KText::_('Your password should contain at least 8 characters and should contain numbers and letters.');
	}
	
	//TODO: Implement
	public function changeUserPassword($userId, $passwordClear) {
		return true;
	}
	
	//TODO: Implement
	public function getRootDirectory() {
		return ABSPATH;
	}
	//TODO: Implement
	public function getAppParameters() {
		$params = new KStorage();
		return $params;
	}
	
	public function renderOutput(&$output) {

		if ($this->getDocumentType() != 'html' or KRequest::getKeyword('format') == 'raw') {
			echo $output;
			return;
		}

		if (KRequest::getKeyword('tmpl') == 'component') {
			require(__DIR__.'/tmpl/component.php');
			return;
		}

		if ($this->isAdminArea()) {
			require(__DIR__.'/tmpl/component.php');
		}
		else {
			require(__DIR__.'/tmpl/index.php');
		}

	}
	
	public function startSession() {
		return true;
	}
	
	//TODO: Implement
	public function getPasswordResetLink() {
		return '';
	}
	
	public function getPlatformLoginLink() {
		return '';
	}
	
	public function getRoute($url, $encode = true, $secure = NULL) {

		$parsed = parse_url($url);

		if (isset($parsed['query'])) {

			$query = array();

			parse_str($parsed['query'], $query);

			if (!empty($query['view'])) {

				$postViews = [

					'productlisting' => [
						'meta_key' => 'cb_listing_id',
						'name_id_param' => 'listing_id',
						'unset'=> array(),
						],

					'product' => [
						'meta_key' => 'cb_product_id',
						'name_id_param' => 'prod_id',
						'unset'=> array(),
					],

					'configuratorpage' => [
						'meta_key' => 'cb_page_id',
						'name_id_param' => 'page_id',
						'unset'=> array('prod_id'),
					],

				];

				if (isset($postViews[$query['view']])) {
					$idParamName = $postViews[$query['view']]['name_id_param'];
					if (empty($query[$idParamName])) {
						KLog::log('Got a view parameter "'.$query['view'].'" in URL, but record ID parameter "'.$idParamName.'" is missing. Whole URL was '.$url, 'custom_wp_requests');
						return $url;
					}

					$db = KenedoPlatform::getDb();
					$dbQuery = "SELECT `post_id` FROM `#__postmeta` WHERE `meta_key` = '".$db->getEscaped($postViews[$query['view']]['meta_key'])."' and `meta_value` = ".intval($query[$idParamName]);
					$db->setQuery($dbQuery);
					$postId = $db->loadResult();

					$url = get_post_permalink($postId);

					foreach ($postViews[$query['view']]['unset'] as $var) {
						unset($query[$var]);
					}
					unset($query['view'], $query['option'], $query[$idParamName]);

					if (count($query)) {
						$url .= (strstr($url, '?')) ? '&':'?';
						$url .= http_build_query($query);
					}

					return $url;

				}
			}


			// Since WP wants page to be set and CB works with controller (or view), we set 'page' here (see lower for one more thing)
			if (!empty($query['view'])) {
				$query['page'] = $query['view'];
			}
			elseif (!empty($query['controller'])) {
				$query['page'] = $query['controller'];
			}

			if (!empty($query['task'])) {
				$action = '';
				if (!empty($query['controller'])) {
					$action = $query['controller'].'.';
				}
				$action .= $query['task'];
				$query['action'] = $action;
			}

			$queryString = http_build_query($query);

		}
		else {
			// And in case we got no query string, then we add our wildcard 'configbox' as page (will be dealt with in static::initialize)
			$queryString = 'page=configbox';
		}



		if ($this->isAdminArea()) {

			if (strstr($url, 'format=raw') || strstr($url, 'format=json')) {
				$frontController = 'admin-ajax.php';
			}
			else {
				$frontController = 'admin.php';
			}

			return admin_url( $frontController.'?'.$queryString );
		}
		else {
			return site_url( 'index.php?'.$queryString );
		}

	}
	
	public function getActiveMenuItemId() {
		return 0;
	}
	
	//TODO: Implement
	public function getLanguages() {

		$languages = wp_get_installed_translations('core');
		if (count($languages) == 0) {
			$language = new stdClass();
			$language->tag = $this->getLanguageTag();
			$language->label = $this->getLanguageTag();
			return array($language);
		}
		else {
			$response = array();
			foreach ($languages as $language) {
				$obj = new stdClass();
				$obj->tag = $language;
				$obj->label = $language;
				$response[] = $obj;
			}
			return $response;
		}

	}
	
	//TODO: Implement
	public function platformUserEditFormIsReachable() {
		return false;
	}
	
	//TODO: Implement
	public function userCanEditPlatformUsers() {
		return false;
	}
	
	//TODO: Implement
	public function getPlatformUserEditUrl($platformUserId) {
		return '';
	}

	public function getComponentDir($componentName) {
		return WP_PLUGIN_DIR.'/'.str_replace('com_', '', $componentName).'/app';
	}

	public function getDirAssets() {
		$path = $this->getComponentDir('com_configbox').DS.'assets';
		return $path;
	}

	public function getUrlAssets() {
		return plugins_url('configbox/app/assets');
	}

	public function getDirCache() {
		// Not using JPATH_CACHE on purpose to avoid writing into the admin cache
		return $this->getRootDirectory().DS.'cache';
	}

	public function getDirCustomization() {
		$path = $this->getComponentDir('com_configbox').DS.'data'.DS.'customization';
		return $path;
	}

	public function getUrlCustomization() {
		return plugins_url('configbox/app/data/customization');
	}

	public function getDirCustomizationAssets() {
		$path = $this->getComponentDir('com_configbox').DS.'data'.DS.'customization'.DS.'assets';
		return $path;
	}

	public function getUrlCustomizationAssets() {
		return plugins_url('configbox/app/data/customization/assets');
	}

	public function getDirCustomizationSettings() {
		$path = $this->getComponentDir('com_configbox').DS.'data'.DS.'store'.DS.'private'.DS.'settings';
		return $path;
	}

	public function getDirDataCustomer() {
		$path = $this->getComponentDir('com_configbox').DS.'data'.DS.'customer';
		return $path;
	}

	public function getUrlDataCustomer() {
		return plugins_url('configbox/app/data/customer');
	}

	public function getDirDataStore() {
		$path = $this->getComponentDir('com_configbox').DS.'data'.DS.'store';
		return $path;
	}

	public function getUrlDataStore() {
		return plugins_url('configbox/app/data/store');
	}

	public function getTemplateOverridePath($component, $viewName, $templateName) {
		$path = '';
		return $path;
	}

	/**
	 * Should set the given error handler callable unless the app should not deal with custom error handling on this platform
	 * @param callable $errorHandler
	 * @see set_error_handler()
	 */
	public function setErrorHandler($errorHandler) {
		set_error_handler($errorHandler);
	}

	/**
	 * Should call restore_error_handler unless the app should not deal with custom error handling on this platform.
	 * @see restore_error_handler()
	 */
	public function restoreErrorHandler() {
		restore_error_handler();
	}

	/**
	 * Should set the given shutdown function callable unless the app should not deal with custom error handling on
	 * this platform
	 * @param callable $callback
	 * @see register_shutdown_function()
	 */
	public function registerShutdownFunction($callback) {
		register_shutdown_function($callback);
	}

}