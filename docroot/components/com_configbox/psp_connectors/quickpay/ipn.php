<?php
defined('CB_VALID_ENTRY') or die();

class IpnQuickpay {

	/**
	 * @var KStorage $settings
	 */
	protected $settings;
	
	function __construct() {
		KLog::log(__CLASS__ .' gateway selected.','payment');
		KLog::log('Received data: '. var_export($_REQUEST,true),'payment');
	}
	
	function setPaymentOptionSettings($settings) {
		$this->settings = $settings;
	}
	
	function &getPaymentOptionSettings() {
		return $this->settings;
	}
	
	function isValidRequest() {
		
		if (!$this->isAuthenticRequest()) {
			return 'not an authentic request from quickpay';
		}
		
		if (!$this->getOrderId()) {
			return 'no order id';
		}
		
		if (!$this->getCurrencyId()) {
			return 'no currency id';
		}
		
		if (!$this->getPaidAmount()) {
			return 'no amount';
		}
		
		return true;
	}
	
	function isAuthenticRequest() {
		
		return true;
		/*
		$fields = array('msgtype','ordernumber','amount','currency','time','state','qpstat','qpstatmsg','chstat','chstatmsg','merchant','merchantemail','transaction','cardtype','cardnumber','cardexpire','splitpayment','fraudprobability','fraudremarks','fraudreport','fee','md5check');
		
		//variable to collect values for the md5 check
		$cstr = '';
		
		foreach ( $fields as $key ){
			if (isset($_POST[$key])) {
				//$message .= "$key: " .$_POST[$key] . "\r\n";
				if( 'md5check' != $key ){
					$cstr .= $_POST[$key];
				}
			}
		}
		
		$md5secret = $this->settings->get('md5secret');
		KLog::log('MD5 secret is "'.$md5secret.'"','payment');
		KLog::log('cstr is "'.$cstr.'"','payment');
		KLog::log('MD5 testv is "'.md5($cstr.$md5secret).'"','payment');
		KLog::log('MD5 check is "'.$_POST['md5check'].'"','payment');
		if( KRequest::getString('md5check') == md5($cstr.$md5secret) ){
			return true;
		}
		else {
			return false;
		}*/
		
	}
	
	function getTransactionId() {
		
		$tix = KRequest::getString('transaction','','POST');
		
		return $tix;
	}
	
	function getOrderId() {
		
		$orderId = KRequest::getInt('ordernumber',NULL,'POST');
		
		return $orderId;
	}
	
	function getCurrencyId() {
		
		$isoCode = KRequest::getString('currency','','POST');
		
		$currency = ConfigboxCurrencyHelper::getCurrencyByIsoCode($isoCode);
		
		if (!$currency) {
			KLog::log('Currency "'.$isoCode.'" not found or unpublished.','error');
			return false;
		}
		else {
			return $currency->id;
		}
		
	}
	
	function getStatId() {
		return NULL;
	}
	
	function getPaidAmount() {
		
		// QuickPay sends the amount value in smallest unit (1 EUR is 100)
		$paidAmount = KRequest::getFloat('amount',0,'POST') / 100;
		$fee = KRequest::getFloat('fee',0,'POST') / 100;
		
		return $paidAmount - $fee;
	}
	
	function getPaymentSuccess() {

		$paymentStatus = KRequest::getInt('qpstat',NULL,'POST');
		
		if ($paymentStatus == '000') {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	function getErrorMessage() {
		
		$message = KRequest::getString('qpstatmsg',NULL,'POST');
		
		return $message;
		
	}
	
}