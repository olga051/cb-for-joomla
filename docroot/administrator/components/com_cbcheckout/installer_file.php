<?php
defined('_JEXEC') or die();

class com_cbcheckoutInstallerScript {
	
	public function __constructor($adapter = NULL) {
		ini_set('memory_limit',-1);
		set_time_limit(0);
	}
	
	public function preflight($route, $adapter = NULL) {
		return true;
	}
	
	public function postflight($route, $adapter = NULL) {
		return true;
	}
	
	public function install($adapter = NULL) {
		return true;
	}
	
	public function update($adapter = NULL) {
		return true;
	}
	
	public function uninstall($adapter = NULL) {
		
		if (!defined('DS')) {
			define('DS', DIRECTORY_SEPARATOR);
		}
		
		// It happens that uninstallation fails if the folders are already gone - so lets add them if missing
		$folder = JPATH_SITE.DS.'components'.DS.'com_cbcheckout';
		if (!is_dir($folder)) {
			mkdir($folder,0777,true);
		}
			
		$folder = JPATH_SITE.DS.'administrator'.DS.'components'.DS.'com_cbcheckout';
		if (!is_dir($folder)) {
			mkdir($folder,0777,true);
		}
		
		return true;
	}
}