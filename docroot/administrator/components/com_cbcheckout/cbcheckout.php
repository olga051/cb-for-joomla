<?php
if (!defined('CB_VALID_ENTRY')) {
	define('CB_VALID_ENTRY',true);
}

if (empty($_REQUEST['option'])) {
	$_POST['option'] = $_GET['option'] = $_REQUEST['option'] = 'com_cbcheckout';
}

if (empty($_REQUEST['controller']) && empty($_REQUEST['view'])) {
	$_POST['controller'] = $_GET['controller'] = $_REQUEST['controller'] = 'adminorders';
}

require(dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR .'components'.DIRECTORY_SEPARATOR.'com_configbox'.DIRECTORY_SEPARATOR.'configbox.php');
