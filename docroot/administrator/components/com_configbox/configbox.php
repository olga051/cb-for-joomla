<?php
if (!defined('CB_VALID_ENTRY')) {
	define('CB_VALID_ENTRY',true);
}

if (empty($_REQUEST['controller']) && empty($_REQUEST['view'])) {
	$_POST['controller'] = $_GET['controller'] = $_REQUEST['controller'] = 'admin';
}

require(dirname(__FILE__).'/../../../components/com_configbox/configbox.php');
