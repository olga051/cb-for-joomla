<?php
defined('_JEXEC') or die();

class com_configboxInstallerScript {
	
	public function __constructor($adapter = NULL) {
		
		if (!defined('DS')) {
			define('DS', DIRECTORY_SEPARATOR);
		}
		
		ini_set('memory_limit',-1);
		set_time_limit(0);
		
	}

	public function preflight($route, $adapter = NULL) {

		if (version_compare(phpversion(), '5.6') == -1) {
			$msg = 'Your webserver needs PHP 5.6 or higher (incl. PHP 7) installed. There are separate installer packages for PHP versions 5.4 and 5.5. You find these in your ConfigBox download zip file. The installation/Update was aborted, no need for cleaning up.';
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return false;
		}

		if (extension_loaded('ionCube Loader') == false) {
			$msg = 'Your webserver needs ionCube loader version 5 or later installed. Please refer to the ConfigBox manual in the section "Installation". The installation/update was aborted, no need for cleaning up.';
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return false;
		}

		if (function_exists('ioncube_loader_version')) {

			$loaderVersion = ioncube_loader_version();
			$phpVersion = phpversion();

			// PHP 7 needs ionCube loader 6, lower PHP versions need loader 5
			if (version_compare($phpVersion, '7.1') != -1) {
				if (version_compare($loaderVersion, '10') == -1) {
					$msg = 'For PHP 7.1 support you need ionCube loader version 10 or later installed. Please refer to the ConfigBox manual in the section "Installation". The installation/update was aborted, no need for cleaning up.';
					JFactory::getApplication()->enqueueMessage($msg, 'error');
					return false;
				}
			}
			elseif (version_compare($phpVersion, '7') != -1) {
				if (version_compare($loaderVersion, '6') == -1) {
					$msg = 'For PHP 7.0 support you need ionCube loader version 6 or later installed. Please refer to the ConfigBox manual in the section "Installation". The installation/update was aborted, no need for cleaning up.';
					JFactory::getApplication()->enqueueMessage($msg, 'error');
					return false;
				}
			}
			else {
				if (version_compare($loaderVersion, '5') == -1) {
					$msg = 'You need ionCube loader version 5 or later installed. Please refer to the ConfigBox manual in the section "Installation". The installation/update was aborted, no need for cleaning up.';
					JFactory::getApplication()->enqueueMessage($msg, 'error');
					return false;
				}
			}

		}

		return true;
	}

	public function postflight($route, $adapter = NULL) {
		
		if (!defined('DS')) {
			define('DS', DIRECTORY_SEPARATOR);
		}
		
		if( $route == 'update') {
			
			// Init Kenedo framework
			require_once(JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'external'.DS.'kenedo'.DS.'init.php');
			ConfigboxCacheHelper::purgeCache();
			
		}
		
		return true;
	}

	public function install($adapter = NULL) {
		return true;
	}

	public function update($adapter = NULL) {
		return true;
	}

	public function uninstall($adapter = NULL) {

		// Init Kenedo framework
		require_once(JPATH_SITE.'/components/com_configbox/external/kenedo/init.php');

		$db = KenedoPlatform::getDb();

		$query = "SHOW TABLES LIKE '%configbox_%'";
		$db->setQuery($query);
		$tables = $db->loadResultList();

		$query = "SHOW TABLES LIKE '%cbcheckout_%'";
		$db->setQuery($query);
		$tables1 = $db->loadResultList();

		$tables = array_merge($tables, $tables1);
		foreach ($tables as &$table) {
			$table = "'".$table."'";
		}

		$query = "
		SELECT TABLE_NAME,COLUMN_NAME,CONSTRAINT_NAME, REFERENCED_TABLE_NAME,REFERENCED_COLUMN_NAME
		FROM INFORMATION_SCHEMA.KEY_COLUMN_USAGE
		WHERE REFERENCED_TABLE_SCHEMA = DATABASE() AND REFERENCED_TABLE_NAME IN (".implode(',', $tables).")";
		$db->setQuery($query);
		$infos = $db->loadAssocList();
		foreach ($infos as $info) {
			$query = "ALTER TABLE `".$info['TABLE_NAME']."` DROP FOREIGN KEY `".$info['CONSTRAINT_NAME']."`";
			$db->setQuery($query);
			$db->query();
		}

		foreach ($tables as $table) {
			$query = "DROP TABLE IF EXISTS `".str_replace("'", '', $table)."`";
			$db->setQuery($query);
			$db->query();
		}

		KenedoFileHelper::deleteFolder(KenedoPlatform::p()->getDirCache().'/configbox');
		KenedoFileHelper::deleteFolder(KenedoPlatform::p()->getLogPath().'/configbox');

		define('CONFIGBOX_GOT_UNINSTALLED', 1);

		return true;
	}
	
}