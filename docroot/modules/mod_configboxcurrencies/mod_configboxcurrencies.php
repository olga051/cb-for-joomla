<?php
defined('_JEXEC') or die('Restricted access');

if (!empty($_REQUEST['option']) && $_REQUEST['option'] == 'com_configbox') {

	// Init Kenedo framework
	require_once(JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'external'.DS.'kenedo'.DS.'init.php');
	
	// Add $params variable if module is inserted
	if (!isset($params)) {
		$params = new KStorage();
	}
	
	// Render view
	$view = KenedoView::getView('ConfigboxViewBlockCurrencies',KPATH_ROOT.DS.'components'.DS.'com_configbox'.DS.'views'.DS.'block_currencies'.DS.'view.html.php');
	$view->assignRef('params',$params);
	$view->display();

}