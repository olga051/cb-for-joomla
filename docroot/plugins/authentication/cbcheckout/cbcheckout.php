<?php
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgAuthenticationCbcheckout extends JPlugin {
	
	function __construct(&$subject, $config = array()) {
		
		parent::__construct($subject, $config);
		
		if (!defined('DS')) {
			define('DS', DIRECTORY_SEPARATOR);
		}
		
		if (!defined('DS')) {
			define('CB_VALID_ENTRY',true);
		}
		
		$files = array(
				JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'external'.DS.'kenedo'.DS.'init.php',
				JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'helpers'.DS.'autoloads.php',
				JPATH_SITE.DS.'components'.DS.'com_cbcheckout'.DS.'helpers'.DS.'user.php',
				);

		foreach ($files as $file) {
			if (!is_file($file)) {
				if (method_exists('KLog','log')) {
					KLog::log('ConfigBox user plugin failed. Cannot read required files. Check if com_configbox and com_cbcheckout is installed and all versions match. Missing file is ("'.$file.'")', 'error');
				}
				return;
			}
			else {
				require_once($file);
			}
		}
		
	}
	
	function onUserAuthenticate( $credentials, $options, &$response ) {
		
		if (method_exists('CbcheckoutUserHelper','onAuthenticate') == false) {
			return null;
		}
		$response->type = 'Cbcheckout';
		$return = CbcheckoutUserHelper::onAuthenticate( $credentials, $options, $response );
		
		if ($return == false && empty($response->error_message)) {
			$response->error_message = JText::_('JGLOBAL_AUTH_INVALID_PASS');
		}
		
		return $return;
		
	}
	
	// Old joomla version pipe
	function onAuthenticate( $credentials, $options, &$response ) {
		return self::onUserAuthenticate( $credentials, $options, $response );
	}
	
	
}
