<?php
defined('_JEXEC') or die;

class plgSystemDemoactivity extends JPlugin {
	
	function onAfterDispatch() {
		
		$demoNumber = $this->getDemoNumber();
		
		$incrementInterval = 60;
		$updateInterval = 120;
		
		if (!$demoNumber) return;
		
		$activity = $this->readActivityLog();
				
		$time = time();
		$app = JFactory::getApplication();
		
		// Update backend visit in case
		if ($time - $activity['last_backend'] > $incrementInterval && $app->isAdmin()) {

			$activity['duration_backend']++;
			$activity['last_backend'] = $time;
			$day = date('Y-m-d',$time);

			if (!isset($activity['daily_visit_duration'][$day])) {
				$activity['daily_visit_duration'][$day] = 0;
			}

			$activity['daily_visit_duration'][$day]++;

		}

		// Update frontend visit in case
		if ($time - $activity['last_frontend'] > $incrementInterval && $app->isSite()) {

			$activity['duration_frontend']++;
			$activity['last_frontend'] = $time;
			$day = date('Y-m-d',$time);

			if (!isset($activity['daily_visit_duration'][$day])) {
				$activity['daily_visit_duration'][$day] = 0;
			}

			$activity['daily_visit_duration'][$day]++;

		}
		
		// Send activity to master server in intervals
		if ($time - $activity['last_update'] > $updateInterval) {
			$activity['demo_number'] = $demoNumber;
			$this->updateMaster($activity);
			$activity['last_update'] = $time;
		}
		
		// Save data in activity log
		$this->writeActivityLog($activity);
		
	}
	
	protected function updateMaster($activity) {

		$url = $this->params->get('update_url','https://www.configbox.at');
		$url = rtrim($url,'/');
		$url = $url.'/index.php';
		
		$post['option'] = 'com_cbcrm';
		$post['task'] = 'demo.updateActivity';
		$post['lang'] = 'en';
		$post['activity_json'] = json_encode($activity);
		
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_exec($ch);
		
	}
	
	protected function readActivityLog() {
		
		$file = JPATH_SITE.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'demoactivity.ini';
		
		jimport('joomla.filesystem.file');
		
		if (JFile::exists($file)) {
			$content = file_get_contents($file);
			$activity = json_decode($content,true);
			return $activity;
		}
		else {
			return array(
				'last_update'=>0,
				'last_backend'=>0,
				'last_frontend'=>0,
				'duration_frontend'=>0,
				'duration_backend'=>0,
			);
		}
	}
	
	protected function writeActivityLog($activity) {
		$file = JPATH_SITE.DIRECTORY_SEPARATOR.'logs'.DIRECTORY_SEPARATOR.'demoactivity.ini';
		jimport('joomla.filesystem.file');
		$content = json_encode($activity);
		JFile::write($file, $content);
	}
	
	protected function getDemoNumber() {
		
		$uri = $_SERVER['REQUEST_URI'];
		$exp = '/\/demo[0-9]+\//';
		
		preg_match_all($exp, $uri, $matches);
		
		if (isset($matches[0][0])) {
			$demoNumber = str_replace('/demo', '', $matches[0][0]);
			$demoNumber = str_replace('/','',$demoNumber);
			return $demoNumber;
		}
		else {
			return false;
		}

	}
}
