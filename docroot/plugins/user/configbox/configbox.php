<?php
defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgUserConfigbox extends JPlugin {

	function __construct(&$subject, $config = array()) {
		
		parent::__construct($subject, $config);
		
		if (!defined('DS')) {
			define('DS', DIRECTORY_SEPARATOR);
		}
		
		if (!defined('DS')) {
			define('CB_VALID_ENTRY',true);
		}
		
		$files = array(
				JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'external'.DS.'kenedo'.DS.'init.php',
				JPATH_SITE.DS.'components'.DS.'com_configbox'.DS.'helpers'.DS.'autoloads.php',
				JPATH_SITE.DS.'components'.DS.'com_cbcheckout'.DS.'helpers'.DS.'user.php',
				);

		foreach ($files as $file) {
			if (!is_file($file)) {
				if (method_exists('KLog','log')) {
					KLog::log('ConfigBox user plugin failed. Cannot read required files. Check if com_configbox and com_cbcheckout is installed and all versions match. Missing file is ("'.$file.'")', 'error');
				}
				return;
			}
			else {
				require_once($file);
			}
		}
		
	}
	
	function onUserAfterSave($user, $isnew, $success, $msg) {
		if (method_exists('CbcheckoutUserHelper','onAfterStoreUser')) {
			return CbcheckoutUserHelper::onAfterStoreUser($user, $isNew, $success, $msg);
		}
	}
	
	function onUserAfterDelete($user, $success, $msg) {
		if (method_exists('CbcheckoutUserHelper','deleteUser')) {
			return CbcheckoutUserHelper::deleteUser($user);
		}
	}
	
	function onUserLogin($user, $options = array()) {
		if (method_exists('CbcheckoutUserHelper','onLoginUser')) {
			return CbcheckoutUserHelper::onLoginUser($user);
		}
	}
	
	function onUserLogout($user, $options = array()) {
		if (method_exists('CbcheckoutUserHelper','logoutUser')) {
			CbcheckoutUserHelper::logoutUser($user);
		}
	}
	
	// Old joomla version pipe
	function onAfterStoreUser($user, $isNew, $success, $msg) {
		return $this->onUserAfterSave($user, $isNew, $success, $msg);
	}	
	
	// Old joomla version pipe
	function onAfterDeleteUser($user, $success, $msg) {
		return $this->onUserAfterDelete($user, $success, $msg);
	}
	
	// Old joomla version pipe
	function onLoginUser($user, $options = array()) {
		return self::onUserLogin($user, $options);
	}
	
	// Old joomla version pipe
	function onLogoutUser($user, $options = array()) {
		return self::onUserLogout($user, $options);
	}
	
}
